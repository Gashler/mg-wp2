<!doctype html>
<html>
<head>
	<meta name="csrf-token" content="<?php echo $_POST['csrf_token'] ?>">
</head>
<body>
	<img src="<?php echo $_POST['app_url'] ?>/img/loading.gif">
	<form name="loginform" id="loginform1" action="<?php echo $_POST['host_url'] ?>/login-redirect" method="post">
		<input type="hidden" name="_token" value="<?php echo $_POST['csrf_token'] ?>">
		<input type="hidden" name="log" id="user_login1" class="input" value="<?php echo $_POST['email'] ?>">
		<input type="hidden" name="pwd" id="user_pass1" class="input" value="<?php echo $_POST['password'] ?>">
		<input name="rememberme" type="hidden" id="rememberme1" value="forever">
		<input type="hidden" name="wp-submit" id="wp-submit1" value="Log In">
		<input type="hidden" name="redirect_to" value="<?php echo $_POST['host_url'] ?>/wp-admin/">
		<input type="hidden" name="app_url" value="<?php echo $_POST['app_url'] ?>">
		<input type="hidden" name="instance" value="1">
		<input type="hidden" name="action" value="login">
	</form>
	<script type="text/javascript">
		document.getElementById('loginform1').submit();
	</script>
</body>
</html>
