/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// remove popup

function removePopup() {
	jQuery(".popup").fadeOut(500, function() {
	    jQuery(this).remove();
	});
	jQuery("div#gray").fadeOut(500, function() {
	    jQuery(this).remove();
	});
        popupOpen = false;
}

// open popup

var popupOpen = false;

function popupAlert(content) {
	jQuery("body").prepend("<div id='gray'><div class='popup'>" + content + "</div></div>");
	jQuery("div#gray, .popup").fadeIn(500,function(){
        jQuery(this).css({ // for IE8 and older
            "filter" : "alpha(opacity=50)"
        });
		// close popup by clicking outside of it
		
		jQuery("#gray").click(function() {
		    if (popupOpen == true) {
		        removePopup();
		    }
		});
	});
}	

function popup(href, id) {
	jQuery("body").prepend("<div id='gray'></div>");
	var x;
	if ( id !== undefined ) {
		x = id;
	}
	else ( x = "popup" );
	jQuery(".popup").attr("id", x).load(href, function(response) {
		jQuery("div#gray").fadeIn(500,function(){
            jQuery(this).css({ // for IE8 and older
                "filter" : "alpha(opacity=50)"
            });
            
			// close popup by clicking outside of it
			
			jQuery("#gray").click(function() {
			    if (popupOpen == true) {
			        removePopup();
			    }
			});
        });
	});
    jQuery(document).ajaxStop(function() {
		//var marginLeft = jQuery(".popup").width() / 2 + 40;
		//var marginTop = jQuery(".popup").height() / 2 + 40;
		jQuery(".popup")/*.css({"margin-left" : "-" + marginLeft + "px"})*/.prepend("<div id='x' onclick='removePopup()'>&times;</div>");
		jQuery(".popup").fadeIn(500,function() {
		    popupOpen = true;
		});
	});
}

// end popup

// pop
function pop(url, data) {
	jQuery("body").prepend("<div class='pop'></div><div class='gray'></div>");
	jQuery(".gray, .pop").fadeIn();
	giver = data['giver'];
	receiver = data['receiver'];
	giverName = data['giverName'];
	receiverName = data['receiverName'];
	service = data['service'];
	jQuery(".pop").load(url, {
		giver: giver,
		receiver: receiver,
		giverName: giverName,
		receiverName: receiverName,
		service: service
	});
	jQuery(".gray").click(function() {
		jQuery(".gray, .pop").fadeOut(function() {
			jQuery(".gray, .pop").remove();
		});
	});
}
// end pop