<div class="entry-summary">
	<a href="<?php the_permalink(); ?>" style="text-decoration:none;"><h3 style="margin:0 0 .5em 0;"><?php the_title() ?></h3></a>
	<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
		<?php the_post_thumbnail('thumbnail', array('class' => 'alignleft')); ?>
	</a>
	<?php the_excerpt(); ?>
</div><!-- entry-summary -->