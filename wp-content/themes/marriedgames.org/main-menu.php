<?php if (!(is_front_page()) && !(is_page(1513))) { ?>
	<ul>
		<li><a id="menu-button"><span></span></a></li>
		<li><a id="search-button"><span></span></a></li>
		<div id="menuSearch" style="display:none;">
			<?php if (!(is_front_page()) && !(is_page(1513))) { ?>
				<?php get_search_form(); ?>
			<?php } else { ?>
				<?php if (current_user_cannot("access_s2member_level1")) { ?>
					<a href="/account" class="button" style="float:right; position:relative;">Member Login</a>
				<?php } else { ?>
					<a href="/sexy-games-and-tools" class="button" style="float:right; position:relative;">Dashboard</a>
				<?php } ?>
			<?php } ?>
		</div><!-- search -->
		<div class="clear"></div>
		<div class="container">
			<nav id="mainNav">
				<?php if (current_user_cannot("access_s2member_level0")) { ?>
					<li><a href="/choose-plan"><span></span>Register to Play</a></li>
				<?php } ?>
				<?php if (current_user_is("s2member_level0")) { ?>
					<li><a style="cursor:pointer;" onclick="window.top.location='/choose-plan'"><span></span>Unlock Full Suite</a></li>
				<?php } ?>
				<?php if (current_user_can("access_s2member_level0")) { ?>
					<?php if (current_user_can("access_s2member_level1")) { ?>
						<li><a href="/sexy-games-and-tools"><span style="background-position:0 -72px;"></span></a></li>
					<?php } else { ?>
						<li><a href="/dashboard-free" ?><span style="background-position:0 -72px;"></span></a></li>
					<?php } ?>
				<?php } ?>
				<li>
					<a style="cursor:default;"><span style="background-position:0 -48px;"></span>Games</a>
					<ul>
						<li><a href="/lovers-lane"><span style="background-position:0 -48px;"></span>Lovers Lane</a></li>
						<li><a href="/sex-dice"><span style="background-position:0 -48px;"></span>Sex Dice</a></li>
						<li><a href="/strip-poker"><span style="background-position:-216px -72px;"></span>Strip Poker</a></li>
					</ul>
				</li>
				<li>
					<a style="cursor:default;"><span style="background-position:-168px -48px;"></span>Ideas</a>
					<ul>
						<li><a href="/sexy-questions"><span style="background-position:-24px -48px;"></span>Sexy Questions</a></li>
						<li><a href="/foreplay-generator"><span style="background-position:-24px -48px;"></span>Foreplay Generator</a></li>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/category/sex-position-generator"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-48px -48px;"></span>Sex Position Generator</a></li>
						<?php wp_list_categories('exclude=31,51&title_li='); ?>
					</ul>
				</li>
				<li>
					<a style="cursor:default;"><span style="background-position:-72px -72px;"></span>Tools</a>
					<ul>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/tools/romantic-diary"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-144px -48px;"></span>Romantic Diary</a></li>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/tools/sexual-favor-clock"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-120px -48px;"></span>Sexual Favor Clock</a></li>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/tools/sexy-services"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-96px -48px;"></span>Sexy Services</a></li>
					</ul>
				</li>
				<li><a style="cursor:default;"><span style="background-position:-72px -48px;"></span>Role-playing</a>
					<ul>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/sexy-role-playing/generator/"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-72px -48px;"></span>Role-playing Generator</a></li>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/sexy-role-playing-outlines/"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-72px -48px;"></span>Role-playing Outlines</a></li>
						<li><a <?php if (!(current_user_is("s2member_level1"))) { ?>href="/sexy-role-playing-scripts"<?php } else { ?>style="color:rgba(255,255,255,.5);"<?php } ?>><span style="background-position:-72px -48px;"></span>Role-playing Scripts</a></li>
					</ul>
				</li>
				<li><a href="http://astore.amazon.com/frelovgam-20?_encoding=UTF8&node=3"><span style="background-position:-192px -48px;"></span>Store</a></li>
				<li><a style="cursor:default;"><span style="background-position:-48px -72px;"></span>Support</a>
					<ul>
						<li><a href="/category/romantic-ideas"><span style="background-position:-168px -48px;"></span>Articles</a></li>
						<li><a href="/marriage-support"><span style="background-position:-48px -72px;"></span>Relationship Support</a></li>
						<li><a href="/contact-us"><span style="background-position:-48px -72px;"></span>Contact Us</a></li>
					</ul>
				</li>
			</nav>
		</div>
	</ul>
<?php } else { ?>
	<div class="container" style="padding:10px;"></div>
<?php } ?>
