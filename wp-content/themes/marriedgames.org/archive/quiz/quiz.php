

It’s important to know where you both are individually to know how you are doing as a couple, so we’ve created separate quizzes for you both to take.

Your honesty is the crucial first step in improving your love relationship. We recommend one spouse takes their quiz while the other leaves the room to ensure the most honest responses.

After you both have completed the quiz, you will receive the results in your e-mail tomorrow.

Don’t be embarrassed by the results! Being willing to evaluate where you are now shows that you really love each other and want the best for your relationship.

Quiz for Her link            Quiz for Him link

(once a quiz is completed, the link is transparent)

1) About how often do you have sex?

    a) I’m starting to forget what sex is...

    b) Once to 3 times a month

    c) Once a week

d) 2+ times a week

2) How often do you want to have sex?

    a) I just want to start having sex again.

    b) A few times a month

    c) At least once a week

    d) A couple times or more a week

3) Do you schedule sex?

    a) Umm... we don’t talk about sex, let alone schedule it

    b) I would never schedule it. That takes out all the fun

    c) We schedule it, but it usually doesn’t work out

    d) We schedule it and most of the time it works out well

4) Is most of your sex scheduled or spontaneous?

    a) Uhhh......

    b) All spontaneous

    c) All scheduled

    d) A good mix of both

5) How often do you orgasm every time you engage in sex?

    a) Rarely

    b) About half the time

    c) Most of the time

    d) Almost always or always

6) How much do you enjoy sex?

    a) 25% of the time or less

    b) 50% of the time

    c) 75% of the time

    d) 95-100% of the time

7) Do you ever fake an orgasm?

    a) Yes

    b) Sometimes

    c) Once in awhile

    d) Never

8) Do you use toys or games?

a) No. The thought never occurred to me

b) No. I’m afraid of yucky stuff like that.

c) Yes.

9) How often do you wear lingerie?

    a)

10) How much do you focus on pleasing the other person?

11) How much do you focus on finding pleasure for yourself?

12) How often do you express what you want?

13) How much fun is sex for you?

14) How much fun do you think sex is for your spouse?

15) How often is sex uncomfortable or painful?

16) How often do you think sex is uncomfortable or painful for your spouse?