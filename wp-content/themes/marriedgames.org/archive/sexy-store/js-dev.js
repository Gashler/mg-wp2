/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// add row
var id;
var price;
jQuery(".addService").click(function() {
	gender = jQuery(this).closest(".catalogue").attr("data-gender");
	var str = jQuery(this).siblings(".selectService").val();
	var strs = str.split("~")
	service = strs[0];
	price = strs[1];
	//if (service !== "" && price !== "") {
		addRow(gender,service,price);
	//}
	
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/getLastRow.php", {
		user: S2MEMBER_CURRENT_USER_ID
	}, function(result) {
		id = result;
		
		// hack to fix id mysteriously sometimes being the same as the last row
		if (jQuery("#" + id).length > 0) id ++;
		
		jQuery(".catalogue." + gender + " tbody").append("<tr id='" + id + "'><td><input class='service' type='text' value='" + service + "' /></td><td><div style='display:table-cell;'>$&nbsp;</div><div style='display:table-cell;'><input class='price' type='text' value='" + price + "' /></div></td><td><div class='container'><div class='x'>&times;</div><button type='button' class='small purchase'>Purchase</button></div></tr>");

		// purchase service
	
		jQuery("#" + id).on("click", ".purchase", function() {
			id = jQuery(this).closest("tr").attr("id");
			purchaseService(id);
		});

		// delete row
		
		jQuery("#" + id + " .x").click(function() {
			rowId = jQuery(this).closest("tr").attr("id");
			deleteRow(rowId);
		});
		
		// update row in DB on keyup
		
		jQuery(".catalogue input").keyup(function() {
			id = jQuery(this).closest("tr").attr("id");
			service = jQuery("#" + id + " .service").val();
			price = jQuery("#" + id + " .price").val();
			updateRow(id,service,price);
		});
	});
});

// purchase service

function purchaseService(id) {
	if (jQuery("#" + id).parents("table").hasClass("him")) {
		buyer = "her";
		buyerName = wife;
		seller = "him";
	}
	else {
		buyer = "him";
		buyerName = husband;
		seller = "her";
	}
	service = jQuery("#" + id + " .service").val();
	price = parseInt(jQuery("#" + id + " .price").val());
	buyerMoney = parseInt(jQuery("#moneyTables ." + buyer + " .money").html());
	sellerMoney = parseInt(jQuery("#moneyTables ." + seller + " .money").html());
	if (buyerMoney < price) alert(buyerName + " can't afford this service");
	else {
		alert(buyerName + " has purchased this service.");
		buyerMoney -= price;
		sellerMoney += price;
		jQuery("#moneyTables ." + buyer + " .money").html(buyerMoney);
		jQuery("#moneyTables ." + seller + " .money").html(sellerMoney);
		updateMoney();
		
		// add owed service to lists
		
		if (jQuery("#moneyTables ." + buyer + " .owed li").hasClass(id) == true) {
			quantity = jQuery(".owed li." + id + " .quantity").html();
			quantity ++;
			jQuery(".owed li." + id + " .quantity").html(quantity);
			

			
		}
		else {
			jQuery("#moneyTables ." + buyer + " .owed ul").append("<li class='" + id + "'><span class='service'>" + service + "</span> (&times;<span class='quantity'>1</span>) <div class='actions'><span class='email'>Email</span> | <span class='completed'>Completed</span></div></li>");

			// complete service

			jQuery("li." + id + " .email").click(function() {
				emailService(id);
			});			
			jQuery("li." + id + " .completed").click(function() {
				completeService(id);
			});
		
		}
		jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateOwed.php", {
			id: id,
			operation: "add"
		});
	}
}
jQuery(".purchase").click(function() {
	id = jQuery(this).closest("tr").attr("id");
	purchaseService(id);
});

// add new row in DB

function addRow(gender,name,price) {
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/addRow.php", {
		user: S2MEMBER_CURRENT_USER_ID,
		gender: gender,
		name: name,
		price: price
	});
}

// update row in DB on keyup

jQuery(".catalogue input").keyup(function() {
	id = jQuery(this).closest("tr").attr("id");
	service = jQuery("#" + id + " .service").val();
	price = jQuery("#" + id + " .price").val();
	updateRow(id,service,price);
});

// update row in DB

function updateRow(id,name,price) {
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateRow.php", {
		id: id,
		name: name,
		price: price
	});
}

// delete row

function deleteRow(id, gender) {
	if (gender == "him") {
		var name = husband;
		var spouse = wife;
	}
	else {
		var name = wife;
		var spouse = husband;
	}
	if (jQuery(".owed li." + id).length > 0) {
		alert("This service cannot yet be removed, because it has already been purchased by " + spouse + " and must be completed by " + name + ".");
	}
	else {
		service = jQuery("#" + id + " .service").val();
		jQuery("#" + rowId).remove();
		jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/deleteRow.php", {
			user: S2MEMBER_CURRENT_USER_ID,
			gender: gender,
			name: service,
		});
	}
}
jQuery("td").on("click", ".x", function() {
	rowId = jQuery(this).closest("tr").attr("id");
	var gender = jQuery(this).closest(".catalogue").attr("data-gender");
	deleteRow(rowId, gender);
});

function getReceiver(giver) {
	if (giver == "him") {
		return receiver = "her";
	}
	if (giver == "her") {
		return receiver = "him";
	}
}

jQuery(".giveMoney").click(function() {
	var giver = jQuery(this).closest("table").attr("class");
	var receiver = getReceiver(giver);
	var amount = jQuery("table." + giver + " #amount").val();
	if (amount !== "") {
		var type = jQuery("." + giver + " input:radio:checked").val();
		amount = parseInt(amount);
		if (type == "payment") {
			originalAmount = jQuery("table." + giver + " .money").html();
			originalAmount = parseInt(originalAmount);
			newAmount = originalAmount - amount;
			jQuery("table." + giver + " .money").html(newAmount);
		}
		originalAmount = jQuery("table." + receiver + " .money").html();
		originalAmount = parseInt(originalAmount);
		newAmount = originalAmount + amount;
		jQuery("table." + receiver + " .money").html(newAmount);
		updateMoney();
	}
});

// update sex cash in DB

function updateMoney() {
	him = jQuery("#moneyTables table.him .money").html();
	her = jQuery("#moneyTables table.her .money").html();
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateSexCash.php", {
		user: S2MEMBER_CURRENT_USER_ID,
		him: him,
		her: her
	});
}

// email service

function emailService(id) {
	var data = new Array();
	data['giver'] = jQuery("#" + id).closest("table").attr("data-gender");
	data['receiver'] = getReceiver(data['giver']);
	data['service'] = jQuery("li." + id + " .service").html();
	if (data['giver'] == "him") {
		data['giverName'] = husband;
		data['receiverName'] = wife;
	}
	else {
		data['giverName'] = wife;
		data['receiverName'] = husband;
	}
	pop("//wp-content/themes/marriedgames.org/sexy-store/email.php", data);
}
jQuery(".email").click(function() {
	var id = jQuery(this).parents("li").attr("class");
	emailService(id);
});

// complete service

function decreaseService(id) {
	quantity --;
	jQuery("li." + id + " .quantity").html(quantity);
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateOwed.php", {
		id: id,
		operation: "subtract"
	});	
}

function completeService(id) {
	var quantity = parseInt(jQuery("li." + id + " .quantity").html());
	if (quantity == 1) {
		jQuery("li." + id).remove();
	}
	else {
		quantity --;
		jQuery("li." + id + " .quantity").html(quantity);
	}
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateOwed.php", {
		id: id,
		operation: "subtract"
	});	
}
jQuery(".completed").click(function() {
	var id = jQuery(this).parents("li").attr("class");
	completeService(id);
});

// prevent form from submitting by pressing enter

jQuery(document).ready(function() {
  jQuery(window).keydown(function(event){
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});
