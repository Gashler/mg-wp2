<?php
$title = "Sexy Store Game";
include ("../header/header.php");
?>
<h1><?php echo $title ?></h1>
<p>
	Use these widgets as a way of awarding and treating each other with imaginary money. You could award money for a hard day's work or a delicious dinner. You can purchase services from each other such as a massage or a striptease. The one giving the service sets the price. You could also use the widgets as a way of keeping track of money for a game of strip poker.
</p>
<table class="male" style="float: left;">
	<tr>
		<th> His Money </th>
	</tr>
	<tr>
		<td class="money"></td>
	</tr>
	<tr>
		<th> Give Her </th>
	</tr>
	<tr>
		<td>
			<p>
				<input id="award" type="radio" name="type" value="award" checked="checked" />
				<label for="award">Award</label>
			</p>
			<p>
				<input id="payment" type="radio" name="type" value="payment" />
				<label for="payment">Payment</label>
			</p><label for="amount">Amount:</label>
			<input id="amount" type="text" /><br />
			<input type="submit" class="giveMoney button-small" value="Give Money" />
		</td>
	</tr>
</table>
<table class="female" style="float: left; margin-left: 2em;">
	<tr>
		<th> Her Money </th>
	</tr>
	<tr>
		<td class="money"></td>
	</tr>
	<tr>
		<th> Give Him </th>
	</tr>
	<tr>
		<td>
			<p>
				<input id="award2" type="radio" name="type2" value="award" checked="checked" />
				<label for="award2">Award</label>
			</p>
			<p>
				<input id="payment2" type="radio" name="type2" value="payment">
				<label for="payment2">Payment</label>
			</p><label for="amount">Amount:</label>
			<input id="amount" type="text" /><br />
			<input type="submit" class="giveMoney button-small" value="Give Money" />
		</td>
	</tr>
</table>
<div style="clear: both;"></div>
<div id="hisStore">
	<!--<select class="selectService">
		<option selected="selected">Select one:</option>
		<?php
			$sql = $sql = "SELECT * FROM sexyStore ORDER BY name");
			while($row = $result->fetch_assoc()) {
				echo "<option>" . $row['name'] . "</option>";
			}
		?>
	</select>-->
	<table id="hisServices" class="catalogue male" gender="hisPrice">
		<caption>His Services</caption>
		<thead>
			<tr>
				<th>Name</th>
				<th>Price</th>
			</tr>
			<tr>
				<?php
				$sql = $sql = "SELECT * FROM sexyStore ORDER BY name");
				while ($row = $result->fetch_assoc()) {
					echo "
							<tr>
								<td>" . $row['name'] . "</td>
								<td><input class='price' type='text' id='" . $row['name'] . "' value='" . $row['hisPrice'] . "'/></td>
							</tr>
						";
				}
				?>
			</tr>
			<tr>
				<td colspan="2" style="background: none;">
					<input style="margin: 0;" type="submit" class="newRow button-small" value="New Item" />
				</td>
			</tr>
		</thead>
	</table>
	<table id="herServices" class="catalogue" gender="herPrice" style="margin-left: 30px;">
		<caption>Her Services</caption>
		<thead>
			<tr>
				<th>Name</th>
				<th>Price</th>
			</tr>
			<tr>
				<?php
				$sql = $sql = "SELECT * FROM sexyStore ORDER BY name");
				while ($row = $result->fetch_assoc()) {
					echo "
							<tr>
								<td>" . $row['name'] . "</td>
								<td><input class='price' type='text' id='" . $row['name'] . "' value='" . $row['herPrice'] . "'/></td>
							</tr>
						";
				}
				?>
			</tr>
			<tr>
				<td colspan="2" style="background: none;">
					<input style="margin: 0;" type="submit" class="newRow button-small" value="New Item" />
				</td>
			</tr>
		</thead>
	</table>
	<div style="clear: both;"></div>
</div>
<script>
	// get money

	function getMoney(gender) {
		jQuery("table." + gender + " .money").load("getMoney.php", {
			gender : gender
		});
	}

	getMoney("male");
	getMoney("female");

	// give money

	jQuery(".giveMoney").click(function() {
		var giver = jQuery(this).parent().parent().parent().parent().attr("class");
		var receiver;
		if (giver == "male") {
			receiver = "female";
		}
		if (giver == "female") {
			receiver = "male";
		}
		var type = jQuery("." + giver + " input:radio:checked").val();
		var amount = jQuery("table." + giver + " #amount").val();
		if (type == "payment") {
			jQuery("table." + giver + " .money").load("giveMoney.php", {
				giver : giver,
				amount : amount
			});
		}
		jQuery("table." + receiver + " .money").load("receiveMoney.php", {
			receiver : receiver,
			amount : amount
		});
	});
	/*
	jQuery(".selectService option").click(function() {
	jQuery("#hisServices").append("<tr><td>" + jQuery(this).html() + "</td><td><input type='text' /></td></tr>");
	});*/

	// save price

	jQuery("input").blur(function() {
		gender = jQuery(this).parents("table").attr("gender");
		if (jQuery(this).val() !== "") {
			name = jQuery(this).attr("id");
			price = jQuery(this).val();
			jQuery(this).load("update.php", {
				gender : gender,
				name : name,
				price : price
			});
		}
	});
	
	jQuery(".newRow").click(function(){
		jQuery(this).parents("table").append("<tr><td><input type='text' /></td><td><input type='text' class='price' /></tr>");
	});

</script>
<?php
include ("../footer/footer.php");
 ?>