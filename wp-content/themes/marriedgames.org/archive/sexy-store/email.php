<h2 style="margin-top:0;">Schedule this Service</h2>
<label for="day">Choose a Day:</label>
<select name="day" id="day">
	<option value="Today">Today</option>
	<option value="Monday">Monday</option>
	<option value="Tuesday">Tuesday</option>
	<option value="Wednesday">Wednesday</option>
	<option value="Thursday">Thursday</option>
	<option value="Friday">Friday</option>
	<option value="Saturday">Saturday</option>
	<option value="Sunday">Sunday</option>
</select>
<label>Choose a Time:</label>
<fieldset>
	<label for="beginHour<?php echo $rule ?>">From: </label>
	<select style="display:inline;" id="beginHour<?php echo $rule ?>" name="beginHour">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9" selected="selected">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
	</select>
	 : 
	<select style="display:inline;" id="beginMinute<?php echo $rule ?>" name="beginMinute">
		<option value="00">00</option>
		<option value="15">15</option>
		<option value="30" selected="selected">30</option>
		<option value="45">45</option>
	</select>
	<select style="display:inline;" id="beginAMPM<?php echo $rule ?>" name="beginAMPM">
		<option value="am">AM</option>
		<option value="pm" selected="selected">PM</option>
	</select>
</fieldset>
<fieldset>
	<label for="endHour<?php echo $rule ?>">To: </label>
	<select style="display:inline;" id="endHour<?php echo $rule ?>" name="endHour">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10" selected="selected">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
	</select>
	 : 
	<select style="display:inline;" id="endMinute<?php echo $rule ?>" name="endMinute">
		<option value="00">00</option>
		<option value="15">15</option>
		<option value="30" selected="selected">30</option>
		<option value="45">45</option>
	</select>
	<select style="display:inline;" id="endAMPM<?php echo $rule ?>" name="endAMPM">
		<option value="am">AM</option>
		<option value="pm" selected="selected">PM</option>
	</select>
</fieldset>
<h3>Email <?php echo $_POST['giverName'] ?> About this Service</h3>
<form action="/contact-us">
	<label for="email"><?php echo $_POST['giverName'] ?>'s Email:</label>
	<input type="text" id="email" name="email">
	<label for="subject">Subject:</label>
	<input type="text" id="subject" name="subject" value="<?php echo $_POST['receiverName'] ?> has purchased your service">
	<label for="message">Message:</label>
	<textarea id="message" name="message">
		
	</textarea>
	<input type="submit" class="button" value="submit">
</form>
<script>
	function getValues() {
		day = jQuery("#day").val();
		beginHour = jQuery("#beginHour").val();
		beginMinute = jQuery("#beginMinute").val();
		beginAMPM = jQuery("#beginAMPM").val();
		endHour = jQuery("#endHour").val();
		endMinute = jQuery("#endMinute").val();
		endAMPM = jQuery("#endAMPM").val();
		jQuery("#message").html("<?php echo $_POST['receiverName'] ?> has purchased the following service from your store: <?php echo $_POST['service']?>, and would like to schedule it for <span class="time"></span>. If you accept this request, please reply with the word "Accept" as the subject.");
		jQuery("#message .time").html(day + " at " + beginHour + ":" + beginMinute + " " + beginAMPM + " to " + endHour + ":" + endMinute + " " + endAMPM);
	}
	getValues();
	jQuery("select").change(function() {
		getValues();
	});
</script>
<?php
if (isset($_REQUEST['email']))
//if "email" is filled out, send email
  {
  //send email
  $email = $_REQUEST['email'];
  $subject = "MarriedGames.org contact form: " . $_REQUEST['subject'];
  $message = $_REQUEST['message'];
  mail("americanknight@gmail.com", $subject,
  $message, "From:" . $email);
  echo "Your message has been sent.";
  }
?>