<?php
$con=mysqli_connect("localhost","root","asdf","mg");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
?>

<?php
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	if ($husband == null) $husband = "Zack";
	if ($wife == null) $wife = "Vanessa";
?>
<div id="load" style="display:none;"></div>

<nav class="tabs-menu">
	<a href="#balances" data-tab="balances" class="active">Balances</a>
	<a href="#services1" data-tab="services1"><?php echo $husband ?>'s Services</a>
	<a href="#services2" data-tab="services2"><?php echo $wife ?>'s Services</a>
</nav>
	<div class="tabs-content">
		<div data-tab="balances" class="active">
		<p><a id="resetMoney" style="cursor:pointer;">Reset Love Bucks</a></p>
		<?php for ($x = 1; $x <= 2; $x ++) {
			if ($x == 1) {
				$gender = "him";
				$oppositeGender = "her";
				$name = $husband;
				$spouse = $wife;
			}
			else {
				$gender = "her";
				$oppositeGender = "him";
				$name = $wife;
				$spouse = $husband;
			}
		?>
		<div id="moneyTables">
		<table class="<?php echo $gender ?>" style="float: left;<?php if ($x == 1) echo"margin-right:5%;" ?>">
			<tr>
				<th><?php echo $name ?>'s Love Bucks</th>
			</tr>
			<tr>
				<?php
					$sql = "SELECT $gender FROM sexCash WHERE user = " . S2MEMBER_CURRENT_USER_ID;
					$result = $con->query($sql);
					while($row = $result->fetch_assoc()) {
						$money = $row[$gender];
					}
					if ($money == null) $money = 200;
				?>
				<td class="money-container" style="font-size:2em;">$<span class="money" data-money="<?php echo $money; ?>"><?php echo number_format($money); ?></span></td>
			</tr>
			<tr>
				<th> Give <?php echo $spouse ?> </th>
			</tr>
			<tr>
				<td style="text-align:left;">
					<p>
						<input id="award<?php echo $x ?>" type="radio" name="type<?php echo $x ?>" value="award" checked="checked" style="display:inline;" />
						<label for="award<?php echo $x ?>" style="display:inline;">Award</label>
					</p>
					<p>
						<input id="payment<?php echo $x ?>" type="radio" name="type<?php echo $x ?>" value="payment" style="display:inline;" />
						<label for="payment<?php echo $x ?>" style="display:inline;">Payment</label>
					</p>
					<label for="amount">Amount:</label>
					$ <input id="amount" type="text" style="margin-bottom:0; display:inline;" />
					<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> type="button" class="giveMoney button-small">Give Money</button>
				</td>
			</tr>
			<tr>
				<th style="line-height:1em;">Purchased Services<br /><span style="font-size:.75em;">(<?php echo $spouse ?> owes <?php echo $name ?>)</span></th>
			</tr>
			<tr>
				<td class="owed">
					<ul>
						<?php
							$sql = "SELECT * FROM sexyStore WHERE user = " . S2MEMBER_CURRENT_USER_ID . " AND gender = '$oppositeGender' AND owed > 0";
							$result = $con->query($sql);
							$y = 1;
							while($row = $result->fetch_assoc()) {
								echo "<li class='" . $row['id'] . "'><span class='service'>" . $row['name'] . "</span> (&times;<span class='quantity'>" . $row['owed'] . "</span>) <div class='actions'><span class='completed'>Completed</span></div></li>";
								$y ++;
							}
						?>
					</ul>
				</td>
			</tr>
		</table>
		</div>
		<?php } ?>
		</div><!-- blanaces -->
		<?php
			for ($x = 1; $x <= 2; $x ++) {
				if ($x == 1) {
					$gender = "him";
					$name = $husband;
					$spouse = $wife;
				}
				else {
					$gender = "her";
					$name = $wife;
					$spouse = $husband;
				}
		?>
			<div data-tab="services<?php echo $x ?>">
				<h2><?php echo $name ?>'s Services for <?php echo $spouse ?></h2>
				<table style="width:100%;" id="catalogue-<?php echo $x ?>" class="catalogue <?php echo $gender ?>" data-gender="<?php echo $gender ?>">
					<thead>
						<tr>
							<th style="width:60%; line-height:1em;">Service<br /><span style="font-size:.75em;">(Set by <?php echo $name ?>)</span></th>
							<th style="line-height:1em;">Price<br /><span style="font-size:.75em;">(Set by <?php echo $name ?>)</span></th>
							<th style="line-height:1em;">Purchase<br /><span style="font-size:.75em;">(As <?php echo $spouse ?>)</span></th>
						</tr>
					</thead>
					<tbody style="box-shadow:none;">
						<?php
							$sql = "SELECT * FROM sexyStore WHERE user = " . S2MEMBER_CURRENT_USER_ID . " AND gender = '$gender' ORDER BY name";
                            $result = $con->query($sql);
                            while ($row = $result->fetch_assoc()) {
								include "row.php";
							}
						?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="3" style="background:none; text-align:left;">
								<h3>Add a Service</h3>
								<select class="selectService">
									<optgroup label="Custom">
										<option class="option" value="~">Create your own service</option>
									</optgroup>
									<optgroup label="Default">
										<?php
										$sql = "SELECT * FROM sexyStore WHERE user = 0 AND (gender = '$gender' OR gender = '') ORDER BY name";
                                        $result = $con->query($sql);
                                        while ($row = $result->fetch_assoc()) { ?>
											<option value="<?php echo $row['name'] ?>~<?php echo $row['price'] ?>"><?php echo $row['name'] ?></option>
										<?php }	?>
									</optgroup>
								</select>
								<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> type="button" class="addService">Add Service</button>
							</td>
						</tr>
						<div class="row"></div>
					</tfoot>
				</table>
			</div>
		<?php } ?>
	</div><!-- tabs-content -->
<script>
	var husband = "<?php echo $husband ?>";
	var wife = "<?php echo $wife ?>";
	var user = "<?php echo S2MEMBER_CURRENT_USER_ID ?>";
</script>
<?php if (current_user_can("access_s2member_level1")) { ?>
	<script src="/wp-content/themes/marriedgames.org/sexy-store/js.js"></script>
<?php } ?>
