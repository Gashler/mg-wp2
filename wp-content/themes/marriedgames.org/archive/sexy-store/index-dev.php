<?php
$con=mysqli_connect("localhost","root","asdf","mg");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }
?>

<?php
	$fields = json_decode(S2MEMBER_CURRENT_USER_FIELDS, true);
	$husband = $fields["husband"];
	$wife = $fields["wife"];
?>
<div id="load" style="display:none;"></div>
<?php for ($x = 1; $x <= 2; $x ++) {
	if ($x == 1) {
		$gender = "him";
		$oppositeGender = "her";
		$name = $husband;
		$spouse = $wife;
	}
	else {
		$gender = "her";
		$oppositeGender = "him";
		$name = $wife;
		$spouse = $husband;
	}
?>
<form autocomplete="off">
<div id="moneyTables">
<table class="<?php echo $gender ?>" style="float: left; margin-right:5%;">
	<tr>
		<th><?php echo $name ?>'s Money</th>
	</tr>
	<tr>
		<?php
			$sql = "SELECT $gender FROM sexCash WHERE user = " . S2MEMBER_CURRENT_USER_ID;
			$result = $con->query($sql);
			while($row = $result->fetch_assoc()) {
				$money = $row[$gender];
			}
			if ($money == null) $money = 200; 
		?>
		<td class="money-container" style="font-size:2em;">$<span class="money"><?php echo $money ?></span></td>
	</tr>
	<tr>
		<th> Give <?php echo $spouse ?> </th>
	</tr>
	<tr>
		<td style="text-align:left;">
			<p>
				<input id="award<?php echo $x ?>" type="radio" name="type<?php echo $x ?>" value="award" checked="checked" style="display:inline;" />
				<label for="award<?php echo $x ?>" style="display:inline;">Award</label>
			</p>
			<p>
				<input id="payment<?php echo $x ?>" type="radio" name="type<?php echo $x ?>" value="payment" style="display:inline;" />
				<label for="payment<?php echo $x ?>" style="display:inline;">Payment</label>
			</p>
			<label for="amount">Amount:</label>
			$ <input id="amount" type="text" style="margin-bottom:0; display:inline;" />
			<button type="button" class="giveMoney button-small">Give Money</button>
		</td>
	</tr>
	<tr>
		<th style="line-height:1em;">Purchased Services<br /><span style="font-size:.75em;">(<?php echo $spouse ?> owes <?php echo $name ?>)</span></th>
	</tr>
	<tr>
		<td class="owed">
			<ul>
				<?php
					$sql = "SELECT * FROM sexyStore WHERE user = " . S2MEMBER_CURRENT_USER_ID . " AND gender = '$oppositeGender' AND owed > 0";
					$result = $con->query($sql);
					$y = 1;
					while($row = $result->fetch_assoc()) {
						echo "<li class='" . $row['id'] . "'><span class='service'>" . $row['name'] . "</span> (&times;<span class='quantity'>" . $row['owed'] . "</span>) <div class='actions'><span class='email'>Email</span> | <span class='completed'>Completed</span></div></li>";
						$y ++;
					}
				?>			
			</ul>
		</td>
	</tr>
</table>
</div>
<?php } ?>
<div style="clear:both;"></div>

<?php
	for ($x = 1; $x <= 2; $x ++) {
		if ($x == 1) {
			$gender = "him";
			$name = $husband;
			$spouse = $wife;
		}
		else {
			$gender = "her";
			$name = $wife;
			$spouse = $husband;
		}
?>
	<h2><?php echo $name ?>'s Services for <?php echo $spouse ?></h2>
	<table style="width:100%;" id="catalogue-<?php echo $x ?>" class="catalogue <?php echo $gender ?>" data-gender="<?php echo $gender ?>">
		<thead>
			<tr>
				<th style="width:60%; line-height:1em;">Service<br /><span style="font-size:.75em;">(Set by <?php echo $name ?>)</span></th>
				<th style="line-height:1em;">Price<br /><span style="font-size:.75em;">(Set by <?php echo $name ?>)</span></th>
				<th style="line-height:1em;">Purchase<br /><span style="font-size:.75em;">(As <?php echo $spouse ?>)</span></th>
			</tr>
		</thead>
		<tbody style="box-shadow:none;">
			<?php
				$sql = "SELECT * FROM sexyStore WHERE user = " . S2MEMBER_CURRENT_USER_ID . " AND gender = '$gender' ORDER BY name";
				$result = $con->query($sql);
				while ($row = $result->fetch_assoc()) {
					include "row.php";
				}
			?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3" style="background:none; text-align:left;">
					<h3>Add a Service</h3>
					<select class="selectService">
						<optgroup label="Custom">
							<option class="option" value="~">Create your own service</option>
						</optgroup>
						<optgroup label="Default">
							<?php
							$sql = "SELECT * FROM sexyStore WHERE user = 0 ORDER BY name";
							$result = $con->query($sql);
							while ($row = $result->fetch_assoc()) { ?>
								<option value="<?php echo $row['name'] ?>~<?php echo $row['price'] ?>"><?php echo $row['name'] ?></option>
							<?php }	?>
						</optgroup>
					</select>
					<button type="button" class="addService">Add Service</button>
				</td>
			</tr>
			<div class="row"></div>
		</tfoot>
	</table>
<?php } ?>
</form>
<script>
	var husband = "<?php echo $husband ?>";
	var wife = "<?php echo $wife ?>";
</script>
<script src="/wp-content/themes/marriedgames.org/sexy-store/js-dev.js"></script>
