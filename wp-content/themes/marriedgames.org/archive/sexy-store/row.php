<tr id="<?php echo $row['id'] ?>">
	<td class="service-container"><input class="service" type="text" value="<?php echo $row['name'] ?>" /></td>
	<td class="price-container"><div style="display:table-cell;">$&nbsp;</div><div style="display:table-cell;"><input class='price' type='text' value="<?php echo $row['price'] ?>" /></div></td>
	<td>
		<div class="container">
			<div class="x" style="float:right;">&times;</div>
			<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> type='button' class='small purchase'>Purchase</button>
		</div>
	</td>
</tr>