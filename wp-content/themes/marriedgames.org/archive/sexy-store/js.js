/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// add row
var id;
var price;
jQuery(".addService").click(function() {
	gender = jQuery(this).closest(".catalogue").attr("data-gender");
	var str = jQuery(this).siblings(".selectService").val();
	var strs = str.split("~");
	service = strs[0];
	price = strs[1];
	//if (service !== "" && price !== "") {
		addRow(gender,service,price);
	//}
	
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/getLastRow.php", {
		user: user
	}, function(result) {
		id = result;
		
		// hack to fix id mysteriously sometimes being the same as the last row
		if (jQuery("#" + id).length > 0) id ++;
		
		jQuery(".catalogue." + gender + " tbody").append("<tr id='" + id + "'><td><input class='service' type='text' value='" + service + "' /></td><td><div style='display:table-cell;'>$&nbsp;</div><div style='display:table-cell;'><input class='price' type='text' value='" + price + "' /></div></td><td><div class='container'><div class='x'>&times;</div><button type='button' class='small purchase'>Purchase</button></div></tr>");

		// purchase service
	
		jQuery("#" + id).on("click", ".purchase", function() {
			id = jQuery(this).closest("tr").attr("id");
			purchaseService(id);
		});

		// delete row
		
		jQuery("#" + id + " .x").click(function() {
			rowId = jQuery(this).closest("tr").attr("id");
			deleteRow(rowId);
		});
		
		// update row in DB on keyup
		
		jQuery(".catalogue input").keyup(function() {
			id = jQuery(this).closest("tr").attr("id");
			service = jQuery("#" + id + " .service").val();
			price = jQuery("#" + id + " .price").val();
			updateRow(id,service,price);
		});
	});
});

// purchase service

function purchaseService(id) {
	if (jQuery("#" + id).parents("table").hasClass("him")) {
		buyer = "her";
		buyerName = wife;
		seller = "him";
	}
	else {
		buyer = "him";
		buyerName = husband;
		seller = "her";
	}
	service = jQuery("#" + id + " .service").val();
	price = parseInt(jQuery("#" + id + " .price").val());
	buyerMoney = parseInt(parseInt(jQuery("#moneyTables ." + buyer + " .money").attr("data-money")));
	sellerMoney = parseInt(jQuery("#moneyTables ." + seller + " .money").attr("data-money"));
	if (buyerMoney < price) alert(buyerName + " can't afford this service");
	else {
		alert(buyerName + " has purchased this service.");
		buyerMoney -= price;
		sellerMoney += price;
		jQuery("#moneyTables ." + buyer + " .money").attr("data-money", buyerMoney);
		jQuery("#moneyTables ." + seller + " .money").attr("data-money", sellerMoney);
		buyerMoney = addCommas(buyerMoney);
		jQuery("#moneyTables ." + buyer + " .money").html(buyerMoney);
		sellerMoney = addCommas(sellerMoney);
		jQuery("#moneyTables ." + seller + " .money").html(sellerMoney);
		updateMoney();
		
		// add owed service to lists
		
		if (jQuery("#moneyTables ." + buyer + " .owed li").hasClass(id) == true) {
			quantity = jQuery(".owed li." + id + " .quantity").html();
			quantity ++;
			jQuery(".owed li." + id + " .quantity").html(quantity);
		}
		else {
			jQuery("#moneyTables ." + buyer + " .owed ul").append("<li class='" + id + "'><span class='service'>" + service + "</span> (&times;<span class='quantity'>1</span>) <div class='actions'><span class='completed'>Completed</span></div></li>");

			// complete service
			
			jQuery("li." + id + " .completed").click(function() {
				completeService(id);
			});
		
		}
		jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateOwed.php", {
			id: id,
			operation: "add"
		});
	}
}
jQuery(".purchase").click(function() {
	id = jQuery(this).closest("tr").attr("id");
	purchaseService(id);
});

// add new row in DB

function addRow(gender,name,price) {
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/addRow.php", {
		user: user,
		gender: gender,
		name: name,
		price: price
	});
}

// update row in DB on keyup

jQuery(".catalogue input").keyup(function() {
	id = jQuery(this).closest("tr").attr("id");
	service = jQuery("#" + id + " .service").val();
	price = jQuery("#" + id + " .price").val();
	updateRow(id,service,price);
});

// update row in DB

function updateRow(id,name,price) {
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateRow.php", {
		id: id,
		name: name,
		price: price
	});
}

// delete row

function deleteRow(id, gender) {
	if (gender == "him") {
		var name = husband;
		var spouse = wife;
	}
	else {
		var name = wife;
		var spouse = husband;
	}
	if (jQuery(".owed li." + id).length > 0) {
		alert("This service cannot yet be removed, because it has already been purchased by " + spouse + " and must be completed by " + name + ".");
	}
	else {
		service = jQuery("#" + id + " .service").val();
		jQuery("#" + rowId).remove();
		jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/deleteRow.php", {
			user: user,
			gender: gender,
			name: service,
		});
	}
}
jQuery("td").on("click", ".x", function() {
	rowId = jQuery(this).closest("tr").attr("id");
	var gender = jQuery(this).closest(".catalogue").attr("data-gender");
	deleteRow(rowId, gender);
});

jQuery(".giveMoney").click(function() {
	var giver = jQuery(this).closest("table").attr("class");
	var amount = jQuery("table." + giver + " #amount").val();
	if (amount !== "") {
		var receiver;
		if (giver == "him") {
			receiver = "her";
		}
		if (giver == "her") {
			receiver = "him";
		}
		var type = jQuery("." + giver + " input:radio:checked").val();
		amount = parseInt(amount);
		if (type == "payment") {
			originalAmount = parseInt(jQuery("table." + giver + " .money").attr("data-money"));
			newAmount = originalAmount - amount;
			jQuery("table." + giver + " .money").attr("data-money", newAmount);
			newAmount = addCommas(newAmount);
			jQuery("table." + giver + " .money").html(newAmount);
		}
		originalAmount = parseInt(jQuery("table." + receiver + " .money").attr("data-money"));
		newAmount = originalAmount + amount;
		jQuery("table." + receiver + " .money").attr("data-money", newAmount);
		newAmount = addCommas(newAmount);
		jQuery("table." + receiver + " .money").html(newAmount);
		updateMoney();
	}
});

// add commas

function addCommas(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ',' + '$2');
	}
	return x1 + x2;
}

// update sex cash in DB

function updateMoney() {
	var him = jQuery("#moneyTables .him .money").attr("data-money");
	var her = jQuery("#moneyTables .her .money").attr("data-money");
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateSexCash.php", {
		user: user,
		him: him,
		her: her
	});
}

// complete service

function decreaseService(id) {
	quantity --;
	jQuery("li." + id + " .quantity").html(quantity);
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateOwed.php", {
		id: id,
		operation: "subtract"
	});	
}

function completeService(id) {
	var quantity = parseInt(jQuery("li." + id + " .quantity").html());
	if (quantity == 1) {
		jQuery("li." + id).remove();
	}
	else {
		quantity --;
		jQuery("li." + id + " .quantity").html(quantity);
	}
	jQuery("#load").load("/wp-content/themes/marriedgames.org/sexy-store/updateOwed.php", {
		id: id,
		operation: "subtract"
	});	
}
jQuery(".completed").click(function() {
	var id = jQuery(this).parents("li").attr("class");
	completeService(id);
});

// reset money

jQuery("#resetMoney").click(function() {
	var r = confirm("Reset all money to $200?");
	if (r == true) {
		jQuery("#moneyTables .money").html(200).attr("data-money", 200);
		updateMoney();
	}
});
