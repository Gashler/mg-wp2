<?php
	
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	if ($husband == null) $husband = "Zack";
	if ($wife == null) $wife = "Vanessa";
	$id = S2MEMBER_CURRENT_USER_SUBSCR_OR_WP_ID;
	
	// get giver and time

	$con=mysqli_connect("localhost","root","asdf","mg");
	// Check connection
	if (mysqli_connect_errno())
	  {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }
	
	$sql = "SELECT * FROM favorClock WHERE id='$id'";
	$result = $con->query($sql);
	while($row = $result->fetch_assoc()) {
		  $hours = $row['hours'];
		  $giver = $row['giver'];
		  $oldGiver = $row['oldGiver'];
		  $owes = $row['owes'];
		  $minutes = $row['minutes'];
		  $seconds = $row['seconds'];
	}
?>
<script>
	var husband = "<?php echo $husband ?>";
	var wife = "<?php echo $wife ?>";
	var user = "<?php echo $id ?>";
</script>
<div id="favor-clock" class="generatedContent" style="margin:6% 0; text-align:center;">
	<?php
		if ($owes !== null) {
			if ($owes == "he") {
				$ower = $husband;
				$owed = $wife;
				$class = "she";
			}
			else {
				$ower = $wife;
				$owed = $husband;
				$class = "he";
			}
		}
		else {
			$ower = $wife;
			$owed = $husband;
			$class = "he";
		}
	?>
	<div class="caption <?php echo $class ?>" style="float:none;">Time <?php echo $ower ?> owes <?php echo $owed ?>:</div>
	<div id="time" style="font-size:2em; margin:1em 0; float:none;">
		<span id="counterHour">
			<?php
				if ($hours !== null) echo $hours;
				else echo "0";
			?>
		</span>&nbsp;:
		<span id="counterMin">
			<?php
				if ($minutes !== null) echo $minutes;
				else echo "0";
			?>
		</span>&nbsp;:
		<span id="counterSec">
			<?php
				if ($seconds !== null) echo $seconds;
				else echo "0";
			?>
		</span><br />
	</div>
	<label for="giver" style="font-weight:bold;">Performer:</label>
	<select name="giver" id="giver" style="margin:0 auto; width:185px; clear:both;">
		<option value="he" <?php if ($giver == "he") echo "selected" ?> /><?php echo $husband ?>'s serving <?php echo $wife ?></option>
		<option value="she" <?php if ($giver == "she") echo "selected" ?> /><?php echo $wife ?>'s serving <?php echo $husband ?></option>
	</select>
	<label for="selectService" style="font-weight:bold;">Service:</label>
	<select class="selectService" style="margin:0 auto;">
		<optgroup label="Custom">
			<option class="option">Custom Service</option>
		</optgroup>
		<optgroup label="Default">
			<?php
			$sql = "SELECT * FROM sexyStore WHERE user = 0 ORDER BY name";
			$result = $con->query($sql);
			while ($row = $result->fetch_assoc()) { ?>
				<option value="<?php echo $row['name'] ?>~<?php echo $row['price'] ?>"><?php echo $row['name'] ?></option>
			<?php }	?>
		</optgroup>
	</select>
	<br>
	<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> style="margin:0 auto; clear:both;" id="timer" class="start" onclick="check_timer()">Start Timer</button>
	<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> class="reset" style="margin:0 auto;">Reset</button>
	<div id="result"></div>
</div>
<?php if ($oldGiver !== null) { ?>
	<script>
		var oldGiver = "<?php echo $oldGiver ?>"
	</script>
<?php } else { ?>
	<script>
		var oldGiver = "he";
	</script>
<?php } ?>
<?php if ($owes !== null) { ?>
	<script>
		var owes = "<?php echo $owes ?>"
	</script>
<?php } else { ?>
	<script>
		var owes = "she";
	</script>
<?php } ?>
<?php if (current_user_can("access_s2member_level1")) { ?>
	<script src="/wp-content/themes/marriedgames.org/favor-clock/js.js"></script>
<?php } ?>