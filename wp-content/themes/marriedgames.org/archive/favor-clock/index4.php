<?php
$title = "Favor Clock";
include ("../header/header.php");
?>
<h1><?php echo $title; ?></h1>
<table id="favor-clock">
	<thead>
		<tr>
			<th>Time he owes her:</th>
		</tr>			
	</thead>
	<tbody>
		<tr>
			<td class="him" style="text-align:center;">
				<div id="time" style="font-size:2em; margin-bottom:6%;">
					<span id="counterHour">0</span>&nbsp;:
					<span id="counterMin">0</span>&nbsp;:
					<span id="counterSec">0</span><br />
				</div>
				<select name="giver" id="giver">
					<option value="he" />He's serving her</option>
					<option value="she" />She's serving him</option>
				</select>
				<button style="margin:0 auto;" id="timer" class="start" onclick="check_timer('him')">Start Timer</button>
				<button class="reset" style="margin:0 auto;">Reset</button>
			</td>
		</tr>			
	</tbody>
</table>

<script>

	// get giver

	var giver;
	function getGiver() {
		giver = jQuery("#giver").val();
	}
	getGiver();
	var oldGiver = giver;
	jQuery("#giver").change(function() {
		getGiver();
	});

	function check_timer(gender) {
		if (jQuery("." + gender + " #timer").hasClass('start')) {
			jQuery("." + gender + " #timer").html("Stop Timer");
			if (gender == "him") timer = setInterval("increaseCounter('him')", 1000);
			else timer = setInterval("increaseCounter('her')", 1000);
			jQuery("." + gender + " #timer").removeClass('start')
		} else {
			if ( typeof timer != "undefined") {
				clearInterval(timer);
			}
			jQuery("." + gender + " #timer").html("Start Timer");
			jQuery("." + gender + " #timer").addClass('start')
		}
	}

	function increaseCounter(gender) {

		var secVal;
		var minVal;
		secVal = parseInt(jQuery("." + gender + " #counterSec").html(), 10);
		minVal = parseInt(jQuery("." + gender + " #counterMin").html(), 10);
		hourVal = parseInt(jQuery("." + gender + " #counterHour").html(), 10);
		/*if (secVal > 0 || minVal > 0 || hourVal > 0) counterEmpty = true;
		else {
			var giver = jQuery("#giver").val();
		}*/
		if (secVal !== 59) {
			if (giver == oldGiver) jQuery("." + gender + " #counterSec").html(secVal + 1);
			else jQuery("." + gender + " #counterSec").html(secVal - 1);
		}
		else {
			if (minVal !== 59) {
				if (giver == oldGiver) jQuery("." + gender + " #counterMin").html(minVal + 1);
				else jQuery("." + gender + " #counterSec").html(secVal - 1);
			}
			else {
				if (giver == oldGiver) jQuery("." + gender + " #counterHour").html(hourVal + 1);
				else jQuery("." + gender + " #counterHour").html(hourVal - 1);
				jQuery("." + gender + " #counterMin").html(0);
			}
			jQuery("." + gender + " #counterSec").html(0);
			if (minVal == -59) {
				jQuery("." + gender + " #counterHour").html(hourVal + 1);
				jQuery("." + gender + " #counterMin").html(0);
			}
		}
		if (secVal == -59) {
			jQuery("." + gender + " #counterMin").html(minVal + 1);
			jQuery("." + gender + " #counterSec").html(0);
		}
	}
	jQuery(".reset").click(function() {
		gender = jQuery(this).parent().attr("class");
		jQuery("." + gender + " #counterSec").fadeOut(500).html(0).fadeIn(500);
		jQuery("." + gender + " #counterMin").fadeOut(500).html(0).fadeIn(500);
		jQuery("." + gender + " #counterHour").fadeOut(500).html(0).fadeIn(500);
	}); 
</script>
<?php
include ("../footer/footer.php");
 ?>
