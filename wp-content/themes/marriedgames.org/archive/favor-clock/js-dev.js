/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// get giver

var secVal;
var minVal;
var hourVal;

function getGiver() {
	giver = jQuery("#giver").val();
}
getGiver();
jQuery("#giver").change(function() {
	oldGiver = giver;
	getGiver();
	if ((jQuery("#counterSec").html() == 0) && (jQuery("#counterSec").html() == 0) && (jQuery("#counterSec").html() == 0)) updateCaption(giver);
});

function check_timer() {
	if (jQuery("#timer").hasClass('start')) {
		jQuery("#timer").html("Stop Timer");
		timer = setInterval("updateCounter()", 1000);
		jQuery("#timer").removeClass('start')
	} else {
		if ( typeof timer != "undefined") {
			clearInterval(timer);
		}
		jQuery("#timer").html("Start Timer");
		jQuery("#timer").addClass('start')
	}
}

function updateDB(giver,oldGiver,hourVal,minVal,updatedSecVal) {
	jQuery("#result").load("/wp-content/themes/marriedgames.org/favor-clock/insertTime.php", {
		id: S2MEMBER_CURRENT_USER_SUBSCR_OR_WP_ID,
		giver: giver,
		oldGiver: oldGiver,
		owes: owes,
		hours: hourVal,
		minutes: minVal,
		seconds: updatedSecVal
	});
}

function updateCounter() {
	secVal = parseInt(jQuery("#counterSec").html(), 10);
	minVal = parseInt(jQuery("#counterMin").html(), 10);
	hourVal = parseInt(jQuery("#counterHour").html(), 10);
	if (giver == oldGiver) increaseCounter();
	else decreaseCounter();
	function increaseCounter() {
		if (secVal !== 59) {
			jQuery("#counterSec").html(secVal + 1);
		}
		else {
			if (minVal !== 59) {
				jQuery("#counterMin").html(minVal + 1);
			}
			else {
				jQuery("#counterHour").html(hourVal + 1);
				jQuery("#counterMin").html(0);
			}
			jQuery("#counterSec").html(0);
		}
		updatedSecVal = secVal + 1;
		updateDB(giver,oldGiver,hourVal,minVal,updatedSecVal);
	}
	function decreaseCounter() {
		if (secVal !== -59) {
			jQuery("#counterSec").html(secVal - 1);
		}
		else {
			if (minVal !== -59) {
				jQuery("#counterMin").html(minVal - 1);
			}
			else {
				jQuery("#counterHour").html(hourVal - 1);
				jQuery("#counterMin").html(0);
			}
			jQuery("#counterSec").html(0);
		}
		if ((secVal !== 0) || (minVal !== 0) || (hourVal !== 0)) {
			if (secVal == 0) {
				if (minVal !== 0) {
					jQuery("#counterMin").html(minVal - 1);
					jQuery("#counterSec").html(59);
				}
				else {
					if (hourVal !== 0) {
						jQuery("#counterHour").html(hourVal - 1);
						jQuery("#counterMin").html(59);
						jQuery("#counterSec").html(59);
					}
					else {
						jQuery("#counterHour").html(0);
						jQuery("#counterMin").html(0);
						jQuery("#counterSec").html(0);						
					}
				}
			}
		}
		else {
			oldGiver = giver;
			updateCaption();
			increaseCounter();
		}
		updatedSecVal = secVal - 1;
		updateDB(giver,oldGiver,hourVal,minVal,updatedSecVal);
	}
}

function updateCaption() {
	if (giver == "he") {
		var text = "Time " + wife + " owes " + husband + ":";
		owes = "she";
	}
	else {
		var text = "Time " + husband + " owes " + wife + ":";
		owes = "he";
	}
	jQuery(".caption").removeClass("he").removeClass("she").addClass(giver).html(text);
}

jQuery(".reset").click(function() {
	jQuery("#counterSec").fadeOut(500).html(0).fadeIn(500);
	jQuery("#counterMin").fadeOut(500).html(0).fadeIn(500);
	jQuery("#counterHour").fadeOut(500).html(0).fadeIn(500);
}); 