<?php
$title = "Favor Clock";
include ("../header/header.php");
?>
<h1><?php $title; ?></h1>
<table id="favor-clock">
	<thead>
		<tr>
			<th>
				Him
			</th>
			<th>
				Her
			</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="him">
				<span id="counterHour">0</span>&nbsp;:&nbsp;
				<span id="counterMin">0</span>&nbsp;:&nbsp;
				<span id="counterSec">0</span>
				<button id="timer" class="start" onclick="check_timer('him')">Start Timer</button>
				<button class="reset">Reset</button>
			</td>
			<td class="her">
				<span id="counterHour">0</span>&nbsp;:&nbsp;
				<span id="counterMin">0</span>&nbsp;:&nbsp;
				<span id="counterSec">0</span>
				<button id="timer" class="start" onclick="check_timer('her')">Start Timer</button>
				<button class="reset">Reset</button>
			</td>
		</tr>
	</tbody>
</table>

<script>

	function check_timer(gender) {
		if (jQuery("." + gender + " #timer").hasClass('start')) {
			jQuery("." + gender + " #timer").html("Stop Timer");
			if (gender == "him") timer = setInterval("increaseCounter('him')", 1000);
			else timer = setInterval("increaseCounter('her')", 1000);
			jQuery("." + gender + " #timer").removeClass('start')
		} else {
			if ( typeof timer != "undefined") {
				clearInterval(timer);
			}
			jQuery("." + gender + " #timer").html("Start Timer");
			jQuery("." + gender + " #timer").addClass('start')
		}
	}

	function increaseCounter(gender) {

		var secVal;
		var minVal;
		secVal = parseInt(jQuery("." + gender + " #counterSec").html(), 10)
		minVal = parseInt(jQuery("." + gender + " #counterMin").html(), 10)
		if (secVal != 59)
			jQuery("." + gender + " #counterSec").html((secVal + 1));
		else {
			if (minVal != 59) {
				jQuery("." + gender + " #counterMin").html((minVal + 1));
			}
			else {
				jQuery("." + gender + " #counterHour").html((parseInt(jQuery('#counterHour').html(), 10) + 1));
				jQuery("." + gender + " #counterMin").html(0);
			}
			jQuery("." + gender + " #counterSec").html(0);
		}
	}
	jQuery(".reset").click(function() {
		gender = jQuery(this).parent().attr("class");
		jQuery("." + gender + " #counterSec").fadeOut(500).html(0).fadeIn(500);
		jQuery("." + gender + " #counterMin").fadeOut(500).html(0).fadeIn(500);
		jQuery("." + gender + " #counterHour").fadeOut(500).html(0).fadeIn(500);
	}); 
</script>
<?php
include ("../footer/footer.php");
 ?>
