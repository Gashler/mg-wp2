<?php
$title = "Favor Clock";
include ("../header/header.php");
?>
<h1><?php echo $title; ?></h1>
<div id="favor-clock" class="generatedContent" style="margin-bottom:6%; text-align:center;">
	<div class="caption he">Time he owes her:</div>
	<div id="time" style="font-size:2em; margin:1em 0;">
		<span id="counterHour">0</span>&nbsp;:
		<span id="counterMin">0</span>&nbsp;:
		<span id="counterSec">0</span><br />
	</div>
	<select name="giver" id="giver">
		<option value="he" />He's serving her</option>
		<option value="she" />She's serving him</option>
	</select>
	<button style="margin:0 auto;" id="timer" class="start" onclick="check_timer('him')">Start Timer</button>
	<button class="reset" style="margin:0 auto;">Reset</button>
</div>
<script>

	// get giver

	var giver;
	function getGiver() {
		giver = jQuery("#giver").val();
	}
	getGiver();
	var oldGiver = giver;
	jQuery("#giver").change(function() {
		getGiver();
		if ((jQuery("#counterSec").html() == 0) && (jQuery("#counterSec").html() == 0) && (jQuery("#counterSec").html() == 0)) updateCaption(giver);
	});

	function check_timer() {
		if (jQuery("#timer").hasClass('start')) {
			jQuery("#timer").html("Stop Timer");
			timer = setInterval("updateCounter()", 1000);
			jQuery("#timer").removeClass('start')
		} else {
			if ( typeof timer != "undefined") {
				clearInterval(timer);
			}
			jQuery("#timer").html("Start Timer");
			jQuery("#timer").addClass('start')
		}
		oldGiver = giver;
	}

	function updateCounter() {

		var secVal;
		var minVal;
		secVal = parseInt(jQuery("#counterSec").html(), 10);
		minVal = parseInt(jQuery("#counterMin").html(), 10);
		hourVal = parseInt(jQuery("#counterHour").html(), 10);
		if (giver == oldGiver) increaseCounter();
		else decreaseCounter();
		function increaseCounter() {
			if (secVal !== 59) {
				jQuery("#counterSec").html(secVal + 1);
			}
			else {
				if (minVal !== 59) {
					jQuery("#counterMin").html(minVal + 1);
				}
				else {
					jQuery("#counterHour").html(hourVal + 1);
					jQuery("#counterMin").html(0);
				}
				jQuery("#counterSec").html(0);
			}
		}
		function decreaseCounter() {
			if (secVal !== -59) {
				jQuery("#counterSec").html(secVal - 1);
			}
			else {
				if (minVal !== -59) {
					jQuery("#counterMin").html(minVal - 1);
				}
				else {
					jQuery("#counterHour").html(hourVal - 1);
					jQuery("#counterMin").html(0);
				}
				jQuery("#counterSec").html(0);
			}
			if ((secVal !== 0) || (minVal !== 0) || (hourVal !== 0)) {
				if (secVal == 0) {
					if (minVal !== 0) {
						jQuery("#counterMin").html(minVal - 1);
						jQuery("#counterSec").html(59);
					}
					else {
						if (hourVal !== 0) {
							jQuery("#counterHour").html(hourVal - 1);
							jQuery("#counterMin").html(59);
							jQuery("#counterSec").html(59);
						}
						else {
							jQuery("#counterHour").html(0);
							jQuery("#counterMin").html(0);
							jQuery("#counterSec").html(0);						
						}
					}
				}
			}
			else {
				oldGiver = giver;
				updateCaption();
				increaseCounter();
			}
		}
	}
	
	function updateCaption() {
		if (giver == "he") var text = "Time she owes him:";
		else var text = "Time he owes her:";
		jQuery(".caption").removeClass("he").removeClass("she").addClass(giver).html(text);
	}
	
	jQuery(".reset").click(function() {
		jQuery("#counterSec").fadeOut(500).html(0).fadeIn(500);
		jQuery("#counterMin").fadeOut(500).html(0).fadeIn(500);
		jQuery("#counterHour").fadeOut(500).html(0).fadeIn(500);
	}); 
</script>
<?php
include ("../footer/footer.php");
 ?>
