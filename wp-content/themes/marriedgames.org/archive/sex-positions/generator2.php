<div id="generator">
	<div class="generatedContentContainer"></div>
	<div style="clear:both;"></div>
	<input type="submit" id="newPosition" value="New Position" class="button">
</div>
<?php
	$fields = json_decode(S2MEMBER_CURRENT_USER_FIELDS, true);
	$husband = $fields["husband"];
	$wife = $fields["wife"];
?>
<script>
	
	var husband = "<?php echo $husband ?>";
	var wife = "<?php echo $wife ?>";
	function loadContent() {
		jQuery(".generatedContentContainer").load("//sex-position-generator-content/", {
			husband: husband,
			wife: wife
		}, function() {
			jQuery(window).load(function() {
				makeBlock();
			});
		});
	}
	loadContent();
	jQuery("#newPosition").click(function() {
		jQuery("#generator").fadeOut(250, function() {
			loadContent();
			jQuery("#generator").fadeIn(250);
		});
	});
	
	// resize floating generatedContent boxes
	
	function makeBlock() {
		jQuery("#primary > #content").css({
			"width": "100%",
			"max-width": "575px"
		});
		if (jQuery(".entry-content img").width() > 300) {
			jQuery("p.generatedContent").css("clear", "both");
		}
	}
	jQuery(window).load(function() {
		makeBlock();
	});
	jQuery(window).resize(function() {
		makeBlock();
	})
	
</script>