<script>

	function randomRow() {
		x = Math.floor((Math.random() * rows) + 1);
		return x;
	}
	function randomColumn() {
		x = Math.floor((Math.random() * columns) + 1);
		return x;
	}

	// add player1
	
	var player1StartRow;
	var player1StartColumn;
	function generatePlayers() {
		player1StartRow = randomRow();
		player1StartColumn = randomColumn();
		if (!(jQuery(".space[row=" + player1StartRow + "][column=" + player1StartColumn + "]").hasClass("solid"))) {
			jQuery(".space[row=" + player1StartRow + "][column=" + player1StartColumn + "]").append("<div id='player1'></div>");								
		}
		else {
			generatePlayers();
		}
	}
	generatePlayers();
	
	// scroll screen to characters' starting positions
	
	viewerWidth = <?php echo $viewerWidth ?>;
	viewerHeight = <?php echo $viewerHeight ?>;
	halfWidth = viewerWidth / 2;
	//halfWidth = (Math.round(halfWidth / 10) * 10);
	halfHeight = viewerHeight / 2;
	//halfHeight = (Math.round(halfHeight / 10) * 10);
	scrollY = (player1StartRow - halfHeight)*unitSize;
	scrollX = (player1StartColumn - halfWidth)*unitSize;
	//alert("halfWidth: " + halfWidth + ", halfHeight: " + halfHeight);
	jQuery("#grid").css({
		bottom: scrollY + "px",
		right: scrollX + "px"
	});

	// clear path to edge of screen

	function clearPath(space) {
		row = jQuery(space).attr("row");
		column = jQuery(space).attr("column");
		if (row <= rows || column <= columns) {
			left = parseInt(jQuery(space).attr("column")) - 1;
			right = parseInt(jQuery(space).attr("column")) + 1;
			topz = parseInt(jQuery(space).attr("row")) - 1;
			bottom = parseInt(jQuery(space).attr("row")) + 1;
			terrain = jQuery(space).attr("terrain");
			x = Math.floor((Math.random() * 4));
			
			// clear top
			
			if (x == 0) {
				if (jQuery(".space[row=" + topz + "][column=" + column + "]").hasClass("solid")) {
					jQuery(".space[row=" + topz + "][column=" + column + "]").removeClass("solid").attr("terrain", terrain);
				}
				space = jQuery(".space[row=" + topz + "][column=" + column + "]");
				clearPath(space);
			}
			
			// clear right
			
			if (x == 1) {
				if (jQuery(".space[row=" + row + "][column=" + right + "]").hasClass("solid")) {
					jQuery(".space[row=" + row + "][column=" + right + "]").removeClass("solid").attr("terrain", terrain);
				}
				space = jQuery(".space[row=" + row + "][column=" + right + "]");
				clearPath(space);
			}
			
			// clear bottom
			
			if (x == 2) {
				if (jQuery(".space[row=" + bottom + "][column=" + column + "]").hasClass("solid")) {
					jQuery(".space[row=" + bottom + "][column=" + column + "]").removeClass("solid").attr("terrain", terrain);
				}
				space = jQuery(".space[row=" + bottom + "][column=" + column + "]");
				clearPath(space);
			}
			
			// clear left
			
			if (x == 3) {
				if (jQuery(".space[row=" + row + "][column=" + left + "]").hasClass("solid")) {
					jQuery(".space[row=" + row + "][column=" + left + "]").removeClass("solid").attr("terrain", terrain);
				}
				space = jQuery(".space[row=" + row + "][column=" + left + "]");
				clearPath(space);
			}
		}
	}
	clearPath(jQuery(".space[row=" + player1StartRow + "][column=" + player1StartColumn + "]"));

	function generateUnits() {
		row = Math.floor((Math.random() * rows) + 1);
		column = Math.floor((Math.random() * columns) + 1);
		if (!(jQuery(".space[row=" + row + "][column=" + column + "]").hasClass("solid"))) {
			jQuery(".space[row=" + row + "][column=" + column + "]").append("<img id='tiara' src='style/tiara.png' />");								
		}
		else {
			generateUnits();
		}
	}
	generateUnits();
	
	// clear path to tiara
	
	currentSpace = jQuery(".space[row=" + player1StartRow + "][column=" + player1StartColumn + "]");
	currentRow = jQuery(currentSpace).attr("row");
	currentColumn = jQuery(currentSpace).attr("column");
	goalRow = jQuery("#tiara").parent().attr("row");
	goalColumn = jQuery("#tiara").parent().attr("column");
	if (currentRow < goalRow && currentColumn < goalRow) {
		currentRow ++;
		currentColumn ++;
		currentSpace = jQuery(".space[row=" + row + "][column=" + column + "]");
		jQuery(".decoration", currentSpace).css({"z-index": "2000", "background": "red"});
	}

</script>