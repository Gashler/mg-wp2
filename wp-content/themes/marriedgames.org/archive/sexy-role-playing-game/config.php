<?php

// meta

$title = "Ariah's Quest";

// map

$unitSize = 50; // pixels
$viewerWidth = 12; // units
$viewerHeight = 8; // units
$rows = 16; // units
$columns = 24; // units
$prairiebg = "rgb(255,225,175)";
$grassbg = "rgb(150,200,100)";
$waterbg = "rgb(64,128,255)";
$rockbg = "rgb(128,64,0)";

// characters

$speed = 250; // milliseconds

?>