
			// round corners of mape
			
			jQuery(".space").each(function(){
				row = jQuery(this).attr("row");
				column = jQuery(this).attr("column");
				left = parseInt(jQuery(this).attr("column")) - 1;
				right = parseInt(jQuery(this).attr("column")) + 1;
				topz = parseInt(jQuery(this).attr("row")) - 1;
				bottom = parseInt(jQuery(this).attr("row")) + 1;
				terrain = jQuery(this).attr("terrain");
				bg = jQuery(this).css("background-color");
				z = 1;
				
				// top-left corner
				
				if (jQuery(".space[row=" + row + "][column=" + left + "]").attr("terrain") !== terrain && jQuery(".space[row=" + topz + "][column=" + left + "]").attr("terrain") !== terrain && jQuery(".space[row=" + topz + "][column=" + column + "]").attr("terrain") !== terrain) {
					jQuery(".space[row=" + row + "][column=" + column + "] .decorationContainer").prepend("<div class='decoration'></div>");
					jQuery(".decoration", this).css({
						"border-left" : "24px solid " + bg,
						"border-top" : "24px solid " + bg,
						"margin-left" : "-24px",
						"margin-top" : "-24px",
						"z-index" : z
					});
				}

				// top-right corner
				
				if (jQuery(".space[row=" + row + "][column=" + right + "]").attr("terrain") !== terrain && jQuery(".space[row=" + topz + "][column=" + right + "]").attr("terrain") !== terrain && jQuery(".space[row=" + topz + "][column=" + column + "]").attr("terrain") !== terrain) {
					jQuery(".space[row=" + row + "][column=" + column + "] .decorationContainer").prepend("<div class='decoration'></div>");
					jQuery(".decoration", this).css({
						"border-right" : "24px solid " + bg,
						"border-top" : "24px solid " + bg,
						"margin-right" : "-24px",
						"margin-top" : "-24px",
						"z-index" : z
					});
				}
				
				// bottom-left corner
				
				if (jQuery(".space[row=" + row + "][column=" + left + "]").attr("terrain") !== terrain && jQuery(".space[row=" + bottom + "][column=" + left + "]").attr("terrain") !== terrain && jQuery(".space[row=" + bottom + "][column=" + column + "]").attr("terrain") !== terrain) {
					jQuery(".space[row=" + row + "][column=" + column + "] .decorationContainer").prepend("<div class='decoration'></div>");
					jQuery(".decoration", this).css({
						"border-left" : "24px solid " + bg,
						"border-bottom" : "24px solid " + bg,
						"margin-bottom" : "-24px",
						"margin-left" : "-24px",
						"z-index" : z
					});
				}
				
				// bottom-right corner
				
				if (jQuery(".space[row=" + row + "][column=" + right + "]").attr("terrain") !== terrain && jQuery(".space[row=" + bottom + "][column=" + right + "]").attr("terrain") !== terrain && jQuery(".space[row=" + bottom + "][column=" + column + "]").attr("terrain") !== terrain) {
					jQuery(".space[row=" + row + "][column=" + column + "] .decorationContainer").prepend("<div class='decoration'></div>");
					jQuery(".decoration", this).css({
						"border-left" : "24px solid " + bg,
						"border-bottom" : "24px solid " + bg,
						"margin-bottom" : "-24px",
						"margin-right" : "-24px",
						"z-index" : z
					});
				}
				z ++;
			});