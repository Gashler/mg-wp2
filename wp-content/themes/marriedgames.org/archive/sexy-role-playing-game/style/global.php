<style>

/* css reset */

html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed, 
figure, figcaption, footer, header, hgroup, 
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
/* HTML5 display-role reset for older browsers */
article, aside, details, figcaption, figure, 
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}

/* end css reset */

/* global */

body {
	background: black;
}

/* end global */

#viewer {
	width: <?php echo $viewerWidth * $unitSize ?>px;
	height: <?php echo $viewerHeight * $unitSize; ?>px;
	overflow: hidden;
	margin: <?php echo $unitSize ?>px auto 0 auto;	
}

#grid {
	position: relative;
}

.row {
	height: <?php echo $unitSize ?>px;
	width: <?php echo $unitSize * $columns ?>px;
	clear: both;
}

.space {
	float: left;
	height: <?php echo $unitSize ?>px;
	width: <?php echo $unitSize ?>px;
	vertical-align: top;
	overflow: hidden;
	overflow: visible;
	position: relative;
}

.decorationContainer {
	position: relative;
}

.decoration {
	position: absolute;
	top: 0;
	left: 0;
	height: <?php echo $unitSize ?>px;
	width: <?php echo $unitSize ?>px;
	margin-left: -<?php echo $unitSize / 4 ?>px;px;
	/*margin-top: -<?php echo $unitSize / 4 ?>px;*/
}

#player1 {
	background: url("style/art.png");
	background-position: -100px 0px;
	border-radius: 4px;
	height:<?php echo $unitSize ?>px;
	width:<?php echo $unitSize ?>px;
	position: relative;
	z-index: 10000;
	margin-top: -12px;
}

/* spaces */

.space[terrain="prairie"], .space[terrain="prairie"] .decoration {
	background-color: <?php echo $prairiebg ?>;
}

.space[terrain="grass"], .sapce[terrain="grass"] .decoration {
	background-color: <?php echo $grassbg ?>;
}

.space[terrain="water"], .space[terrain="water"] .decoration {
	background-color: <?php echo $waterbg ?>;
}

.space[terrain="rock"], .space[terrain="rock"] .decoration {
	background-color: <?php echo $rockbg ?>;
}

.space[terrain="prairie"] .decorationContainer {
	z-index: 0;
}

.space[terrain="grass"] .decorationContainer {
	z-index: 100;
}

.space[terrain="water"] .decoration {
	z-index: -699;
}

.space[terrain="water"] .decorationContainer {
	z-index: -900;
}

.space[terrain="rock"] .decoration {
 	z-index: 899;
}

.space[terrain="rock"] .decorationContainer {
	z-index: 1000;
}

/* end spaces */

/* objects */

#tiara {
	position: relative;
	z-index: 1000;
}

/* end objects */

/* info */

.info {
	display:none;
}

#info {
	width: <?php echo $unitSize * $columns ?>px;
	color:white;
	margin:0 auto;
	margin-top: <?php echo $unitSize / 2 ?>px;
}

/* end info */

</style>