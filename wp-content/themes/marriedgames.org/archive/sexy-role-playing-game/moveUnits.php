<script>

	// scroll screen to characters' starting positions
	
	viewerWidth = <?php echo $viewerWidth ?>;
	viewerHeight = <?php echo $viewerHeight ?>;
	halfWidth = viewerWidth / 2;
	//halfWidth = (Math.round(halfWidth / 10) * 10);
	halfHeight = viewerHeight / 2;
	//halfHeight = (Math.round(halfHeight / 10) * 10);
	scrollBottom = (player1StartRow - halfHeight)*unitSize;
	scrollRight = (player1StartColumn - halfWidth)*unitSize;
	//alert("halfWidth: " + halfWidth + ", halfHeight: " + halfHeight);
	jQuery("#grid").css({
		"bottom": scrollBottom + "px",
		"right": scrollRight + "px"
	});

	// move players
	
	row = jQuery("#player1").parent().attr("row");
	column = jQuery("#player1").parent().attr("column");
	animating = false;
	scrollTop = -scrollBottom;
	scrollLeft = -scrollRight;
	speed = <?php echo $speed ?>;

	jQuery(document).keydown(function(e) {
		if (animating == false) {
	
				player1 = jQuery("#player1");
				
				function placePlayers() {
					
					// get tiara
		
					if (jQuery(".space[row=" + row + "][column=" + column + "]").children("#tiara").length > 0) {
						alert("You have a tiara!");
						jQuery("#tiara").remove();
					}
					jQuery(player1).css({
						"top" : 0,
						"right" : 0,
						"bottom" : 0,
						"left" : 0
					});
					jQuery(".space[row=" + row + "][column=" + column + "]").append(player1);
					animating = false;
				};
	
			// move right
	
			if (e.keyCode == 39) {
				if (column++ < columns && !(jQuery(".space[row=" + row + "][column=" + column + "]").hasClass("solid"))) {
					animating = true;
					jQuery("#player1").animate({
						left : unitSize + "px"
					}, speed, "linear", function() {
						placePlayers();
					}).css({"background-position": "-100px 0px"});
					setTimeout(function() {
						jQuery("#player1").css({"background-position": "-100px -50px"});
					}, speed);
					scrollRight += unitSize;
					scrollLeft -= unitSize;
					jQuery("#grid").animate({
						right: scrollRight + "px",
						left: scrollLeft + "px"
						}, speed);
				}
				else (column--);
			}
	
			// move left
	
			if (e.keyCode == 37) {
				if (column-- >= 2 && !(jQuery(".space[row=" + row + "][column=" + column + "]").hasClass("solid"))) {
					animating = true;
					jQuery("#player1").animate({
						left : "-50px"
					}, speed, "linear", function() {
						placePlayers();
					}).css({"background-position": "-150px 0px"});
					setTimeout(function() {
						jQuery("#player1").css({"background-position": "-150px -50px"});
					}, speed);
					scrollRight -= unitSize;
					scrollLeft += unitSize;
					jQuery("#grid").animate({
						right: scrollRight + "px",
						left: scrollLeft + "px"
						}, speed);
				}
				else (column++);
			}
	
			// move down
	
			if (e.keyCode == 40) {
				if (row++ < rows && !(jQuery(".space[row=" + row + "][column=" + column + "]").hasClass("solid"))) {
					animating = true;
					jQuery("#player1").animate({
						top : unitSize + "px"
					}, speed, "linear", function() {
						placePlayers();
					}).css({"background-position": "0px 0px"});
					setTimeout(function() {
						jQuery("#player1").css({"background-position": "0px -50px"});
					}, speed);
					scrollTop -= unitSize;
					scrollBottom += unitSize;
					jQuery("#grid").animate({
						bottom: scrollBottom + "px",
						top: scrollTop + "px"
						}, speed);
				}
				else (row--);
			}
	
			// move up
	
			if (e.keyCode == 38) {
				if (row-- >= 2 && !(jQuery(".space[row=" + row + "][column=" + column + "]").hasClass("solid"))) {
					animating = true;
					jQuery("#player1").animate({
						top : "-50px"
					}, speed, "linear", function() {
						placePlayers();
					}).css({"background-position": "-50px 0px"});
					setTimeout(function() {
						jQuery("#player1").css({"background-position": "-50px -50px"});
					}, speed);
					scrollTop += unitSize;
					scrollBottom -= unitSize;
					jQuery("#grid").animate({
						bottom: scrollBottom + "px",
						top: scrollTop + "px"
						}, speed);
				}
				else(row++);
			}
		}
		info = jQuery(".space[row=" + row + "][column=" + column + "] .info").html();
		jQuery("#info").html(info);
	});

</script>