<?php include("config.php") ?>
<!doctype html>
<html>
	<head>
		<?php
			include("jquery.php");
			include("style/global.php");
		?>
		<title><?php echo $title ?></title>
	</head>
	<body>
		<div id="viewer">
			<div id="grid"></div>
		</div>
		<div id="info"></div>
		<?php
			include("generateMap.php");
			include("addUnits.php");
			include("moveUnits.php");
		?>
	</body>
</html>