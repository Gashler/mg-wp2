<script>

// spaces

jQuery(".space[terrain='prairie'], .space[terrain='prairie'] .decoration").css({
	"background-color": "<?php echo $prairiebg ?>"
});

jQuery(".space[terrain='grass'], .space[terrain='grass'] .decoration").css({
	"background-color": "<?php echo $grassbg ?>"
});

jQuery(".space[terrain='water'], .space[terrain='water'] .decoration").css({
	"background-color": "<?php echo $waterbg ?>"
});

jQuery(".space[terrain='rock'], .space[terrain='rock'] .decoration").css({
	"background-color": "<?php echo $rockbg ?>"
});

jQuery(".space[terrain='prairie'] .decorationContainer").css({
	//"z-index": "70"
});

jQuery(".space[terrain='grass'] .decorationContainer").css({
	//"z-index": "80"
});

jQuery(".space[terrain='water'] .decoration").css({
	//"z-index": "-69"
});

jQuery(".space[terrain='water'] .decorationContainer").css({
	//"z-index": "-90"
});

jQuery(".space[terrain='rock'] .decoration").css({
 	//"z-index": "89"
});

jQuery(".space[terrain='rock'] .decorationContainer").css({
	//"z-index": "100"
});

// end spaces

</script>