<script>

	// generate map
	
	rows = <?php echo $rows ?>;
	columns = <?php echo $columns ?>;
	unitSize = <?php echo $unitSize ?>;
	var ids = new Array();
	var id = Math.floor(Math.random()*(rows * columns));
	ids.push(id);
	function getID() {
		id = Math.floor(Math.random()*(rows * columns));
		if ( $.inArray(id, ids) !== -1 ) {
			id = Math.floor(Math.random()*(rows * columns));
			getID();
			return id;
		}
		else {
			ids.push(id);
			return id;
		}
	}

	function generateUnitSize() {
		var range = 8;
		return (Math.floor( Math.random() * range / 2 ) * 2) + 4;
	}
	var firstSpace = true;
	var terrain;
	var border;
	var unitWidth = generateUnitSize();
	var unitHeight = generateUnitSize();
	for (r = 1; r <= rows; r++) {
		jQuery("#grid").append("<div class='row'>");
		for (c = 1; c <= columns; c ++) {
			function generateTerrain() {
				rand = Math.floor((Math.random() * 4));
				if (rand == 0) return "prairie";
				if (rand == 1) return "grass";
				if (rand == 2) return "water";
				if (rand == 3) return "rock";
			}
			if (firstSpace == true) {
				terrain = generateTerrain();
			}
			function addSpace() {
				if (unitWidth > 2 && firstSpace == false) {
				column = jQuery(".space:last").attr("column");
				row = jQuery(".space:last").attr("row");
					leftSpace = column - 1;
					terrain = jQuery(".space[column=" + leftSpace + "]").attr("terrain");
				}
				else if (unitHeight > 2 && firstSpace == false) {
				column = jQuery(".space:last").attr("column");
				row = jQuery(".space:last").attr("row");
					topSpace = row - 1;
					terrain = jQuery(".space[column=" + topSpace + "]").attr("terrain");
				}
				else {
					if (unitWidth == 2) {
						unitWidth = generateUnitSize();
						border = "border-right:2px solid black; width:48px;";
					}
					if (unitHeight == 2) {
						unitHeight = generateUnitSize();
						border = "border-bottom:2px solid black; height:48px;";
					}
					terrain = generateTerrain();
				}
				id = getID();
				jQuery("#grid .row:last").append("<div unitWidth='" + unitWidth + "' unitHeight='" + unitHeight + "' id='" + id + "' class='space' terrain='" + terrain + "' row='" + r + "' column='" + c + "' style='" + border + "'><div class='info'></div></div>");
				jQuery("div#" + id + " .info").load("getSpaceInfo.php", { id : id });
				if (terrain == "water" || terrain == "rock") {
					jQuery(".space:last").addClass("solid");
				}
				unitWidth --;
				unitHeight --;
			}
			if (firstSpace == true) {
				firstSpace = false;
			}
			addSpace();
			/*
			if (firstSpace == true) {
				generateTerrain();
				firstSpace = false;
			}
			else {
				rand = Math.floor((Math.random() * 5));
				column = jQuery(".space:last").attr("column");
				row = jQuery(".space:last").attr("row");
				leftSpace = column --;
				topSpace = row --;

				// base terrain type off of left space
				if (jQuery(".space[column=" + leftSpace + "]").attr("terrain") == jQuery(".space[column=" + topSpace + "]").attr("terrain")) {
					terrain = jQuery(".space[column=" + leftSpace + "]").attr("terrain");
					addSpace(terrain);
				}

				// base terrain type off of left space

				else if (rand < 2 && jQuery(".row:last .space:last").not(":first")) {
					terrain = jQuery(".space[column=" + leftSpace + "]").attr("terrain");
					addSpace(terrain);
				}

				// base terrain type off of top space

				else if (rand > 2 && rand < 5 && jQuery(".row:last").not(":first")) {
					terrain = jQuery(".space[column=" + topSpace + "]").attr("terrain");
					addSpace(terrain);
				}
				// generate a new terrain type

				else {
					generateTerrain();
				}
			}*/
		}
	}

// stylize map

jQuery(".space").each(function(){
	row = jQuery(this).attr("row");
	column = jQuery(this).attr("column");
	left = parseInt(jQuery(this).attr("column")) - 1;
	right = parseInt(jQuery(this).attr("column")) + 1;
	topz = parseInt(jQuery(this).attr("row")) - 1;
	bottom = parseInt(jQuery(this).attr("row")) + 1;
	terrain = jQuery(this).attr("terrain");
	
	// get space bg color and lighten it
	
	bg = jQuery(this).css("background-color").split("(");
	bg = bg[1].split(")");
	bg = bg[0].split(",");
	for (var x = 0; x < bg.length; x ++) {
		bg[x] = parseInt(bg[x]);
		if (bg[x] + 20 <= 255) {
			bg[x] += 20;
		}
		else bg[x] = 255;
	}
	lighterBG = "rgb(" + (bg[0]) + "," + (bg[1]) + "," + (bg[2]) + ")";

	// get space bg color and darken it
	
	bg = jQuery(this).css("background-color").split("(");
	bg = bg[1].split(")");
	bg = bg[0].split(",");
	for (var x = 0; x < bg.length; x ++) {
		if (bg[x] - 20 >= 0) {
			bg[x] -= 20;
		}
		else bg[x] = 0;
	}
	darkerBG = "rgb(" + (bg[0]) + "," + (bg[1]) + "," + (bg[2]) + ")";
		
	// top
	
	if (jQuery(".space[row=" + topz + "][column=" + column + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-top", "12px solid " + lighterBG);
	
	// top-right
	
	if (jQuery(".space[row=" + topz + "][column=" + column + "]").attr("terrain") !== terrain && jQuery(".space[row=" + row + "][column=" + right + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-top-right-radius", "12px");
	
	// right
	
	if (jQuery(".space[row=" + row + "][column=" + right + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-right", "12px solid " + darkerBG);

	// bottom-right
	
	if (jQuery(".space[row=" + row + "][column=" + right + "]").attr("terrain") !== terrain && jQuery(".space[row=" + bottom + "][column=" + column + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-bottom-right-radius", "12px");
	
	// bottom
	
	if (jQuery(".space[row=" + bottom + "][column=" + column + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-bottom", "12px solid " + darkerBG);
	
	// bottom-left
	
	if (jQuery(".space[row=" + bottom + "][column=" + column + "]").attr("terrain") !== terrain && jQuery(".space[row=" + row + "][column=" + left + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-bottom-left-radius", "12px");

	// left
	
	if (jQuery(".space[row=" + row + "][column=" + left + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-left", "12px solid " + lighterBG);
	
	// top-left
	
	if (jQuery(".space[row=" + row + "][column=" + left + "]").attr("terrain") !== terrain && jQuery(".space[row=" + topz + "][column=" + column + "]").attr("terrain") !== terrain) jQuery(".decoration", this).css("border-top-left-radius", "12px");

});

// scroll map to player's location
	
</script>