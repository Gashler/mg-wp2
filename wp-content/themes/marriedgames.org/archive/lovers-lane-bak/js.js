/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// hack to fix Wordpress changes & into &amp;

jQuery(window).load(function() {
	var x = jQuery("body").html();
	x = x.replace("&#038;", "&");
	jQuery("body").html(x);
});

// add prices to spaces

jQuery(".space").each(function() {
	if (!(jQuery(this).hasClass("go"))) {
		cost = jQuery(this).attr("cost") * 100;
		jQuery(this).append("<div class='cost'>$" + cost + "</div>");
	}
});

// size board

function sizeBoard() {
	//mainContentWidth = jQuery(window).width() - jQuery("#mainNav").width();
	//jQuery("#mainContent").css("width", mainContentWidth + "px");
	height = jQuery(".space").css("width");
	jQuery(".space").each(function() {
		jQuery(this).css("height", height);
	});
	jQuery(".cost").each(function() {
		jQuery(this).css("width", height);
	});
}

sizeBoard();
jQuery(window).resize(function() {
	sizeBoard();
});

// determine first turn

var turn;
function alternateTurn(gender) {
	if (gender == "She") {
		turn = "He"
		turnName = husband;
		turnPossesive = husband + "'s";
		oppositeTurn = "She";
		oppositeTurnName = wife;
	}
	else {
		turn = "She";
		turnName = wife;
		turnPossesive = wife + "'s";
		oppositeTurn = "He";
		oppositeTurnName = husband;
	}
}
rand = Math.round(Math.random() * 1);
if (rand == "0") alternateTurn("He");
else alternateTurn("She");
alternateTurn(rand);
firstTurn = true;
jQuery("#text").html(turnName + " rolls first.");
function turnNotice() {
	jQuery(".turnNotice").remove();
	jQuery("#centerContent").removeClass(oppositeTurn).addClass(turn).prepend("<div class='turnNotice'>" + turnPossesive + " turn</div>");
}
turnNotice();

// roll die

jQuery("#roll").click(function() {
	bonus = Math.round(Math.random() * 2);
	if (bonus == 0 && firstTurn == false) {
		jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/foreplayContent.php",
		{
			husband: husband,
			wife: wife
		},	function() {
			jQuery("#load").prepend("<h2>Free Bonus:</h2>");
		});
	}
	else {
		
	// alternate turn
	
	if (firstTurn == false) {
		alternateTurn(turn);
	}
	jQuery("#centerContent").removeClass(oppositeTurn).addClass(turn);
	turnNotice();
	jQuery("#text, #load").html("");
	jQuery("#roll").hide();
	var notice = null;
	jQuery(".spaceContainer.current").removeClass("current");
	jQuery(".turn").removeClass("turn");
	jQuery(".player." + turn).addClass("turn");
	space = jQuery(".turn").parents(".space").attr("id");
	space = parseInt(space);
	roll = Math.round(Math.random() * 5);
	var x = 0;
	var interval = setInterval(function() {
		player = jQuery(".player." + turn);
		jQuery(".player." + turn).remove();
		space ++;
		if (space <= 16) // last space
			jQuery("#" + space).append(player);
		else {
			
			// collect money for passing "Go"
			
			space = 1; // reset count to beginning space
			jQuery("#" + space).append(player);
			money = jQuery(".money." + turn).html();
			originalMoney = money;
			money = parseInt(money);
			jQuery(".spaceContainer." + turn).each(function() {
				cost = jQuery(" .space", this).attr("cost");
				cost = parseInt(cost);
				cost /= 4;
				cost *= 100;
				money += cost;
			});
			money += 400;
			jQuery(".money." + turn).html(money);
			increase = money - originalMoney;
			notice = "Collected $" + increase;
		}
		x ++;
	    if (x == roll + 1) {
	    	
	    	firstTurn = false;
	        clearInterval(interval);
			jQuery("#" + space).parent().addClass("current");
	    	title = jQuery("#" + space + " .label").html();
	    	cost = jQuery("#" + space).attr("cost");
	    	cost *= 100;
			money = jQuery(".money." + turn).html();
			
			function moveOn() {
		    	if (jQuery("#" + space).attr("id") !== "1") { // if not on "Go"
					jQuery("#text").html("<p id='notice' style='display:none;'>" + notice + "</p><button class='small purchaseProperty'>Purchase Property</button><div id='info'><div id='cost'>Cost: $" + cost + "</div></div>");
				}
				if (notice !== null) jQuery("#notice").show();
				
				// build options
				
				function showBuildOptions() {
					
					// assign prices to properties

					cost = jQuery("#" + space).attr("cost");
					cost = parseInt(cost) * 100;
					club = cost * .25;
					club = parseInt(club);
					spa = cost * .5;
					spa = parseInt(spa);
					theater = cost * .75;
					theater = parseInt(theater);
					hotel = cost;
					hotel = parseInt(hotel);
					
					jQuery("#info").html("<p>You own this property.</p><ul class='buttonList'><li><button class='build' data-property-type='club' cost='" + club + "'>Build Club</button> $" + club + "</li><li><button class='build' data-property-type='spa' cost='" + spa + "'>Build Spa</button> $" + spa + "</li><li><button class='build' data-property-type='theater' cost='" + theater + "'>Build Theater</button> $" + theater + "</li><li><button class='build' data-property-type='hotel' cost='" + hotel + "'>Build Hotel</button> $" + hotel + "</li></ul>");
					
					// hide build options out of price range
					
					jQuery(".build").each(function() {
						cost = jQuery(this).attr("cost");
						cost = parseInt(cost);
						if (money < cost) {
							jQuery(this).parent().hide();
						}
					});
					
					// build property
					
					jQuery(".build").click(function() {
						cost = jQuery(this).attr("cost");
						propertyType = jQuery(this).attr("data-property-type");
						if (money >= cost) {
							money -= cost;
							jQuery(".money." + turn).html(money);
							jQuery("#info").html("You now own a " + propertyType + " on this property.");
							jQuery("#" + space + " .label").after("<div class='property' data-property-type='" + propertyType + "' cost='" + cost + "'>" + propertyType + "</div>");
							jQuery("#" + space + " .cost").remove();
						}
					});
					
				}
				
				jQuery("#" + space).addClass("visited");
				
				if (jQuery("#" + space).parent().hasClass(turn) || jQuery("#" + space + " .property").length > 0 || jQuery("#" + space).parent().hasClass(oppositeTurn)) {
					jQuery(".purchaseProperty, #cost").hide();
					jQuery("#cost").hide();
				}
				else {
					jQuery(".purchaseProperty").show();
				}
				if (money < cost) {
					jQuery(".purchaseProperty, #cost").hide();
				}
				
				// show build options if player owns an empty space
				
				if (jQuery("#" + space).hasClass(turn) && jQuery("#" + space + " .property") == 0) {
					showBuildOptions();
				}
				jQuery("#roll").show().html("Next Turn");
				money = parseInt(money);
				
				// purchase property
				
				jQuery(".purchaseProperty").click(function() {
					jQuery(this).remove();
					jQuery("#notice").hide();
					money = jQuery(".money." + turn).html();
					money = parseInt(money);
					money -= cost;
					jQuery(".money." + turn).html(money);
					jQuery("#" + space).addClass(turn).parent().addClass(turn);
					showBuildOptions();
				});
				
				// land on opponent's property
	
				if (jQuery("#" + space + "." + oppositeTurn + " .property").length > 0) {
					jQuery("#roll").hide();
					type = jQuery("#" + space + " .property").attr("type");
					cost = jQuery("#" + space + " .property").attr("cost");
					cost = parseInt(cost);
					var action;
					jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/actions.php", {
					recipient: oppositeTurn,
					category: category,
					turn: turn,
					husband: husband,
					wife: wife
					}, function(result) {
						action = jQuery("#load").html();
						jQuery("#load").html("");
						if (money > cost) {
							jQuery("#text").html("<p>You must either " + action + " or pay " + oppositeTurnName + " $" + cost + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li><li><button id='pay'>Pay Up</button></li></ul>");
						}
						else {
							jQuery("#text").html("<p>You must " + action + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li></ul>");
						}
						jQuery("#perform").on("click", function() {
							jQuery("#roll").hide();
							t = 61;
							jQuery("#text").html("<p>You must " + action + " until the timer runs out.</p><p class='countdown'></p>");
							var interval = setInterval(function() {
								jQuery("#roll").hide();
								t --;
								jQuery("#text .countdown").html(t);
							    if (t == 0) {
							        clearInterval(interval);
							        jQuery("#text").html("<p>Time's up.</p><audio autoplay><source src='ding.ogg' type='audio/ogg'></audio>");
							        jQuery("#roll").show();
							    }
							}, 1000);
							jQuery("#roll").show();
						});
						jQuery("#pay").on("click", function() {
							money -= cost;
							jQuery(".money." + turn).html(money);
							opponentMoney = jQuery(".money." + oppositeTurn).html();
							opponentMoney = parseInt(opponentMoney);
							opponentMoney += cost;
							jQuery(".money." + oppositeTurn).html(opponentMoney);
							jQuery("#text").html("");
							jQuery("#roll").show();
						});
					});
				}
			}
	    	
			// first time on new space
			
			if (jQuery("#" + space).hasClass("visited") == false) {
				jQuery("#text").html("<h2>Welcome to " + title + "</h2><p>To celebrate your first visit, <span class='load'></span>.</p><button id='finished'>Finished</button>");
				category = jQuery("#" + space).attr("category");
				jQuery(".load").load("/wp-content/themes/marriedgames.org/lovers-lane/actions.php", {
					recipient: oppositeTurn,
					category: category,
					turn: turn,
					husband: husband,
					wife: wife
				});
				jQuery("#" + space).addClass("visited");
				jQuery("#finished").click(function() {
					moveOn();
				});
			}
			else {
				moveOn();
			}
		}
	}, 500);
	}
});