<script>

	// Get husband and wife names
	
	<?php
		$fields = json_decode(S2MEMBER_CURRENT_USER_FIELDS, true);
		$husband = $fields["husband"];
		$wife = $fields["wife"];
	?>
	var husband = "<?php echo $husband; ?>";
	var wife = "<?php echo $wife; ?>";
	
</script>
<table class="male" style="float: left; margin-bottom:2em;">
	<tbody style="box-shadow:none !important;">
		<tr>
			<th style="color:rgb(0,128,255); background:white !important; font-weight:bold;"><?php echo $husband ?>'s Money</th>
			<th style="color:rgb(255,64,64); background:white !important; font-weight:bold;"><?php echo $wife ?>'s Money</th>
		</tr>
		<tr>
			<td style="background:rgb(0,128,255); color:white; font-weight:bold;">$<span class="money He">2400</span></td>
			<td style="background:rgb(255,64,64); color:white; font-weight:bold;">$<span class="money She">2400</span></td>
		</tr>
	</tbody>
</table>
<table id="board" cellspacing="1" style="box-shadow:none;">
	<tbody style="box-shadow:none;">
		<tr class="row" id="r1">
			<td class="spaceContainer go">
				<div  class="space go current" id="1" category="club">
					<div class="label">Go</div>
					<div class="player He">&#9794;</div>
					<div class="player She">&#9792;</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="2" cost="2" category="club">
					<div class="label">Bawdy Boulevard</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="3" cost="2" category="club">
					<div class="label">Horny Hollows</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="4" cost="3" category="club" category="club">
					<div class="label">Lovers Lane</div>
				</div>
			</td>
			<td class="spaceContainer jackpot">
				<div class="space jackpot" id="5" cost="3" category="club">
					<div class="label">Erotic Environment</div>
				</div>
			</td>
		</tr><!-- r1 -->
		<tr class="row" id="r2">
			<td class="spaceContainer">
				<div class="space" id="16" cost="9" category="hotel">
					<div class="label">Sexy Street</div>
				</div>
			</td>
			<td colspan="3" rowspan="3" id="centerContent" style="vertical-align:middle !important;">
				<div id="load"></div>
				<div id="text"></div>
				<button id="roll">Roll</button>
			</td>
			<td class="spaceContainer">
				<div class="space" id="6" cost="4" category="spa">
					<div class="label">Arousal Avenue</div>
				</div>
			</td>
		</tr><!-- r2 -->
		<tr class="row" id="r3">
			<td class="spaceContainer">
				<div class="space" id="15" cost="8" category="hotel">
					<div class="label">Luscious Lounge</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="7" cost="4" category="spa">
					<div class="label">Amorous Alley</div>
				</div>
			</td>
		</tr><!-- r3 -->
		<tr class="row" id="r4">
			<td class="spaceContainer">
				<div class="space" id="14" cost="8" category="hotel">
					<div class="label">Lustful Lane</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="8" cost="5" category="spa">
					<div class="label">Flirtatious Flats</div>
				</div>				
			</td>
		</tr><!-- r5 -->
		<tr class="row" id="r5">
			<td class="spaceContainer bankrupt">
				<div class="space bankrupt" id="13" cost="7" category="theater">
					<div class="label">Voluptuous Valley</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="12" cost="7" category="theater">
					<div class="label">Provocative Parkway</div>
				</div>			
			</td>
			<td class="spaceContainer">
				<div class="space" id="11" cost="6" category="theater">
					<div class="label">Seductive Street</div>
				</div>
			</td>
			<td class="spaceContainer">
				<div class="space" id="10" cost="6" category="theater">
					<div class="label">Romantic Road</div>
				</div>
			</td>
			<td class="spaceContainer jail">
				<div class="space jail" id="9" cost="5" category="spa">
					<div class="label">Passion Place</div>
				</div>
			</td>
		</tr><!-- r5 -->
	</tbody>
</table><!-- board -->
<script src="/wp-content/themes/marriedgames.org/lovers-lane/js.js"></script>