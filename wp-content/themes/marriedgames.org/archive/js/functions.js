/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// pop

function pop(content) {
	jQuery("body").prepend("<div class='black'></div><div class='pop'><div class='x'>&times;</div>" + content + "</div>");
	jQuery(".black, .pop").fadeIn(function() {
		jQuery(".black, .x").click(function() {
			jQuery(".black, .pop").fadeOut(function() {
				closePopup();
			});
		});
	});
}
function closePopup() {
	jQuery(".black, .pop").fadeOut(function() {
		jQuery(".black, .pop").remove();
	});
}
jQuery(".closePopup").click(function() {
	closePopup();
});

// popbox

function popbox(content) {
	var dark;
	if (jQuery(".dark").length == 0) dark = "<div class='dark'></div>";
	else dark = "";
	var popbox = dark + "<div class='popboxWrapperWrapper'><div class='popboxWrapper'><div class='popboxContainer'><div class='popbox'>" + content + "</div></div></div></div>";
	if (jQuery("popboxWrapperWrapper").length > 0) jQuery(".popboxWrapperWrapper").after(popbox);
	else jQuery("body").prepend(popbox);
	jQuery(".dark, .popboxContainer").fadeIn(function() {
		jQuery(".close").click(function() {
			closePopbox();
		});
		jQuery(".dark").click(function() {
			if (!(jQuery(this).hasClass("static"))) closePopbox();
		});
	});
}
function closePopbox() {
	if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function() {
		jQuery(this).remove();
	});
	if (jQuery(".popboxWrapperWrapper").length > 1) {
		jQuery(".popboxWrapperWrapper:last").fadeOut(function() {
			jQuery(this).remove();
		});
		if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function() {
			jQuery(this).remove();
		});
	}
	else {
		jQuery(".popboxWrapperWrapper").fadeOut(function() {
			jQuery(this).remove();
		});
	}
}

// end popbox

// switch tabs

function toggleTabs(tab) {
	jQuery(".tabs-menu > a.active").removeClass("active");
	jQuery(".tabs-content > div.active").removeClass("active");
	jQuery(".tabs-menu > a[data-tab='" + tab + "'], .tabs-content > div[data-tab='" + tab + "']").addClass("active");	
}
jQuery(".tabs-menu > a").click(function() {
	jQuery(this).addClass("active");
	var tab = jQuery(this).attr("data-tab");
	toggleTabs(tab);
});

// reopen active tab upon page reload

if (location.href.indexOf("#") > 0) {
	var activeTab = location.href.split("#");
	toggleTabs(activeTab[1]);
}

// toggle definition list items

jQuery("body:not(.page-id-1870) dt").click(function() {
	jQuery(this).next("dd").slideToggle();
});

// submit subscription

jQuery("#submit").click(function() {
	var email = jQuery("#email").val();
	var atpos = email.indexOf("@");
	var dotpos = email.lastIndexOf(".");
	if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
		alert("Not a valid e-mail address");
	} else {
		jQuery("div.submitFields").slideUp();
		jQuery("div#result").html("Thank you for subscribing. Please check your email to complete the process.").slideDown();
		$.post("<?php echo $siteURL ?>registration/subscribe.php", {
			email : email
		}, function(result) {
			
		});
	}
});

// add text to email input

email = "Email address";
firstName = "First name";
jQuery("#email").val(email).css("color", "gray").click(function() {
	if (jQuery(this).val() == email) {
		jQuery(this).val("").css("color", "black");
	}
});
jQuery("#name").val(firstName).css("color", "gray").click(function() {
	if (jQuery(this).val() == firstName) {
		jQuery(this).val("").css("color", "black");
	}
});
jQuery("#email").blur(function() {
	if (jQuery(this).val() == "") {
		jQuery(this).val(email).css("color", "gray");
	}
});
jQuery("#name").blur(function() {
	if (jQuery(this).val() == "") {
		jQuery(this).val(firstName).css("color", "gray");
	}
});

// highlight current page

path = window.location.pathname;
jQuery("#mainNav a[href = '" + path + "']").addClass("currentPage");
if (path == "/") {
	jQuery("#mainNav a[href = '/index.php']").parent().addClass("currentPage");
}
//jQuery("#logo").css("font-size", )

// main menu

jQuery("#main-menu li li").each(function() {
	if (jQuery(this).children("ul").length > 0) {
		jQuery("> a", this).append("<div class='raq'>&#8250;</div>");
	}
});

jQuery("#main-menu li").hover(function() {
	if (jQuery(window).width() >= 840) {
		jQuery("ul", this).show();
		jQuery("li ul", this).hide();
	}
}, function() {
	if (jQuery(window).width() >= 840) {
		jQuery("ul", this).hide();
	}
});

jQuery("#main-menu li").click(function() {
	if (jQuery(window).width() < 840) {
		jQuery("ul", this).toggle();
		jQuery("li ul", this).toggle();
	}
});

// mobile menu button

jQuery("#menu-button").click(function() {
	jQuery(".menu .container").slideToggle();
});

// mobile search button

jQuery("#search-button").click(function() {
	jQuery(this).hide();
	jQuery(".menu #menuSearch").show().slideLeft();
});

// check opt-In Subscribe on main form

jQuery("#wysija-box-after-register").attr("checked", "checked");

// generate checkout page order summary

// if (jQuery("form").hasClass("s2member-pro-paypal-checkout-form")) {
	// jQuery(document).ready(function() {
		// var option = jQuery("#s2member-pro-paypal-checkout-options option:selected").html();
		// var freeTrial;
		// var months;
		// var lifetime;
		// if (jQuery("body").hasClass("page-id-2232") || jQuery("body").hasClass("page-id-3074")) {
			// freeTrial = "Free 7 day trial, then ";
			// months = option.split("then ");
			// months = months[1].split(" ");
			// months = months[0];
		// }
		// else {
			// months = option.split(" ");
			// months = months[0];
		// }		
		// //months = years * 12;
		// if (months == "Lifetime") {
			// lifetimePrice = months;
			// lifetimePrice = option.split("$");
			// lifetimePrice = lifetimePrice[1];
			// alert(lifetimePrice);
			// lifetime = true;
		// }
		// var standardRate = 9.99;
		// var rate = option.split("$");
		// rate = rate[1].split("/");
		// rate = rate[0];
		// var total;
		// if (lifetime == true) total = lifetimePrice;
		// else total = rate * months;
		// var promotionalDiscount = .9;
		// var promotionalDiscountLabel = "10% Off";
		// var before;
		// if (lifetime == true) before = total / .9;
		// else before = standardRate * months;
		// if (lifetime !== true) total = (Math.round((total * 100)/100)*promotionalDiscount).toFixed(2);
		// var tax = .07;
		// if (lifetime == true) longTermDiscount = "Huge!";
		// if (months == 12) longTermDiscount = "(none with this plan)";
		// if (months == 24) longTermDiscount = "40% Off";
		// if (months == 36) longTermDiscount = "55% Off";
		// tax *= before;
		// tax = (Math.round(tax * 100)/100).toFixed(2);
		// tax = parseFloat(tax);
		// before = parseFloat(before);
		// before = before + tax;
		// before = (Math.round(before * 100)/100).toFixed(2);
		// total = "<div><div class='strike'>$" + before + "<div class='line'></div></div></div>$" + total;
		// displayTax = (Math.round(tax * 100)/100).toFixed(2);
		// displayTax = "<div><div class='strike'>$" + tax + "<div class='line'></div></div></div>$0.00";
		// /*if (years == 1) {
			// if (freeTrial !== undefined) jQuery("#checkoutSummary #plan").html(freeTrial + years + " year auto-renewing subscription");
			// else jQuery("#checkoutSummary #plan").html(years + " year auto-renewing subscription");
		// }
		// else {*/
			// if (freeTrial !== undefined) jQuery("#checkoutSummary #plan").html(freeTrial + months + " months access");
			// else if (lifetime == true) jQuery("#checkoutSummary #plan").html("Lifetime membership");
			// else jQuery("#checkoutSummary #plan").html(months + " months access");
		// //}
		// if (lifetime == true) jQuery("#checkoutSummary #rate").html("-");
		// else jQuery("#checkoutSummary #rate").html("$" + rate);
		// if (months == 12) jQuery("#checkoutSummary #months").html("Every " + months + " months");
		// if (months == 24 || months == 36) jQuery("#checkoutSummary #months").html("One time");
		// /*else*/ if (lifetime == true) jQuery("#checkoutSummary #months").html("One time for unlimited access");
		// jQuery("#checkoutSummary #promotionalDiscount").html(promotionalDiscountLabel);
		// jQuery("#checkoutSummary #longTermDiscount").html(longTermDiscount);
		// jQuery("#checkoutSummary #tax").html(displayTax);
		// if (freeTrial !== undefined) jQuery("#checkoutSummary #total").html("$0.00 for 7 days, then <br>" + total);
		// else jQuery("#checkoutSummary #total").html(total);
// 		
	// });
// }