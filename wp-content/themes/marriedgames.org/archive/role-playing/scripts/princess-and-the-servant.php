<?php
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	if ($husband == null) $husband = "Zack";
	if ($wife == null) $wife = "Vanessa";
?>
<article class="roleplay">
<div class="section">
<h2 style="margin-top:0;">A Sexy Role-play for a Husband and Wife</h2>
<p class="generatedContent">A fobidden and unlikely love affair between a lonely princess and her adoring servant. This gentle and romantic role-play is aimed at fulfilling her fantasies.</p>
	<div class="action">
		<?php echo $wife ?>, a princess, is in her bedchamber preparing for her wedding. She lays out lingerie and a dress on her bed. <?php echo $husband ?>, a servant, enters the bedchamber.
	</div><!-- action -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			You sent for me, <?php echo $wife ?>?
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Yes. The prince is on his way and I need to get ready. My maids are all busy. Would my favorite manservant, <?php echo $husband ?>, be willing to help?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Anything for you, <?php echo $wife ?>.
		</div><!-- dialog -->
	</div><!-- him -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="parenthetical">
			points to lingerie and dress
		</div><!-- parenthetical -->
		<div class="dialog">
			I just need to get changed into these. For the wedding this afternoon.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			<?php echo $wife ?>, are you sure you want me to see you naked?
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			It doesn’t bother me. Does it bother you?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Of course not, I just thought for your sake...
		</div><!-- dialog -->
	</div><!-- him -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			For my sake, I need your help. Please.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="action">
		<?php echo $wife ?> stands as <?php echo $husband ?> undresses her and dresses her back up in the lingerie and dress. He smiles and she notices him smiling.
	</div><!-- action -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="parenthetical">
			playfully
		</div><!-- parenthetical -->
		<div class="dialog">
			Why are you smiling?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Please forgive me, <?php echo $wife ?>. You are the most beautiful woman in the world. The prince is a lucky man.
		</div><!-- dialog -->
	</div><!-- him -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			I wish I felt like a lucky girl. I’ve never even met the man. Today he’s going to take me to his faraway kingdom. My life is over.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			No, <?php echo $wife ?>. You’re just nervous. Here, let me help you relax.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="action">
		<?php echo $husband ?> massages <?php echo $wife ?>
	</div><!-- action -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Thank you, <?php echo $husband ?>. You always know how to make me feel better.
		</div><!-- dialog -->
	</div><!-- her -->
</div><!-- section -->

<div class="section">
	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="parenthetical">
			softly, to himself
		</div><!-- parenthetical -->
		<div class="dialog">
			You have the most lovely skin.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			What?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			I’m sorry. I mean, I’ll do anything to help my princess be happy.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Thank you. You do make me happy. You have always made me happy.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			I have been happy to be your servant, <?php echo $wife ?>.
		</div><!-- dialog -->
	</div><!-- him -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Could you help me relax my whole body? Please?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Yes, of course.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="action">
		<?php echo $husband ?> massages <?php echo $wife ?> everywhere but her private parts.
	</div><!-- action -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			That feels so nice, thank you. Would you be willing to do a deeper massage? I know it would really help me.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Of course.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="action">
		<?php echo $wife ?> takes off her dress.
	</div><!-- action -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- "character" -->
		<div class="dialog">
			Oh, I didn’t realize... I don’t think that’s a good idea, <?php echo $wife ?>.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			You don’t need to worry about anything, <?php echo $husband ?>. Please.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Yes, my princess.
		</div><!-- dialog -->
	</div><!-- him -->
</div><!-- section -->

<div class="section">
	<div class="action">
		<?php echo $husband ?> turns his head away and rubs <?php echo $wife ?> everywhere but her private parts
	</div><!-- action -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			What’s wrong? Why are you turning your head away?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Forgive me, you are just so beautiful. I don’t deserve to look at you.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			That is not true at all. Please, look at me.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="action">
		<?php echo $husband ?> looks at <?php echo $wife ?> all over her body
	</div><!-- action -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			I love your eyes. They are the most breathtaking eyes I’ve ever seen.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			They are nothing compared to yours.
		</div><!-- dialog -->
	</div><!-- him -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			I need to ready my whole body. Do you understand?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Yes.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="action">
		<?php echo $wife ?> takes <?php echo $husband ?>’s hand and guides him to rub her breasts, vagina, and anywhere else in ways that help to stimulate her.
	</div><!-- action -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			I love how you touch me.
		</div><!-- dialog -->
	</div><!-- her -->
</div><!-- section -->

<div class="section">
	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Well, I think you’re ready now. My job is done.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			<?php echo $husband ?>, wait. I don’t love the prince.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			You can learn to love him, princess.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Please, call me <?php echo $wife ?>. I no longer want to be a princess. There is only one man that I love, and no one else will do.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="action">
		<?php echo $wife ?> embraces and kisses <?php echo $husband ?>, then pulls him onto the bed
	</div><!-- action -->
</div><!-- section -->

<div class="section">
	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Oh, <?php echo $wife ?>!
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			I have always loved you.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			I have always loved you, too.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Then we will run away together. After we finish what we’ve started.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="action">
		<?php echo $wife ?> takes off <?php echo $husband ?>’s clothes and rubs his penis
	</div><!-- action -->
</div><!-- section -->

<div class="section">
	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="parenthetical">
			points at lingerie
		</div><!-- parenthetical -->
		<div class="dialog">
			You are amazing. Will you do the honors of removing these from me?
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="action">
		<?php echo $husband ?> takes off <?php echo $wife ?>’s lingerie.
	</div><!-- action -->

	<div class="him">
		<div class="character">
			 <?php echo $husband ?> 
		</div><!-- character -->
		<div class="dialog">
			Princess. I mean, <?php echo $wife ?>. You will always be a princess to me.
		</div><!-- dialog -->
	</div><!-- him -->

	<div class="her">
		<div class="character">
			<?php echo $wife ?>
		</div><!-- character -->
		<div class="dialog">
			Oh, <?php echo $husband ?>.
		</div><!-- dialog -->
	</div><!-- her -->

	<div class="action">
		<?php echo $wife ?> pulls <?php echo $husband ?> onto the bed and they start having sex.
	</div><!-- action -->
</div><!-- section -->
</article class="roleplay">
<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> id='nextLines'>Next Lines</button>
<script>
	id = 1;
	reveal = 2;
	jQuery(".section").each(function() {
		jQuery(this).attr("id", id).hide();
		id++;
	});
	jQuery(".section#" + 1).show();
	jQuery("#nextLines").click(function() {
		jQuery(".section").hide();
		jQuery("#" + reveal).show();
		reveal++;
		if (reveal == id) {
			jQuery("#nextLines").remove();
		}
	});
</script>