<?php
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	if ($husband == null) $husband = "Zack";
	if ($wife == null) $wife = "Vanessa";
?>
<article class="roleplay">
<div class="section">
<h2 style="margin-top:0;">A Sexy Role-play for a Husband and Wife</h2>
<p class="generatedContent">In this Star Trek/scifi-themed role-play in space, a mysterious erotic force has taken over the minds and sexual appetites of the crew. Could it be that the only way to overcome this vortex of temptation is by giving in?</p>
<div class="action"><?php echo $wife ?> enters.</div><!-- action -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Captain, you asked to meet with me?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Yes, lieutenant. As you know, our ship is in great danger. The crew –</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">They've lost their minds. I know.</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Much more than their minds. They've lost their shame. Everywhere I look, they're making out with each other, tearing off each other's clothes, too busy in each other's beds to fulfill their duties.</div><!-- dialog -->
</div><!-- him -->
</div><!-- section -->
<div class="section">
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">But why is this happening?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">That's what I wished to speak with you about. It appears that our ship has been seized by an eroticon.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">A what?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">An eroticon, one of the great mysteries of deep space. All we know is that these gaseous forces are extremely dangerous, deluding the reason of their prey by attacking beneath the waistline.</div><!-- dialog -->
</div><!-- him -->
</div><!-- section -->
<div class="section">
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">But what can we do?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">You and I, lieutenant, are the only ones who haven't yet lost control. We must stay strong.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Whatever you ask, captain, I'll do.</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">I knew I could trust you.</div><!-- dialog -->
</div><!-- him -->
</div><!-- section -->
<div class="section">
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">What are your orders?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Take off your uniform.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">What!?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">You promised to obey my orders.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="parenthetical">backing up</div><!-- parenthetical -->
<div class="dialog">Not you too!</div><!-- dialog -->
</div><!-- her -->
</div><!-- section -->
<div class="section">
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Your eyes are so lovely. Your voice … your hair … lieutenant, let me see the rest of you.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="parenthetical">struggling to get away</div><!-- parenthetical -->
<div class="dialog">Snap out of it, captain. You're not yourself.</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="parenthetical">seizing her</div><!-- parenthetical -->
<div class="dialog">Nonsense. I've never felt more alive.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Can't you see what's happening? You're being controlled by the eroticon.</div><!-- dialog -->
</div><!-- her -->
</div><!-- section -->
<div class="section">
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Ever since you walked into my quarters, I've felt a burning lust for you. Believe me, lieutenant, I tried to fight it, but then I realized … resistance is futile.
</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">No, Captain.</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">You don't understand. As long as we fight the eroticon, we'll be driven mad by our desires. Tell me the truth, lieutenant. Do you not want me?</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Captain!</div><!-- dialog -->
</div><!-- her -->
</div><!-- section -->
<div class="section">
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Answer me!</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Yes! Passionately. But –</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character 	-->
<div class="dialog">Then there's only thing we can do. Give in.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">We can't be selfish. We must save the ship!</div><!-- dialog -->
</div><!-- her -->
</div><!-- section -->
<div class="section">
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">I'm trying to tell you. This is the only way <em>to</em> save the ship. We must release the sexual tension. Only then will be be immune to the power of the eroticon. Only then will we regain our minds.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">I see. But do you really think this will work?</div><!-- dialog -->
</div><!-- her -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">What choice do we have? Lieutenant, for the sake of our ship, I order you to take off your clothes.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Aye-aye, captain.</div><!-- dialog -->
</div><!-- him -->
</div><!-- section -->
<div class="section">
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">And make it sexy. If we're going to relieve all the tension, we must achieve the greatest possible orgasms.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">As you wish.</div><!-- dialog -->
</div><!-- her -->
<div class="action"><?php echo $wife ?> performs a striptease for <?php echo $husband ?>.</div><!-- action -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Very good, lieutenant. I always knew you were the best. In fact, I think you deserve a promotion. How would you like to be my personal secretary?</div><!-- dialog -->
</div><!-- him -->
</div><!-- section -->
<div class="section">
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Oh, captain, I'll do anything to get more of you.</div><!-- dialog -->
</div><!-- her -->
<div class="action"><?php echo $husband ?> hands <?php echo $wife ?> a container of lubrication.</div><!-- action -->
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Then quickly, rub my penis. We have no time to waste.</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">With pleasure.</div><!-- dialog -->
</div><!-- her -->
<div class="action">They caress each other's genitals until both are ready for sex.</div><!-- action -->
</div><!-- section -->
<div class="section">
<div class="him">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Are you ready to experience the final frontier?</div><!-- dialog -->
</div><!-- him -->
<div class="her">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Take me, captain!</div><!-- dialog -->
</div><!-- her -->
<div class="action">They have sex.</div><!-- action -->
</div><!-- section -->
</article>
<button <?php if (current_user_cannot("access_s2member_level1")) echo "disabled" ?> id='nextLines'>Next Lines</button>
<script>
	id = 1;
	reveal = 2;
	jQuery(".section").each(function() {
		jQuery(this).attr("id", id).hide();
		id++;
	});
	jQuery(".section#" + 1).show();
	jQuery("#nextLines").click(function() {
		jQuery(".section").hide();
		jQuery("#" + reveal).show();
		reveal++;
		if (reveal == id) {
			jQuery(this).hide();
		}
	}); 
</script>