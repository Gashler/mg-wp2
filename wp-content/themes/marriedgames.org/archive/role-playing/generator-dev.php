<?php
$title = "Role-playing Generator";
include ("../header/header.php");
?>
<h1><?php echo $title ?></h1>
<p>Using this widget, bring your fantasies to life for a sizzling roleplay. If possible, find costumes and lingerie to match your characters. If you don't like what's generated, you can customize your characters by clicking on any of their traits. You may want to try creating characters for each other.</p>
<?php
function buildSelect($table, $gender) {
	$sql = "SELECT name FROM $table WHERE gender = '$gender' OR gender = 'either' ORDER BY name ASC";
	$result = $con->query($sql);
	$count = $result->num_rows;
	$rand = rand(1, $count);
	$x = 1;
	echo "<select>";
	while ($row = $result->fetch_assoc()) {
		echo "<option";
		if ($x == $rand) {
			echo " selected";
		}
		echo ">" . $row['name'] . "</option>";
		$x++;
	}
	echo "</select>";
}
function buildSelectNeutral($table, $gender) {
	$sql = "SELECT name FROM $table ORDER BY name ASC";
	$result = $con->query($sql);
	$count = $result->num_rows;
	$rand = rand(1, $count);
	$x = 1;
	echo "<select>";
	while ($row = $result->fetch_assoc()) {
		echo "<option";
		if ($x == $rand) {
			echo " selected";
		}
		echo ">" . $row['name'] . "</option>";
		$x++;
	}
	echo "</select>";
}
?>
<form autocomplete="off">
<table>
	<caption>Setting</caption>
	<thead>
		<tr>
			<th>Location</th>
			<th>Story Type</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='location'>
				<?php echo buildSelectNeutral("location"); ?>
			</td>
			<td class='storyType'>
				<?php echo buildSelectNeutral("storyType"); ?>
			</td>
		</tr>
	</tbody>
</table>
<table class="male">
	<caption>Him</caption>
	<thead>
		<tr>
			<th>Name</th>
			<th>Occupation</th>
			<th>Personality</th>
			<!--<th>Desire</th>-->
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='name'>
				<?php echo buildSelect("name", "male"); ?>
			</td>
			<td class='occupation'>
				<?php echo buildSelect("occupation", "male"); ?>
			</td>
			<td class='personality'>
				<?php echo buildSelect("personality", "male"); ?>
			</td>
		</tr>
	</tbody>
</table>
<table class="female">
	<caption>Her</caption>
	<thead>
		<tr>
			<th>Name</th>
			<th>Occupation</th>
			<th>Personality</th>
			<!--<th>Desire</th>-->
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='name'>
				<?php echo buildSelect("name", "female"); ?>
			</td>
			<td class='occupation'>
				<?php echo buildSelect("occupation", "female"); ?>
			</td>
			<td class='personality'>
				<?php echo buildSelect("personality", "female"); ?>
			</td>
		</tr>
	</tbody>
</table>
<table>
	<caption>First Line</caption>
	<thead>
		<tr>
			<th>Who Says It</th>
			<th>Line</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class='whoSaysIt'>
				<?php
					$rand = rand(0, 1);
					if ($rand == 0) {
						$gender1 = "He";
						$gender2 = "She";
					}
					else {
						$gender1 = "She";
						$gender2 = "He";
					}
				?>
				<select>
					<option><?php echo $gender1 ?></option>
					<option><?php echo $gender2 ?></option>
				</select>
			</td>
			<td class='firstLine'>
				<?php echo buildSelectNeutral("firstLine"); ?>
			</td>
		</tr>
	</tbody>
</table>
</form>
<input id="refresh" type="submit" value="Regenerate" class="button" />
<script>
	// regenerate

	jQuery("#refresh").click(function() {
		location.reload();
	});

</script>
<?php
include ("../footer/footer.php");
?>