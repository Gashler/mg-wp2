<?php
	include("../../header/header.php");
	$title = "The Convict and the Inspector | Sexy Role-play Outline";
?>
<h1>The Convict and the Inspector</h1>
<article>
	<h2>Description</h2>
	<p>A swashbuckling adventure with sex, lies, and true love, perfect for women who enjoy both a dashing hero and commanding villain.</p>
	<h2>Cast</h2>
	<ul>
		<li>He - (1) Inspector Jacque, the chief of police (2) Val Julio, an escaped convict</li>
		<li>She - Angelique, Val Julio's lover</li>
	</ul>
	<h2>Preperation</h2>
	<ul>
		<li>She puts on something sexy.</li>
		<li>He prepares two different outfits, one for Inspector Jacque, one for Val Julio. For example, Inspecto Jacque could wear a vest; Val Julio could be bare-chested.</li>
	</ul>
	<h2>Outline</h2>
	<ul>
		<li>There's a gentle knock at the door. It's Val Julio. He and Angelique receive each other with warm kisses.</li>
		<li>Val Julio tells Angelique that he can't stay because he's being hunted by Inspector Jacque. He makes her promise that if Inspector Jacque comes looking for him that she won't tell him anything.</li>
		<li>Angelique promises to leave a candle in the window when it's safe for Val Julio to return. Then, with a goodbye kiss, the two sadly part.</li>
		<li>There's a harsh knock at the door. It's Inspector Jacque. Inspector Jacque tells Angelique that Val Julio, the escaped convict, was seen around this house, and he asks her if she knows anything. Though Angelique pleads innocent, the inspector forcefully searches her room ... and her body ... for clues.</li>
		<li>To quell his suspicions and save her lover, a desperate Angelique begins to flirt with the Inspector.</li>
		<li>The inspector is less than convinced, though seduced by Angelicque, he's willing to play along, perhaps in hopes of getting her to talk.</li>
		<li>Angelique insists that the inspector leaves with an excuse such as, "Jacque, I think I hear your men at the door ..." and Jacque reluctantly complies, saying that he's not finished with her.</li>
		<li>There's a gentle knock at the door. It's Val Julio, saying, "I saw your candle. Is it safe?"</li>
		<li>The story cycles back to the beginning. Val Julio visits the girl he loves for a forbidden kiss, then fleas as danger approaches. Inspector Jacque continues to barge in with a mad determination to catch Val Julio and a burning desire for Angelique. Angelique continues to both eagerly wait for the man she loves and seduce the man she hates.</li>
		<li>In the end, Angelique decides whether to have intercourse with the dashing hero or the commanding villain.</li>
	</ul>
</article>
<?php include("../../footer/footer.php") ?>
