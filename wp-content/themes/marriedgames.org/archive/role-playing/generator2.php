<?php
	
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	$con = mysqli_connect("localhost","root","asdf","mg");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

function buildSelect($div, $gender) {
	$sql = "SELECT name FROM $div WHERE gender = '$gender' OR gender = 'either' ORDER BY name ASC");
	$count = $result->num_rows;
	$rand = rand(1, $count);
	$x = 1;
	echo "<select>";
	while ($row = $result->fetch_assoc()) {
		echo "<option";
		if ($x == $rand) {
			echo " selected";
		}
		echo ">" . $row['name'] . "</option>";
		$x++;
	}
	echo "</select>";
}
function buildSelectNeutral($div) {
	$sql = "SELECT name FROM $div ORDER BY name ASC");
	$count = $result->num_rows;
	$rand = rand(1, $count);
	$x = 1;
	echo "<select>";
	while ($row = $result->fetch_assoc()) {
		echo "<option";
		if ($x == $rand) {
			echo " selected";
		}
		echo ">" . $row['name'] . "</option>";
		$x++;
	}
	echo "</select>";
}
mysqli_close($con);
?>
<form autocomplete="off">
<div>
	<h2>Setting</h2>
	<div class="generatedContent">

			<div class='location'>
				<label>Location</label>
				<?php echo buildSelectNeutral("location"); ?>
			</div>
			<div class='storyType'>
				<label>Story Type</label>
				<?php echo buildSelectNeutral("storyType"); ?>
			</div>

	</div><!-- generatedContent -->
</div>
<div class="male">
	<h2><?php echo $husband ?>'s Character</h2>
	<div class="generatedContent">
		<div class='name'>
			<label>Name</label>
			<?php echo buildSelect("name", "male"); ?>
		</div>
		<div class='occupation'>
			<label>Occupation</label>
			<?php echo buildSelect("occupation", "male"); ?>
		</div>
		<div class='personality'>
			<label>Personality</label>
			<?php echo buildSelectNeutral("personality"); ?>
		</div>
	</div><!-- generatedContent -->
</div>
<div class="female">
	<h2><?php echo $wife ?>'s Character</h2>
	<div class="generatedContent">
		<div class='name'>
			<label>Name</label>
			<?php echo buildSelect("name", "female"); ?>
		</div>
		<div class='occupation'>
		<label>Occupation</label>
			<?php echo buildSelect("occupation", "female"); ?>
		</div>
		<div class='personality'>
		<label>Personality</label>
			<?php echo buildSelectNeutral("personality"); ?>
		</div>
	</div><!-- generatedContent -->
</div>
<div>
	<h2>First Line</h2>
	<div class="generatedContent">
		<div class='whoSaysIt'>
			<label>Who Says It</label>
			<?php
				$rand = rand(0, 1);
				if ($rand == 0) {
					$gender1 = $husband;
					$gender2 = $wife;
				}
				else {
					$gender1 = $wife;
					$gender2 = $husband;
				}
			?>
			<select>
				<option><?php echo $gender1 ?></option>
				<option><?php echo $gender2 ?></option>
			</select>
		</div>
		<div class='firstLine'>
			<label>Line</label>
			<?php echo buildSelectNeutral("firstLine"); ?>
		</div>
	</div><!-- generatedContent -->
</div>
</form>
<button id="refresh">Regenerate</button>
<script>
	// regenerate

	jQuery("#refresh").click(function() {
		location.reload();
	});

</script>