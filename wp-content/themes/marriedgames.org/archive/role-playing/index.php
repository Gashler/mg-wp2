<?php
$title = "Sexy Role-playing";
include ("../header/header.php");
?>
<h1><?php echo $title ?></h1>
<ul class="featuredList">
	<li>
		<a href="generator.php">
			<h2>Role-playing generator</h2>
			Instantly come up with characters, names, settings, and more
		</a>
	</li>
	<li>
		<a href="outlines/index.php">
			<h2>Outlines</h2>
			Get quick ideas for breaking the old routine and making it feel like the first time every time
		</a>
	</li>
	<li>
		<a href="scripts/index.php">
			<h2>Scripts</h2>
			The ultimate no-stress sex: just follow the script
		</a>
	</li>
</ul>
<div style="clear:both"></div>
<?php
include ("../footer/footer.php");
?>