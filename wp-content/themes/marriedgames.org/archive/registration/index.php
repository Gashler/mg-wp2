<?php
	$title = "Register";
	include("../header/header.php");
?>
<h1>Register</h1>
<p>Registration is free and easy, allowing you to personalize, keep logs,<br /> save games, and unlock the full power of MarriedGames.org.</p>
<form action="<?php echo $siteURL ?>registration/register.php" method="post">
	<label for="newUsername">Username:</label>
	<input type="text" id="newUsername" name="username" />
	<label for="newPassword">Password:</label>
	<input type="password" id="newPassword" name="password" />
	<label for="newPassword">Email:</label>
	<input type="text" id="newEmail" name="email" />
	<label for="newFirstName">First Name:</label>
	<input type="text" id="newFirstName" name="firstName" />
	<label for="newGender">Gender:</label>
	<select id="newGender" name="gender">
		<option value="male">Male</option>
		<option value="female">Female</option>
	</select>
	<label for="newSpouseFirsttName">Spouse's First Name:</label>
	<input type="text" id="newSpouseFirstName" name="spouseFirstName" />
	<label for="newSpouseGender">Spouse's Gender:</label>
	<select id="newSpouseGender" name="spouseGender">
		<option value="female">Female</option>
		<option value="male">Male</option>
	</select>
	<p style="line-height:.7em; margin-bottom:1.5em;"><input type="checkbox" checked id="newSubscription" name="optIn" style="display:inline;" /> I would like to receive occasional updates with<br /> new ideas and features for improving my sex life:</p>
	<input type="submit" class="button" id="submitNewUser" value="Register" />
</form>
<?php
	include("../footer/footer.php");
?>