<script>

	// submit subscription

	jQuery("#submit").click(function() {
		var email = jQuery("#email").val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
			alert("Not a valid e-mail address");
		} else {
			jQuery("div.submitFields").slideUp();
			jQuery("div#result").html("Thank you for subscribing. Please check your email to complete the process.").slideDown();
			$.post("<?php echo $siteURL ?>registration/subscribe.php", {
				email : email
			}, function(result) {
				
			});
		}
	});

	// add text to email input

	text = "Email address"
	jQuery("#email").val(text).css("color", "gray").click(function() {
		if (jQuery(this).val() == text) {
			jQuery(this).val("").css("color", "black");
		}
	});
	jQuery("#email").blur(function() {
		if (jQuery(this).val() == "") {
			jQuery(this).val(text).css("color", "gray");
		}
	});

	// highlight current page
	
	path = window.location.pathname;
	jQuery("#mainNav a[href = '" + path + "']").addClass("currentPage");
	if (path == "/") {
		jQuery("#mainNav a[href = '/index.php']").parent().addClass("currentPage");
	}
	//jQuery("#logo").css("font-size", )
	
	// dropdown menu
	
	jQuery("#mainNav li").hover(function() {
		jQuery(" li", this).slideDown();
	}, function() {
		jQuery(" li", this).slideUp();
	});

</script>