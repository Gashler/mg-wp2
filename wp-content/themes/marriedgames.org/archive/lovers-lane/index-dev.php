<?php $title = "Lovers Lane";
include ("../header/header.php");
?>
<h1 style="margin-bottom:0;"><?php echo $title ?></h1>
<h2 style="margin-top:0; font-size:12pt;">The Online Sexy Board Game ( Version .5.2 )</h2>
<p>Get ready for a stimulating adventure from foreplay to intercourse in this free and exciting game designed to make lovemaking easier and more fun than ever. The more properties you buy and romantic locations you build, the better chance you'll have of getting your lover to take of their clothes, perform sexual favors, and initiate sex.</p>
<table class="male" style="float: left;">
	<tr>
		<th style="color:rgb(0,128,255);">His Money</th>
		<th style="color:rgb(255,64,64);">Her Money</th>
	</tr>
	<tr>
		<td style="background:rgb(0,64,128);">$<span class="money He">1200</span></td>
		<td style="background:rgb(128,32,32);">$<span class="money She">1200</span></td>
	</tr>
</table>
<table id="board" cellspacing="1">
	<tr class="row" id="r1">
		<td class="spaceContainer go">
			<div  class="space go current" id="1">
				<div class="label">Go</div>
				<div class="player He">&#9794;</div>
				<div class="player She">&#9792;</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="2" cost="13">
				<div class="label">Bawdy Boulevard</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="3" cost="11">
				<div class="label">Horny Hollows</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="4" cost="7">
				<div class="label">Lovers Lane</div>
			</div>
		</td>
		<td class="spaceContainer jackpot">
			<div class="space jackpot" id="5" cost="7">
				<div class="label">Erotic Environment</div>
			</div>
		</td>
	</tr><!-- r1 -->
	<tr class="row" id="r2">
		<td class="spaceContainer">
			<div class="space" id="16" cost="12">
				<div class="label">Sexy Street</div>
			</div>
		</td>
		<td colspan="3" rowspan="3" id="centerContentContainer">
			<div id="centerContent">
				<table style="width:100%; height:100%; margin-top:0">
					<td style="text-align:center; vertical-align:middle; background:none;">
						<div id="text"></div>
						<button id="roll">Roll</button>
					</td>
				</table>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="6" cost="8">
				<div class="label">Arousal Avenue</div>
			</div>
		</td>
	</tr><!-- r2 -->
	<tr class="row" id="r3">
		<td class="spaceContainer">
			<div class="space" id="15" cost="2">
				<div class="label">Luscious Lounge</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="7" cost="3">
				<div class="label">Amorous Alley</div>
			</div>
		</td>
	</tr><!-- r3 -->
	<tr class="row" id="r4">
		<td class="spaceContainer">
			<div class="space" id="14" cost="9">
				<div class="label">Lustful Lane</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="8" cost="6">
				<div class="label">Flirtatious Flats</div>
			</div>				
		</td>
	</tr><!-- r5 -->
	<tr class="row" id="r5">
		<td class="spaceContainer bankrupt">
			<div class="space bankrupt" id="13" cost="3">
				<div class="label">Voluptuous Valley</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="12" cost="10">
				<div class="label">Provocative Parkway</div>
			</div>			
		</td>
		<td class="spaceContainer">
			<div class="space" id="11" cost="4">
				<div class="label">Seductive Street</div>
			</div>
		</td>
		<td class="spaceContainer">
			<div class="space" id="10" cost="1">
				<div class="label">Romantic Road</div>
			</div>
		</td>
		<td class="spaceContainer jail">
			<div class="space jail" id="9" cost="5">
				<div class="label">Passion Place</div>
			</div>
		</td>
	</tr><!-- r5 -->
</table><!-- board -->
<script>

	// add prices to spaces

	jQuery(".space").each(function() {
		if (!(jQuery(this).hasClass("go"))) {
			cost = jQuery(this).attr("cost") * 100;
			jQuery(this).append("<div class='cost'>$" + cost + "</div>");
		}
	});
	
	// size board

	function sizeBoard() {
		//mainContentWidth = jQuery(window).width() - jQuery("#mainNav").width();
		//jQuery("#mainContent").css("width", mainContentWidth + "px");
		height = jQuery(".space").css("width");
		jQuery(".space").each(function() {
			jQuery(this).css("height", height);
		});
		jQuery(".cost").each(function() {
			jQuery(this).css("width", height);
		});
		height = jQuery("#centerContentContainer").height();
		jQuery("#centerContent").css({
			"height": height,
		});
	}

	sizeBoard();
	jQuery(window).resize(function() {
		sizeBoard();
	});

	// determine first turn

	var turn;
	function alternateTurn(gender) {
		if (gender == "She") {
			turn = "He";
			turnPossesive = "His";
			oppositeTurn = "She";
		}
		else {
			turn = "She";
			turnPossesive = "Her";
			oppositeTurn = "He";
		}
	}
	rand = Math.round(Math.random() * 1);
	if (rand == 0) alternateTurn("He");
	else alternateTurn("She");
	firstTurn = true;
	jQuery("#text").prepend(turn + " rolls first.");
	function turnNotice() {
		jQuery(".turnNotice").remove();
		jQuery("#centerContent").removeClass(oppositeTurn).addClass(turn).prepend("<div class='turnNotice'>" + turnPossesive + " turn</div>");
	}
	turnNotice();

	// roll die

	jQuery("#roll").click(function() {
		
		// alternate turn
		
		if (firstTurn == false) {
			alternateTurn(turn);
		}
		jQuery("#centerContent").removeClass(oppositeTurn).addClass(turn);
		turnNotice();
		jQuery("#text").html("");
		jQuery("#roll").hide();
		var notice = null;
		jQuery(".spaceContainer.current").removeClass("current");
		jQuery(".turn").removeClass("turn");
		jQuery(".player." + turn).addClass("turn");
		space = jQuery(".turn").parents(".space").attr("id");
		space = parseInt(space);
		roll = Math.round(Math.random() * 5);
		var x = 0;
		var interval = setInterval(function() {
			player = jQuery(".player." + turn);
			jQuery(".player." + turn).remove();
			space ++;
			if (space <= 16) // last space
				jQuery("#" + space).append(player);
			else {
				
				// collect money for passing "Go"
				
				space = 1; // reset count to beginning space
				jQuery("#" + space).append(player);
				money = jQuery(".money." + turn).html();
				money = parseInt(money);
				jQuery(".spaceContainer." + turn).each(function() {
					cost = jQuery(" .space", this).attr("cost");
					cost = parseInt(cost);
					cost /= 4;
					cost *= 100;
					money += cost;
				});
				money += 200;
				jQuery(".money." + turn).html(money);
				notice = "Collected $" + (money);
			}
			x ++;
		    if (x == roll + 1) {
		    	firstTurn = false;
		        clearInterval(interval);
				jQuery("#" + space).parent().addClass("current");
		    	title = jQuery("#" + space + " .label").html();
		    	cost = jQuery("#" + space).attr("cost");
		    	cost *= 100;
				money = jQuery(".money." + turn).html();
		    	if (jQuery("#" + space).attr("id") !== "1") { // if not on "Go"
					jQuery("#text").html("<p id='notice' style='display:none;'>" + notice + "</p><button class='small purchaseProperty'>Purchase Property</button><div id='info'><div id='cost'>Cost: $" + cost + "</div></div>");
				}
				if (notice !== null) jQuery("#notice").show();
				
				// build options
				
				function showBuildOptions() {
					
					// assign prices to properties

					cost = jQuery("#" + space).attr("cost");
					cost = parseInt(cost) * 100;
					club = cost / 5;
					club = parseInt(club);
					spa = cost / .75;
					spa = parseInt(spa);
					theater = cost;
					theater = parseInt(theater);
					hotel = cost * 1.25;
					hotel = parseInt(hotel);
					
					jQuery("#info").html("<p>You own this property.</p><ul class='buttonList'><li><button class='build' type='club' cost='" + club + "'>Build Club</button> $" + club + "</li><li><button class='build' type='spa' cost='" + spa + "'>Build Spa</button> $" + spa + "</li><li><button class='build' type='theater' cost='" + theater + "'>Build Theater</button> $" + theater + "</li><li><button class='build' type='hotel' cost='" + hotel + "'>Build Hotel</button> $" + hotel + "</li></ul>");
					
					// hide build options out of price range
					
					jQuery(".build").each(function() {
						cost = jQuery(this).attr("cost");
						cost = parseInt(cost);
						if (money < cost) {
							jQuery(this).parent().hide();
						}
					});
					
					// build property
					
					jQuery(".build").click(function() {
						cost = jQuery(this).attr("cost");
						type = jQuery(this).attr("type");
						if (money >= cost) {
							money -= cost;
							jQuery(".money." + turn).html(money);
							jQuery("#info").html("You now own a " + type + " on this property.");
							jQuery("#" + space + " .label").after("<div class='property' type='" + type + "' cost='" + cost + "'>" + type + "</div>");
						}						
					});
					
				}
				if (jQuery("#" + space).parent().hasClass(turn) || jQuery("#" + space + " .property").length > 0 || jQuery("#" + space).parent().hasClass(oppositeTurn)) {
					jQuery(".purchaseProperty, #cost").hide();
					jQuery("#cost").hide();
				}
				else {
					jQuery(".purchaseProperty").show();
				}
				if (money < cost) {
					jQuery(".purchaseProperty, #cost").hide();
				}
				
				// show build options if player owns an empty space
				
				if (jQuery("#" + space).hasClass(turn) && jQuery("#" + space + " .property") == 0) {
					showBuildOptions();
				}
				jQuery("#roll").show().html("Next Turn");
				money = parseInt(money);
				
				// purchase property
				
				jQuery(".purchaseProperty").click(function() {
					jQuery(this).remove();
					jQuery("#notice").hide();
					money = jQuery(".money." + turn).html();
					money = parseInt(money);
					money -= cost;
					jQuery(".money." + turn).html(money);
					jQuery("#" + space).addClass(turn).parent().addClass(turn);
					showBuildOptions();
				});
				
				// land on opponent's property
				
				if (jQuery("#" + space + "." + oppositeTurn + " .property").length > 0) {
					jQuery("#roll").hide();
					type = jQuery("#" + space + " .property").attr("type");
					cost = jQuery("#" + space + " .property").attr("cost");
					cost = parseInt(cost);
					cost /= 4;
					var action;
					if (money > cost) {
					jQuery("#text").load("actions.php", {
						recipient: oppositeTurn,
						category: type,
						cost: cost
					});
					}
					else {
						jQuery("#info").html("<p>You must " + action + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li></ul>");
					}
					jQuery("#perform").click(function() {
						jQuery("#roll").hide();
						t = 61;
						jQuery("#text").html("<p>You must " + action + " until the timer runs out.</p><p class='countdown'></p>");
						var interval = setInterval(function() {
							jQuery("#roll").hide();
							t --;
							jQuery("#text .countdown").html(t);
						    if (t == 0) {
						        clearInterval(interval);
						        jQuery("#text").html("<p>Time's up.</p><audio><source src='ding.ogg' type='audio/ogg'></audio>");
						        jQuery("#roll").show();
						    }
						}, 1000);
						jQuery("#roll").show();
					});
					jQuery("#pay").click(function() {
						money -= cost;
						jQuery(".money." + turn).html(money);
						opponentMoney = jQuery(".money." + oppositeTurn).html();
						opponentMoney = parseInt(opponentMoney);
						opponentMoney += cost;
						jQuery(".money." + oppositeTurn).html(opponentMoney);
						jQuery("#text").html("");
						jQuery("#roll").show();
					});
				}
			}
		}, 500);
	});

</script>
<?php
include ("../footer/footer.php");
?>