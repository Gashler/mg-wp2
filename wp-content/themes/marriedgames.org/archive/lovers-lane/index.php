<script>

	// Get husband and wife names
	
	<?php
		$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
		$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
		if ($husband == null) $husband = "Zack";
		if ($wife == null) $wife = "Vanessa";
	?>
	var husband = "<?php echo $husband; ?>";
	var wife = "<?php echo $wife; ?>";
	var userLevel = <?php echo S2MEMBER_CURRENT_USER_ACCESS_LEVEL ?>;

</script>
<?php

	// load saved game

	$con=mysqli_connect("localhost","root","asdf","mg");
	// Check connection
	if (mysqli_connect_errno())
	  {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	  }
	
	$id = S2MEMBER_CURRENT_USER_ID;
	$sql = "SELECT * FROM loversLane WHERE id=$id";
	$result = $con->query($sql);
	
		while($row = $result->fetch_assoc())
		  {
			  $game = $row['game'];
		  }
	
		  $game = base64_decode($game);
	  
?>
<div id="game-container" style="display:table-cell; position:relative;">
	<div id="game">
	<?php
		if ($game !== "") {  $game;
		  echo 'Reset the game to play';
		} else {
	?>
		<div id="firstTurn" style="display:none;">true</div>
		<table class="male" style="float:left;">
			<tbody style="box-shadow:none !important;">
				<tr>
					<th style="color:rgb(0,128,255); background:white !important; font-weight:bold;"><?php echo $husband ?>'s Money</th>
					<th style="color:rgb(255,64,64); background:white !important; font-weight:bold;"><?php echo $wife ?>'s Money</th>
				</tr>
				<tr>
					<td style="background:rgb(0,128,255); color:white; font-weight:bold;">$<span class="money He">2400</span></td>
					<td style="background:rgb(255,64,64); color:white; font-weight:bold;">$<span class="money She">2400</span></td>
				</tr>
			</tbody>
		</table>
		<!--
		<?php if (current_user_cannot("access_s2member_level1")) { ?>
			<div style="float:right; text-align:center; top:2%; background:rgb(225,225,225); border-radius:2px; padding:4% 5%; box-shadow:2px 2px 4px gray;">
				<a href="http://freelovegames.org/register" target="_blank" >Premium Members:<br /></a>
				<strong style="color:gray;">Bonus Cards</strong>
			</div>
		<?php } ?>
		-->
		<div style="clear:both;"></div>
		<table id="board" cellspacing="1" style="box-shadow:none; max-width:700px;">
			<tbody style="box-shadow:none;">
				<tr class="row" id="r1">
					<td class="spaceContainer go" style="background-image:url('//wp-content/themes/marriedgames.org/images/icons/icon-go.png') !important; background-repeat:no-repeat !important; background-position:center center !important; background-size:80%;">
						<div  class="space go current" id="1" data-category="club">
							<div class="label special" style="font-weight:bold;">Go</div><br />
							<div class="player He">&#9794;</div>
							<div class="player She">&#9792;</div>
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="2" data-cost="2" data-category="club">
							<div class="label">Bawdy Boulevard</div>
							<div class="cost">$200</div>
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="3" data-cost="2" data-category="club">
							<div class="label">Horny Hollows</div>
							<div class="cost">$200</div>
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="4" data-cost="3" data-category="club" data-category="club">
							<div class="label">Lovers Lane</div>
							<div class="cost">$300</div>
						</div>
					</td>
					<td class="spaceContainer strip" style="background-image:url('//wp-content/themes/marriedgames.org/images/icons/icon-strip.png') !important; background-repeat:no-repeat !important; background-position:center center !important; background-size:80%;">
						<div class="space strip" id="5">
							<div class="label special">Strip</div><br />
							<!--<img src="//wp-content/themes/marriedgames.org/images/icons/icon-strip.png" style="position:absolute; width:50px; opacity:.5;" />-->
						</div>
					</td>
				</tr><!-- r1 -->
				<tr class="row">
					<td class="spaceContainer" rowspan="2">
						<div class="space" id="16" data-cost="9" data-category="hotel">
							<div class="label">Sexy Street</div>
							<div class="cost">$900</div>
						</div>
					</td>
					<td colspan="3" id="turn"></td>
					<td class="spaceContainer" rowspan="2">
						<div class="space" id="6" data-cost="4" data-category="spa">
							<div class="label">Arousal Avenue</div>
							<div class="cost">$400</div>
						</div>
					</td>
				</tr>
				<tr class="row" id="r2">
					<td colspan="3" rowspan="3" id="centerContentContainer">
						<div id="centerContent">
							<div id="load"></div>
							<div id="text"></div>
							<?php if (current_user_can("access_s2member_level1")) { ?>
								<div id="roll">Roll</div>
							<?php } else { ?>
								<button disabled style="padding-top:1.25em !important; padding-bottom:1.25em !important;">Roll</button>
							<?php } ?>
						</div>
					</td>
				</tr><!-- r2 -->
				<tr class="row" id="r3">
					<td class="spaceContainer">
						<div class="space" id="15" data-cost="8" data-category="hotel">
							<div class="label">Luscious Lounge</div>
							<div class="cost">$800</div>
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="7" data-cost="4" data-category="spa">
							<div class="label">Erotic Environment</div>
							<div class="cost">$400</div>
						</div>
					</td>
				</tr><!-- r3 -->
				<tr class="row" id="r4">
					<td class="spaceContainer">
						<div class="space" id="14" data-cost="8" data-category="hotel">
							<div class="label">Lustful Lane</div>
							<div class="cost">$800</div>
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="8" data-cost="5" data-category="spa">
							<div class="label">Flirtatious Flats</div>
							<div class="cost">$800</div>
						</div>				
					</td>
				</tr><!-- r5 -->
				<tr class="row" id="r5">
					<td class="spaceContainer bankrupt" style="background-image:url('//wp-content/themes/marriedgames.org/images/icons/icon-bankrupt.png') !important; background-repeat:no-repeat !important; background-position:center center !important; background-size:80%;">
						<div class="space bankrupt" id="13">
							<div class="label special" style="font-weight:bold;">Bankrupt</div><br />
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="12" data-cost="7" data-category="theater">
							<div class="label">Provocative Parkway</div>
							<div class="cost">$700</div>
						</div>			
					</td>
					<td class="spaceContainer">
						<div class="space" id="11" data-cost="6" data-category="theater">
							<div class="label">Voluptuous Valley</div>
							<div class="cost">$600</div>
						</div>
					</td>
					<td class="spaceContainer">
						<div class="space" id="10" data-cost="6" data-category="theater">
							<div class="label">Romantic Road</div>
							<div class="cost">$600</div>
						</div>
					</td>
					<td class="spaceContainer jail" style="background-image:url('//wp-content/themes/marriedgames.org/images/icons/icon-jail.png') !important; background-repeat:no-repeat !important; background-position:center center !important; background-size:80%;">
						<div class="space jail" id="9">
							<div class="label special" style="font-weight:bold;">Jail</div><br />
						</div>
					</td>
				</tr><!-- r5 -->
			</tbody>
		</table><!-- board -->
	<?php } ?>	
	</div><!-- game -->
	<div style="overflow:hidden;">
		<!--
		<?php if (current_user_cannot("access_s2member_level1")) { ?>
			<a href="http://freelovegames.org/register" target="_blank" ><button class="small" style="margin-top:1em; float:right;">Unlock Full Game</button></a>
		<?php } ?>
		-->
		<button class="small" id="save" style="margin-top:1em; float:right;">Save Game</button>
		<button class="small" id="reset" style="margin-top:1em; float:right;">Reset Game</button>
	</div>
</div><!-- game-container -->
<div id="game-sidebar" style="display:table-cell; position:relative; vertical-align:middle; padding-left:2%; width:16%;">
	<div class="meter">
		<strong>Heat</strong>
		<div>Orgasmic</div>
		<div>Passionate</div>
		<div>Erotic</div>
		<div>Sexy</div>
		<div class="active">Warming Up</div>
	</div>
	<!--
	<?php if (current_user_cannot("access_s2member_level1")) { ?>
		<a href="http://freelovegames.org/register" target="_blank" style="box-shadow:0 0 50px rgb(255,192,225); position:relative; z-index:5;">
			<img style="margin-top:1em;" src="/wp-content/themes/marriedgames.org/images/promo/sidebar-promo-1.jpg" />
		</a>
	<?php } ?>
	-->
</div>
<div id="result"></div>
<?php include "insertRow.php" ?>
<script src="/wp-content/themes/marriedgames.org/lovers-lane/js.js"></script>