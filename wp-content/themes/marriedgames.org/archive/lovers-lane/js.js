/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/jQuery(window).load(function() {
	
	var inJail = false;
	var personInJail;
	var turnsInJail = 3;
	var turnNumber = 0;
	//if (S2MEMBER_CURRENT_USER_IS_LOGGED_IN_AS_MEMBER) var restricted = false;
	//else var restricted = true;
	if(loverslane == 'yes'){ var restricted = false; } else { var restricted = true; } 
	
	// hack to fix Wordpress changes & into &amp;
	
	jQuery(window).load(function() {
		var x = jQuery("body").html();
		x = x.replace("&#038;", "&");
		jQuery("body").html(x);
	});
	
	// size board
	
	function sizeBoard() {
		//mainContentWidth = jQuery(window).width() - jQuery("#mainNav").width();
		//jQuery("#mainContent").css("width", mainContentWidth + "px");
		height = jQuery(".space").css("width");
		jQuery(".space").each(function() {
			jQuery(this).css("height", height);
		});
		jQuery(".cost").each(function() {
			jQuery(this).css("width", height);
		});
	}
	
		sizeBoard();
		jQuery(window).resize(function() {
			sizeBoard();
		});
	
	// determine first turn
	
	var turn;
	function alternateTurn(gender) {
		if (gender == "She") {
			turn = "He"
			turnName = husband;
			turnPossesive = husband + "'s";
			oppositeTurn = "She";
			oppositeTurnName = wife;
			oppositeTurnAccusative = "her";
		}
		else {
			turn = "She";
			turnName = wife;
			turnPossesive = wife + "'s";
			oppositeTurn = "He";
			oppositeTurnName = husband;
			oppositeTurnAccusative = "him";
		}
		turnNumber ++;
		
		// update meter
		
		if (turnNumber == 10) jQuery(".meter div:nth-child(5)").addClass("active");
		if (turnNumber == 20) jQuery(".meter div:nth-child(4)").addClass("active");
		if (turnNumber == 30) {
			if (userLevel > 0) jQuery(".meter div:nth-child(3)").addClass("active");
			else alert("You've reached the maximum level of heat as a free member. Upgrade to a premium account for sexier actions.");
		}
		if (turnNumber == 40 && userLevel > 0) jQuery(".meter div:nth-child(2)").addClass("active");
	}
	if (number=Math.floor(Math.random()*2) == "1") alternateTurn("He");
	else alternateTurn("She");
	firstTurn = jQuery("#firstTurn").html();
	function turnNotice() {
		jQuery(".turnNotice").remove();
		jQuery("#turn").html("<div class='turnNotice'>" + turnPossesive + " turn</div>").removeClass(oppositeTurn).addClass(turn);
		jQuery("#centerContentContainer").removeClass(oppositeTurn).addClass(turn);
	}
	if (firstTurn == "true") {
		jQuery("#text").html(turnName + " rolls first.");
		turnNotice();
	}
	else {
		// hack for fixing mysteriously disappearing roll button when loading saved game
		jQuery("#text").html("<h2>Resume Game</h2>");
		jQuery("#roll").show();
	}
	
	// intro phrases
	
	var intro = new Array();
	intro[0] = "In order to spice things up, ";
	intro[1] = "Because the luscious curves in front of your eyes fill you with the desire to do so, ";
	intro[2] = "To appease that warm feeling beneath your waist line, ";
	intro[3] = "Because you've been dying for an opportunity to do so, ";
	intro[4] = "Unfortuntaley, a sign informs you that you can't proceed unless you ";
	intro[5] = "With this perfect opportunity to get a little closer, ";
	intro[6] = "Because no one is watching, ";
	intro[7] = "To help fight back the urge to do something naughtier, ";
	intro[8] = "Because it's the tradition here, ";
	intro[9] = "Because you can't resist the urge to do so, ";
	intro[10] = "Because the mood is just right, ";
	intro[11] = "Your heart rate is picking up a bit. You'd better ";
	intro[12] = "Because you're not going to get a better opportunity, ";
	intro[13] = "You know you're feeling a burning desire to ";
	intro[14] = "Because the sweet perfume in the air is driving you crazy, ";
	intro[15] = "Because you're feeling drunk on love, ";
	intro[16] = "It's time to live your fantasies, so ";
	intro[17] = "With a slight moan, ";
	intro[18] = "Just for fun, ";
	
	// resize spaces
	
	function resizeSpaces() {
		jQuery("#16").parent().css("height", "auto");
		jQuery("#15").parent().css("height", "auto");
		jQuery("#14").parent().css("height", "auto");
		topCellHeight = jQuery("#16").parent().height();
		middleCellHeight = jQuery("#15").parent().height();
		newHeight = ((topCellHeight - middleCellHeight) / 3) + middleCellHeight;
		jQuery("#16").parent().css("height", newHeight + "px");
		jQuery("#15").parent().css("height", newHeight + "px");
		jQuery("#14").parent().css("height", newHeight + "px");
	}

	// roll die
	
	jQuery("#roll").click(function() {
			
		// alternate turn
		
		if (firstTurn == "false") {
			alternateTurn(turn);
		}
		jQuery("#centerContent").removeClass(oppositeTurn).addClass(turn);
		turnNotice();
		jQuery("#text, #load").html("");
		jQuery("#roll").hide();
		var notice = null;
		jQuery(".spaceContainer.current").removeClass("current");
		jQuery(".turn").removeClass("turn");
		jQuery(".player." + turn).addClass("turn");
		space = jQuery(".turn").parents(".space").attr("id");
		space = parseInt(space);
		
	function roll() {
			roll = Math.round(Math.random() * 5);
			var x = 0;
			var interval = setInterval(function() {
				player = jQuery(".player." + turn);
				jQuery(".player." + turn).remove();
				space ++;
				if(typeof(firstJail) === 'undefined' || firstJail == ' '){	
				  firstJail = ' ';
				 } 
				if (space <= 16) // last space
					jQuery("#" + space).append(player);
				else {
					
					// collect money for passing "Go"
					
					space = 1; // reset count to beginning space
					jQuery("#" + space).append(player);
					money = parseInt(jQuery(".money." + turn).html()) + 500;
					jQuery(".money." + turn).html(money);
					jQuery("#turn").html(turnName + " collected $500 for passing Go.");
				}
				x ++;
			    if (x == roll + 1) {
			    	
			    	firstTurn = "false";
			    	jQuery("#firstTurn").html("false");
			        clearInterval(interval);
					jQuery("#" + space).parent().addClass("current");
			    	title = jQuery("#" + space + " .label").html();
			    	cost = jQuery("#" + space).attr("data-cost");
			    	cost *= 100;
					money = jQuery(".money." + turn).html();
					/*
					// bonus card
					if (userLevel > 0 && (turnNumber !== 5 || turnNumber !== 10 || turnNumber !== 15 || turnNumber !== 20 || turnNumber !== 25)) {
						bonus = Math.round(Math.random() * 3);
						if (bonus == 0 && firstTurn == "false" && jQuery("#" + space + " .property").length == 0) {
							//jQuery("#text").hide();
							jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/popup.php", { husband:husband, wife:wife }, function(result) {
								jQuery("#x, #gray").on("click", function() {
									jQuery("#text").fadeIn();
								});
							});
						}
					}
					else {
						if (turnNumber == 5 || turnNumber == 10 || turnNumber == 15 || turnNumber == 20 || turnNumber == 25 || turnNumber == 30) {
							jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/promo.php", { turnNumber: turnNumber });
						}
					}
					*/
					function moveOn() {
						jQuery("#text").html("");
						
						 // if not on "Go", "Strip", "Jail", or "Bankrupt"
						
				    	if (jQuery("#" + space).attr("id") !== "1" && jQuery("#" + space).attr("id") !== "5" && jQuery("#" + space).attr("id") !== "9" && jQuery("#" + space).attr("id") !== "13") {
							jQuery("#text").html("<p id='notice' style='display:none;'>" + notice + "</p><button class='small purchaseProperty'>Purchase Property</button><div id='info'><div id='cost'>Cost: $" + cost + "</div></div>");
						}
						
						if (notice !== null) jQuery("#notice").show();
						else jQuery("#roll").show();
		
						// build options
						
						function showBuildOptions() {
							
								// assign prices to properties
			
								cost = jQuery("#" + space).attr("data-cost");
								cost = parseInt(cost) * 100;
								club = cost * .25;
								club = parseInt(club);
								spa = cost * .5;
								spa = parseInt(spa);
								theater = cost * .75;
								theater = parseInt(theater);
								hotel = cost;
								hotel = parseInt(hotel);
								
								jQuery("#info").load("/wp-content/themes/marriedgames.org/lovers-lane/buildOptions.php", {
									cost: cost,
									club: club,
									spa: spa,
									theater: theater,
									hotel: hotel,
									restricted: restricted
								}, function(result) {
									
									// hide build options out of price range
									
									jQuery(".build").each(function() {
										cost = jQuery(this).attr("data-cost");
										cost = parseInt(cost);
										if (money < cost) {
											jQuery(this).parent().hide();
										}
									});
									
									resizeSpaces();
									
									// build property
									
									jQuery(".build").click(function() {
										cost = jQuery(this).attr("data-cost");
										propertyType = jQuery(this).attr("data-property-type");
										if(firstJail == ' '){	
 										 firstJail = ' ';
										} 
										if (money >= cost) {
											money -= cost;
											jQuery(".money." + turn).html(money);
											jQuery("#info").html("You now own a " + propertyType + " on this property.");
											jQuery("#" + space + " .label").after("<div class='property' data-property-type='" + propertyType + "' data-cost='" + cost + "'>" + propertyType + "</div>");
											jQuery("#" + space + " .cost").remove();
										}
									});
								});
						}
						
						jQuery("#" + space).addClass("visited");
						
						if (jQuery("#" + space).parent().hasClass(turn) || jQuery("#" + space + " .property").length > 0 || jQuery("#" + space).parent().hasClass(oppositeTurn)) {
							jQuery(".purchaseProperty, #cost").hide();
							jQuery("#cost").hide();
						}
						else {
							jQuery(".purchaseProperty").show();
						}
						if (money < cost) {
							jQuery(".purchaseProperty, #cost").hide();
						}
						
						// show build options if player owns an empty space
						
						if (jQuery("#" + space).hasClass(turn) && !(jQuery("#" + space).find('.property').length > 0)) {
							showBuildOptions();
						}
						jQuery("#roll").show().html("Next Turn");
						if(firstJail == ' '){	
                			firstJail = ' ';
			            }	
            			money = parseInt(money);
						
						// purchase property
						
						jQuery(".purchaseProperty").click(function() {
							jQuery(this).remove();
							jQuery("#notice").hide();
							if(firstJail == ' '){	
							 firstJail = ' ';
							} 
							money = jQuery(".money." + turn).html();
							money = parseInt(money);
							money -= cost;
							jQuery(".money." + turn).html(money);
							jQuery("#" + space).addClass(turn).parent().addClass(turn);
							showBuildOptions();
						});
						
						// land on opponent's property
			
						if (jQuery("#" + space + "." + oppositeTurn + " .property").length > 0) {
							t = 60;
							jQuery("#roll").hide();
							category = jQuery("#" + space + " .property").attr("data-property-type");
							cost = jQuery("#" + space + " .property").attr("data-cost");
							cost = parseInt(cost);
							var action;
							jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/actions.php", {
							recipient: oppositeTurn,
							category: category,
							turn: turn,
							husband: husband,
							wife: wife
							}, function(result) {
								action = jQuery("#load").html();
								jQuery("#load").html("");
								if (money > cost) {
									jQuery("#text").html("<p>You landed on " + oppositeTurnName + "'s " + category + ". You must either " + action + ", or pay " + oppositeTurnName + " $" + cost + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li><li><button id='pay'>Pay Up</button></li></ul>");
								}
								else {
									jQuery("#text").html("<p>You landed on " + oppositeTurnName + "'s " + category + ", and unfortunately, you can't afford the fees. You must either " + action + " or accept defeat and initiate sex with " + oppositeTurnName + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li><li><button id='initiate-sex'>Initiate Sex</button></li></ul>");
								}
								
								// perform action
								
								jQuery("#perform").on("click", function() {
									jQuery("#text").html("<p>You must " + action + " until the timer runs out.</p><p class='countdown'>60</p>");
									var interval = setInterval(function() {
										jQuery("#roll").hide();
										t --;
										jQuery("#text .countdown").html(t);
									    if (t == 0) {
									        clearInterval(interval);
									        jQuery("#text").html("<p>Time's up.</p><audio autoplay><source src='/wp-content/themes/marriedgames.org/lovers-lane/audio/ding.ogg' type='audio/ogg'></audio>");
									        jQuery("#roll").show();
									    }
									}, 1000);
								});
								jQuery("#pay").on("click", function() {
								    if(firstJail == ' '){	
								       firstJail = ' ';
									}  
									money -= cost;
									jQuery(".money." + turn).html(money);
									opponentMoney = jQuery(".money." + oppositeTurn).html();
									opponentMoney = parseInt(opponentMoney);
									opponentMoney += cost;
									jQuery(".money." + oppositeTurn).html(opponentMoney);
									jQuery("#text").html("");
									jQuery("#roll").show();
								});
								
								// initiate sex
								
								jQuery("#initiate-sex").on("click", function() {
									jQuery("#text").html("<p><h2>" + oppositeTurnName + " Wins!</h2><p>Congratulations, " + oppositeTurnName + ", you've won yourself a sex slave. Lie back and enjoy yourself while " + turnName + " does all the work.</p>");
								});
		
							});
						}
					}
			    	
					// first time on new space
					
					if (space !== 1 && space !== 5 && space !== 9 && space !== 13) {
						if (jQuery("#" + space).hasClass("visited") == false) {
							x = Math.round(Math.random() * 18);
							jQuery("#text").html("<h2>Welcome to " + title + "</h2><p>" + intro[x] + "<span class='load'></span>.</p><button id='finished'>Finished</button>");
							category = jQuery("#" + space).attr("data-category");
							jQuery(".load").load("/wp-content/themes/marriedgames.org/lovers-lane/actions.php", {
								recipient: oppositeTurn,
								category: category,
								turn: turn,
								husband: husband,
								wife: wife
							});
							resizeSpaces();							
							jQuery("#" + space).addClass("visited");
							jQuery("#finished").click(function() {
								jQuery("#roll").show();
								moveOn();
							});
						}
						else {
							moveOn();
						}
					}
					
					// special spaces
					
					// strip
					
					else {
						
						// go
						
						if (space == 1) {
							jQuery("#roll").show();
						}
						
						// strip
						
						if (space == 5) {
							jQuery("#text").html("<h2>Time to Strip</h2><p>For " + oppositeTurnName +"'s viewing pleasure, you must strip off an article of clothing.</p><button id='finished'>Finished</button>");
							jQuery("#finished").click(function() {
								moveOn();
							});
						}
					
						// jail
					
						if (space == 9) {
							jQuery("#text").html("<h2>You're Trapped in Jail</h2><p>Uh-oh. Either you've got some time to serve, or " + oppositeTurnName + " will bail you out. Time will tell.</p>");
							jQuery("#roll").show();
							inJail = true;
							personInJail = turnName;
						}
		
						// bankrupt
		
						if (space == 13) {
							jQuery(".money." + turn).html("0");
							jQuery("#text").html("<h2>You're Bankrupt!</h2><p>Uh-oh, better make some more money! Who knows what " + oppositeTurnName + " will do to you in your desperation?</p><button id='finished'>Finished</button>");
							jQuery("#finished").click(function() {
								moveOn();
							});
						}
					}
				}
			}, 500);
		}
		
		// if someone's in jail
		
		if (inJail == true) {
			if (personInJail == oppositeTurnName && jQuery("#" + space + " .player." + turn) > 0 ) {
				jQuery("#text").html("<p>With swashbuckling finesse, you broke into to the jail to rescue your true love. But before you both escape, when will there be a more cozy opportunity for a makeout session?</p><button id='continue'>Continue</button>");
				jQuery("#continue").click(function() {
					jQuery("#text").html("");
					roll();
				});
			}
			else if (personInJail == oppositeTurnName) {
				if (turnsInJail > 1) {
				if(firstJail != 'yes'){
					if (money > 500) {
						jQuery("#text").html("<p>Your beloved " + oppositeTurnName + " is in jail. For the price of $500, do you wish to bail " + oppositeTurnAccusative + " out?</p><button id='yes'>Yes</button><button id='no'>No</button>");
					}
					else {
						jQuery("#text").html("<p>Your beloved " + oppositeTurnName + " is in jail. Unfortunately, you can't afford to bail " + oppositeTurnAccusative + " out.</p><button id='continue'>Continue</button>");
						jQuery("#continue").on("click", function() {
							jQuery("#text").html("");
							roll();
						});
					}
				} else {
				 jQuery("#text").html("<p>Your beloved " + oppositeTurnName + " is in jail. Unfortunately, you can't afford to bail " + oppositeTurnAccusative + " out.</p><button id='continue'>Continue</button>");
						jQuery("#continue").on("click", function() {
							jQuery("#text").html("");
							roll();
						});
				}
					jQuery("#yes").click(function() {
					   firstJail = 'yes';
						money -= 500;
						jQuery(".money." + turn).html(money);
						jQuery("#text").html(oppositeTurnName + " owes you big. " + oppositeTurn + "'s now free.<br><button id='continue'>Continue</button>");
						inJail = false;
						jQuery("#continue").on("click", function() {
							jQuery("#text").html("");
							roll();
						});
					});
					jQuery("#no").click(function() {
						jQuery("#text").html("It probably serves " + oppositeTurnAccusative + " right.<br><button id='continue'>Continue</button>");
						jQuery("#continue").click(function() {
							jQuery("#text").html("");
							roll();
						});
					});
				}
				else {
					roll();
				}
			}
			else {
					turnsInJail --;
					if (turnsInJail > 0) {
						if (turnsInJail > 1) turns = "turns";
						else turns = "turn";
						jQuery("#text").html("No thanks to " + oppositeTurnName + ", you're still trapped in jail for " + turnsInJail + " more " + turns + ".");
						jQuery("#roll").show();
					}
					else {
						inJail = false;
						jQuery("#text").html("<p>Finally you're free!</p><button id='continue'>Continue</button>");
						jQuery("#continue").click(function() {
							jQuery("#text").html("");
							roll();
						});
					}
			}
		}
		else roll();
		
	});
	
	// reset game
	
	function confirmReset() {
		var r=confirm("Are you sure you want to reset?")
		if (r==true) {
			var newGame = "";
			jQuery("#result").load("/wp-content/themes/marriedgames.org/lovers-lane/updateRow.php",{
				id: S2MEMBERCURRENT_USER_ID,
				game: newGame
			}, function() {
				location.reload();
			});
		}
	}
	
	// save game
			
	jQuery("#save").click(function() {
		if (userLevel > 0) {
			game = jQuery("#game").html();
			jQuery("#result").load("/wp-content/themes/marriedgames.org/lovers-lane/updateRow.php",{
				id: S2MEMBERCURRENT_USER_ID,
				game: game
			}, function(result) {
				alert("Game Saved");
			});
		}
		else {
			alert("This feature is reserved for premium members.");
		}
	});	
	
	jQuery("#reset").click(function() {
		confirmReset();
	});
});