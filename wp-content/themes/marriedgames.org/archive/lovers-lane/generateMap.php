<script>

	// generate map
	
	rows = <?php echo $rows ?>;
	columns = <?php echo $columns ?>;
	unitSize = <?php echo $unitSize ?>;
	firstSpace = true;
	var ids = new Array();
	var id;
	alert("test");
	function getID() {
		var unique = false;
		while (unique == false) {
			//id = Math.floor(Math.random()*(rows * columns));
			alert(id);
			if( $.inArray(id, ids) == -1 ) {
				unique == true;
			}
		}
	}
	
	for (r = 1; r <= rows; r++) {
		jQuery("#grid").append("<div class='row'>");
		for (c = 1; c <= columns; c++) {
			function generateTerrain() {
				rand = Math.floor((Math.random() * 4));
				if (rand == 0)
					terrain = "prairie";
				if (rand == 1)
					terrain = "grass";
				if (rand == 2)
					terrain = "water";
				if (rand == 3)
					terrain = "rock";
				addSpace(terrain);							
			}
			function addSpace(terrain) {
				id = getID();
				alert(id);
				jQuery("#grid .row:last").append("<div id='" + id + "' class='space " + terrain + "' terrain='" + terrain + "' row='" + r + "' column='" + c + "'></div>");
				if (terrain == "water" || terrain == "rock") {
					jQuery(".space:last").addClass("solid");
				}
			}
			if (firstSpace == true) {
				generateTerrain();
				firstSpace = false;
			}
			else {
				rand = Math.floor((Math.random() * 5));

				// base terrain type off of left space

				if (rand < 2 && jQuery(".row:last .space:last").not(":first")) {
					column = jQuery(".space:last").attr("column");
					leftSpace = column--;
					terrain = jQuery(".space[column=" + leftSpace + "]").attr("terrain");
					addSpace(terrain);
				}

				// base terrain type off of top space

				else if (rand > 2 && rand < 5 && jQuery(".row:last").not(":first")) {
					row = jQuery(".space:last").attr("row");
					topSpace = row--;
					terrain = jQuery(".space[column=" + topSpace + "]").attr("terrain");
					addSpace(terrain);
				}
				// generate a new terrain type

				else {
					generateTerrain();
				}
			}
		}
	}

</script>