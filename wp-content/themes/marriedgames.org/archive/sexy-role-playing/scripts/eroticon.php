<article class="roleplay">
<div class="section">
<div class="action">W enters.</div><!-- action -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Captain, you asked to meet with me?</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Yes, lieutenant. As you know, our ship is in great danger. The crew –</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">They've lost their minds. I know.</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">No, no, much more than their minds. They've lost their shame. Everywhere I look, they're making out with each other, tearing off each other's clothes, too busy in each other's beds to fulfill their duties.</div><!-- dialog -->
</div><!-- husband -->
</div><!-- section -->
<div class="section">
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">But why is this happening?</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">That's what I wished to speak with you about. It appears that our ship has been seized by an eroticon.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">A what?</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">An eroticon, one of the great mysteries of deep space. All we know is that these gaseous forces are extremely dangerous, deluding the reason of their prey by attacking beneath the waistline.</div><!-- dialog -->
</div><!-- section -->
<div class="section">
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">But what can we do?</div><!-- dialog -->­
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">You and I, lieutenant, are the only ones aboard the ship who haven't yet lost control. We must stay strong.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Whatever you ask, captain, I'll do.</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">I knew I could trust you, lieutenant.</div><!-- dialog -->
</div><!-- husband -->
</div><!-- section -->
<div class="section">
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">What are your orders?</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Take off your uniform.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">What!?</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">You promised to obey my orders.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<p>backing up</p>
<div class="dialog">Not you too!</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Your eyes are so lovely. Your voice … your hair … lieutenant, let me see the rest of you.</div><!-- dialog -->
</div><!-- husband -->
</div><!-- section -->
<div class="section">
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<p>struggling to get away</p>
<div class="dialog">Snap out of it, captain. You're not yourself.</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<p>seizing her</p>
<div class="dialog">Nonsense. I've never felt more alive.</div><!-- dialog -->
</div><!-- husband -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Can't you see what's happening? You're being controlled by the eroticon.</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">The eroticon is powerful, yes. I confess, ever since you walked into my quarters, I've felt a burning lust for you. Believe me, lieutenant, I tried to fight it, but then I realized … resistance is futile.
</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">No, Captain.</div><!-- dialog -->
</div><!-- wife -->
</div><!-- section -->
<div class="section">
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">You don't understand. As long as we fight the eroticon, we'll be driven mad by our desires. Tell me the truth, lieutenant. Do you not want me?</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Captain!</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Answer me!</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Yes! Passionately. But –</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Then there's only thing we can do. Give in.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">We can't be selfish. We must save the ship!</div><!-- dialog -->
</div><!-- section -->
<div class="section">
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">I'm trying to tell you. This is the only way to save the ship. We must release the sexual tension. Only then will be be immune the power of the eroticon. Only then will we regain our minds.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">I see. But do you really think that will work?</div><!-- dialog -->
</div><!-- wife -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">What choice do we have? Lieutenant, for the sake of our ship, I order you to take off your clothes.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Aye-aye, captain.</div><!-- dialog -->
</div><!-- wife -->
</div><!-- section -->
<div class="section">
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">And make it sexy. If we're going to relieve all the tension, we must achieve the greatest possible orgasms.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">As you wish.</div><!-- dialog -->
</div><!-- wife -->
<div class="action"><?php echo $wife ?> performs a striptease for <?php echo $husband ?>.</div><!-- action -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Very good, lieutenant. I always knew you were the best. In fact, I think you deserve a promotion. How would you like to be personal secretary to the captain?</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Oh, captain, I'll do anything to get more of you.</div><!-- dialog -->
</div><!-- wife -->
</div><!-- section -->
<div class="section">
<div class="action"><?php echo $husband ?> hands <?php echo $wife ?> a container of lubrication.</div><!-- action -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Then quickly, rub my penis. We have no time to waste.</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">With pleasure.</div><!-- dialog -->
</div><!-- wife -->
<div class="action">They caress each other's genitals until both are ready for sex.</div><!-- action -->
<div class="husband">
<div class="character"><?php echo $husband ?></div><!-- character -->
<div class="dialog">Are you ready to experience the final frontier?</div><!-- dialog -->
</div><!-- husband -->
<div class="wife">
<div class="character"><?php echo $wife ?></div><!-- character -->
<div class="dialog">Take me, captain!</div><!-- dialog -->
</div><!-- wife -->
<div class="action">They have sex.</div><!-- action -->
</article>
<script>
	id = 1;
	reveal = 2;
	jQuery(".section").each(function() {
		jQuery(this).attr("id", id).hide();
		id++;
	});
	jQuery(".section#" + 1).show();
	jQuery("#nextLines").click(function() {
		jQuery(".section").hide();
		jQuery("#" + reveal).show();
		reveal++;
		if (reveal == id) {
			jQuery(this).hide();
		}
	});
</script>