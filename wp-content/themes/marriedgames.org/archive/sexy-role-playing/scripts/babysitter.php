<h1><?php echo $title; ?></h1>
<article class="roleplay">
<div class='section'>
	<p class="action">Jimmy is home alone. There's a knock at the door. In walks a beautiful girl in a tank top and short skirt.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Hi, I'm Chelsie, your new babysitter. What's your name?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Jimmy, and I'm way too old for a babysitter. My mom and dad are crazy.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I know what you mean, Jimmy. If I didn't really need the money, I would have told them no. Well, that's not entirely true.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">What do you mean?</p><!-- dialog -->	
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Just between the two of us, I've been hoping your parents would call me for a long time.</p><!-- dialog -->	
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Why?</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Because you are the cutest boy in the neighborhood. My friends are totally jealous of me right now.</p><!-- dialog -->	
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Thanks, I guess ...</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Wow, I can't believe I just told you that. Now that we're off to an awkward start, let's see what's on the to-do list. Hmm ... did you have dinner yet?</p><!-- dialog -->	
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Yes.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Did you take a bath?</p><!-- dialog -->	
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">No.</p><!-- dialog -->	
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">All right then, hop to it.</p><!-- dialog -->	
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">I don't want to take a bath.</p><!-- dialog -->	
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">No? I love to take baths.</p><!-- dialog -->	
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Well I don't.</p><!-- dialog -->	
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="parenthetical">leaning in close</p><!-- parenthetical -->
	<p class="dialog">Come on, Jimmy. For me?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="parenthetical">beginning to be turned on</p><!-- parenthetical -->
	<p class="dialog">Okay.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Here, I'll help you take off your shirt.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">You're going to watch?</p><!-- dialog -->	
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">It says here on the list that I have to make <em>sure</em> you take a bath. Come on. There's a big boy. Now let's take off the pants.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">I really don't think you're supposed to watch.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I'm just doing my job. What's the matter, chicken?</p><!-- dialog -->
	</div>
	<p class="action">With her help, he nervously takes off his pants.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Was that so bad? Now we just need to get rid of the underwear.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">But I ...</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="parenthetical">leaning close</p><!-- parenthetical -->
	<p class="dialog">Jimmy, I really want to see what you've got under there. You don't want to disappoint me, do you?</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<p class="action">Jimmy takes off his underwear.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Wow, Jimmy, the girls were right about you. You <em>are</em> adorable. Now just lie down and relax.</p><!-- dialog -->
	</div>
	<p class="action">Still with reservations, Jimmy lies down, and Chelsie begins to run a washcloth all over his body.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Mmm, doesn't that feel good?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">A little, I guess.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Uh-oh, you've got something on your neck, and it's not coming off. Hold still. We're going to have to do this the old-fashioned way.</p><!-- dialog -->
	</div>
	<p class="action">She necks him.</p>
</div>
<div class='section'>
	<div class='her'>
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Oh, and you've also got something on your ear.</p><!-- dialog -->
	</div>
	<p class="action">She nibbles his ear.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Good as new. Now we only have one more spot, the most important of all.</p><!-- dialog -->
	</div>
	<p class="action">She begins to rub his penis with lubricant.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">It's always important to keep your penis clean, because you never know when you're going to need to use it. Mmm, you sure are hard. Someday you'll make some lucky girl really happy.</p><!-- dialog -->
	<p class="parenthetical">reflects</p><!-- parenthetical -->
	<p class="dialog">Did I just say all that? Oh my goodness, I can't believe how I'm acting tonight. You just have a way loosening me up. Here.</p><!-- dialog -->
	</div>
	<p class="action">She places his hand over his penis.</p>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I shouldn't be seeing that, and you should be doing this, not me. Just make sure you gent plenty of lubrication, because it's good for the skin.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Okay.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Well, the last thing on the list is to tuck you in for bed, but I'm not ready for that, are you?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">No.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Hmm, since I do have a captive audience, would you like a little entertainment?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Yes.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I need to practice my new cheer leading routine. But it's kind of naughty. Do you mind?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">I'll look away if it gets too naughty.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">All right. Tell me what I can improve on, okay?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Okay.</p><!-- dialog -->
	</div>
	<p class="action">She does an exotic dance, stripping down to her underwear.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">What do you think?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">That was awesome.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">You're sweet. Now I better tuck you in.</p><!-- dialog -->
	</div>
	<p class="action">She covers him with a blanket.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Good night, Jimmy.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Good night, Chelsie.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I don't know about you, but I can never sleep until I've gotten a good night kiss. Would you like one?</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Yes.</p><!-- dialog -->
	</div>
	<p class="action">They kiss.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Mmm, that felt nice. You know, I've really been hoping for a chance to practice my kissing, and with you being such a cute boy, would you mind if we do it a few more times?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">I think that would be okay.</p><!-- dialog -->
	</div>
	<p class="action">They make out.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Wow. I had no idea kissing could be so amazing. Could I ask one more thing?</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Go ahead.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I know this might sound crazy, but I've been studying sex in my health class, and I kind of want to see what it's like.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">That doesn't sound so crazy.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">I think it might turn me on to show you my body. Have you ever seen what's beneath a girl's panties?</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">No.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Would you like to?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Yes.</p><!-- dialog -->
	</div>
	<p class="action">She takes off her panties, unabashedly showing off her bum and vagina.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Mmm, Jimmy, I really like how you look at me. Can I show you my breasts?</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Yes.</p><!-- dialog -->
	</div>
	<p class="action">She takes off her bra.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Will you touch them?</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Yes.</p><!-- dialog -->
	</div>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">And now a little lower ... a little lower ... a little lower. Yeah, that's the spot. I think I need some lubricant too. Here, let's rub each other.</p><!-- dialog -->
	</div>
</div>
<div class='section'>
	<p class="action">They rub each other's genitals.</p>
	<div class="her">
	<p class="character">Chelsie</p><!-- character -->
	<p class="dialog">Oh, Jimmy ... let's go all the way.</p><!-- dialog -->
	</div>
	<div class="him">
	<p class="character">Jimmy</p><!-- character -->
	<p class="dialog">Okay.</p><!-- dialog -->
	</div>
	<p class="action">They have sex.</p>
</div>
</article><!-- roleplay -->
<button id='nextLines'>Next Lines</button>
<script>
	id = 1;
	reveal = 2;
	jQuery(".section").each(function() {
		jQuery(this).attr("id", id).hide();
		id++;
	});
	jQuery(".section#" + 1).show();
	jQuery("#nextLines").click(function() {
		jQuery(".section").hide();
		jQuery("#" + reveal).show();
		reveal++;
		if (reveal == id) {
			jQuery(this).hide();
		}
	});
</script>