/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/var inJail = false;
var personInJail;

// hack to fix Wordpress changes & into &amp;

jQuery(window).load(function() {
	var x = jQuery("body").html();
	x = x.replace("&#038;", "&");
	jQuery("body").html(x);
});

// size board

function sizeBoard() {
	//mainContentWidth = jQuery(window).width() - jQuery("#mainNav").width();
	//jQuery("#mainContent").css("width", mainContentWidth + "px");
	height = jQuery(".space").css("width");
	jQuery(".space").each(function() {
		jQuery(this).css("height", height);
	});
	jQuery(".cost").each(function() {
		jQuery(this).css("width", height);
	});
}

sizeBoard();
jQuery(window).resize(function() {
	sizeBoard();
});

// determine first turn

var turn;
function alternateTurn(gender) {
	if (gender == "She") {
		turn = "He"
		turnName = husband;
		turnPossesive = husband + "'s";
		oppositeTurn = "She";
		oppositeTurnName = wife;
		oppositeTurnAccusative = "her";
	}
	else {
		turn = "She";
		turnName = wife;
		turnPossesive = wife + "'s";
		oppositeTurn = "He";
		oppositeTurnName = husband;
		oppositeTurnAccusative = "him";
	}
}
if (number=Math.floor(Math.random()*2) == "1") alternateTurn("He");
else alternateTurn("She");
firstTurn = jQuery("#firstTurn").html();
function turnNotice() {
	jQuery(".turnNotice").remove();
	jQuery("#turn").html("<div class='turnNotice'>" + turnPossesive + " turn</div>").removeClass(oppositeTurn).addClass(turn);
	jQuery("#centerContentContainer").removeClass(oppositeTurn).addClass(turn);
}
if (firstTurn == "true") {
	jQuery("#text").html(turnName + " rolls first.");
	turnNotice();
}
else {
	// hack for fixing mysteriously disappearing roll button when loading saved game
	jQuery("#text").html("<h2>Resume Game</h2>");
	jQuery("#roll").show();
}

// intro phrases

var intro = new Array();
intro[0] = "In order to spice things up, ";
intro[1] = "Because the luscious curves in front of your eyes fill you with the desire to do so, ";
intro[2] = "To appease that warm feeling beneath your waist line, ";
intro[3] = "Because you've been dying for an opportunity to do so, ";
intro[4] = "Unfortuntaley, a sign informs you that you can't proceed unless you ";
intro[5] = "With this perfect opportunity to get a little closer, ";
intro[6] = "Because no one is watching, ";
intro[7] = "To help fight back the urge to do something naughtier, ";
intro[8] = "An automated tole booth will only let you proceed if you ";
intro[9] = "Because it's the tradition here, ";
intro[10] = "Because you can't resist the urge to do so, ";
intro[11] = "Because the mood is just right, ";
intro[12] = "Your heart rate is picking up a bit. You'd better ";
intro[13] = "Because you're not going to get a better opportunity, ";
intro[14] = "You know you're feeling a burning desire to ";
intro[15] = "Because the sweet perfume in the air is driving you crazy, ";
intro[16] = "Because you're feeling drunk on love, ";
intro[17] = "It's time to live your fantasies, so ";
intro[18] = "With a slight moan, ";

// roll die

jQuery("#roll").click(function() {
		
	// alternate turn
	
	if (firstTurn == "false") {
		alternateTurn(turn);
	}
	jQuery("#centerContent").removeClass(oppositeTurn).addClass(turn);
	turnNotice();
	jQuery("#text, #load").html("");
	jQuery("#roll").hide();
	var notice = null;
	jQuery(".spaceContainer.current").removeClass("current");
	jQuery(".turn").removeClass("turn");
	jQuery(".player." + turn).addClass("turn");
	space = jQuery(".turn").parents(".space").attr("id");
	space = parseInt(space);
	
function roll() {
		roll = Math.round(Math.random() * 5);
		var x = 0;
		var interval = setInterval(function() {
			player = jQuery(".player." + turn);
			jQuery(".player." + turn).remove();
			space ++;
			if (space <= 16) // last space
				jQuery("#" + space).append(player);
			else {
				
				// collect money for passing "Go"
				
				space = 1; // reset count to beginning space
				jQuery("#" + space).append(player);
				money = jQuery(".money." + turn).html();
				originalMoney = money;
				money = parseInt(money);
				jQuery(".spaceContainer." + turn).each(function() {
					cost = jQuery(" .space", this).attr("data-cost");
					cost = parseInt(cost);
					cost /= 4;
					cost *= 100;
					money += cost;
				});
				money += 400;
				jQuery(".money." + turn).html(money);
				increase = 500;
				jQuery("#turn").html(turnName + " collected $" + increase + " for passing Go.");
			}
			x ++;
		    if (x == roll + 1) {
		    	
		    	firstTurn = "false";
		    	jQuery("#firstTurn").html("false");
		        clearInterval(interval);
				jQuery("#" + space).parent().addClass("current");
		    	title = jQuery("#" + space + " .label").html();
		    	cost = jQuery("#" + space).attr("data-cost");
		    	cost *= 100;
				money = jQuery(".money." + turn).html();
				
				// bonus card
	
				bonus = Math.round(Math.random() * 3);
				if (bonus == 0 && firstTurn == "false" && jQuery("#" + space + " .property").length == 0) {
					jQuery("#text").hide();
					jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/popup.php", { husband:husband, wife:wife }, function(result) {
						jQuery("#x, #gray").on("click", function() {
							jQuery("#text").fadeIn();
						});
					});
				}
				
				function moveOn() {
					jQuery("#text").html("");
					
					 // if not on "Go", "Strip", "Jail", or "Bankrupt"
					
			    	if (jQuery("#" + space).attr("id") !== "1" && jQuery("#" + space).attr("id") !== "5" && jQuery("#" + space).attr("id") !== "9" && jQuery("#" + space).attr("id") !== "13") {
						jQuery("#text").html("<p id='notice' style='display:none;'>" + notice + "</p><button class='small purchaseProperty'>Purchase Property</button><div id='info'><div id='cost'>Cost: $" + cost + "</div></div>");
					}
					if (notice !== null) jQuery("#notice").show();
					else jQuery("#roll").show();
	
					// build options
					
					function showBuildOptions() {
						
							// assign prices to properties
		
							cost = jQuery("#" + space).attr("data-cost");
							cost = parseInt(cost) * 100;
							club = cost * .25;
							club = parseInt(club);
							spa = cost * .5;
							spa = parseInt(spa);
							theater = cost * .75;
							theater = parseInt(theater);
							hotel = cost;
							hotel = parseInt(hotel);
							
							jQuery("#info").html("<p>You own this property.</p><ul class='buttonList'><li><button class='build small' data-property-type='club' data-cost='" + club + "'>Build Club</button> $" + club + "</li><li><button class='build small' data-property-type='spa' data-cost='" + spa + "'>Build Spa</button> $" + spa + "</li><li><button class='build small' data-property-type='theater' data-cost='" + theater + "'>Build Theater</button> $" + theater + "</li><li><button class='build small' data-property-type='hotel' data-cost='" + hotel + "'>Build Hotel</button> $" + hotel + "</li></ul>");
							
							// hide build options out of price range
							
							jQuery(".build").each(function() {
								cost = jQuery(this).attr("data-cost");
								cost = parseInt(cost);
								if (money < cost) {
									jQuery(this).parent().hide();
								}
							});
							
							// build property
							
							jQuery(".build").click(function() {
								cost = jQuery(this).attr("data-cost");
								propertyType = jQuery(this).attr("data-property-type");
								if (money >= cost) {
									money -= cost;
									jQuery(".money." + turn).html(money);
									jQuery("#info").html("You now own a " + propertyType + " on this property.");
									jQuery("#" + space + " .label").after("<div class='property' data-property-type='" + propertyType + "' data-cost='" + cost + "'>" + propertyType + "</div>");
									jQuery("#" + space + " .cost").remove();
								}
							});
						
					}
					
					jQuery("#" + space).addClass("visited");
					
					if (jQuery("#" + space).parent().hasClass(turn) || jQuery("#" + space + " .property").length > 0 || jQuery("#" + space).parent().hasClass(oppositeTurn)) {
						jQuery(".purchaseProperty, #cost").hide();
						jQuery("#cost").hide();
					}
					else {
						jQuery(".purchaseProperty").show();
					}
					if (money < cost) {
						jQuery(".purchaseProperty, #cost").hide();
					}
					
					// show build options if player owns an empty space
					
					if (jQuery("#" + space).hasClass(turn) && !(jQuery("#" + space).find('.property').length > 0)) {
						showBuildOptions();
					}
					jQuery("#roll").show().html("Next Turn");
					money = parseInt(money);
					
					// purchase property
					
					jQuery(".purchaseProperty").click(function() {
						jQuery(this).remove();
						jQuery("#notice").hide();
						money = jQuery(".money." + turn).html();
						money = parseInt(money);
						money -= cost;
						jQuery(".money." + turn).html(money);
						jQuery("#" + space).addClass(turn).parent().addClass(turn);
						showBuildOptions();
					});
					
					// land on opponent's property
		
					if (jQuery("#" + space + "." + oppositeTurn + " .property").length > 0) {
						t = 60;
						jQuery("#roll").hide();
						category = jQuery("#" + space + " .property").attr("data-property-type");
						cost = jQuery("#" + space + " .property").attr("data-cost");
						cost = parseInt(cost);
						var action;
						jQuery("#load").load("/wp-content/themes/marriedgames.org/lovers-lane/actions.php", {
						recipient: oppositeTurn,
						category: category,
						turn: turn,
						husband: husband,
						wife: wife
						}, function(result) {
							action = jQuery("#load").html();
							jQuery("#load").html("");
							if (money > cost) {
								jQuery("#text").html("<p>You landed on " + oppositeTurnName + "'s " + propertyType + ". You must either " + action + " or pay " + oppositeTurnName + " $" + cost + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li><li><button id='pay'>Pay Up</button></li></ul>");
							}
							else {
								jQuery("#text").html("<p>You landed on " + oppositeTurnName + "'s " + propertyType + ", and unfortunately, you can't afford the fees. You must either " + action + " or accept defeat and initiate sex with " + oppositeTurnName + ".</p><ul class='buttonList'><li><button id='perform'>Perform Action</button></li><li><button id='initiate-sex'>Initiate Sex</button></li></ul>");
							}
							
							// perform action
							
							jQuery("#perform").on("click", function() {
								jQuery("#text").html("<p>You must " + action + " until the timer runs out.</p><p class='countdown'>60</p>");
								var interval = setInterval(function() {
									jQuery("#roll").hide();
									t --;
									jQuery("#text .countdown").html(t);
								    if (t == 0) {
								        clearInterval(interval);
								        jQuery("#text").html("<p>Time's up.</p><audio autoplay><source src='/wp-content/themes/marriedgames.org/lovers-lane/audio/ding.ogg' type='audio/ogg'></audio>");
								        jQuery("#roll").show();
								    }
								}, 1000);
							});
							jQuery("#pay").on("click", function() {
								money -= cost;
								jQuery(".money." + turn).html(money);
								opponentMoney = jQuery(".money." + oppositeTurn).html();
								opponentMoney = parseInt(opponentMoney);
								opponentMoney += cost;
								jQuery(".money." + oppositeTurn).html(opponentMoney);
								jQuery("#text").html("");
								jQuery("#roll").show();
							});
							
							// initiate sex
							
							jQuery("#initiate-sex").on("click", function() {
								jQuery("#text").html("<p><h2>" + oppositeTurnName + " Wins!</h2><p>Congratulations, " + oppositeTurnName + ", you've won yourself a sex slave. Lie back and enjoy yourself while " + turnName + " does all the work.</p>");
							});
	
						});
					}
				}
		    	
				// first time on new space
				
				if (space !== 1 && space !== 5 && space !== 9 && space !== 13) {
					if (jQuery("#" + space).hasClass("visited") == false) {
						x = Math.round(Math.random() * 18);
						jQuery("#text").html("<h2>Welcome to " + title + "</h2><p>" + intro[x] + "<span class='load'></span>.</p><button id='finished'>Finished</button>");
						category = jQuery("#" + space).attr("data-category");
						jQuery(".load").load("/wp-content/themes/marriedgames.org/lovers-lane/actions.php", {
							recipient: oppositeTurn,
							category: category,
							turn: turn,
							husband: husband,
							wife: wife
						});
						jQuery("#" + space).addClass("visited");
						jQuery("#finished").click(function() {
							moveOn();
						});
					}
					else {
						moveOn();
					}
				}
				
				// special spaces
				
				// strip
				
				else {
					
					// go
					
					if (space == 1) {
						jQuery("#roll").show();
					}
					
					// strip
					
					if (space == 5) {
						jQuery("#text").html("<h2>Time to Strip</h2><p>For " + oppositeTurnName +"'s viewing pleasure, you must strip off an article of clothing.</p><button id='finished'>Finished</button>");
						jQuery("#finished").click(function() {
							moveOn();
						});
					}
				
					// jail
				
					if (space == 9) {
						jQuery("#text").html("<h2>You're Trapped in Jail</h2><p>Uh-oh. Either you've got some time to serve, or " + oppositeTurnName + " will bail you out. Time will tell.</p>");
						jQuery("#roll").show();
						inJail = true;
						personInJail = turnName;
					}
	
					// bankrupt
	
					if (space == 13) {
						jQuery(".money." + turn).html("0");
						jQuery("#text").html("<h2>You're Bankrupt!</h2><p>Uh-oh, better make some more money! Who knows what " + oppositeTurnName + " will do to you in your desperation?</p><button id='finished'>Finished</button>");
						jQuery("#finished").click(function() {
							moveOn();
						});
					}
				}
			}
		}, 500);
	}
	
	// if someone's in jail
	
	if (inJail == true) {
		if (personInJail == oppositeTurnName) {
			if (money > 500) {
				jQuery("#text").html("<p>Your beloved " + oppositeTurnName + " is in jail. For the price of $500, do you wish to bail " + oppositeTurnAccusative + " out?</p><button id='yes'>Yes</button><button id='no'>No</button>");
			}
			else {
				jQuery("#text").html("<p>Your beloved " + oppositeTurnName + " is in jail. Unfortunately, you can't afford to bail " + oppositeTurnAccusative + " out.</p>");
				moveOn();
			}
			jQuery("#yes").click(function() {
				money -= 500;
				jQuery(".money." + turn).html(money);
				jQuery("#text").html("How kind. " + oppositeTurnName + " is now free.<button id='continue'>Continue</button>");
				return inJail = false;
				jQuery("#continue").click(function() {
					roll();
				});
			});
			jQuery("#no").click(function() {
				jQuery("#text").html("It probably serves " + oppositeTurnAccusative + " right.<button id='continue'>Continue</button>");
				jQuery("#continue").click(function() {
					roll();
				});
			});
		}
		else {
				jQuery("#text").html("Unfortunately, you're still trapped in jail.");
				jQuery("#roll").show();
		}
	}
	else roll();
	
});

// reset game

function confirmReset() {
	var r=confirm("Are you sure you want to reset?")
	if (r==true) {
		var newGame = "";
		jQuery("#result").load("/wp-content/themes/marriedgames.org/lovers-lane/updateRow.php",{
			id: S2MEMBER_CURRENT_USER_ID,
			game: newGame
		}, function() {
			location.reload();
		});
	}
}

// save game
		
jQuery("#save").click(function() {
	if (userLevel > 0) {
		game = jQuery("#game").html();
		jQuery("#result").load("/wp-content/themes/marriedgames.org/lovers-lane/updateRow.php",{
			id: S2MEMBER_CURRENT_USER_ID,
			game: game
		}, function(result) {
			alert("Game Saved");
		});
	}
	else {
		alert("This feature is reserved for premium members.");
	}
});	

jQuery("#reset").click(function() {
	confirmReset();
});