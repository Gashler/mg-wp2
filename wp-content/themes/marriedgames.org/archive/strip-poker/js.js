/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/var folder = false;
var articles = 4;
var interval = 100;
var opponentStrip;
var playerStrip;
var called = false;
var gethttp = location.protocol;

jQuery(document).ready(function() {
	
	// settings
	
	jQuery("#settings").click(function() {
		popbox(jQuery("#settingsContent").html() + "<br><br><button class='close'>Close</button>");
		jQuery("#resetMoney").click(function() {
			resetMoney();
		});
	});
	
	// resize cards for mobile displays
	
	resizeHands();
	jQuery(window).resize(function() {
		resizeHands();
	});
	var originalGameHeight = jQuery("#gameContent").height();
	function resizeHands() {
		var maxWidth = 835;
		if (jQuery(window).width() < maxWidth) {
			width = jQuery(window).width() / maxWidth;
			gameWidth = (maxWidth * width) * .9;
			marginBottom = (maxWidth - (width * maxWidth)) * .26;
			gameHeight = originalGameHeight - (marginBottom * 1.5);
			jQuery("#playerHandContainer").css("width", gameWidth + "px");
			jQuery("#gameContent, #playerHandContainer").css({
				"width": gameWidth + "px",
				//"height": gameHeight + "px"
			});
			jQuery(".hand").css({
				"transform": "scale(" + width + ")",
				"transform-origin": "0 0",
				"-webkit-transform": "scale(" + width + ")",
				"-webkit-transform-origin": "0 0",
				"margin-bottom": "-" + marginBottom + "px"
			});
		}
	}
				
	// cancel functionality that closes opening popup upon clicking on gray background
	
	jQuery("body").on({
		click: function(e) {
			jQuery(this).off("click");
		}
	}, ".dark");
	
	// cancel waiting for opponent to join
	
	jQuery("#cancelWaitingForOpponent").click(function() {
		deleteGame();
		location.reload();
	});
	
	// disable buttons
	
	if (phase < 2) {
		jQuery("#discard, #raise, #call, #deal").attr("disabled", "disabled");
		if (playerReady == 1 && opponentReady == 0) waitingMessage();
	}
	if (phase > 1) {
		jQuery("#discard, #deal").attr("disabled", "disabled");
	}
	
	// waiting message
	
	function waitingMessage(message) {
		jQuery("#waitingMessage").slideUp();
		if (message == undefined) {
			if (phase == 1) message = "Waiting on " + opponent + " to stay or discard ...";
			if (phase == 2) message = "Waiting on " + opponent + " to raise or call ...";
		}
		jQuery("#waitingMessage").html(message).slideDown();
	}
	
	// new game
	
	var startingGame = false;
	var playerStartedGame = false;
	if (newGame == true) makeNewGame();
	function makeNewGame() {
		startingGame = true;
		popbox('' +
			'<h2>How to Play</h2>' +
			'<p style="text-align:left;">(1) Each of you put on 4 articles of clothing. (2) Access this game on 2 different devices (using the same account). (3) One of you select your name and start the game. The other player will be automatically added.</p>' +
			'<small>(To be completed by only one of you)</small><br>' +
			'<select id="player">' +
				'<option value="m">' + husband + '</option>' +
				'<option value="f">' + wife + '</option>' +
			'</select>' +
			'<button id="begin">Begin Game</button>' +
		'');
		jQuery(".dark").addClass("static");
		jQuery("#begin").click(function() {
			playerStartedGame = true;
			player1gender = jQuery("#player").val();
			if (player1gender == "m") {
				player2gender = "f";
				opponent = wife;
			}
			else {
				player2gender = "m";
				opponent = husband;
			}
			jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/newGame.php", {
				user: user,
				player1id: playerId,
				player1gender: player1gender,
				player2gender: player2gender
			}, function() {
				closePopbox();
			});
			jQuery("#waitingForOpponentToJoin .opponent").html(opponent);
			jQuery("#waitingForOpponentToJoin").show();
			jQuery("#waitingForOpponentToJoin p").slideDown();
			updateStatus("reload");
		});
	}
	
	// game rules
	
	if (newGame == false && firstGame !== 1) {
		var playerMoney = parseInt(jQuery("#playerMoney").html());
		recommendedAmount = (articles * interval) - interval;
		possibleArticles = parseInt(playerMoney / interval) + 1;
		var s = "";
		if (possibleArticles !== 1) s = "s";
		if (playerMoney < recommendedAmount && playerMoney > 0) {
			popbox("<p>You lack sufficient love bucks for a full game. You can either reset yours and " + opponent + "'s love bucks or start off with " + possibleArticles + " article" + s + " of clothing.</p><button class='close resetMoney'>Reset Love Bucks</button><button class='close'>I'll Wear " + possibleArticles + " Article" + s + " of Clothing</button>");
		}
		jQuery(".resetMoney").click(function() {
			resetMoney();
		});
	}
	
	if (playerNumber == 1) {
		if (jQuery("#playerHand .cardContainer").length < 1) {
			deal();
		}
		flipCards();
	}
	
	// get status
	
	function getStatus() {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getStatus.php", {
			opponentNumber: opponentNumber,
			user: user
		}, function(result) {
			opponentStatus = result;
		});
	}
	
	// phase 2 (betting)
	
	if (phase == 2) phase2();
	function phase2() {
		
		// if either player has no money, disable betting
		
		var opponentMoney = parseInt(jQuery("#opponentMoney").html());
		var playerMoney = parseInt(jQuery("#playerMoney").html());
		if (opponentMoney >= 5 && playerMoney >= 5) {
		
			// first bet
			
			if (turn == playerNumber && firstBet == 0) updateReady(2);
			if (playerReady == 2) {
				jQuery("#stay, #raise, #call").removeAttr("disabled");
				jQuery("#waitingMessage").slideUp();
				if (turn == playerNumber && firstBet == 0) {
					setTimeout(function() { // this delay prevents this popup from rising above other sequential popups
						popbox("<h2>You start the betting</h2><button class='close'>OK</button>");
					}, 500);
					updateFirstBet(1);
					updateTurn(opponentNumber);
				}
				
				// every other bet
				
				else popbox("<h2>Your turn to bet</h2><button class='close'>OK</button>");
			}
			else {
				if (opponentMoney >= 5 && playerMoney >= 5) {
					jQuery("#discard, #stay, #raise, #call, #deal").attr("disabled", "disabled");
					jQuery("#waitingMessage").slideUp(function() {
						waitingMessage();
					});
				}
			}
		}
		else {
			jQuery("#fold, #stay, #raise").attr("disabled", "disabled");
			jQuery("#call").removeAttr("disabled");
			jQuery("#waitingMessage").slideUp();
		}
	}

	// function update first bet

	function updateFirstBet(number) {
		firstBet = number;
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateFirstBet.php", {
			firstBet: number,
			user: user
		});
	}
	
	// function turn

	function updateTurn(number) {
		firstBet = number;
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateTurn.php", {
			number: number,
			user: user
		});
	}
	
	// update ready
	
	function updateReady(ready) {
		playerReady = ready;
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateReady.php", {
			playerNumber: playerNumber,
			ready: ready,
			user: user
		});
	}
	
	// increment phase
	
	function incrementPhase() {
		phase ++;
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/incrementPhase.php", {
			user: user
		});		
	}
	
	// reset phase
	
	function resetPhase() {
		phase = 1;
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/resetPhase.php", {
			user: user
		});		
	}
	
	// reset game
	
	if (resetGame == true) {
		startingGame = true;
		deleteGame();
	}
	
	// add player 2
	
	if (addingPlayer2 == true) {
		jQuery("#waitingForOpponentToJoin").hide();
		getMoney();
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/addPlayer2.php", {
			user: user,
			player2id: playerId,
			player2gender: player2gender
		}, function() {
			deal();
			jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateReady.php", {
				
				// unmark opponent as ready (setup for the next phase)
				
				playerNumber: opponentNumber,
				ready: 0,
				user: user
			}, function(result) {
				opponentReady = result;
			});
		});
		jQuery("#gameContent").hide().css("opacity", "1").fadeIn(500);
	}
	
	// deal
	
	var anted = false;
	jQuery("#deal").click(function() {
		updateStatus("deal");
		deal();
	});
	function deal() {
		jQuery("#pot").html(0);
		clearOpponentStatus();
		if (newGame == 0) closePopbox();
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateFirstGame.php", { user: user });
		var opponentMoney = parseInt(jQuery("#opponentMoney").html());
		var playerMoney = parseInt(jQuery("#playerMoney").html());
		
		// if both players can afford the ante
		
		if (opponentMoney >= 5 && playerMoney >= 5) {
			
			// ante up
			
			setTimeout(function() {
				opponentMoney -= 5;
				playerMoney -= 5;
				jQuery("#opponentMoney").html(opponentMoney);
				jQuery("#playerMoney").html(playerMoney);
				jQuery("#pot").html(10);
				updateMoney();
				updatePot(10);
			}, 2000);

			// reset round variables
			
			marked = 0;
			called = false;
			getTurn();
			playerStrip = 0;
			opponentStrip = 0;
			called = false;
			resetPhase();
			updateReady(0);
			updateFirstBet(0);
			jQuery("#waitingMessage").slideUp();
			var hand = "";
			
			// create player's hand
			
			jQuery("#playerHand").html("");
			jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getOpponentHand.php", {
				opponentNumber: opponentNumber,
				user: user
			}, function() {
				for (z = 1; z <= 10; z ++) {
					function generateCard() {
						x = 1 + Math.floor(Math.random() * 13);
						y = 1 + Math.floor(Math.random() * 4);
						col = (x * cardWidth) - cardWidth;
						row = (y * cardHeight) - cardHeight;
						
						// give "new" class to cards 5 - 10 (hidden cards used to replace cards that the user discards), to distinguish them from the other cards (so animation effects can be applied when they're added)
						
						var newCard = "";
						if (z >= 6) newCard = " new";
						
						function appendCard() {
							jQuery("#playerHand").append('<div class="cardContainer"><div class="card' + newCard + '"><figure class="front" style="background:white; box-shadow:0 0 2px 2px white !important;"></figure><figure class="back" data-value="' + x + '" data-suit="' + y + '" style="background-position:-' + col + 'px -' + row + 'px"></figure></div></div>');
						}
						
						// prevent duplicate cards
						
						var cardExists = false;
						if (jQuery("#load").html() !== "") {
							jQuery("#load figure.back").each(function() {
								if (parseInt(jQuery(this).attr("data-value")) == x && parseInt(jQuery(this).attr("data-suit")) == y) {
									cardExists = true;
								}
							});
						}
						if (cardExists == false) {
							if (jQuery("#playerHand .cardContainer").length > 0) {
								jQuery("#playerHand figure.back").each(function() {
									if (parseInt(jQuery(this).attr("data-value")) == x && parseInt(jQuery(this).attr("data-suit")) == y) cardExists = true;
								});
								if (cardExists == false) appendCard();
								else generateCard();
							}
							else appendCard();
						}
						else generateCard();
					}
					generateCard();
				}
				if (addingPlayer2 == 1 && playerNumber == 2) {
					updateStatus("join");
					addingPlayer2 = false;
				}
				
				// flip cards
				
				flipCards();
				updateDB();
			});
			
			// create backsided cards for opponent's hand
			
			hand = "";
			for(x = 1; x <= 5; x ++) {
				row = (2 * cardHeight);
				col = (13 * cardWidth);
				hand += '<div class="cardContainer"><div class="card"><figure style="background-position:-' + col + 'px -' + row + 'px"></figure></div></div>';
			}
			jQuery("#opponentHand").html(hand);
			
			// disable and enable buttons
			
			jQuery("#raise, #call, #deal").attr("disabled", "disabled");
			jQuery("#fold, #stay").removeAttr("disabled");
	
		}
		else {

			// if players can't afford the ante

			if (newGame == 0 && playerNumber == 1) {				
				if (opponentMoney < 5) person = opponent;
				if (playerMoney < 5) person = "You";
				if (opponentMoney < 5 && playerMoney < 5) person = "You and " + opponent;
				popbox("<h2>Reset Love Bucks?</h2><p>" + person + " can't afford the ante. In order to play, you must either reset all love bucks or add love bucks elsewhere in MarriedGames.org, such as through the <a href='/sexy-services'>Sexy Services</a> app.</p><button class='close resetMoney'>Reset Love Bucks</button>");
				jQuery(".resetMoney").click(function() {
					resetMoney();
				});
			}
			else if (newGame == 0 && playerNumber == 2) {
				jQuery("#fold, #stay, #deal").attr("disabled", "disabled");
				waitingMessage("Waiting on " + opponent + " to reset love bucks ...");
				if (addingPlayer2 == true) updateStatus("join");
			}
		}
	}
	
	// flip cards
	
	function flipCards() {
		jQuery("#playerHand .cardContainer:nth-child(-n+5)").each( function(e) {
		    var element = jQuery(this);
		    setTimeout(function() {
		        jQuery(".card", element).show().addClass("flipped");
		    }, e * 250);
		});
	    setTimeout(function() { // adding the front side is a hack to make the spin look right in IE
	    	jQuery(".front").remove();
	    	updateDB();
	    }, 1250);
	}
	
	// alternate turn
	
	function alternateTurn() {
		if (playerReady == 1) {
			playerReady = 0;
			opponentReady = 1;
		}
		else {
			playerReady = 1;
			opponentReady = 0;
		}
		if (playerNumber == 1) {
			player1ready = playerReady;
			player2ready = opponentReady;
		}
		else {
			player2ready = playerReady;
			player1ready = opponentReady;
		}
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/alternateTurn.php", {
			player1ready: player1ready,
			player2ready: player2ready,
			user: user
		});
	}
	
	// fold
	
	jQuery("#fold").click(function() {
	  	folder = true;
		fold();
	});
	function fold() {		
		
		/*************revealed through an animation*********************/
		  jQuery("#playerHand .cardContainer:nth-child(n+6)").remove();		
		 jQuery("#fold, #raise, #call").attr("disabled", "disabled");
		 if (called == false) {			
			updateStatus("fold");
			called = true;
		 }
		  jQuery.post(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getOpponentHand.php", {
			opponentNumber: opponentNumber,
			user: user
		}, function(result) {
			text_clean = result.replace(/\\/g, "");
			jQuery("#loadOpponentHand").html(text_clean);
			jQuery("#loadOpponentHand figure").addClass("back");
			index = 1;
			jQuery("#opponentHand .cardContainer").each( function() {
			    var element = jQuery(this);
			    jQuery(".card", element).prepend(jQuery("#loadOpponentHand .cardContainer:nth-of-type(" + index + ") .card").html());
			    jQuery("figure", element).addClass("front");
			    index ++;
			});
			var playerValues;
			var playerPoints;
			var playerHand;
			var playerHighestSetValue;
			var opponentValues;
			var opponentPoints;
			var opponentHand;
			var opponentHighestSetValue;
			
			// add up points of hands
			
			jQuery(".hand").each(function() {
				var values = [];
				values[1] = 0;
				values[2] = 0;
				values[3] = 0;
				values[4] = 0;
				values[5] = 0;
				values[6] = 0;
				values[7] = 0;
				values[8] = 0;
				values[9] = 0;
				values[10] = 0;
				values[11] = 0;
				values[12] = 0;
				values[13] = 0;
				var cards = [];
				var suits = [];
				var x = 0;
				var points;
				var straightExists = false;
				var flushExists = false;
				var hand;
				var uniquePairs = 0;
				var largestMatch = 2;
				var highestSetValue = 0;
				jQuery(".card .back", this).each(function() {
					
					// add to array
					
					cards.push(parseInt(jQuery(this).attr("data-value")));
					
					value = parseInt(jQuery(this).attr("data-value"));
					suit = parseInt(jQuery(this).attr("data-suit"));
					
					// determine matches
					
					values[value] ++;
					if (values[value] >= 2) {
						if (values[value] == 2) uniquePairs ++;
						if (values[value] >= 3) largestMatch ++;
						if (values[value] > highestSetValue) highestSetValue = values[value];
						points += 8192;
					}

					x ++;
					
					// add suits to list
					
					suits.push(suit);
				});
				
				// determine nothing
				
				if (uniquePairs == 0) {
					hand = "nothing";
					points = 0;
				}
				
				// determine 1 pair
				
				if (uniquePairs == 1) {
					hand = "1 pair";
					points = 2;
				}
				
				// determine 2 pairs
				
				if (uniquePairs == 2) {
					hand = "2 pairs";
					points = 3;
				}

				// determine 3 of a kind
				
				if (largestMatch == 3) {
					hand = "3 of a kind";
					points = 4;
				}
				
				// determine straights
				
				if (uniquePairs == 0) {
					cards.sort(function(a, b) {
						return a - b;
					});
					var straight = 1;
					for (x = 0; x <= 5; x++) {
						previousCard = x - 1;
						if (previousCard !== -1 && cards[x] == cards[previousCard] + 1) straight ++;
					}
					if (straight == 5) {
						straightExists = true;
						hand = "a straight";
						points = 5;
					}
				}

				// determine flushes
				
				var startingSuit;
				var sameSuit = 1;
				for (x = 0; x <= suits.length; x++) {
					if (x == 0) startingSuit = suits[x];
					else if (suits[x] == startingSuit) sameSuit ++;
				}
				if (sameSuit == suits.length) {
					flushExists = true;
					hand = "a flush";
					points = 6;
				}
				
				// determine full house
				
				if (uniquePairs == 2 && largestMatch == 3) {
					hand = "a full house";
					points = 7;
				}
				
				// determine 4 of a kind
				
				if (largestMatch == 4) {
					hand = "4 of a kind";
					points = 8;
				}
				
				// straight flush
				
				if (straightExists && flushExists == true) {
					hand = "a straight flush";
					points = 9;
				}
				
				// assign scores and hand descriptions to players

				cards.sort(function(a, b) {
					return b - a;
				});
				var id = jQuery(this).attr("id");
				if (id == "playerHand") {
					playerCards = cards;
					playerPoints = points;
					playerHand = hand;
					playerHighestSetValue = highestSetValue;
				}
				if (id == "opponentHand") {
					opponentCards = cards;
					opponentPoints = points;
					opponentHand = hand;
					opponentHighestSetValue = highestSetValue;
				}
				
			});

			// determine winner
			
			var winner;
			var winnerMoney;
						
			// settle ties

			if (playerPoints == opponentPoints) {
				for (x = 1; x <= 5; x ++) {
					if (playerCards[x] !== opponentCards[x]) {
						if (playerCards[x] > opponentCards[x]) {
							winner = player;
							winnerMoney = "#playerMoney";
							if (playerHand == "nothing" && opponentHand == "nothing") {
								playerHand = "the high card";
								break;
							}
							if (playerHand == opponentHand) {
								playerHand += " with higher cards";
							}
						}
						else {
							winner = opponent;
							winnerMoney = "#opponentMoney";
							if (playerHand == "nothing" && opponentHand == "nothing") {
								opponentHand = "the high card";
								break;
							}
							if (playerHand == opponentHand) {
								opponentHand += " with higher cards";
							}
						}
					}
					if (winner !== undefined) break;
				}
			}
			else {
				if (playerPoints > opponentPoints) {
					winner = player;
					winnerMoney = "#playerMoney";
				}
				else {
					winner = opponent;
					winnerMoney = "#opponentMoney";
				}
			}
			
			// reveal opponent's hand
			
			jQuery("#opponentHand .cardContainer").each(function(e) {
				var element = jQuery(this);
			    setTimeout(function() {
			        jQuery(".card", element).addClass("flipped");
			    }, e * 1000);
			});
			
			// announce winner
			
			setTimeout(function() {
				var person = winner;
				var s = "s";
				var exclamation = "";
				if (winner == player) {
					person = "You";
					s = "";
					exclamation = "!";
				}
				else opponentStrip = 0;
				popbox("<h2>" + person + " win" + s + " $" + jQuery("#pot").html() + " love bucks" + exclamation + "</h2><p>You have " + playerHand + ".</p><p>" + opponent + " has " + opponentHand + ".</p><button class='close'>OK</button>");
				jQuery(".close").click(function() {
					closePopbox();
					jQuery("#discard, #stay, #raise, #call").attr("disabled", "disabled");
					if (winner == player) jQuery("#deal").removeAttr("disabled");
					else waitingMessage("Waiting on " + opponent + " to deal ...");
				});
				
				// update money
				
				var pot = parseInt(jQuery("#pot").html());
				var newMoney = parseInt(jQuery(winnerMoney).html()) + pot;
				setTimeout(function() {
					if ((winner == player && opponentStrip > 0) || (winner == opponent && playerStrip > 0)) applyClothingChanges(winner);
				}, 1000);
				jQuery("#pot").html(0);
				jQuery(winnerMoney).html(newMoney);
				updateMoney();
				updatePot();
			}, 5000);
		});			
		/**************************************************************/
		//if (playerStrip > 0) applyClothingChanges(opponent);
		//jQuery("#discard, #fold, #stay, #raise, #call, #deal").attr("disabled", "disabled");
		//waitingMessage("Waiting on " + opponent + " to deal ...");
	}
	
	// mark for discarding
	
	var marked = 0;
	jQuery("#playerHand").on({
		click: function() {
			if (phase == 1 && playerReady == 0) {
				if (jQuery(".discard", this).length == 0 && marked < 4) {
					jQuery(this).prepend("<div class='discard'><div class='discardX'>&times;</div></div>");
					marked ++;
				}
				else if (jQuery(".discard", this).length > 0) {
					jQuery(".discard", this).remove();
					marked --;
				}
				if (jQuery("#playerHand .discard").length !== 0) jQuery("#discard").removeAttr("disabled");
				else jQuery("#discard").attr("disabled", "disabled");
			}
		}
	}, ".cardContainer");
	
	// discard

	jQuery("#discard").click(function() {
		if (playerReady == 0 && phase == 1) {
			updateReady(1);
			getReady();
			var amount = 0;
			jQuery("#playerHand .cardContainer").each(function() {
				if (jQuery(".discard", this).length > 0) {
					card = jQuery("#playerHand .cardContainer:last-child");
					jQuery("#playerHand .cardContainer:last-child").remove();
					jQuery(this).after(card);
					jQuery(this).remove();
					amount ++;
					setTimeout(function() {
						jQuery("#playerHand .card.new").addClass("flipped");
					}, 1);
				}
			});
			updateStatus("discard", amount);
			jQuery("#discard").attr("disabled", "disabled");
			updateDB();
			if (opponentReady == 0) {
				jQuery("#stay").attr("disabled", "disabled");
				waitingMessage();
			}
			else {
				incrementPhase();
				phase2();
			}
		}
	});
	
	// stay
	
	jQuery("#stay").click(function() {
		updateDB();
		if (phase == 1)	{
			jQuery("#playerHand .discard").remove();
			updateReady(1);
		}
		if (phase == 2) updateReady(3);
		getReady();
		updateStatus("stay");
		jQuery("#discard, #stay, #raise, #call, #deal").attr("disabled", "disabled");
		if (phase == 1 && opponentReady == 0) {
			waitingMessage();
		}
		else if (phase == 1) {
			incrementPhase();
			phase2();
		}
	});
	
	// raise

	jQuery("#raise").click(function() {
		popbox("" +
			"<h2>Raise $<span id='raiseTotal'>0</span></h2>" +
			"<p id='stripTotal'></p>" +
			"<table style='box-shadow:none;'>" +
			"<tr><th>$5</th><td><button data-amount='5' class='small raise'>+</button><button data-amount='5' class='small lower'>-</button></td></tr>" +
			"<tr><th>$25</th><td><button data-amount='25' class='small raise'>+</button><button data-amount='25' class='small lower'>-</button></td></tr>" +
			"<tr><th>$100</th><td><button data-amount='100' class='small raise'>+</button><button data-amount='100' class='small lower'>-</button></td></tr>" +
			"</table><br>" +
			"<button class='close' id='confirmRaise'>OK</button>" +
		"");
		var opponentMoney = parseInt(jQuery("#opponentMoney").html());
		var playerMoney = parseInt(jQuery("#playerMoney").html());
		var raiseTotal = 0;
		var raise;
		canAfford();
		function canAfford() {
			raise = parseInt(jQuery("#raiseTotal").html());
			jQuery(".raise").each(function() {
				var amount = parseInt(jQuery(this).attr("data-amount")) + raiseTotal;
				if (amount > opponentMoney || amount > playerMoney) {
					jQuery(this).attr("disabled", "disabled");
				}
				else (jQuery(this).removeAttr("disabled"));
			});
			jQuery(".lower").each(function() {
				if (raiseTotal - parseInt(jQuery(this).attr("data-amount")) < 0) jQuery(this).attr("disabled", "disabled");
				else (jQuery(this).removeAttr("disabled"));
			});
			if (playerMoney >= raise) return true;
		}
		jQuery(".raise").each(function() {
			if (!(parseInt(jQuery(this).attr("data-raise")) >= playerMoney)) jQuery(this).attr("disabled");
		});
		
		// raise amount
		
		jQuery(".raise").click(function() {
			if (!(jQuery(this).attr("disabled"))) {
				var raise = parseInt(jQuery(this).attr("data-amount"));
				raiseTotal += raise;
				determineClothingChanges(-raiseTotal);
				if (playerStrip > 0) jQuery("#stripTotal").html("+ " + playerStrip + " of your clothing articles");
				else jQuery("#stripTotal").html("");
				jQuery("#raiseTotal").html(raiseTotal);
				canAfford();
			}
		});
		
		// lower amount
		
		jQuery(".lower").click(function() {
			if (!(jQuery(this).attr("disabled"))) {
				var lower = parseInt(jQuery(this).attr("data-amount"));
				raiseTotal -= lower;
				determineClothingChanges(-raiseTotal);
				if (playerStrip > 0) jQuery("#stripTotal").html("+ " + playerStrip + " of your clothing articles");
				else jQuery("#stripTotal").html("");
				if (raiseTotal >= 0) jQuery("#raiseTotal").html(raiseTotal);
				canAfford();
			}
		});
		
		// apply amount
		
		jQuery("#confirmRaise").click(function() {
			if (raiseTotal > 0) {
				updateReady(3);
				var pot = parseInt(jQuery("#pot").html()) + raise;
				jQuery("#pot").html(pot);
				var playerNewMoney = playerMoney - raise;
				jQuery("#playerMoney").html(playerNewMoney);
				updateStatus("raise", raise, playerStrip);
				updateMoney();
				updatePot(pot);
				jQuery("#stay, #raise, #call").attr("disabled", "disabled");
				waitingMessage();
			}
		});
	});
	
	// call
	
	jQuery("#call").click(function() {
		call();
	});
	function call() {
		
		// remove hidden cards from hand
		
		jQuery("#playerHand .cardContainer:nth-child(n+6)").remove();
		
		jQuery("#fold, #raise, #call").attr("disabled", "disabled");
		if (called == false) {
			updateStatus("call");
			called = true;
		}
		jQuery.post(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getOpponentHand.php", {
			opponentNumber: opponentNumber,
			user: user
		}, function(result) {
			text_clean = result.replace(/\\/g, "");
			jQuery("#loadOpponentHand").html(text_clean);
			jQuery("#loadOpponentHand figure").addClass("back");
			index = 1;
			jQuery("#opponentHand .cardContainer").each( function() {
			    var element = jQuery(this);
			    jQuery(".card", element).prepend(jQuery("#loadOpponentHand .cardContainer:nth-of-type(" + index + ") .card").html());
			    jQuery("figure", element).addClass("front");
			    index ++;
			});
			var playerValues;
			var playerPoints;
			var playerHand;
			var playerHighestSetValue;
			var opponentValues;
			var opponentPoints;
			var opponentHand;
			var opponentHighestSetValue;
			
			// add up points of hands
			
			jQuery(".hand").each(function() {
				var values = [];
				values[1] = 0;
				values[2] = 0;
				values[3] = 0;
				values[4] = 0;
				values[5] = 0;
				values[6] = 0;
				values[7] = 0;
				values[8] = 0;
				values[9] = 0;
				values[10] = 0;
				values[11] = 0;
				values[12] = 0;
				values[13] = 0;
				var cards = [];
				var suits = [];
				var x = 0;
				var points;
				var straightExists = false;
				var flushExists = false;
				var hand;
				var uniquePairs = 0;
				var largestMatch = 2;
				var highestSetValue = 0;
				jQuery(".card .back", this).each(function() {
					
					// add to array
					
					cards.push(parseInt(jQuery(this).attr("data-value")));
					
					value = parseInt(jQuery(this).attr("data-value"));
					suit = parseInt(jQuery(this).attr("data-suit"));
					
					// determine matches
					
					values[value] ++;
					if (values[value] >= 2) {
						if (values[value] == 2) uniquePairs ++;
						if (values[value] >= 3) largestMatch ++;
						if (values[value] > highestSetValue) highestSetValue = values[value];
						points += 8192;
					}

					x ++;
					
					// add suits to list
					
					suits.push(suit);
				});
				
				// determine nothing
				
				if (uniquePairs == 0) {
					hand = "nothing";
					points = 0;
				}
				
				// determine 1 pair
				
				if (uniquePairs == 1) {
					hand = "1 pair";
					points = 2;
				}
				
				// determine 2 pairs
				
				if (uniquePairs == 2) {
					hand = "2 pairs";
					points = 3;
				}

				// determine 3 of a kind
				
				if (largestMatch == 3) {
					hand = "3 of a kind";
					points = 4;
				}
				
				// determine straights
				
				if (uniquePairs == 0) {
					cards.sort(function(a, b) {
						return a - b;
					});
					var straight = 1;
					for (x = 0; x <= 5; x++) {
						previousCard = x - 1;
						if (previousCard !== -1 && cards[x] == cards[previousCard] + 1) straight ++;
					}
					if (straight == 5) {
						straightExists = true;
						hand = "a straight";
						points = 5;
					}
				}

				// determine flushes
				
				var startingSuit;
				var sameSuit = 1;
				for (x = 0; x <= suits.length; x++) {
					if (x == 0) startingSuit = suits[x];
					else if (suits[x] == startingSuit) sameSuit ++;
				}
				if (sameSuit == suits.length) {
					flushExists = true;
					hand = "a flush";
					points = 6;
				}
				
				// determine full house
				
				if (uniquePairs == 2 && largestMatch == 3) {
					hand = "a full house";
					points = 7;
				}
				
				// determine 4 of a kind
				
				if (largestMatch == 4) {
					hand = "4 of a kind";
					points = 8;
				}
				
				// straight flush
				
				if (straightExists && flushExists == true) {
					hand = "a straight flush";
					points = 9;
				}
				
				// assign scores and hand descriptions to players

				cards.sort(function(a, b) {
					return b - a;
				});
				var id = jQuery(this).attr("id");
				if (id == "playerHand") {
					playerCards = cards;
					playerPoints = points;
					playerHand = hand;
					playerHighestSetValue = highestSetValue;
				}
				if (id == "opponentHand") {
					opponentCards = cards;
					opponentPoints = points;
					opponentHand = hand;
					opponentHighestSetValue = highestSetValue;
				}
				
			});

			// determine winner
			
			var winner;
			var winnerMoney;
						
			// settle ties

			if (playerPoints == opponentPoints) {
				for (x = 1; x <= 5; x ++) {
					if (playerCards[x] !== opponentCards[x]) {
						if (playerCards[x] > opponentCards[x]) {
							winner = player;
							winnerMoney = "#playerMoney";
							if (playerHand == "nothing" && opponentHand == "nothing") {
								playerHand = "the high card";
								break;
							}
							if (playerHand == opponentHand) {
								playerHand += " with higher cards";
							}
						}
						else {
							winner = opponent;
							winnerMoney = "#opponentMoney";
							if (playerHand == "nothing" && opponentHand == "nothing") {
								opponentHand = "the high card";
								break;
							}
							if (playerHand == opponentHand) {
								opponentHand += " with higher cards";
							}
						}
					}
					if (winner !== undefined) break;
				}
			}
			else {
				if (playerPoints > opponentPoints) {
					winner = player;
					winnerMoney = "#playerMoney";
				}
				else {
					winner = opponent;
					winnerMoney = "#opponentMoney";
				}
			}
			
			// reveal opponent's hand
			
			jQuery("#opponentHand .cardContainer").each(function(e) {
				var element = jQuery(this);
			    setTimeout(function() {
			        jQuery(".card", element).addClass("flipped");
			    }, e * 1000);
			});
			
			// announce winner
			
			setTimeout(function() {
				var person = winner;
				var s = "s";
				var exclamation = "";
				if (winner == player) {
					person = "You";
					s = "";
					exclamation = "!";
				}
				else opponentStrip = 0;
				popbox("<h2>" + person + " win" + s + " $" + jQuery("#pot").html() + " love bucks" + exclamation + "</h2><p>You have " + playerHand + ".</p><p>" + opponent + " has " + opponentHand + ".</p><button class='close'>OK</button>");
				jQuery(".close").click(function() {
					closePopbox();
					jQuery("#discard, #fold, #stay, #raise").attr("disabled", "disabled");
					if (winner == player) jQuery("#deal").removeAttr("disabled");
					else waitingMessage("Waiting on " + opponent + " to deal ...");
				});
				
				// update money
				
				var pot = parseInt(jQuery("#pot").html());
				var newMoney = parseInt(jQuery(winnerMoney).html()) + pot;
				setTimeout(function() {
					if ((winner == player && opponentStrip > 0) || (winner == opponent && playerStrip > 0)) applyClothingChanges(winner);
				}, 1000);
				jQuery("#pot").html(0);
				jQuery(winnerMoney).html(newMoney);
				updateMoney();
				updatePot();
			}, 5000);
		});
	}
	
	// determine clothing changes
	
	function determineClothingChanges(amount) {
		var playerMoney = parseInt(jQuery("#playerMoney").html());
		var playerNewMoney = playerMoney + amount;
		playerStrip = 0;
		for (x = articles - 1; x >= 0; x --) {
			if (playerMoney > interval * x && (playerNewMoney <= interval * x)) {
				playerStrip ++;
			}
		}
		for (x = 0; x <= articles - 1; x ++) {
			if (playerMoney <= interval * x && (playerNewMoney >= interval * x)) {
				playerStrip --;
			}
		}
	};
	
	// apply clothing changes
	
	function applyClothingChanges(winner) {
		if (opponentStrip !== 0 || playerStrip !== 0) {
			var wins = "wins";
			if (winner == player) {
				loser = opponent;
				winner = "You";
				wins = "win";
			}
			else {
				winner = opponent;
				loser = "You";
			}
			var header = "";
			var dressMessage = "";
			var stripMessage = "";
			var s = "";
			var timerOption = false;
			if ((playerStrip > 0 && opponentStrip < 0) || (playerStrip < 0 && opponentStrip > 0)) header = "Double Win for " + winner;
			else if (winner == "You" && opponentStrip > 0 && playerStrip >= 0) header = "For Your Viewing Pleasure";
			else if (loser == "You" && playerStrip > 0 && opponentStrip >= 0) header = "Time to Strip";
			else if (opponentStrip == 0 && playerStrip < 0) {
				if (winner == "You") header = "Lucky for You";
				else header = "How Disappointing";
			}
			if (opponentStrip > 0 || playerStrip > 0) {
				var clothesToStrip;
				if (loser == "You") clothesToStrip = playerStrip;
				else clothesToStrip = opponentStrip;
				if (clothesToStrip !== 1) s = "s";
				stripMessage = "<p>" + loser + " must strip off " + clothesToStrip + " article" + s + " of clothing.</p>";
				timerOption = true;
			}
			if (opponentStrip < 0 || playerStrip < 0) {
				var clothesToDress;
				if (winner == player) clothesToDress = playerStrip;
				else clothesToDress = opponentStrip;
				if (clothesToDress < -1) s = "s";
				dressMessage = "<p>" + winner + " " + wins + " back " + (clothesToDress * (-1)) + " article" + s + " of clothing.</p>";
			}
			if (stripMessage !== "" || dressMessage !== "") {
				if (timerOption == false || loser == "You") popbox("<h2>" + header + "</h2>" + stripMessage + dressMessage + "<button class='close'>OK</button>");
				else popbox("<h2>" + header + "</h2>" + stripMessage + dressMessage + "<p id='timer'></p><button id='useTimer'>Use Timer</button><button class='close' id='done'>Done</button>");
				var timer;
				jQuery("#useTimer").click(function() {
					updateStatus("timer");
					jQuery("#useTimer").remove();
					jQuery("#timer").show();
					t = 60 * opponentStrip;
					function calculateTime() {
						t --;
						minutes = parseInt(t / 60);
						seconds = t % 60;
						if (seconds < 10) seconds = "0" + seconds;
						time = minutes + ":" + seconds;
						jQuery("#timer").html(time);
					}
					calculateTime();
					timer = setInterval(function() {
						calculateTime();
					    if (t == 0) {
					        clearInterval(timer);
					        updateStatus("timerDone");
					        jQuery("body").prepend("<audio autoplay><source src='/wp-content/themes/marriedgames.org/lovers-lane/audio/ding.ogg' type='audio/ogg'></audio>");
					        closePopbox();
					    }
					}, 1000);
				});
				jQuery("#done").click(function() {
					clearInterval(timer);
					updateStatus("timerDone");
				});
			}
		}
	}
	
	// get money
	
	function getMoney() {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getMoney.php", {
			user: user
		}, function(result) {
			result = result.split(",");
			var hisMoney = result[0];
			var herMoney = result[1];
			if (playerGender == "him") {
				jQuery("#playerMoney").html(hisMoney);
				jQuery("#opponentMoney").html(herMoney);
			}
			else {
				jQuery("#playerMoney").html(herMoney);
				jQuery("#opponentMoney").html(hisMoney);
			}
		});
	}
	
	// get pot
	
	function getPot() {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getPot.php", {
			user: user
		}, function(result) {
			jQuery("#pot").html(result);
		});
	}
	
	// update database
	
	function updateDB() {
		hand = jQuery("#playerHand").html();
		if (playerNumber == 1) {
			jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateDB.php", {
				user: user,
				player1hand: hand
			});
		}
		else if (playerNumber == 2) {
			jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateDB.php", {
				user: user,
				player2hand: hand
			});
		}
	}
	
	// update pot
	
	function updatePot(pot) {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updatePot.php", {
			pot: pot,
			user: user
		});
	}
	
	// clear opponent status
	
	function clearOpponentStatus() {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/clearOpponentStatus.php", {
			opponentNumber: opponentNumber,
			user: user
		});
	}
	
	// check for status updates
	
	var lastTime;
	setInterval(function() {
		if (newGame == 1 && playerStartedGame == false) opponentNumber = 1;
		else if (newGame == true && playerStartedGame == true) opponentNumber = 2;
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/checkForUpdates.php", {
			newGame: newGame,
			opponentNumber: opponentNumber,
			user: user
		}, function(result) {
						
			// new game
			
			if (result == "newGame" && startingGame !== true) {
				newGame = 1;
				popbox("<h2>" + opponent + " has restarted the game</h2><button class='close' id='reload'>OK</button>");
				jQuery(".dark").addClass("static");
				jQuery("#reload").click(function() {
					location.reload();
				});
			}
			
			// all other statuses
			
			result = result.split(",");
			time = result[0];
			status = result[1];
			amount = parseInt(result[2]);
			player1ready = result[3];
			player2ready = result[4];
			opponentStrip = parseInt(result[5]);
			if (opponentNumber == 1) opponentReady = player1ready;
			else opponentReady = player2ready;
			
			// move into phase 2
			
			if (phase == 1 && playerReady == 1 && parseInt(opponentReady) !== 0) {
				phase = 2;
				phase2();
			}
			
			// if status is new
			
			if (lastTime !== time && time !== "") {
				lastTime = time;
				
				// player 2 joins
				
				if (status == "join") {
					
					// unmark player 2's status as "join"
					
					jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateStatus.php", {
						playerNumber: opponentNumber,
						action: "",
						amount: "",
						user: user
					});
					if (newGame == true) location.reload();
				}
				
				// reload player 2 upon starting new game
				
				if (status == "reload") {
					jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateStatus.php", {
						playerNumber: opponentNumber,
						action: "",
						amount: "",
						user: user
					}, function() {
						jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateReady.php", {
							playerNumber: 1,
							ready: 0,
							user: user
						});
						location.reload();
					});
				}
				
				// discard
				
				if (status == "discard") {
					var s = "";
					if (amount > 1) s = "s";
					popbox("<h2>" + opponent + " discards " + amount + " card" + s + "</h2><button class='close'>OK</button>");
					jQuery(".popbox");
					getReady();
				}
				
				// fold				
				if (status == "fold") {	
					/***********fold same call************/
				      if (called == false) {	
                        jQuery("#waitingMessage").slideUp();
					    popbox("<h2>" + opponent + " folds</h2><button class='close'>OK</button>");						
						jQuery(".close").click(function() {
							if (called == false) call();
							called = true;
						});
					   }
					  clearOpponentStatus();
					/************************************/
				}	
				
				// stay
				
				if (status == "stay") {
					if (phase == 1)	popbox("<h2>" + opponent + " stays</h2><button class='close'>OK</button>");
					if (phase == 2 && opponentReady == 3) {
						popbox("<h2>" + opponent + " stays</h2><button class='close'>OK</button>");
						updateReady(2);
						phase2();
					}
				}
				
				// raise
				
				if (status == "raise") {
					getMoney();
					getPot();
					var opponentStripMessage = "";
					if (opponentStrip !== 0) {
						var s = "";
						if (opponentStrip !== 1) s = "s";
						opponentStripMessage = "<p>(And throws in " + opponentStrip + " article" + s + " of clothing)</p>";
					}
					var playerMoney = parseInt(jQuery("#playerMoney").html());
					var playerNewMoney = playerMoney - pot;
					var s = "";
					var playerStripMessage = "";
					determineClothingChanges(-amount);
					if (playerStrip > 0) {
						if (playerStrip !== 1) s = "s";
						playerStripMessage = "<p>If you meet this amount, you must throw in " + playerStrip + " article" + s + " of clothing.</p>";
					}
					popbox("<h2>" + opponent + " raises $" + amount + "</h2>" + opponentStripMessage + playerStripMessage + "<button class='close meet'>Meet $" + amount + "</button><button class='close fold'>Fold</button>");
					jQuery(".meet").click(function() {
						var playerMoney = parseInt(jQuery("#playerMoney").html()) - amount;
						jQuery("#playerMoney").html(playerMoney);
						var pot = parseInt(jQuery("#pot").html()) + parseInt(amount);
						jQuery("#pot").html(pot);
						updateMoney();
						updatePot(pot);
						updateStatus("meet", amount);
						updateReady(2);
						phase2();
					});
					jQuery(".fold").click(function() {
						fold();
					});
				}
				
				// meet
				
				if (status == "meet") {
					var opponentStripMessage = "";
					if (opponentStrip !== 0) {
						var s = "";
						if (opponentStrip !== 1) s = "s";
						opponentStripMessage = "<p>(And throws in " + opponentStrip + " article" + s + " of clothing)</p>";
					}
					popbox("<h2>" + opponent + " meets your $" + amount + "</h2>" + opponentStripMessage + "<button class='close meet'>OK</button>");
					getMoney();
					getPot();
				}
				
				// call
				
				if (status == "call") {
					if (called == false) {
						jQuery("#waitingMessage").slideUp();
						popbox("<h2>" + opponent + " calls</h2><button class='close'>OK</button>");
						jQuery(".close").click(function() {
							if (called == false) call();
							called = true;
						});
					}
					clearOpponentStatus();
				}
				
				// deal
				
				if (status == "deal") {
					deal();
				}
				
				// reset money
				
				if (status == "resetMoney") {
					closePopbox();
					popbox("<h2>" + opponent + " reset the love bucks to $400</h2><button class='close'>OK</button>");
					getMoney();
					jQuery("#pot").html(0);
					jQuery(".close").click(function() {
						deal();
						if (newGame == 1) updateStatus("reload");
					});
				}
				
				// reset-deal
				
				if (status == "reset-deal") {
					popbox("<h2>" + opponent + " reset the love bucks</h2><button class='close'>OK</button>");
					jQuery(".close").click(function() {
						resetMoneyQuick();
						deal();
					});
				}
				
				// timer
				
				if (status == "timer") {
					popbox("<h2>" + opponent + " has started the timer</h2><p>Perform a striptease until you hear a bell.</p>");
					getMoney();
				}
				
				// timer done
				
				if (status == "timerDone") {
					jQuery("body").prepend("<audio autoplay><source src='/wp-content/themes/marriedgames.org/lovers-lane/audio/ding.ogg' type='audio/ogg'></audio>");
					closePopbox();
				}
				
			}
		});
	}, 1000);
	
	// check if opponent is ready
	
	function getReady() {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getReady.php", {
			opponentNumber: opponentNumber,
			user: user
		}, function(result) {
			return result;
		});
	}
	
	// get turn
	
	function getTurn() {
		jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/getTurn.php", {
			user: user
		}, function(result) {
			turn = result;
		});
	}
	
	// reset money
	
	function resetMoney() {
		closePopbox();
		jQuery("#playerMoney").html(400);
		jQuery("#opponentMoney").html(400);
		jQuery("#pot").html(0);
		updateMoney();
		updateStatus("resetMoney");
		deal();
	}
		
});

// update status

function updateStatus(action, amount, clothes) {
	if (amount == undefined) amount = "";
	jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateStatus.php", {
		playerNumber: playerNumber,
		action: action,
		amount: amount,
		playerStrip: playerStrip,
		user: user
	});
}

// update money

function updateMoney() {
	playerMoney = parseInt(jQuery("#playerMoney").html());
	opponentMoney = parseInt(jQuery("#opponentMoney").html());
	jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/updateMoney.php", {
		playerGender: playerGender,
		opponentGender: opponentGender,
		playerMoney: playerMoney,
		opponentMoney: opponentMoney,
		user: user
	});
}

// delete game

function deleteGame() {
	jQuery("#load").load(gethttp+"//wp-content/themes/marriedgames.org/strip-poker/deleteGame.php", {
		user: user
	}, function() {
		location.reload();
	});
}