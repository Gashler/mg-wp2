<?php
	$user = S2MEMBER_CURRENT_USER_ID;
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	if ($husband == null) $husband = "Zack";
	if ($wife == null) $wife = "Vanessa";
	$cardWidth = 150;
	$cardHeight = $cardWidth * 1.4;
	
	$con = mysqli_connect("localhost","root","asdf","mg");
	// Check connection
	if (mysqli_connect_errno()) {
	  echo "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	$player;
	$opponent;
	$playerGender;
	$opponentGender;
	$newGame;
	$player1id;
	$player2id;
	$player1gender;
	$player2gender;
	$playerNumber;
	$opponentNumber;
	$addingPlayer2;
	$resetGame;
	$phase;
	$turn;
	$player1ready;
	$player2ready;
	$playerReady;
	$opponentReady;
	$turn;
	$firstBet;
	$firstGame;
	$playerStrip;
	$opponentStrip;
	$sql = "SELECT * FROM stripPoker WHERE user = '$user'";
	$result = $con->query($sql);
	while($row = $result->fetch_assoc()) {
		$player1id = $row['player1id'];
		$player2id = $row['player2id'];
		$player1gender = $row['player1gender'];
		$player2gender = $row['player2gender'];
		$phase = $row['phase'];
		$turn = $row['turn'];
		$player1status = $row['player1status'];
		$player2status = $row['player2status'];
		$player1ready = $row['player1ready'];
		$player2ready = $row['player2ready'];
		$turn = $row['turn'];
		$firstBet = $row['firstBet'];
		$firstGame = $row['firstGame'];
		$playerStrip = $row['playerStrip'];
		$opponentStrip = $row['opponentStrip'];
	}
	if ($player1gender !== "" && $player2gender == "") {
		if ($player1gender == "m") $player2gender = "f";
		else $player2gender = "m";
	}
	
	// if no game is saved
	
	if (!(isset($player1id))) {
		$newGame = true;
		$playerNumber = 1;
		$opponentNumber = 2;
		$playerReady = $player1ready;
		$opponentReady = $player2ready;
		$playerStatus = $player1status;
		$opponentStatus = $player2status;
	}
	
	// if player 1 is setup but player 2 is not and a second user logs on
	
	else if (($player1id !== "") && ($player2id == "") && $_SESSION['playerId'] !== $player1id) {
		$playerNumber = 2;
		$playerReady = $player2ready;
		$opponentReady = $player1ready;
		$playerStatus = $player2status;
		$opponentStatus = $player1status;
		$opponentNumber = 1;
		$addingPlayer2 = true;
		if ($player1gender == "m") {
			$player = $wife;
			$playerGender = "her";
			$opponentGender = "him";
			$opponent = $husband;
		}
		else {
			$player = $husband;
			$playerGender = "him";
			$opponentGender = "her";
			$opponent = $wife;
		}
	}
		
	// reset game after terminating sessions
	
	if ((isset($player1id) && $player2id !== "") && ($_SESSION['playerId'] !== $player1id && $_SESSION['playerId'] !== $player2id)) $resetGame = true;
	
	// resume game
	
	if (isset($_SESSION['playerId'])) {
		if ($player1id == $_SESSION['playerId']) {
			$playerNumber = 1;
			$opponentNumber = 2;
			$playerReady = $player1ready;
			$opponentReady = $player2ready;
			$playerStatus = $player1status;
			$opponentStatus = $player2status;
			if ($player1gender == "m") {
				$player = $husband;
				$playerGender = "him";
				$opponent = $wife;
				$opponentGender = "her";
			}
			else {
				$player = $wife;
				$playerGender = "her";
				$opponent = $husband;
				$opponentGender = "him";
			}
		}
		if ($player2id == $_SESSION['playerId']) {
			$playerNumber = 2;
			$opponentNumber = 1;
			$playerReady = $player2ready;
			$opponentReady = $player1ready;
			$playerStatus = $player2status;
			$opponentStatus = $player1status;
			if ($player2gender == "m") {
				$player = $husband;
				$playerGender = "him";
				$opponent = $wife;
				$opponentGender = "her";
			}
			else {
				$player = $wife;
				$playerGender = "her";
				$opponent = $husband;
				$opponentGender = "him";
			}
		}
	}
		
	// create player
	
	else {
		function generateRandomString($length = 10) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, strlen($characters) - 1)];
		    }
		    return $randomString;
		}
		$_SESSION['playerId'] = generateRandomString();
	}
?>


<style>
	
	h1 { float:left; }
	h2 { margin:0; }
	.popbox button { margin-top:.5em; }
	.popbox button.small { margin-top:0; }
	span { display:inline-block; }
	#gameContent { display:inline-block; max-width:775px; }
	#loadOpponentHand { display:none; }
	#timer { font-size:2em; text-align:center; display:none; }
	#playerHand .cardContainer:nth-of-type(n+6) { display:none; }
	.hand { width:835px; height:216px; margin-top:.5em; }
	.cardContainer { margin-right:5px; perspective:800px; -webkit-perspective:800px; display:inline-block; position:relative; width:<?php echo $cardWidth ?>px; height:<?php echo $cardHeight ?>px; }
	.card { transform-style:preserve-3d; -webkit-transform-style:preserve-3d; transition: 1s; -webkit-transition: 1s; position:absolute; width:100%; height:100%; }
	.card.draw { transition:.5s; }
	.card figure { display: block; position: absolute; width: 100%; height: 100%; backface-visibility: hidden; -webkit-backface-visibility: hidden; background:url("<?php echo get_template_directory_uri() ?>/images/strip-poker/cards.png"); background-size:2100px; box-shadow:1px 1px 4px rgba(0,0,0,.5); border-radius:8px; }
	.card .front { -webkit-transform: translate3d(0, 0, 0); }
	.card .back { transform: rotateY(180deg); -webkit-transform: rotateY(180deg); }
	.card.draw figure { transform: rotateY(90deg); -webkit-transform: rotateY(90deg); }
	.card.flipped { transform: rotateY(180deg); -webkit-transform: rotateY(180deg); }
	.card.draw { transform: rotateY(-90deg); -webkit-transform: rotateY(-90deg); }
	.popbox table { margin:0 auto; }
	.popbox table tr td { background:none !important; padding:5px; }
	.popbox table tr th { text-align:right; background:none; padding:5px; color:black; font-weight:bold; }
	.discard { position:absolute; z-index:1; top:0; right:0; bottom:0; left:0; border-radius:8px; text-align:right; color:white; background:red; font-weight:bold; font-size:2em; background:rgba(255,0,0,.5); }
	.discardX { position:absolute; top:5px; right:5px; text-shadow:1px 1px 3px black; }
	.flash { position:relative; }
	button { margin:0 5px 5px 0; }
	
	/* IE10 & IE11 */
	@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
		/* entire container, keeps perspective */
		.cardContainer {
			perspective: 1000;
			transform-style: preserve-3d;
		}
		
		/*  UPDATED! flip the pane when hovered */
		.cardContainer .card.flipped figure.back {
			transform: rotateY(180deg) !important;
			backface-visibility: visible !important;
		}
		.cardContainer .card.flipped figure.front {
		    transform: rotateY(180deg) !important;
		}
		
		/* flip speed goes here */
		.card {
			transition: 1s;
			transform-style: preserve-3d;
			position: relative;
		}
		
		/* hide back of pane during swap */
		.front, .back {
			backface-visibility: hidden;
			transition: 1s;
			transform-style: preserve-3d;
			position: absolute;
			top: 0;
			left: 0;
		}
		
		/*  UPDATED! front pane, placed above back */
		.front {
			z-index: 2;
			transform: rotateY(0deg);
		}
		
		/* back, initially hidden pane */
		.back {
			transform: rotateY(-180deg);
		}
	}
	
	@media (max-width:600px) {
		h1 { display:none; }
		h2 { font-size:18pt; }
		#topHeaders { float:left; }
		#clear1, #clear2, #clear3 { display:none; }
		#clear4 { display:block !important; }
		#options { clear:both; float:none !important; }
		#options br { display:block !important; }
	}
	
	@media (max-width:350px) {
		#waitingMessageContainer { margin-bottom:2em; }
	}
	
</style>
<div id="waitingForOpponentToJoin" <?php if (!($phase == 0 && $player1ready == 1 && $player2ready == 0)) { ?>style="display:none;"<?php } ?>>
	<div id="clear1" class="clear"></div>
	<p class="flash">Waiting for <span class="opponent"><?php echo $opponent ?></span> to join ... <a href="#" id="cancelWaitingForOpponent">Cancel</a></p>
</div>
<div id="gameContent" <?php if ($phase == 0 && $player2ready == 0) { ?>style="opacity:.01;"<?php } ?>>
	<h1 style="float:left;">Strip Poker</h1>
	<button id="settings" class="small" style="float:right;"><span style="background-image: url(<?php echo $wsiteurl; ?>/wp-content/themes/marriedgames.org/images/icons.png); background-position: -120px -72px; height:24px; width:24px; background-size: 240px 240px;"></span></button>
	<div id="settingsContent" style="display:none;">
		<button class="white" id="newGame" onclick="startingGame = true; deleteGame();">New Game</button>
		<button class="white" id="resetMoney">Reset Money</button>
	</div>
	<div id="clear2" class="clear"></div>
	<div id="load" style="display:none;"></div>
	<div id="clear3" class="clear"></div>
	<div id="topHeaders">
		<h2 id="opponentHandHeader" style="display:inline-block;">
			<span class="label"><?php echo $opponent ?></span>: $
			<span id="opponentMoney">
				<?php
					$sql = "SELECT $opponentGender FROM sexCash WHERE user = $user";
					$result = $con->query($sql);
					if($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
							echo $money = $row[$opponentGender];
						}
					}
					if ($money == null) echo 0; 
				?>
			</span>
		</h2>
		<h2 style="display:inline-block;">&nbsp; | &nbsp;Pot: 
			<span style="font-weight:400;">$<span id="pot">
				<?php
					$sql = "SELECT pot FROM stripPoker WHERE user = $user";
					$result = $con->query($sql);
					if($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
							echo $pot = $row['pot'];
						}
					}
					if ($pot == null) echo 0; 
				?>
			</span>
			</span>
		</h2>
	</div>
	<div id="clear4" class="clear" style="display:none;"></div>
	<div id="loadOpponentHand"></div>
	<div id="opponentHand" class="hand">
		<?php
			for ($x = 1; $x <= 5; $x ++) {
				echo '<div class="cardContainer"><div class="card"><figure style="background-position:-1950px -420px"></figure></div></div>';
			}
		?>
	</div>
	<div id="waitingMessageContainer" style="height:24px;">
		<p id="waitingMessage" class="flash" style="display:none; margin-bottom:0;"></p>
	</div>
	<div style="margin-top:10%;" id="playerHandContainer">
		<h2 id="playerHandHeader" style="float:left; margin-top:0;">You: $
			<span id="playerMoney">
				<?php
					$sql = "SELECT $playerGender FROM sexCash WHERE user = $user";
					$result = $con->query($sql);
					if($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
							echo $money = $row[$playerGender];
						}
					}
					if ($money == null) echo 0;
				?>
			</span>	
		</h2>
		<nav id="options" style="float:right;">
			<button autocomplete="off" id="discard">Discard</button>
			<button autocomplete="off" id="fold">Fold</button>
			<button autocomplete="off" id="stay">Stay</button><br style="display:none;">
			<button autocomplete="off" id="raise">Raise</button>
			<button autocomplete="off" id="call">Call</button>
			<button autocomplete="off" id="deal">Deal</button>
		</nav>
		<div class="clear"></div>
		<div id="playerHand" class="hand">
			<?php
				$sql = "SELECT * FROM stripPoker WHERE user = $user";
				$result = $con->query($sql);
				while($row = $result->fetch_assoc()) {
					if ($_SESSION['playerId'] == $player1id) echo base64_decode($row['player1hand']);
					if ($_SESSION['playerId'] == $player2id) echo base64_decode($row['player2hand']);
				}
			?>
		</div>
	</div>
</div><!-- gameContent -->
<script>	
	var husband = "<?php echo $husband ?>";
	var wife = "<?php echo $wife ?>";
	var player1gender = "<?php echo $player1gender ?>";
	var player2gender = "<?php echo $player2gender ?>";
	var newGame = "<?php echo $newGame ?>";
	var playerId = "<?php echo $_SESSION['playerId'] ?>";
	var playerNumber = "<?php echo $playerNumber ?>";
	var opponentNumber = "<?php echo $opponentNumber ?>";
	var addingPlayer2 = "<?php echo $addingPlayer2 ?>";
	var user = "<?php echo $user ?>";
	var player = "<?php echo $player ?>";
	var playerGender = "<?php echo $playerGender ?>";
	var opponent = "<?php echo $opponent ?>";
	var opponentGender = "<?php echo $opponentGender ?>";
	var cardWidth = "<?php echo $cardWidth ?>";
	var cardHeight = "<?php echo $cardHeight ?>";
	var resetGame = "<?php echo $resetGame ?>";
	var phase = "<?php echo $phase ?>";
	var turn = "<?php echo $turn ?>";
	var playerReady = "<?php echo $playerReady ?>";
	var opponentReady = "<?php echo $opponentReady ?>";
	var playerStatus = "<?php echo $playerStatus ?>";
	var opponentStatus = "<?php echo $opponentStatus ?>";
	var turn = "<?php echo $turn ?>";
	var firstBet = "<?php echo $firstBet ?>";
	var firstGame = "<?php echo $firstGame ?>";
	var playerStrip = parseInt("<?php echo $playerStrip ?>");
	var opponentStrip = parseInt("<?php echo $opponentStrip ?>");	
</script>
<script src="<?php echo get_template_directory_uri() ?>/strip-poker/js.js"></script>