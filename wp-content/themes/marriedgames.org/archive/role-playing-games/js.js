/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/// pop

function popbox(content) {
	jQuery("body").prepend("<div class='dark'><div class='popbox'>" + content + "</div></div>");
	jQuery(".dark, .popboxContainer").fadeIn(function() {
		jQuery(".dark").click(function() {
			jQuery(".dark").fadeOut(function() {
				jQuery(".dark").remove();
			});
		});
	});
}

// end pop

// new map

jQuery("#newMap").click(function() {
	var c = confirm("Overrite existing map?");
	if (c == true) {
		var size = jQuery("#rpgSelectDimensions").val();
		jQuery("#rpgGridContainer > div > div").load("/wp-content/themes/marriedgames.org/role-playing-games/generateMap.php", {
			size: size,
			tileWidth: tileWidth,
			borderWidth: borderWidth
		}, function() {
			grid = jQuery("#rpgGridContainer > div > div").html();
			jQuery("#load").load("/wp-content/themes/marriedgames.org/role-playing-games/updateRow.php",{
				user: user,
				id: gameID,
				grid: grid
			}, function(result) {
				location.reload();
			});
		});
    }
});

// activate button

var toolbarButton;
var unitIcon;
var addType;
buttonActive = false;
jQuery(".toolbarButton").click(function() {
	toolbarButton = jQuery(this).attr("data-unit");
	if (jQuery(this).hasClass("unitButton")) {
		unitIcon = jQuery(this).html();
		addType = jQuery(this).attr("data-type");
	}
	if (!(jQuery(this).hasClass("active"))) {
		jQuery("button.active").removeClass("active");
		jQuery(this).addClass("active");
		buttonActive = true;
	}
	else deactivateButton(jQuery(this));
});	

// deactivate button

function deactivateButton() {
	jQuery("button.active").removeClass("active");
	toolbarButton = undefined;
	buttonActive = false;
}

// add units

var id = jQuery("#id").html();
var justAdded = false;
jQuery("#rpgGrid td").click(function() {
	if (jQuery(".toolbarButton.active").hasClass("unitButton")) {
		// tile units
		if (jQuery(this).hasClass("tile") && addType == "tile") {
			jQuery(this).append("<div data-unit='" + id + "' class='unit' data-type='" + toolbarButton + "'>" + unitIcon + "</div>");
		}
		// border units
		else if (jQuery(this).hasClass("border") && addType == "border") {
			jQuery(this).append("<div data-unit='" + id + "' class='unit' data-type='" + toolbarButton + "'></div>");
		}
		if (!(jQuery("#buttonsStayPressed").is(":checked"))) deactivateButton();
		id ++;
		jQuery("#id").html(id);
		justAdded = true;
		
		// drag and drop units
		
		jQuery(function() {
			var unit;
			jQuery(".unit").draggable({
		    	revert: 'invalid',
			    start:  function() {
			    	
					// if dragging from inventory, turn off overflow scroll property of parent
					
					if (jQuery(this).parents("#inventory").length > 0) {
						jQuery(this).parents(".overflow").css("overflow", "visible");
					}
			    }, 
			    stop:   function() {
					
					// if dragging from inventory, reset parent back to overflow scroll
					
					if (jQuery(this).parents("#inventory").length > 0) {
						jQuery(this).parents(".overflow").css("overflow", "auto");
					}
			    } 
			});
			jQuery("#rpgGrid td, .unit").droppable({
				drop : function(event, ui) {
					jQuery(ui.draggable).css({
						"top": "0",
						"left": "0"
					});
					
					// add to inventory
					
					if (jQuery(this).hasClass("unit")) {
						var target = jQuery(this).attr("data-unit");

						// check receiving unit for logic
						
						var subject = jQuery(ui.draggable).attr("data-unit");
						alert("a");
						logic("Give", target, subject);
						
						// create inventory container for this unit if doesn't already exist
						
						if (jQuery("#inventory .unitInfo[data-unit='" + target + "']").length == 0) {
							jQuery("#inventory").append("<div class='unitInfo' data-unit='" + target + "'></div>");
						}
						
						// add label when putting in inventory

						var name = jQuery(ui.draggable).attr("data-name");
						if (name == undefined) name = jQuery(ui.draggable).attr("data-type");
						jQuery(ui.draggable).append("<span class='name'> " + name + "</span>");
						jQuery("#inventory .unitInfo[data-unit='" + target + "']").append(ui.draggable);
					}
					else {
						
						// remove label when putting back into grid
						
						jQuery(" .name", ui.draggable).remove();
							
						// if dragging from inventory, reset parent back to overflow scroll
						if (jQuery("#rpgContent .overflow").css("overflow") === "visible") {
							jQuery("#rpgContent .overflow").css("overflow", "auto");
						}
						jQuery(this).append(ui.draggable);
					}
				}
			});
		});
		
	}
});

jQuery("#buttonsStayPressed").change(function() {
	deactivateButton();
});

// delete units

jQuery("#rpgGrid").on({
	click: function() {
		if (toolbarButton == "delete") {
			jQuery(this).remove();
			if (!(jQuery("#buttonsStayPressed").is(":checked"))) deactivateButton();
		}
	}
}, ".unit");
jQuery(".unit").click(function() {
	if (toolbarButton == "delete") {
		jQuery(this).remove();
		if (!(jQuery("#buttonsStayPressed").is(":checked"))) deactivateButton();
	}
});

// switch tabs

jQuery(".tabs-menu > li").click(function() {
    jQuery(".tabs-menu > li.active").removeClass("active");
    jQuery(this).addClass("active");
    var tab = jQuery(this).attr("data-tab");
    jQuery(".tabs-content > div.active").removeClass("active");
    jQuery(".tabs-content > div[data-tab='" + tab + "']").addClass("active");
});

// activate tab content

function showTabContent(unit) {
	jQuery(".tabContent .unitInfo").hide();
	jQuery(".tabContent, .tabContent .unitInfo[data-unit='" + unit + "']").show();
	jQuery(".selectAUnit").hide();
}
function hideTabContent() {
	jQuery(".tabContent, .tabContent .unitInfo").hide();
	jQuery(".selectAUnit").show();	
}

// select cells

var unitActive = false;
var unit;
var selectingLogicUnit = false;

// select units

jQuery("#rpgGrid .unit, #rpgGrid td").click(function(e) {
	if (selectingLogicUnit == false && buttonActive == false && justAdded == false) {
		if (justAdded == true) justAdded = false;
		if (jQuery(this) == jQuery("#rpgGrid .unit")) e.stopPropagation();
		unit = jQuery(this).attr("data-unit");
		if (!(jQuery(this).hasClass("active"))) {
			jQuery("#rpgGrid td, .unit.active").removeClass("active");
			jQuery(this).addClass("active");
			unitActive = true;
			showTabContent(unit);
		}
		else {
			jQuery(this).removeClass("active");
			unitActive = false;
			hideTabContent();
		}
	}
	else if (justAdded == true) justAdded = false;
});
jQuery("#rpgGrid").on({
	click: function(e) {
		if (selectingLogicUnit == false && buttonActive == false && justAdded == false) {
			if (justAdded == true) justAdded = false;
			e.stopPropagation();
			unit = jQuery(this).attr("data-unit");
			if (!(jQuery(this).hasClass("active"))) {
				jQuery("#rpgGrid td, .unit.active").removeClass("active");
				jQuery(this).addClass("active");
				unitActive = true;
				showTabContent(unit);
			}
			else {
				jQuery(this).removeClass("active");
				unitActive = false;
				hideTabContent();
			}
		}
		else if (justAdded == true) justAdded = false;
	}
}, ".unit");

// actions

jQuery("#actions button").click(function() {
	buttonActive = true;
	jQuery("#actions button.active").removeClass("active");
	var action = jQuery(this).attr("data-action");
	jQuery(this).addClass("active");
	jQuery(".unit, .tile").on("click", false);
	jQuery(".unit, .tile").click(function() {
		if (buttonActive == true) {
			alert("woop");
			logic(action, jQuery(this).attr("data-unit")/*, jQuery(".tile.active, .unit.active").attr("data-unit")*/);
			jQuery("#actions button.active").removeClass("active");
			setTimeout(function() {
				buttonActive = false;
				jQuery(".unit, .tile").on("click", true);
			}, 500);
		}
	});
});

// read and update unit content

jQuery("#rpgGrid").on({
	click: function() {
		if (jQuery(this).hasClass("active")) {
			var description = jQuery(this).attr("data-description");
			var type = jQuery(this).attr("data-type");
			var name = jQuery(this).attr("data-name");
			jQuery("#unitDescription").val(description);
			jQuery("#unitType").val(type);
			jQuery("#unitName").val(name);
			jQuery("#rpgContent input, #rpgContent textarea").keyup(function() {
				var attribute = jQuery(this).attr("data-attribute");
				var value = jQuery(this).val();
				jQuery(".unit.active, #rpgGrid td.active").attr("data-" + attribute, value);
			});
		}
	}
}, ".unit, td");
jQuery(".unit, #rpgGrid td").click(function() {
	if (jQuery(this).hasClass("active")) {
		var description = jQuery(this).attr("data-description");
		var type = jQuery(this).attr("data-type");
		var name = jQuery(this).attr("data-name");
		jQuery("#unitDescription").val(description);
		jQuery("#unitType").val(type);
		jQuery("#unitName").val(name);
		jQuery("#rpgContent input, #rpgContent textarea").keyup(function() {
			var attribute = jQuery(this).attr("data-attribute");
			var value = jQuery(this).val();
			jQuery(".unit.active, #rpgGrid td.active").attr("data-" + attribute, value);
		});
	}
});

// color picker

jQuery(".colorPicker li").click(function() {
	var color = jQuery(this).css("background-color");
	jQuery(".unit[data-unit='" + unit + "'], .tile.active").css("background-color", color);
});


// drag and drop units

jQuery(function() {
	var unit;
	jQuery(".unit").draggable({
    	revert: 'invalid',
	    start:  function() {
	    	
			// if dragging from inventory, turn off overflow scroll property of parent
			
			if (jQuery(this).parents("#inventory").length > 0) {
				jQuery(this).parents(".overflow").css("overflow", "visible");
			}
	    }, 
	    stop:   function() {
			
			// if dragging from inventory, reset parent back to overflow scroll
			
			if (jQuery(this).parents("#inventory").length > 0) {
				jQuery(this).parents(".overflow").css("overflow", "auto");
			}
	    } 
	});
	jQuery("#rpgGrid td, .unit").droppable({
		drop : function(event, ui) {
			jQuery(ui.draggable).css({
				"top": "0",
				"left": "0"
			});
			
			// add to inventory
			
			if (jQuery(this).hasClass("unit")) {
				var target = jQuery(this).attr("data-unit");

				// check receiving unit for logic
				
				var subject = jQuery(ui.draggable).attr("data-unit");
				logic("Give", target, subject);
				
				// create inventory for this unit if doesn't already exist
				
				if (jQuery("#inventory .unitInfo[data-unit='" + target + "']").length == 0) {
					jQuery("#inventory").append("<div class='unitInfo' data-unit='" + target + "'></div>");
				}
				
				// add label when putting in inventory

				var name = jQuery(ui.draggable).attr("data-name");
				if (name == undefined) name = jQuery(ui.draggable).attr("data-type");
				jQuery(ui.draggable).append("<span class='name'> " + name + "</span>");
				jQuery("#inventory .unitInfo[data-unit='" + target + "']").append(ui.draggable);
			}
			else {
				
				// remove label when putting back into grid
				
				jQuery(" .name", ui.draggable).remove();
					
				// if dragging from inventory, reset parent back to overflow scroll
				if (jQuery("#rpgContent .overflow").css("overflow") === "visible") {
					jQuery("#rpgContent .overflow").css("overflow", "auto");
				}
				jQuery(this).append(ui.draggable);
			}
		}
	});
});

// save game
		
jQuery("#save").click(function() {
	jQuery("#rpgGrid .active").removeClass("active");
	grid = jQuery("#rpgGridContainer > div > div").html();
	inventory = jQuery("#inventory").html();
	logic = jQuery("#logic #rules").html();
	jQuery("#load").load("/wp-content/themes/marriedgames.org/role-playing-games/updateRow.php",{
		user: user,
		id: gameID,
		grid: grid,
		inventory: inventory,
		logic: logic
	}, function(result) {
		alert("Game Saved");
	});
});

/////////////////////////// logic //////////////////////////////

// add rule

var rule = parseInt(jQuery("#rule").html());
jQuery("#addRule").click(function() {
	
	// if rule container for this unit doesn't yet exist, add one
	
	if (jQuery("#rules .unitInfo[data-unit='" + unit + "']").length == 0) jQuery("#rules").append("<dl class='unitInfo' data-unit='" + unit + "'></dl>");
	
	// add rule
	
	jQuery("#rules .unitInfo[data-unit='" + unit + "']").append('<dt><div class="x">&times;</div>Rule ' + rule + '</dt><dd class="rule" id="' + id + '" data-rule="' + rule + '"></dd>');
	jQuery("#id").html(id ++);
	rule ++;
	jQuery("#rule").html(rule);
	
	// toogle new dd
	
	toggleDd(jQuery("#rules .unitInfo[data-unit='" + unit + "'] dt:last-of-type"), true);
	
	// toggle definition list items (for dynamic content)
	
	jQuery("dt").click(function() {
		toggleDd(jQuery(this));
	});
	
	// close definition list item for dynamic content
	
	jQuery(".x").click(function() {
		jQuery(this).closest("dt").slideUp(function() {
			jQuery(this).next("dd").slideUp(function() {
				jQuery(this).remove();
			});
			jQuery(this).remove();
		});
	});
		
	// add event container and selector

	jQuery("#rules .unitInfo[data-unit='" + unit + "'] dd.rule:last-of-type").append('' +
		'<p><label><input class="logicRunRuleIf" type="checkbox">Run Rule If ...</label></p>' +
		'<div class="logicRunRuleIfConditions"></div>' +
		'<div class="container logicEvent" id="' + id + '"></div>' +
	'');
	logicAddEvent("#" + id);
	jQuery("#id").html(id ++);

	// add result container and selector
	
	jQuery("#rules .unitInfo[data-unit='" + unit + "'] dd.rule:last-of-type").append('<div class="container logicResult" id="' + id + '"></div>');
	logicAddEvent("#" + id);
	jQuery("#id").html(id ++);
	
	// add completion selector
	
	jQuery("#rules .unitInfo[data-unit='" + unit + "'] dd.rule:last-of-type").append("" +
		"<label>Then Set Rule As:</label>" +
		"<select class='logicSelectUponCompletion'>" +
			"<option>Uncompleted</option>" +
			"<option>Completed</option>" +
			"<option>Disabled</option>" +
		"</select>" +
		"<label>Current Rule Status:</label>" +
		"<select class='logicSelectRuleStatus'>" +
			"<option>Uncompleted</option>" +
			"<option selected>Completed</option>" +
			"<option>Disabled</option>" +
		"</select>" +
	"");

	// apply selects (for generated content)
	
	jQuery(".logicSelectUponCompletion, .logicSelectRuleStatus").change(function() {
		applySelect(jQuery(this));
	});
	
	// select run rule if conditions (for generated content)
	
	jQuery(".logicRunRuleIf").change(function() {
		applyCheckbox(jQuery(this));
		if (jQuery(this).is(':checked')) logicRunRuleIfConditions(jQuery(this));
		else jQuery(".logicRunRuleIfConditions", jQuery(this).closest(".rule")).html("");
	});
	
});

// select run rule if conditions (for static content)

jQuery(".logicRunRuleIf").change(function() {
	if (jQuery(this).is(':checked')) logicRunRuleIfConditions(jQuery(this));
	else jQuery(".logicRunRuleIfConditions", jQuery(this).closest(".rule")).html("");
});

function logicRunRuleIfConditions(element) {
	jQuery(element).after('<div class="logicRunRuleIfConditions">' +
			'<label>Rule:</label>' +
			'<select class="logicRunRuleIfRule"></select>' +
			'<label>Is:</label>' +
			'<select class="logicRunRuleIfRuleStatus">' +
				'<option>Completed</option>' +
				'<option>Active</option>' +
				'<option>Disabled</option>' +
			'</select>' +
			'<select class="logicRunRuleIfConjunction">' +
				'<option>Add another condition</option>' +
				'<option>And</option>' +
				'<option>Or</option>' +
			'</select>' +
		'</div>');

	// list other rules
	
	if (jQuery("#rules .rule").length > 1) {
		jQuery("#rules .rule").each(function() {
			//if (jQuery(this).attr("data-rule") !== jQuery(rule).attr("data-rule")) {
				jQuery(".logicRunRuleIfRule").append("<option>" + jQuery(this).attr("data-rule") + "</option>");
			//}
		});
	}
	/*
	else {
		jQuery(".logicRunRuleIfRule", rule).append("<option>There are no other rules</option>");
	}
	*/
	
	// apply selects (for generated content)
	
	jQuery("select", rule).change(function() {
		applySelect(jQuery(this));
	});
	
	// add another rule condition (for generated content)
	
	jQuery(".logicRunRuleIfConjunction").change(function() {
		alert("beeb");
		logicRunRuleIfConditions(jQuery(this));
	});

}

// add another rule condition (for static content)

jQuery(".logicRunRuleIfConjunction").change(function() {
	logicRunRuleIfConditions(jQuery(this));
});

// toggle definition list items

function toggleDd(element, newElement) {
	var display = jQuery(element).next("dd").css("display");
	if (display == "none") {
		if (newElement == undefined) {
			jQuery(element).parent().children("dd").slideUp();
			jQuery(element).next("dd").slideToggle();
		}
		else {
			jQuery(element).parent().children("dd").hide();
			jQuery(element).next("dd").show();
		}
	}
}

// toggle definition list items (for static content)

jQuery("dt").click(function() {
	toggleDd(jQuery(this));
});

// close definition list item for static content

jQuery(".x").click(function() {
	jQuery(this).closest("dt").slideUp(function() {
		jQuery(this).next("dd").slideUp(function() {
			jQuery(this).remove();
		});
		jQuery(this).remove();
	});
});

// add select event

function logicAddEvent(container) {
	if (jQuery(container).hasClass("logicEvent")) var label = "Upon Event";
	else label = "Result";
	
	jQuery(container).html('' +
		'<label>' + label + ':</label>' +
		'<select class="logicSelectEvent" id=' + (id++) + '>' +
			'<option>Select Event</option>' +
			'<option>Dialog</option>' +
			'<option>Give</option>' +
			'<option>Message</option>' +
			'<option>Remove</option>' +
		'</select>' +
		'<div class="logicMessageCondition"></div>' +
		'<div class="logicMessage"></div>' +
		'<div class="logicSubject"></div>' +
		'<div class="logicRemoveCondition"></div>' +
		'<div class="logicRemove"></div>' +
		'<div class="logicFromCondition"></div>' +
		'<div class="logicFrom"></div>' +
		'<div class="logicToCondition"></div>' +
		'<div class="logicTo"></div>' +
	'');
	
	// upon selecting event, load subject fields (for generated content)
	
	jQuery(".logicSelectEvent").off("change"); // this somehow prevents the function for firing for every matched element
	jQuery(".logicSelectEvent").change(function() {
		applySelect(jQuery(this));
		logicAddConditions("#" + jQuery(this).parents(".container").attr("id"), jQuery(this));
	});
	
}

// upon selecting event, load subject fields (for static content)

jQuery(".logicSelectEvent").change(function() {
	logicAddConditions(jQuery(this).closest(".container"), jQuery(this));
});

// add condition

function logicAddConditions(container, element) {
	jQuery(".logicMessageCondition, .logicMessage, .logicSubject, .logicRemoveCondition, .logicRemove, .logicFromCondition, .logicFrom, .logicToCondition, .logicTo", container).html("");
	
	// message / dialog

	if (jQuery(element).val() == "Message" || jQuery(element).val() == "Dialog") {
		if (jQuery(container).hasClass("logicEvent")) {
			jQuery(".logicMessageCondition", container).html('' +
				'<label>If Content:</label>' +
				'<select class="logicSelectMessageConditon" id="' + (id++) + '">' +
					'<option>Is any</option>' +
					'<option>Is</option>' +
					'<option>Is not</option>' +
					'<option>Contains</option>' +
					'<option>Does not contain</option>' +
				'</select>' +
			'');
			
			// select message condition (for generated content)
			
			jQuery(".logicSelectMessageConditon").change(function() {
				var element = "#" + jQuery(this).attr("id");
				logicSelectMessageCondition(element);
			});
			
		}
		else logicAddMessage(container);
	}
	
	// subject
	
	if (jQuery(element).val() == "Give") {
		addSelectUnit(container, "Subject");
		
		// fire select subject function for generated content
		
		jQuery(".logicSelectUnit").click(function() {
			logicSelectUnit(jQuery(this));
		});
		
	}
	
	// unit conditions
	
	if ((jQuery(element).val() == "Dialog" || jQuery(element).val() == "Give") && jQuery(container).hasClass("logicEvent")) {
		logicAddUnitCondition(container, "To");
		logicAddUnitCondition(container, "From");
	}
	if (jQuery(element).val() == "Remove") logicAddUnitCondition(container, "Remove", "If Unit");
	
	// select subject condition for dynamic content

	jQuery(".logicSelectCondition").change(function() {
		logicSelectCondition(container, jQuery(this));
	});
	
	// apply changes for generated content
	
	jQuery(".rule select").change(function() {
		applySelect(jQuery(this));
	});
}

// select message condition (for static content)

jQuery(".logicSelectMessageConditon").change(function() {
	var element = "#" + jQuery(this).attr("id");
	logicSelectMessageCondition(element);
});

// select message condition

function logicSelectMessageCondition(element) {
	var container = "#" + jQuery(element).closest(".container").attr("id");
	if (jQuery(element).val() !== "Is any") logicAddMessage(container);
	else jQuery(".logicMessage", container).html("");
}

// add message

function logicAddMessage(container) {
	jQuery(".logicMessage", container).html('' +
		'<label>Content:</label>' +
		'<textarea></textarea>' +
	'');
		
	// apply changes for generated content
		
	jQuery(".rule textarea").keyup(function() {
		jQuery(element).html(jQuery(element).val());
	});
}

// add unit condition

function logicAddUnitCondition(container, type, label) {
	if (label == undefined) var label = type;
	jQuery(".logic" + type + "Condition", container).html('' +
		'<label>' + label + ':</label>' +
		'<select class="logicSelectCondition" data-type="' + type + '">' +
			'<option>Any unit</option>' +
			'<option>Is</option>' +
			'<option>Is not</option>' +
			'<option>Is type</option>' +
			'<option>Is not type</option>' +
			'<option>Has tag</option>' +
			'<option>Does not have tag</option>' +
		'</select>' +
	'');
}

// select conditions

function logicSelectCondition(container, element) {
	
	// "is" and "is not"
	
	var type = jQuery(element).attr("data-type");
	jQuery(".logic" + type, container).html("");
	if (jQuery(element).val() == "Is" || jQuery(element).val() == "Is not") {
		addSelectUnit(container, type);
	}
	else if (jQuery(element).val() !== "Any unit") {
		if (jQuery(element).val() == "Is type" || jQuery(element).val() == "Is not type") addInputUnit(container, type, "type");
		else addInputUnit(container, type, "tag");
	}
	
	// "is type" and "has tag"
	
	if (jQuery(element).val() == "Is type" || jQuery(element).val() == "Has tag") {
		if (jQuery(element).val() == "Is type") var placeholder = "Enter a unit type";
		else var placeholder = "Enter a unit tag";
		jQuery(".logicFrom", rule).html('<input type="text" class="logicUnit" placeholder="' + placeholder);
	}
	
	// fire select subject function for generated content
	
	jQuery(".logicSelectUnit").click(function() {
		logicSelectUnit(jQuery(this));
	});
	
	// apply changes for generated content
	
	jQuery(".rule input").keyup(function() {
		applyInput(jQuery(this));
	});
		
	// select result for generated content
	
	jQuery(".logicSelectResult").change(function() {
		logicSelectResult(container, jQuery(this));
	});
	
}

// fire select subject condition for static content

jQuery(".logicSelectCondition").change(function() {
	var rule = jQuery(this).parents(".rule");
	logicSelectCondition(jQuery(this).closest(".container"), jQuery(this));
});

// add select unit

function addSelectUnit(container, type) {
	jQuery(".logic" + type, container).html('' +
		'<button class="logicSelectUnit" title="Click on any unit">Select Unit</button>' +
		'<input type="text" class="logicUnit" placeholder="Click on Unit">' +
	'');
}

// add input unit

function addInputUnit(container, type, label) {
	jQuery(".logic" + type, container).html('' +
		'<input type="text" class="logicUnit" placeholder="Enter ' + label + '">' +
	'');
}

// select subject

function logicSelectUnit(element) {
	selectingLogicUnit = true;
	jQuery(element).addClass("active");
	jQuery(".unit").click(function() {
		if (jQuery(element).hasClass("active")) {
			var name = jQuery(this).attr("data-name");
			if (name == undefined) name = jQuery(this).attr("data-type");
			var unit = jQuery(this).attr("data-unit");
			jQuery(element).parent().children(".logicUnit").attr({
				"value": name + " (id: " + unit + ")",
				"data-value": unit
			});
			jQuery(".logicSelectUnit").removeClass("active");
			setTimeout(function() {
				selectingLogicUnit = false;
			}, 500);
		}
	});
}

// fire select subject function for static content

jQuery(".logicSelectUnit").click(function() {
	logicSelectUnit(jQuery(this));
});

/// apply changes to logic fields

function applyCheckbox(element) {
	if (jQuery(element).is(':checked')) jQuery(element).attr("checked", "checked");
	else jQuery(element).removeAttr("checked");
}
function applySelect(element) {
	var value = jQuery(element).val();
	jQuery("option", element).each(function() {
		jQuery(this).removeAttr("selected");
		if (jQuery(this).val() == value) jQuery(this).attr("selected", "selected");
	});
}
function applyInput(element) {
	var value = jQuery(element).val();
	jQuery(element).attr("value", value);
}
function applyTextarea(element) {
	var value = jQuery(element).val();
	jQuery(element).html(value);
}

// apply changes to fields for static content

//jQuery(".rule select").off("change"); // this somehow prevents the function for firing for every matched element
jQuery("input[type='checkbox']").change(function() {
	applyCheckbox(jQuery(this));
});
jQuery(".rule select").change(function() {
	applySelect(jQuery(this));
});
jQuery(".rule input").keyup(function() {
	applyInput(jQuery(this));
});
jQuery(".rule textarea").keyup(function() {
	jQuery(element).html(jQuery(element).val());
});

//////////////////////////// run logic  //////////////////////////////

function logic(action, target, subject) {
		
	// for each rule of unit and target
	
	jQuery("#logic .unitInfo[data-unit='" + unit + "'] .rule, #logic .unitInfo[data-unit='" + target + "'] .rule").each(function() {
		
		// if the rule has logic for the current action
		
		if (action == jQuery(".logicEvent .logicSelectEvent", this).attr("value")) {
					
			// get variables
			
			var runRuleIf = jQuery(".logicRunRuleIf", this).attr("value");
			var runRuleIfRule = jQuery(".logicRunRuleIfRule", this).attr("value");
			var runRuleIfRuleStatus = jQuery(".logicRunRuleIfRuleStatus", this).attr("value");
			var dependingRuleStatus = jQuery(".rule[data-rule='" + runRuleIfRule + "'] .logicSelectRuleStatus").attr("value");
			var ruleStatus = jQuery(".logicSelectRuleStatus", this).attr("value");
			
			// run rule if rule's meta conditions are met
						
			if ((ruleStatus !== "Disabled") && (runRuleIf == undefined || runRuleIfRuleStatus == dependingRuleStatus)) {
			
				var uponCompletion = jQuery(".logicSelectUponCompletion", this).attr("value");
				var eventEvent = jQuery(".logicEvent .logicSelectEvent", this).attr("value");
				var eventMessageCondition = jQuery(".logicEvent .logicMessageCondition", this).attr("value");
				var eventMessage = jQuery(".logicEvent .logicMessage textarea", this).attr("value");
				var eventSubject = jQuery(".logicEvent .logicSubject .logicUnit", this).attr("data-value");
				var eventRemoveCondition = jQuery(".logicEvent .logicRemoveCondition", this).attr("value");
				var eventRemove = jQuery(".logicEvent .logicRemove", this).attr("value");
				var eventFromCondition = jQuery(".logicEvent .logicFromCondition", this).attr("value");
				var eventFrom = jQuery(".logicEvent .logicFrom input", this).attr("data-value");
				var eventToCondition = jQuery(".logicEvent .logicToCondition", this).attr("value");
				var eventTo = jQuery(".logicEvent .logicTo input", this).attr("data-value");
				var resultEvent = jQuery(".logicResult .logicSelectEvent", this).attr("value");
				var resultMessageCondition = jQuery(".logicResult .logicMessageCondition", this).attr("value");
				var resultMessage = jQuery(".logicResult .logicMessage textarea", this).attr("value");
				var resultSubject = jQuery(".logicResult .logicSubject", this).attr("data-value");
				var resultRemoveCondition = jQuery(".logicResult .logicRemoveCondition", this).attr("value");
				var resultRemove = jQuery(".logicResult .logicRemove", this).attr("value");
				var resultFromCondition = jQuery(".logicResult .logicFromCondition", this).attr("value");
				var resultFrom = jQuery(".logicResult .logicFrom input", this).attr("data-value");
				var resultToCondition = jQuery(".logicResult .logicToCondition", this).attr("value");
				var resultTo = jQuery(".logicResult .logicTo input", this).attr("data-value");
	
				// dialog
								
				if (action == "Dialog" && eventEvent == "Dialog") {
					if (eventFrom == unit) {
												
						// message
						
						if (resultEvent == "Message") pop(resultMessage);
						
						// dialog
						
						if (resultEvent == "Dialog") {
							var name = jQuery(".unit[data-unit='" + target + "']").attr("data-name");
							if (name == undefined) name = "The " + jQuery(".unit[data-unit='" + target + "']").attr("data-type");
							popbox("<p><strong>" + jQuery(".unit[data-unit='" + target + "']").html() + " " + name + " says:</strong></p><p>" + resultMessage + "</p>");
						}
					}
				}
	
				// give
				
				if (eventEvent == "Give") {
					if (eventSubject == subject) {
					
						// message
						
						if (resultEvent == "Message") pop(resultMessage);
						
						// dialog
						
						if (resultEvent == "Dialog") {
							
							var name = jQuery(".unit[data-unit='" + target + "']").attr("data-name");
							if (name == undefined) name = "The " + jQuery(".unit[data-unit='" + target + "']").attr("data-type");
							popbox("<p><strong>" + jQuery(".unit[data-unit='" + target + "']").html() + " " + name + " says:</strong></p><p>" + resultMessage + "</p>");
						}
					}
				}
				
				// upon rule completion
				
				jQuery(".logicSelectRuleStatus", this).attr("value", uponCompletion);
				
			}
		}
	});
}