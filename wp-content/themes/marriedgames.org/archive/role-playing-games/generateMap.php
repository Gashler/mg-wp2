<div id="id" style="display:none;">1</div>
<div id="rule" style="display:none;">1</div>
<?php
	$tableHeight = ($_POST['tileWidth'] * ($_POST['size'])) + ($_POST['borderWidth'] * ($_POST['size'] + 1));
	$tableWidth = $tableHeight * 1.2;
?>
<table id="rpgGrid" style="width:<?php echo $tableWidth ?>px; height:<?php echo $tableHeight ?>px">
<?php
	for ($r = 1; $r <= ($_POST['size'] * 2) + 1; $r ++ ) {
		if ($r == 1) {
			echo "<div class='tr'>";
				for ($c = 1; $c <= ($_POST['size'] * 2) + 1; $c ++ ) {
					echo "<td class='border'></td>";
				}
			echo "</tr>";
		}
		else {
			if ($r % 2 == 0) { // if even row
				echo "<div class='tr'>";
				for ($c = 1; $c <= ($_POST['size'] * 2) + 1; $c ++ ) {
					if ($c % 2 == 0) echo "<td data-unit='r" . $r . "c" . $c . "' class='tile' data-col='" . $c . "' data-row='" . $r . "' data-type='Place'></td>";
					else echo "<td class='border'></td>";
				}
				echo "</tr>";
			}
			else {
				echo "<div class='tr'>";
				for ($c = 1; $c <= ($_POST['size'] * 2) + 1; $c ++ ) {
					echo "<td class='border'></td>";
				}
				echo "</tr>";
			}
		}
	}
?>
</table><!-- rpgGrid -->