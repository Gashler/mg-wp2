<?php

	// variables
	$tileWidth = 100;
	$tilePadding = 5;
	$borderWidth = 20;
	
?>
<script>

	// conver php variables to JavaScript variables
	var tileWidth = <?php echo $tileWidth ?>;
	var borderWidth = <?php echo $borderWidth ?>;
	
</script>
<style>
	dt { position:relative; padding:5px 10px; margin:0 !important; background:rgba(0,0,0,.1); border-top:1px solid rgba(0,0,0,.1); border-left:1px solid rgba(0,0,0,.1); border-right:1px solid rgba(0,0,0,.1); border-bottom:1px solid rgba(0,0,0,.1); }
	dd { padding-top:1em; }
	dt:first-of-type { border-top:1px solid rgba(0,0,0,.1) !important; }
	.x { position:absolute; top:0; right:0; padding:7px; cursor:pointer; }
	fieldset { display:inline-block; }
	.entry-content { overflow:hidden; }
	.overflow { height:inherit; overflow:auto; position:relative; }
	#rpgContainer th { padding:5px 5px 0 0 !important; }
	#rpgContainer input, #rpgContainer select, #rpgContainer button { padding:5px 10px !important; margin:0; }
	#rpgContainer button.toolbarButton { padding:5px !important; }
	#rpgContainer { height:inherit; display:table; width:100%; }
	#rpgToolbar { height:inherit; display:table-cell; background:white; width:18%; vertical-align:top; padding-right:2%; }
	#rpgGridContainer { width:60%; height:inherit; display:table-cell; background:gray; position:relative; }
	#actions { text-align:right; margin:5px 20px -41px 0; position:relative; z-index:1; }
	#rpgGridContainer > div > div { position:absolute; top:0; right:0; bottom:0; left:0; display:table-cell; vertical-align:middle; }
	#rpgGrid { margin:3% auto; }
	#rpgGrid td { background:rgba(255,255,255,.75); }
	#rpgGrid .tile { width:<?php echo $tileWidth ?>px; height:<?php echo $tileWidth ?>px; padding:0 !important; box-shadow:0 0 1px 1px rgba(0,0,0,.1) inset; font-size:10pt; vertical-align:middle !important; }
	#rpgGrid tr:nth-of-type(odd) td.border { height:<?php echo $borderWidth ?>px !important; padding:0 !important; }
	#rpgGrid tr:nth-of-type(even) td.border { width:<?php echo $borderWidth ?>px !important; height:<?php echo $tileWidth + ($tilePadding * 2) ?>px !important; padding:0 !important; }
	#rpgGrid td.active { box-shadow: 0 0 0 2px rgb(255,0,128) inset; }
	#rpgContent { height:inherit; display:table-cell; vertical-align:top; padding-left:2%; width:28%; min-width:225px; }
	#rpgContent table { width:100%; }
	#rpgContent th label { float:right; }
	#rpgContent td { width:100%; }
	#rpgContent td input { width:100%; }
	.unit { display:inline-block; cursor:pointer; background:white; padding:5px; border-radius:4px; box-shadow:1px 1px 3px rgba(0,0,0,.15); margin:1px 1px -6px 1px; }
	.unit.active { box-shadow: 0 0 1px 2px rgb(255,0,128); }
	input[type="text"] { display:inherit; }
	#rpgContent table td, #rpgContent table th { background:none; padding:0; color:black; }
	.unit[data-type="wall"] { display:block; background:black; height:100%; border-radius:0; padding:0 !important; margin:0 !important; }
	.toolbarButton { padding:5px !important; }
	.unitIcon { display:inline-block; vertical-align:middle; width:24px; height:24px; background-image:url("<?php echo get_template_directory_uri() ?>/images/rpg/rpg-artboard.png"); }
	button.active { background:rgb(192,0,96) !important; box-shadow:0 0 10px rgb(128,0,64) inset !important; }
	#unitIconMale { background-position:0 -48px !important }
	#unitIconFemale { background-position:-24px -48px !important }
	#unitIconWall { background-position:0 -24px !important }
	#unitIconObject { background-position:-24px -24px !important }
	#unitIconDelete { style="background-position:0 0;" }
	.place { height: 100%; display: block; margin: 0px; padding: 0px !important; border-radius: 0px !important; }
	#inventory { border:1px solid rgb(192,192,192); border-radius:4px; box-shadow:1px 1px 3px gray inset; padding:5px; min-height:200px; }
	#inventory > div { display:none; }
	
	/* tabs */
	
	.tabs-menu { margin:0; padding:0; border-bottom:1px solid rgb(192,192,192); margin-bottom:2em; }
	.tabs-menu li { cursor:pointer; background:rgb(225,225,225); border-top:1px solid rgb(192,192,192); border-left:1px solid rgb(192,192,192); border-right:1px solid rgb(192,192,192); list-style:none; display:inline-block; padding:5px 15px; border-top-right-radius:24px; margin:0 2px 0 0; font-size:10pt; }
	.tabs-menu li.active { background:white; border-bottom:1px solid white; position:relative; bottom:-1px; }
	.tabs-content > div { display:none; }
	.tabs-content > div.active { display:block; }
	.tabContent { display:none; }
	
	/* end tabs */
	
	/* popbox */

	.dark { position:fixed; top:0; right:0; bottom:0; left:0; background:rgba(0,0,0,.5); z-index:10000; display:table-cell; text-align:center; vertical-align:middle; padding-top:12%; }
	.popbox { display:inline-block; background:white; color:black; z-index:10001; max-height:520px; max-width:520px; padding:20px 40px; overflow-y:auto; box-shadow:0 0 50px rgba(0,0,0,.5); }
	.popbox .x { position:absolute; top:0; right:0; background:white; border-bottom-left-radius:100%; box-shadow:0px 0px 5px white inset; border:1px solid rgb(192,192,192); cursor:pointer; background:rgb(225,225,225); color:black; padding:5px 9px 10px 18px; }
	@media (max-width:1600px ) {
		.popbox { right:25%; left:25%; }
	}
	@media (max-width:1300px ) {
		.popbox { right:18%; left:18%; }
	}
	@media (max-width:1000px ) {
		.popbox { right:10%; left:10%; }
	}
	@media (max-width:600px ) {
		.popbox { right:5%; left:5%; }
	}
	
	.popbox input[type="text"] { width:100%; }
	
	/* end popbox */
	
	.colorPicker { margin:0; padding:0; }
	.colorPicker li { display:inline-block; height:25px; width:25px; box-shadow:1px 1px 2px rgba(0,0,0,.25) inset; border-radius:2px; margin:0 4px 4px 0; list-style:none; }
	
</style>
<?php
	// get game and user info
	$fields = json_decode(S2MEMBER_CURRENT_USER_FIELDS, true);
	$user = S2MEMBER_CURRENT_USER_ID;
	$gameID = get_the_ID();
?>
<script>
	var husband = "<?php echo $fields["husband"] ?>";
	var wife = "<?php echo $fields["wife"] ?>";
	var user = "<?php echo $user ?>";
	var gameID = "<?php echo $gameID ?>";
</script>

<div id="load"></div>
<div id="rpgContainer">
	<div id="rpgToolbar">
		<div class="overflow">
			<input type="checkbox" id="buttonsStayPressed"><label style="display:inline;" for="buttonsStayPressed">Buttons stay pressed</label>
			<dl>
				<dt>Settings</dt>
				<dd>
					<select id="rpgSelectDimensions" style="display:inline-block;">
						<option value="4">4&times;4</option>
						<option value="8">8&times;8</option>
						<option value="16">16&times;16</option>
					</select>
					<button id="newMap" style="display:inline-block;">New Map</button>
				</dd>
				<dt>Units</dt>
				<dd>
					<button class="toolbarButton unitButton" data-unit="Man" data-type="tile" title="Man"><div class="unitIcon" id="unitIconMale"></div></button>
					<button class="toolbarButton unitButton" data-unit="Woman" data-type="tile" title="Woman"><div class="unitIcon" id="unitIconFemale"></div></button>
					<button class="toolbarButton unitButton" data-unit="wall" data-type="border" title="Wall"><div class="unitIcon" id="unitIconWall"></div></button>
					<button class="toolbarButton unitButton" data-unit="Object" data-type="tile" title="Object"><div class="unitIcon" id="unitIconObject"></div></button>
				</li>
				<dt>Edit</dt>
				<dd>
					<button class="toolbarButton" data-unit="delete"><div class="unitIcon" id="unitIconDelete"></div></button>
				</dd>
			</dl>
			<button id="save">Save Game</button>
		</div><!-- overflow -->
	</div><!-- toolbar -->
	<div id="rpgGridContainer">
		<div id="actions">
			<button data-action="Give">Give</button>
			<button data-action="Dialog">Talk</button>
			<button data-action="Use">Use</button>
		</div>
		<div class="overflow">
			<div>
				<?php
				
					// load saved game data
					
					$con = mysqli_connect("localhost","root","asdf","mg");
					if (mysqli_connect_errno()) {
						echo "Failed to connect to MySQL: " . mysqli_connect_error();
					}
					$sql = "SELECT * FROM rpg WHERE user = $user AND id = $gameID";
					$result = $con->query($sql);
					while($row = $result->fetch_assoc()) {
						$grid = $row['grid'];
						$inventory = $row['inventory'];
						$logic = $row['logic'];
					}
					$grid = base64_decode($grid);
					$inventory = base64_decode($inventory);
					$logic = base64_decode($logic);
					
					// output grid
					
					echo $grid;
				?>
			</div>
		</div><!-- overflow -->
	</div><!-- rpgGridContainer -->
	<div id="rpgContent">
		<div class="overflow">
			<nav class="tabs-menu">
				<li class="active" data-tab="info">Info</li>
				<li data-tab="inventory">Inventory</li>
				<li data-tab="logic">Logic</li>
			</nav>
			<div class="tabs-content">
				<div class="tab active" data-tab="info">
					<div class="tabContent">
						<table>
							<tr>
								<th><label for="unitName">Name:</label></th>
								<td><input id="unitName" type="text" data-attribute="name"></td>
							</tr>
							<tr>
								<th><label for="unitType">Type:</label></th>
								<td><input id="unitType" type="text" data-attribute="type"></td>
							</tr>
						</table>
						<label for="description">Description:</label>
						<textarea id="unitDescription" data-attribute="description"></textarea>
						<label>Color:</label>
						<ul class="colorPicker">
							<li style="background-color:#F68E55"></li>
							<li style="background-color:#FBAF5C"></li>
							<li style="background-color:#FFF467"></li>
							<li style="background-color:#ACD372"></li>
							<li style="background-color:#7CC576"></li>
							<li style="background-color:#3BB878"></li>
							<li style="background-color:#1ABBB4"></li>
							<li style="background-color:#00BFF3"></li>
							<li style="background-color:#438CCA"></li>
							<li style="background-color:#5574B9"></li>
							<li style="background-color:#605CA8"></li>
							<li style="background-color:#855FA8"></li>
							<li style="background-color:#A763A8"></li>
							<li style="background-color:#F06EA9"></li>
							<li style="background-color:#F26D7D"></li>
						</ul>
					</div><!-- tabContent -->
					<p class="selectAUnit">Select a unit</p>
				</div><!-- tab -->
				<div class="tab" data-tab="inventory">
					<div class="tabContent">
						<div id="inventory">
							<?php
								// load saved inventory
								echo $inventory;
							?>
						</div>
					</div><!-- tabContent -->
					<p class="selectAUnit">Select a unit</p>
				</div><!-- tab -->
				<div class="tab" data-tab="logic" id="logic">
					<div class="tabContent">
						<div id="rules">
							<?php
								// load saved logic
								echo $logic;
							?>
						</div>
						<button id="addRule" title="Add rule">Add Rule</button>
					</div><!-- tabContent -->
					<p class="selectAUnit">Select a unit</p>
				</div><!-- tab -->
			</div><!-- tabs-content -->
		</div><!-- overflow -->
	</div><!-- rpgContent -->
</div><!-- rpgContainer -->
<script src="/wp-content/themes/marriedgames.org/role-playing-games/js.js"></script>