<!doctype html>
<html>
	<head>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<style>
			.wrap {
				perspective: 800px;
				perspective-origin: 50% 100px;
				text-align:center;
			}
			.cube div {
				position: absolute;
				width: 200px;
				height: 125px;
				padding-top:75px;
				box-shadow:0 0 40px rgba(0,0,0,.5) inset;
				display:table-cell !important;
				background:rgb(255,0,128);
				color:white;
				text-align:center;
				font-size:2em;
			}
			.right {
				transform: rotateY(-270deg) translateX(100px);
				transform-origin: top right;
			}
			.left {
				transform: rotateY(270deg) translateX(-100px);
				transform-origin: center left;
			}
			.front {
				transform: translateZ(100px);
			}
		</style>
		<style id="animation">
			.cube {
				margin:0 auto;
				position: relative;
				width: 200px;
				transform-style: preserve-3d;
			}
			.back {
				transform: translateZ(-100px) rotateY(180deg);
			}
			.top {
				transform: rotateX(-90deg) translateY(-100px);
				transform-origin: top center;
			}
			.bottom {
				transform: rotateX(90deg) translateY(100px);
				transform-origin: bottom center;
			}
		</style>
	</head>
	<body style="text-align:center;">
		<div class="wrap">
			<div class="cube">
				<div class="front">Caress</div>
				<div class="back">Lick</div>
				<div class="top">Suck</div>
				<div class="bottom">Kiss</div>
				<div class="left">Massage</div>
				<div class="right">Scratch</div>
			</div><!-- cube -->
		</div><!-- wrap -->
		<button id="roll" style="margin-top:250px;">Roll</button>
		<button id="remove" style="margin-top:250px;">Remove</button>
		<script>
			jQuery(document).ready(function() {
				jQuery("#remove").click(function() {
					jQuery("#animation").remove();
				});
				jQuery("#roll").click(function() {
					var degrees = (Math.floor(Math.random() * 4)) * 90;
					var direction = Math.floor(Math.random() * 1);
					if (direction == 1) { // horizontal spin
						jQuery("#animation").html("" +
							".cube {" +
								"margin:0 auto;" +
								"position: relative;" +
								"width: 200px;" +
								"transform-style: preserve-3d;" +
							"}" +
							".back {" +
								"transform: translateZ(-100px) rotateY(180deg);" +
							"}" +
							".top {" +
								"transform: rotateX(-90deg) translateY(-100px);" +
								"transform-origin: top center;" +
							"}" +
							".bottom {" +
								"transform: rotateX(90deg) translateY(100px);" +
								"transform-origin: bottom center;" +
							"}" +
							"@keyframes spin {" +
								"from { transform: rotateY(0); }" +
								"to { transform: rotateY(" + degrees + "deg); }" +
							"}" +
							".cube { animation: spin 1s forwards; }" +
						"");
						setTimeout(function() {
							jQuery(".cube").css("transform", "rotateY(" + degrees + "deg)");
							var cube = jQuery(".wrap").html();
							jQuery(".wrap").html("");
							jQuery("#animation").html("");
							jQuery(".wrap").html(cube);
						}, 1000);
					} else { // vertical spin
						jQuery("#animation").html("" +
							".cube {" +
								"margin: 0 auto; /* keeps the cube centered */" +
								"transform-origin: 0 100px;" +
								"animation: spin-vertical 5s infinite linear;" +
							"}" +
							".top {" +
								"transform: rotateX(-270deg) translateY(-100px);" +
							"}" +
							".back {" +
								"transform: translateZ(-100px) rotateX(180deg);" +
							"}" +
							".bottom {" +
								"transform: rotateX(-90deg) translateY(100px);" +
							"}" +
							"@keyframes spin-vertical {" +
								"from { transform: rotateX(0); }" +
								"to { transform: rotateX(-360deg); }" +
							"}" +
							".cube { animation: spin 1s forwards; }" +
						"");
						setTimeout(function() {
							jQuery(".cube").css("transform", "rotateX(" + degrees + "deg)");
							var cube = jQuery(".wrap").html();
							jQuery(".wrap").html("");
							jQuery("#animation").html("");
							jQuery(".wrap").html(cube);
						}, 1000);
					}
				});
			});

		</script>
	</body>
</html>