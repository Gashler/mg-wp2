<!doctype html>
<html>
	<head>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<style>
			.wrap {
				perspective: 800px;
				perspective-origin: 50% 100px;
				text-align:center;
			}
			#wrap1 { margin-left:-400px; }
			#wrap2 { margin-right:-400px; }
			.cube div {
				position: absolute;
				width: 200px;
				height: 125px;
				padding-top:75px;
				box-shadow:0 0 40px rgba(0,0,0,.5) inset;
				display:table-cell !important;
				background:rgb(255,0,128);
				color:white;
				text-align:center;
				font-size:2em;
			}
			.right {
				transform: rotateY(-270deg) translateX(100px);
				transform-origin: top right;
			}
			.left {
				transform: rotateY(270deg) translateX(-100px);
				transform-origin: center left;
			}
			.front {
				transform: translateZ(100px);
			}
		</style>
		<?php for ($x = 1; $x <= 2; $x ++) { ?>
			<style class="animation cube<?php echo $x ?>">
				#cube<?php echo $x ?> {
					margin:0 auto;
					position: relative;
					width: 200px;
					transform-style: preserve-3d;
				}
			</style>
		<?php } ?>
	</head>
	<body style="text-align:center;">
		<?php for ($x = 1; $x <= 2; $x ++) { ?>
			<div class="wrap" data-cube="<?php echo $x ?>">
				<div class="cube" id="cube<?php echo $x ?>">
					<div class="front">Caress</div>
					<div class="back">Lick</div>
					<div class="top">Suck</div>
					<div class="bottom">Kiss</div>
					<div class="left">Massage</div>
					<div class="right">Scratch</div>
				</div><!-- cube -->
			</div><!-- wrap -->
		<?php } ?>
		<button id="roll" style="margin-top:250px;">Roll</button>
		<script src="<?php echo get_template_directory_uri() ?>/sex-dice/js.js"></script>
	</body>
</html>