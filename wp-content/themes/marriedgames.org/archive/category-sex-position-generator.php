<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Sex Position Generator
 */

get_header(); ?>

	<section id="primary" class="site-content">
		<div id="content" role="main">

		<?php 
		
		query_posts(array(
			'showposts' => 1,
			'orderby' => 'rand',
			'category_name' => 'Sex Position Generator' //You can insert any category name
		));
		
		if ( have_posts() ) : ?>

			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( '%s', 'twentytwelve' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>

			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
			<?php endif; ?>
			</header><!-- .archive-header -->

			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="outer-post">
				<div <?php post_class() ?> class="post" id="post-<?php the_ID(); ?>">
					<div class="inner-post">
					<h2 class="post-title" style="margin-top:0;"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h2>
					<?php the_content(); ?>
					<button id="reload" style="float:right;">New Sex Position</button>
					<?php if(function_exists('wp_gdsr_render_article')){ wp_gdsr_render_article(); } ?>
		
					<div class="feedback">
						<small>
						<?php wp_link_pages(); ?>
					</div>
					</div>
				</div>
			</div>
			
			
			<?php endwhile; else: ?>
			<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
		
		<?php
			$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
			$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
		?>
		<script>
			
			var husband = "<?php echo $husband ?>";
			var wife = "<?php echo $wife ?>";
			
			var content = jQuery(".generatedContent").html();
			content = content.replace("He", husband);
			content = content.replace(" he ", " " + husband + " ");
			content = content.replace("She", wife);
			content = content.replace(" she ", " " + wife + " ");
			content = content.replace(" pull ", " pulls ");
			jQuery(".generatedContent").html(content);
			
			jQuery("#reload").click(function() {
				location.reload();
			});
			
		</script>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>