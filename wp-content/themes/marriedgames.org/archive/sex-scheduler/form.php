<?php
	if ($_POST['husband'] !== null) $fields['husband'] = $_POST['husband'];
	if ($_POST['wife'] !== null) $fields['wife'] = $_POST['wife'];
	if (isset($_POST["id"])) $id = $_POST["id"];
	else $id = $event['id'];
?>
	<div class="x">&times;</div>
	<!--<div class="step"><?php echo $id ?></div>-->
	<fieldset>
		<label for="activity<?php echo $id ?>">Schedule:</label>
		<select id="activity<?php echo $id ?>" name="activity[<?php echo $id ?>][name]">
			<?php
				if (isset($_POST['actions'])) echo $_POST['actions'];
				else {
					foreach ($actions as $action) {
						echo "<option";
						if (isset($event['name'])) {
							if ($action == $event['name']) echo " selected";
						}
						else if ($action == "sex") echo " selected";
						echo ">" . $action . "</option>";
					}
				}
			?>
		</select>
	</fieldset>
	<fieldset>
		<label for="date<?php echo $id ?>">&nbsp;On:</label>
		<input type="text" name="activity[<?php echo $id ?>][date]" id="dateValue<?php echo $id ?>" class="datepicker" placeholder="Choose Date" value="<?php echo $event['date'] ?>" />		
	</fieldset>
	<div class="clear"></div>
	<fieldset>
		<label for="beginHour<?php echo $id ?>">From: </label>
		<select id="beginHour<?php echo $id ?>" name="activity[<?php echo $id ?>][beginHour]">
			<?php
				for ($x = 1; $x <= 12; $x ++) {
					echo "<option";
					if (isset($event['beginHour'])) {
						if ($x == $event['beginHour']) echo " selected";
					}
					else if ($x == 9) echo " selected";
					echo ">" . $x . "</option>";
				}
			?>
		</select>
		:
		<select id="beginMinute<?php echo $id ?>" name="activity[<?php echo $id ?>][beginMinute]">
			<?php
				for ($x = 0; $x <= 45; $x += 15) {
					echo "<option";
					if (isset($event['beginMinute'])) {
						if ($x == $event['beginMinute']) echo " selected";
					}
					else if ($x == 30) echo " selected";
					echo ">";
					if ($x == 0) echo "00";
					else echo $x . "</option>";
				}
			?>
		</select>
		<select id="beginAM<?php echo $id ?>" name="activity[<?php echo $id ?>][beginAMPM]">
			<option <?php if ($event['beginAM'] == 1) echo "selected" ?> value="1">AM</option>
			<option <?php if ($event['beginAM'] == 0) echo "selected" ?> value="0">PM</option>
		</select>
	</fieldset>
	<fieldset>
		<label for="endHour<?php echo $id ?>">&nbsp;To: </label>
		<select id="endHour<?php echo $id ?>" name="activity[<?php echo $id ?>][endHour]">
			<?php
				for ($x = 1; $x <= 12; $x ++) {
					echo "<option";
					if (isset($event['endHour'])) {
						if ($x == $event['endHour']) echo " selected";
					}
					else if ($x == 10) echo " selected";
					echo ">" . $x . "</option>";
				}
			?>
		</select>
		 : 
		<select id="endMinute<?php echo $id ?>" name="activity[<?php echo $id ?>][endMinute]">
			<?php
				for ($x = 0; $x <= 45; $x += 15) {
					echo "<option";
					if (isset($event['endMinute'])) {
						if ($x == $event['endMinute']) echo " selected";
					}
					else if ($x == 30) echo " selected";
					echo ">";
					if ($x == 0) echo "00";
					else echo $x . "</option>";
				}
			?>
		</select>
		<select id="endAM<?php echo $id ?>" name="activity[<?php echo $id ?>][endAMPM]">
			<option <?php if ($event['endAM'] == 1) echo "selected" ?> value="1">AM</option>
			<option <?php if ($event['endAM'] == 0) echo "selected" ?> value="0">PM</option>
		</select>
	</fieldset>
	<div class="clear"></div>
	<fieldset>
		<label for="husbandInCharge<?php echo $id ?>">Person in Charge: </label>
		<select id="husbandInCharge<?php echo $id ?>" name="activity[<?php echo $id ?>][husbandInCharge]">
			<option <?php if ($event['wifeInCharge'] == 0) echo "selected" ?> value="0"><?php echo $fields['husband']; ?></option>
			<option <?php if ($event['wifeInCharge'] == 1) echo "selected" ?> value="1"><?php echo $fields['wife']; ?></option>	
		</select>
	</fieldset>
	<div class="clear"></div>
	<fieldset style="margin-bottom:0; margin-right:.5em;">
		<input style="margin-bottom:0; margin-right:0;" type="checkbox" class="repeat" id="repeat<?php echo $id ?>" name="repeat" data-id="<?php echo $id ?>" />
		<label for="repeat<?php echo $id ?>" checked="0">Repeat every:</label>
		<select style="margin-bottom:0 !important;" id="repeatValue<?php echo $id ?>" class="repeatValue" name="activity[<?php echo $id ?>][repeatValue]" disabled>
			<?php
				for ($x = 1; $x <= 15; $x ++) {
					echo "<option";
					if (isset($event['repeatValue'])) {
						if ($x == $event['repeatValue']) echo " selected";
					}
					else if ($x == 3) echo " selected";
					echo ">" . $x . "</option>";
				}
			?>
		</select>
	</fieldset>
	<fieldset style="margin-bottom:0;">
		<select style="margin-bottom:0 !important;" id="repeatType<?php echo $id ?>" class="repeatType" name="activity[<?php echo $id ?>][repeatType]" disabled>
			<option <?php if ($event['repeatType'] == 0) echo "selected" ?> value="1">days</option>
			<option <?php if ($event['repeatType'] == 7) echo "selected" ?> value="7">weeks</option>
			<option <?php if ($event['repeatType'] == 30) echo "selected" ?> value="30">months</option>
			<option <?php if ($event['repeatType'] == 365) echo "selected" ?> value="365">years</option>
		</select>
	</fieldset>
	<script>
		// delete event
		jQuery(".x").click(function() {
			jQuery(this).parent().slideUp(function() {
				jQuery(this).remove();
			});
			var id = jQuery(this).parent().attr("id");
			jQuery("#load").load("/wp-content/themes/marriedgames.org/sex-scheduler/delete.php", {
				user: user,
				id: id
			});
		});;
		
		//jQuery(".datepicker").datepicker();
	</script>