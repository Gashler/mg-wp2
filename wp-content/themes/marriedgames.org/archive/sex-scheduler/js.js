/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/jQuery(document).ready(function() {

	// enable / disable fields
	/*
	jQuery("input").click(function() {
		if (jQuery(this).type("checkbox" || "radio")) alert(true);
	});

	jQuery("#weekDay").click(function() {
		var rule = jQuery(this).parents(".rule").attr("id");
		alert(rule);
		jQuery("#" + rule + " #days").removeAttr("disabled");
		jQuery("#" + rule + " #dateValue").attr("disabled", "disabled");
	});
	
	jQuery("#date").click(function() {
		var rule = jQuery(this).parents(".rule").attr("id");
		jQuery("#" + rule + " #days").attr("disabled", "disabled");
		jQuery("#" + rule + " #dateValue").removeAttr("disabled");
	});
	*/

	jQuery(".repeat").click(function() {
		var rule = jQuery(this).parents(".rule").attr("id");
		if (jQuery("#" + rule + " #repeatValue").attr("disabled")) {
			jQuery("#" + rule + " #repeatValue").removeAttr("disabled");
		}
		else {
			jQuery("#" + rule + " #repeatValue").attr("disabled", "disabled");
		}
	});
	
	// add rule
	
	var rule = 2;
	jQuery("#addRule").click(function() {
		jQuery(".ruleContainer:last").after("<div class='ruleContainer'><div class='step'>" + rule + "</div><div class='generatedContent rule' id='" + rule + "'></div></div>");
		jQuery(".ruleContainer:last .rule").load("/wp-content/themes/marriedgames.org/sex-scheduler/form.php", { rule: rule });
		rule ++;
		
		// close rule
		
		jQuery(".rule").delegate(".x","click", function() {
			jQuery(this).parent().parent().slideUp(function() {
				jQuery(this).remove();
			});
			rule --;
		});
	
	});
	
});