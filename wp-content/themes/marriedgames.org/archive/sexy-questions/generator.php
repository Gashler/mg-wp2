<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/js/m_select/bootstrap.min.css" type="text/css" /> 
<script src="<?php echo get_template_directory_uri(); ?>/js/m_select/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/m_select/bootstrap-multiselect.js" type="text/javascript"></script>
<style>
 .btn-group .dropdown-toggle{
    font-size: 12px !important;
}
#main-content .multiselect-container a {
    text-decoration: none !important;
}
#showCatAlert{
 box-shadow: 0 0 50px #FF2040 inset;
 border:2px solid #FF0080;
}
</style>
<?php global $wpdb;
   $allCat = $wpdb->get_results( 'SELECT DISTINCT category FROM questions', OBJECT ); ?>
<div id="generator">
	<?php if (current_user_can("access_s2member_level1")) { 
	   $totalcatcount = count($allCat);
	?>
	  <div><div style='float:right'>
       <em style="font-weight:bolder">Select category: </em><select id='chkveg' multiple='multiple'> 
        <?php foreach($allCat as $alCat) { ?>
          <option value="<?php echo $alCat->category; ?>" selected><?php echo $alCat->category; ?></option>
        <?php } ?>
        </select>
       </div>
		<div class="generatedContentContainer"></div>
	 </div>	
		<input type="submit" id="newQuestion" value="New Question" class="button">
		
		  <!----------------------------popup------------------------------->	
<div class="modal fade in" aria-hidden="true" id="showCatAlert" aria-labelledby="mySmallModalLabel" role="dialog" tabindex="-1" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
          <button type="button" class="close" id="catClose" data-dismiss="modal"><span>x</span></button>
          <h4 class="modal-title" id="mySmallModalLabel">firstly choose the category in the drop box</h4>
       </div> 
    </div>
  </div>
</div>
<!----------------------------popup------------------------------->	

	<?php } else { ?>
		<div class="generatedContentContainer">
			<div class="caption her" style="margin-bottom:1em;">Vanessa says to Zack:</div><div class="generatedContent her">
				<h2></h2>
				<p><em>Category: appearance</em></p><p class="large">Which lingerie do you think I look best in?</p>
			</div>
		</div>
		<button disabled>New Question</button>
	<?php } ?>
</div>
<?php
	$husband = S2MEMBER_CURRENT_USER_FIRST_NAME;
	$wife = S2MEMBER_CURRENT_USER_LAST_NAME;
	if ($husband == null) $husband = "Zack";
	if ($wife == null) $wife = "Vanessa";
?>
<?php if (current_user_can("access_s2member_level1")) { ?>
	<script>
	  jQuery("#catClose").click(function(){
        jQuery("#showCatAlert").css("display", "none");
      });	
		var husband = "<?php echo $husband ?>";
		var wife = "<?php echo $wife ?>";
		function loadContent() {
			jQuery(".generatedContentContainer").load("/wp-content/themes/marriedgames.org/sexy-questions/content.php", {
				husband: husband,
				wife: wife,
				catName: jQuery('#chkveg').val(),
				catCount: <?php echo $totalcatcount; ?>
			});
		}
		loadContent();
		jQuery("#newQuestion").click(function() {
		   var catval = jQuery('#chkveg').val();	
	       if(catval == null){		     
			 jQuery("#showCatAlert").css("display", "block");
	       } else { 
			jQuery("#generator").fadeOut(200, function() {
				loadContent();
				jQuery("#generator").fadeIn(200);
			});
		   }	
		});
		
	</script>
<?php } ?>