<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: No Container

 */
 

get_header(); ?>
	<div id="primary" class="site-content">
		<div id="content" role="main">
			<div class="socialWidgetTop">
				<?php include "socialWidgetHeader.php" ?>
			</div>
		
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'page' ); ?>

			<?php endwhile; // end of the loop. ?>

		</div><!-- #content -->
	</div><!-- #primary -->
<script>
	function sizeEntryContent() {
		var windowHeight = jQuery(window).height() - 340;
		jQuery(".entry-content").css("height", windowHeight + "px");
	};
	sizeEntryContent();
	jQuery(window).resize(function() {
		sizeEntryContent();
	});
</script>
<?php get_footer(); ?>