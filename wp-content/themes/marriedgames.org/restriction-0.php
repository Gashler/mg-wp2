<?php if ((current_user_cannot("access_s2member_level0")) || is_user_not_logged_in()) { ?>
	<div class="restriction">
		<div class="play-for-free-container">
			<?php if (S2MEMBER_CURRENT_USER_LOGIN_COUNTER > 2) { ?>
				<p>Full access to the MarriedGames.org lovemaking suite is reserved for <a href="/membership-options">premium members</a>.</p>
				<a href="/membership-options" class="button" style="display:inline-block !important; margin:1em auto;">Upgrade your account</a>
			<?php } ?>
			<?php if (current_user_cannot("access_s2member_level0")) { ?>
				<a href="/register" class="play-for-free button">Play for Free</a>
			<?php } else include "getFreeGame.php" ?>
			<p style="margin-top:1em; text-align:center;"><a href="/login">Log In</a> | <a href="/membership-options">Get Premium Membership</a></p>
		</div><!-- play-for-free-container -->
		<div class="restriction-bg"></div><!-- restrictionBG -->
	</div><!-- restriction -->
<?php } ?>