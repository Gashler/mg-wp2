<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div id="primary" class="site-content">
		<?php if ( get_post_status ( $ID ) !== 'private' ) { ?>
			<!-- AddThis Button BEGIN -->
			<div class="addthis_container" style="position:absolute; z-index:501;">
				<div class="addthis_toolbox addthis_floating_style addthis_counter_style">
				<a class="addthis_button_facebook_like" fb:like:layout="box_count"></a>
				<a class="addthis_button_tweet" tw:count="vertical"></a>
				<a class="addthis_button_google_plusone" g:plusone:size="tall"></a>
				<a class="addthis_counter"></a>
				</div>
				<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
				<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f9024373ebb49dc"></script>
			</div>
		<?php } ?>
		<!-- AddThis Button END -->
		<div id="content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', get_post_format() ); ?>
				
				<?php comments_template( '', true ); ?>

			<?php endwhile; // end of the loop. ?>
			<?php if (current_user_cannot("access_s2member_level1")) { ?>
				<!-- promotion -->
				<div style="text-align:center; margin:1em 0;">
					<p>See how MarriedGames.org can strengthen your marriage by making your sex life easier, more fun, and more frequent:</p>
					<p><a href="/checkout-gold-trial" class="button">Free Trial</a></p>
				</div>
				<!-- end promotion -->
			<?php } ?>
		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>