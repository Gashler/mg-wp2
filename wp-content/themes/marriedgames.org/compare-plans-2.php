<style>
	
	section { padding:5%; overflow:hidden; position:relative; }
	/*section:nth-of-type(even) {background:rgba(0,0,0,.1); }*/
	section h2 { margin:0; display: inline-block; padding: .25em 1em; border-radius: 24px; color: white; }
	section h3 { margin-bottom:0; font-weight:400; }
	section.highlight { z-index:1; box-shadow:0 0 100px rgba(0,0,0,.33); }
	.leftCol { float:left; }
	.leftCol p { margin:0 !important; }
	.checkout { float:right; text-align:center; }
	.checkout p { margin:0; line-height:1em; }
	.highlight { border:4px solid rgb(255,0,128); }
	.ribbon { -webkit-transform:rotate(45deg); transform:rotate(45deg); background:rgb(255,0,128); display:inline-block; padding:1em 4em; color:white; font-weight:bold; font-family:oswald; text-transform:uppercase; position:absolute; top:15px; right:-70px; box-shadow:-1px 1px 5px rgba(0,0,0,.5); }
	.checks li { float:left; width:47.5%; }
	.checks li:nth-of-type(odd) { margin-right:5%; }
	@media (max-width:800px) {
		section:nth-of-type(n+2) { text-align:center; }
		section h2, section img, section div { float:none !important; }
		section img { display:block; margin:0 auto !important; }
		section h3 { margin-top:1em !important; }
	}
	
</style>
<section>
	<img src="<?php echo get_template_directory_uri() ?>/images/couples/couple-1.jpg" alt="Couple" class="alignleft">
	<h1>Choose Your Plan</h1>	<img style="float:right; margin:-40px 0 0 10px;" alt="100% Money Back Guarantee" src="/wp-content/themes/marriedgames.org/images/seals/money-back-guarantee.png">

	<p><strong>Couples who utilize the games, tools, and resources with a MarriedGames.org</strong> subscription have transformed occassional and unsatisfying intimacy into a healthy and passionate sex life. Get the resources and ideas you'll need to freshen things up, improve communication, and strengthen your marriage. All plans comes with full access to the complete MarriedGames.org lovemaking suite and a 24 hour 100% money back guarantee. If you're not fully satisified, you've got nothing to lose!</p>
	<h2>What You Get</h2>
	<ul class="checks" style="padding-left:2em;">
		<li>Lovers Lane - the Online Sexy Board Game</li>
		<li>Sex Dice Game</li>
		<li>Strip Poker (coming soon)</li>
		<li>Foreplay Generator</li>
		<li>Sexy Question Generator</li>
		<li>Sex Position Generator</li>
		<li>Daily Articles for Date Ideas and Sex Tips</li>
		<li>Sex Life Reports</li>
		<li>Romantic Diary</li>
		<li>Sexual Favor Clock Tool</li>
		<li>Sexy Services Tool</li>
		<li>Sexy Role-playing Generator</li>
		<li>Sexy Role-playing Outline</li>
		<li>Sexy Role-playing Scripts</li>
		<li>Access to the Discounted Husband and Wife Store</li>
		<li>Marriage and Relationship Support</li>
		<li>All future updates and releases</li>
		<li>Plus much more to come!</li>
	</ul>
</section>
<section style="padding-top:0;">
	<div class="leftCol">
		<h2 style="background:rgba(0,0,0,.33);">Silver Subscription</h2>
		<h3>Less than 50&cent; per use</h3>
		<p>(When you have sex every 3 - 4 days)</p>
	</div>
	<div class="checkout">
		<p><span style="font-weight:bold;">$4<span class="cents">.99/mo</span></span><br>(no contract)</p>
		<a class="button" href="/checkout-silver">Checkout &rsaquo;</a>
	</div>
</section>
<section class="highlight">
	<div class="ribbon">
		Most Popular
	</div>
	<div class="leftCol">
		<h2 style="background:rgba(255,192,0,.5); color:black; float:left;">Gold Subscription</h2>
		<img style="float:left; margin:-40px 0 0 10px;" alt="50% Off" src="/wp-content/themes/marriedgames.org/images/seals/50-percent-off.png">
		<div class="clear"></div>
		<h3 style="margin-top:-1em;">Less than 25&cent; per use</h3>
		<p>(When you have sex every 3 - 4 days)</p>
	</div>
	<div class="checkout">
		<p style="margin-top:2em !important;"><span style="font-weight:bold;">$29<span class="cents">.99/year</span></span><br>(no contract)</p>
		<a class="button" href="/checkout-gold" style="position:relative;">
			Checkout &rsaquo;
			<img style="position:absolute; top:-7px; right:-55px;" src="http://i0.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/super-savings.png?w=960" alt="Super Savings" width="95" height="75">
		</a>
	</div>
</section>
<section>
	<div class="leftCol">
		<h2 style="background:rgba(0,0,0,.33);">Platinum Subscription</h2>
		<h3>One Payment for Lifetime Membership</h3>
		<p>(Best long-term value for couples ready to commit to a better love life)</p>
	</div>
	<div class="checkout">
		<p><span style="font-weight:bold;">$99<span class="cents"></span></span><br>(no contract)</p>
		<a class="button" href="/checkout-platinum">Checkout &rsaquo;</a>
	</div>
</section>