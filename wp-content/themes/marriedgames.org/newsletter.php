<!--START Scripts : this is the script part you can add to the header of your theme-->
<script type="text/javascript" src="//wp-includes/js/jquery/jquery.js?ver=2.6.2"></script>
<script type="text/javascript" src="//wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-en.js?ver=2.6.2"></script>
<script type="text/javascript" src="//wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.6.2"></script>
<script type="text/javascript" src="//wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.2"></script>
<script type="text/javascript">
	/* <![CDATA[ */
	var wysijaAJAX = {
		"action" : "wysija_ajax",
		"controller" : "subscribers",
		"ajaxurl" : "//wp-admin/admin-ajax.php",
		"loadingTrans" : "Loading..."
	};
	/* ]]> */
</script><script type="text/javascript" src="//wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.6.2"></script>
<!--END Scripts-->
<div style="text-align:center;">
	<div class="widget_wysija_cont html_wysija" style="overflow:hidden;">
		<h2 style="color:white; margin:2em 0 -1em; font-family:sexy; font-weight:bold;">Subscribe to Sexy Tips</h2>
		<div style="display:inline-block; max-width:500px; margin-top:2em;" id="msg-form-wysija-html534739e0d6ce4-1" class="wysija-msg ajax"></div>
		<form id="form-wysija-html534739e0d6ce4-1" method="post" action="#wysija" class="widget_wysija html_wysija">
			<div class="wysija-msg"></div>
			<div class="wysija-msg ajax"></div>
			<input type="hidden" value="d204f1c1bc" id="wysijax" />
			<input style="display:inline-block;" placeholder="First Name" type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="First name"  value="" />
			<span class="abs-req">
				<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
			</span>
			<input style="display:inline-block;" placeholder="Email Address" type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email"  value="" />
			<span class="abs-req">
				<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
			</span>
			<input class="wysija-submit wysija-submit-field" type="submit" value="Subscribe!" />
			<input type="hidden" name="form_id" value="1" />
			<input type="hidden" name="action" value="save" />
			<input type="hidden" name="controller" value="subscribers" />
			<input type="hidden" value="1" name="wysija-page" />
			<input type="hidden" name="wysija[user_list][list_ids]" value="1,6" />
		</form>
		<span style="color:white; display:block;">(Join <?php $number = do_shortcode("[wysija_subscribers_count list_id='6,5,1' ]"); echo number_format($number); ?> other subscribers)</span>
	</div>
</div>