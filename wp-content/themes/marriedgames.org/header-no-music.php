<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> style="background: rgb(255,128,192) !important">
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href='http://fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>

<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<?php wp_head(); ?>
<?php the_field('head'); ?>
<script src="/wp-content/themes/marriedgames.org/header-js.js"></script>
</head>

<body <?php body_class(); ?> style="background:url(/wp-content/themes/marriedgames.org/images/noise.png) !important;">

<!-- Facebook like box -->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=223106067842054";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- end Facebook like box -->

<div id="wrapper" class="hfeed site">
	<header id="header-container" class="site-header" role="banner">
			<div id="header">
				<div id="top-menu">
					<ul>
						<?php wp_list_pages('title_li=&child_of=64'); ?>
						<li>
							<a href="//account/">
								<?php
									if (is_user_logged_in()) echo "Account";
									else echo "Log In";
								?> 
							</a>
						</li>
					</ul>
				</div><!-- top-menu -->
				<div id="search">
					<?php get_search_form(); ?>
				</div><!-- search -->
				<div id="branding">
					<div id="site-title">
						<strong id="logo">
 							<a style="font-family: sexy !important; margin: 25px 0 0 0 !imporant; display:inline-block !important; font-size:32px !important; line-height:56px !important;" href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo( 'name' ) ?>" rel="home">MarriedGames.org</a>
						</strong>
						<p id="site-description"><?php bloginfo( 'description' ) ?></p>
					</div><!-- site-title -->
				</div><!-- branding -->
			</div><!-- header -->
			<div id="header-decoration"></div>


	</header><!-- #masthead -->
		<div class="menu-strip-container" id="header-menu-strip-container">
			<div class="menu" id="main-menu">
				<?php include "main-menu.php" ?>
			</div><!-- main-menu -->
		</div><!-- main-menu-container -->
<!-- home-page banner -->
<?php if (is_front_page()) { ?>
<?php include "home-banner.php" ?>
<?php } ?>
		<div id="content-container" class="clearfix">
			<content id="main-content" class="clearfix">

