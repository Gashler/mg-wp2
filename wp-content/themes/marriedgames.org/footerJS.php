<script>

	// pop
	
	function pop(content) {
		jQuery("body").prepend("<div class='black'></div><div class='pop'><div class='x'>&times;</div>" + content + "</div>");
		jQuery(".black, .pop").fadeIn(function() {
			jQuery(".black, .x").click(function() {
				jQuery(".black, .pop").fadeOut(function() {
					closePopup();
				});
			});
		});
	}
	function closePopup() {
		jQuery(".black, .pop").fadeOut(function() {
			jQuery(".black, .pop").remove();
		});
	}
	jQuery(".closePopup").click(function() {
		closePopup();
	});
	
	// end pop

	// toggle definition list items
	
	jQuery("dt").click(function() {
		$(this).next("dd").slideToggle();
	});

	// submit subscription

	jQuery("#submit").click(function() {
		var email = jQuery("#email").val();
		var atpos = email.indexOf("@");
		var dotpos = email.lastIndexOf(".");
		if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
			alert("Not a valid e-mail address");
		} else {
			jQuery("div.submitFields").slideUp();
			jQuery("div#result").html("Thank you for subscribing. Please check your email to complete the process.").slideDown();
			$.post("<?php echo $siteURL ?>registration/subscribe.php", {
				email : email
			}, function(result) {
				
			});
		}
	});

	// add text to email input

	email = "Email address"
	firstName = "First name"
	jQuery("#email").val(email).css("color", "gray").click(function() {
		if (jQuery(this).val() == email) {
			jQuery(this).val("").css("color", "black");
		}
	});
	jQuery("#name").val(firstName).css("color", "gray").click(function() {
		if (jQuery(this).val() == firstName) {
			jQuery(this).val("").css("color", "black");
		}
	});
	jQuery("#email").blur(function() {
		if (jQuery(this).val() == "") {
			jQuery(this).val(email).css("color", "gray");
		}
	});
	jQuery("#name").blur(function() {
		if (jQuery(this).val() == "") {
			jQuery(this).val(firstName).css("color", "gray");
		}
	});

	// highlight current page
	
	path = window.location.pathname;
	jQuery("#mainNav a[href = '" + path + "']").addClass("currentPage");
	if (path == "/") {
		jQuery("#mainNav a[href = '/index.php']").parent().addClass("currentPage");
	}
	//jQuery("#logo").css("font-size", )
	
	// main menu
	
	jQuery("#main-menu li li").each(function() {
		if (jQuery(this).children("ul").length > 0) {
			jQuery("> a", this).append("<div class='raq'>&#8250;</div>");
		}
	})
	
	jQuery("#main-menu li").hover(function() {
		jQuery("ul", this).show();
		jQuery("li ul", this).hide();
	}, function() {
		jQuery("ul", this).hide();
	});
	
	jQuery("#menu-button").click(function() {
		jQuery(".menu .container").slideToggle();
	});

	// check opt-In Subscribe on main form
	
	jQuery("#wysija-box-after-register").attr("checked", "checked");
	
</script>