<style>
	
	.compare-plan { float:left; max-width:250px; min-width:250px; text-align:center; min-height:300px; box-shadow:0 0 100px rgba(255,0,128,.5); }
	.compare-plan.most-popular { border:8px solid white; box-shadow:1px 1px 20px rgba(0,0,0,.5); position:relative; }
	.compare-plan h3 { color:white !important; margin:0; padding:5%; background:rgb(192,32,96); }
	.compare-plan-content { padding:15%; background:white; }
	.compare-plan-content p { margin-top:0 !important; font-size:1.5em; font-weight:300; line-height:1em; font-family:oswald; }
	.compare-plan-content .button { margin-top:1em; }
	
</style>
<h2>Choose Your Subscription Plan</h2>
<div class="compare-plan">
	<h3>Silver</h3>
	<div class="compare-plan-content">
		<p>Semi-yearly<small style="display:block; font-family:arial;">(Auto-renewing)</small></p>
		<div><div class='strike'>$98<div class='line'></div></div></div>
		<div>$49</div>
		<a class="button" style="cursor:pointer;" onclick="window.top.location='/checkout-silver'">Choose Plan</a>
	</div>
</div>
<div class="compare-plan most-popular">
	<h3>Gold</h3>
	<div class="compare-plan-content">
		<p>Yearly<br><small style="display:block; font-family:arial;">(Auto-renewing)</small></p>
		<p class="flash" style="font-family:arial; font-size:12pt;">Most Popular</p>
		<div><div class='strike'>$118<div class='line'></div></div></div>
		<div>$59</div>
		<a class="button" style="cursor:pointer;" onclick="window.top.location='/checkout-gold'">Choose Plan</a>
	</div>
</div>
<div class="compare-plan">
	<h3>Platinum</h3>
	<div class="compare-plan-content">
		<p>Lifetime Membership</p>
		<div><div class='strike'>$398<div class='line'></div></div></div>
		<div>$199</div>
		<a class="button" style="cursor:pointer;" onclick="window.top.location='/checkout-platinum'">Choose Plan</a>
	</div>
</div>