<p>Already registered? <a href="/account">Sign in</a>.</p>
<div class="col 1 generatedContent">
	<div class="login" id="theme-my-login">
		<form name="registerform" id="registerform" action="/account/" method="post">
			<input name="first_name" id="first_name" value="" class="regular-text" type="hidden">
			<div class="section-1">
				<p>
					<label for="ws-plugin--s2member-custom-reg-field-husband"> <span>Husband's First Name *</span></label>
					<input type="text" maxlength="100" autocomplete="off" value="" name="ws_plugin__s2member_custom_reg_field_husband" id="ws-plugin--s2member-custom-reg-field-husband" aria-required="true" tabindex="50" class="ws-plugin--s2member-custom-reg-field">
				</p>
				<p>
					<label for="ws-plugin--s2member-custom-reg-field-wife"> <span>Wife's First Name *</span></label>
					<input type="text" maxlength="100" autocomplete="off" value="" name="ws_plugin__s2member_custom_reg_field_wife" id="ws-plugin--s2member-custom-reg-field-wife" aria-required="true" tabindex="51" class="ws-plugin--s2member-custom-reg-field">
				</p>
				<button id="next" type="button">Next</button>
			</div><!-- section-1 -->
			<div class="section-2" style="display:none;">
				<p>
					<label for="user_login">Username</label>
					<input type="text" name="user_login" id="user_login" class="input" value="" size="20">
				</p>
	
				<p>
					<label for="user_email">E-mail</label>
					<input type="text" name="user_email" id="user_email" class="input" value="" size="20">
				</p>
	
				<input type="hidden" name="ws_plugin__s2member_registration" value="c9d56bf1b2">
				<p>
					<label for="ws-plugin--s2member-custom-reg-field-user-pass1" title="Please type your Password twice to confirm."> <span>Password (please type it twice) *</span>
						<br>
						<input type="password" aria-required="true" maxlength="100" autocomplete="off" name="ws_plugin__s2member_custom_reg_field_user_pass1" id="ws-plugin--s2member-custom-reg-field-user-pass1" class="ws-plugin--s2member-custom-reg-field" value="" tabindex="30">
					</label>
					<label for="ws-plugin--s2member-custom-reg-field-user-pass2" title="Please type your Password twice to confirm.">
						<input type="password" maxlength="100" autocomplete="off" name="ws_plugin__s2member_custom_reg_field_user_pass2" id="ws-plugin--s2member-custom-reg-field-user-pass2" class="ws-plugin--s2member-custom-reg-field" value="" tabindex="40">
					</label>
				</p>
				<div id="ws-plugin--s2member-custom-reg-field-user-pass-strength" class="ws-plugin--s2member-password-strength">
					<em>password strength indicator</em>
				</div>
				<p class="wysija-after-register muted">
					<input type="checkbox" id="wysija-box-after-register" value="1" name="wysija[register_subscribe]" checked="checked">
					I agree to the MarriedGames.org <a href="/terms">terms</a> and <a href="/privacy-policy">privacy policy</a>.</label>
				</p>
				<p class="submit" style="margin-top:1em;">
					<input type="submit" name="wp-submit" id="wp-submit" value="Register">
					<input type="hidden" name="redirect_to" value="//company/account/?checkemail=registered">
					<input type="hidden" name="instance" value="">
					<input type="hidden" name="action" value="register">
				</p>
			</div><!-- section-2 -->
		</form>
	</div>
</div>
<div style="clear:both;"></div>
<script>

	// reveal part 2 of form

	jQuery("#next").click(function() {
		jQuery(".section-1").hide();
		jQuery(".section-2").fadeIn();
	});
	
	// combine husband & wife's name for user's "first name"
	
	jQuery("input").keyup(function() {
		husband = jQuery("#ws-plugin--s2member-custom-reg-field-husband").val();
		wife = jQuery("#ws-plugin--s2member-custom-reg-field-wife").val();
		combinedNames = husband + " and " + wife;
		jQuery("#first_name").val(combinedNames);		
	});
	
</script>