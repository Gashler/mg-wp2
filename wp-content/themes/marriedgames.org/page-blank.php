<?php /**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 * Template Name: Page-blank
 *
 */
?>
<!doctype html>
<html>
	<head>
		<meta name="viewport" content="width=device-width, user-scalable=no">
		<title>Sex Games for Couples | MarriedGames.org</title>
		<meta name="description" content="Reignite your love life and strengthen your marriage with premium online sex games and resources for couples">
	</head>
	<body>
		<?php while ( have_posts() ) : the_post();
		?>
		<?php get_template_part('content', 'page'); ?>
		<?php endwhile; // end of the loop. ?>
	</body>
</html>
