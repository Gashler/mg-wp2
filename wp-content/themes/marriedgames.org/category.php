<?php
/**
 * The template for displaying Category pages.
 *
 * Used to display archive-type pages for posts in a category.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<section id="primary" class="site-content">
		<div id="content" role="main">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php printf( __( '%s', 'twentytwelve' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>

			<?php if ( category_description() ) : // Show an optional category description ?>
				<div class="archive-meta"><?php echo category_description(); ?></div>
			<?php endif; ?>
			</header><!-- .archive-header -->

			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		<div class="bottom-navigation">
			<?php posts_nav_link('  ', __('<span class="newer button small">Newer Posts &raquo;</span>'), __('<span class="older button small">&laquo; Older Posts</span>')); ?>
		</div>
		<?php if (current_user_cannot("access_s2member_level1")) { ?>
			<!-- promotion -->
			<div style="text-align:center; margin:1em 0;">
				<p>See how MarriedGames.org can strengthen your marriage by making your sex life easier, more fun, and more frequent:</p>
				<p><a href="/checkout-gold-trial" class="button">Free Trial</a></p>
			</div>
			<!-- end promotion -->
		<?php } ?>
		</div><!-- #content -->
	</section><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>