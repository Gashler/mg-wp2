<?php
if (!$_SESSION['member']) {
	$_SESSION['member'] = $_GET['member'];
}

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?> style="background: rgb(255,128,192) !important">
<!--<![endif]-->
<head>
	<?php if (is_front_page()) { ?>
		<!-- Google Analytics Content Experiment code -->
		<script>function utmx_section(){}function utmx(){}(function(){var
		k='75352570-6',d=document,l=d.location,c=d.cookie;
		if(l.search.indexOf('utm_expid='+k)>0)return;
		function f(n){if(c){var i=c.indexOf(n+'=');if(i>-1){var j=c.
		indexOf(';',i);return escape(c.substring(i+n.length+1,j<0?c.
		length:j))}}}var x=f('__utmx'),xx=f('__utmxx'),h=l.hash;d.write(
		'<sc'+'ript src="'+'http'+(l.protocol=='https:'?'s://ssl':
		'://www')+'.google-analytics.com/ga_exp.js?'+'utmxkey='+k+
		'&utmx='+(x?x:'')+'&utmxx='+(xx?xx:'')+'&utmxtime='+new Date().
		valueOf()+(h?'&utmxhash='+escape(h.substr(1)):'')+
		'" type="text/javascript" charset="utf-8"><\/sc'+'ript>')})();
		</script><script>utmx('url','A/B');</script>
		<!-- End of Google Analytics Content Experiment code -->
	<?php } ?>

	<meta name="p:domain_verify" content="2468fce047859435d4d4e9432e862ffb"/>

	<?php the_field('head'); ?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width" />
	<title><?php wp_title(); ?></title>
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<!--<link rel="profile" href="http://gmpg.org/xfn/11" />-->
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<link href='//fonts.googleapis.com/css?family=Oswald:400,700,300' rel='stylesheet' type='text/css'>
	<link rel="author" href="https://plus.google.com/u/0/117664311387448756981/posts" />
	<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
	<![endif]-->
	<?php wp_head(); ?>
	<?php if (!(is_page(336))) {
		// include get_template_directory_uri() . "/headerJS.php";
	} ?>
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-43056743-1', 'marriedgames.org');
	  ga('require', 'linkid', 'linkid.js');
	  ga('send', 'pageview');

	</script>
	<script>

		// get cookie

		function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i < ca.length; i ++) {
			var c = ca[i].trim();
			if (c.indexOf(name)==0) return c.substring(name.length,c.length);
		}
		return "";
		}

		// set cookie

		function setCookie(cname,cvalue,exdays)
		{
		var d = new Date();
		d.setTime(d.getTime()+(exdays*24*60*60*1000));
		var expires = "expires="+d.toGMTString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
		}

	</script>
	<?php
      $urlProtocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	  $wsiteurl = $urlProtocol . DOMAIN;
	?>
<?php
  if (is_user_logged_in()) { if (current_user_can('access_s2member_level1')) {
////if (S2MEMBER_CURRENT_USER_IS_LOGGED_IN_AS_MEMBER || is_admin()) {
?>
	<script>
           var loverslane = 'yes';
           var S2MEMBERCURRENT_USER_ID = "<?php echo S2MEMBER_CURRENT_USER_ID; ?>";
        </script>
 <?php	} } ?>
<meta name="google-site-verification" content="FcRw3Fn71TmpbmPOXDnbc1h-zdUnJD-169nPuKTkQas" />	<?php if(is_page(2983)){ ?>	   <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.autofill.js"></script>	   <script type="text/javascript">   	     jQuery(function(){           jQuery("#s2member-pro-paypal-checkout-email").keyup(function(){            jQuery("#s2member-pro-paypal-checkout-username").val(jQuery(this).val());           });         });       </script> 	  <style>	     .s2member-pro-paypal-form-div.s2member-pro-paypal-checkout-form-div.s2member-pro-paypal-checkout-form-username-div {		   display:none;		 }	   </style>	      <?php } ?>
</head>

<body <?php body_class(); ?> style="background:url(/wp-content/themes/marriedgames.org/images/noise.png) !important;">
<!-- Facebook like box -->
<?php if (!(is_page(336)) && current_user_cannot("access_s2member_level0")) { ?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=223106067842054";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<?php } ?>
<!-- end Facebook like box -->
<?php
	if ((is_child(48) || is_page(48)) && current_user_can("access_s2member_level1")) {
		include "music-player.php";
	}
?>
<div id="wrapper" class="hfeed site <?php the_field('wrapper_classes'); ?>">
	<header id="header-container" class="site-header" role="banner">
			<div id="header">
				<div id="top-menu">
					<ul>
						<?php if (is_user_logged_in()) { ?>
							<li><a href="<?php echo APP_URL ?>">My Account</a>.</li>
						<?php } else if (!(is_front_page()) && !(is_page(846))) { ?>
							<li><a href="<?php echo APP_URL ?>/login">Log In</a></li>
						<?php } ?>
					</ul>
				</div><!-- top-menu -->
				<div id="search">
					<?php if (!(is_front_page()) && !(is_page(1513)) && !(is_page(846))) { ?>
						<?php get_search_form(); ?>
					<?php } else { ?>
						<?php if (current_user_cannot("access_s2member_level1")) { ?>
							<a href="<?php echo APP_URL ?>/login" class="button" style="float:right; position:relative;">Member Login</a>
						<?php } else { ?>
							<a href="<?php echo APP_URL ?>" class="button" style="float:right; position:relative;">Dashboard</a>
						<?php } ?>
					<?php } ?>
				</div><!-- search -->
				<div id="branding">
					<div id="site-title">
						<strong id="logo">
 							<a style="font-family: sexy !important; margin: 25px 0 0 0 !imporant; display:inline-block !important; font-size:32px !important; line-height:56px !important;" href="<?php echo APP_URL ?>" title="<?php bloginfo( 'name' ) ?>" rel="home">MarriedGames.org</a>
						</strong>
						<p id="site-description"><?php bloginfo( 'description' ) ?></p>
					</div><!-- site-title -->
				</div><!-- branding -->
			</div><!-- header -->
			<div id="header-decoration"></div>


	</header><!-- #masthead -->
		<div class="menu-strip-container" id="header-menu-strip-container">
			<?php if (!(is_page(846)) && !(is_page(2232)) && !(is_page(2983)) && !(is_page(3503))) { ?>
				<div class="menu" id="main-menu">
					<?php if (!$_SESSION['member']) { ?>
						<button class="button" style="margin: 1em;" href="<?php echo APP_URL ?>/register">Try MarriedGames.org for Free</button>
					<?php } ?>
				</div><!-- main-menu -->
			<?php } ?>
		</div><!-- main-menu-container -->


<?php if (current_user_is("s2member_level1")) { ?>
	<?php
		$con=mysqli_connect("localhost","root","asdf","mg");
		// Check connection
		if (mysqli_connect_errno())
		  {
		  echo "Failed to connect to MySQL: " . mysqli_connect_error();
		  }

		// free trial period

		$startTime;
		$sql = "SELECT * FROM trials WHERE ip = '" . S2MEMBER_CURRENT_USER_IP . "'";
		while($row = $result->fetch_assoc()) {
			$startTime = $row['time'];
		}


	?>


	<style>
		@media (max-width:875px) {
			#trialPeriodCoupon { text-align:center; }
			#trialPeriodCoupon .button { float:none !important; margin-top:.5em; }
		}
	</style>
	<?php if (!(is_page(1976)) && !(is_page(2820)) && !(is_page(2878)) && !(is_page(2917)) && !(is_page(2936)) && !(is_page(2933)) && !(is_page(2930)) && !(is_page(2917))) { ?>
		<div style="max-width:915px; margin:0 auto; position:relative; top:2em; z-index:1;">
			<a href="/apply-coupon">
				<div class="coupon" id="trialPeriodCoupon" style="margin-bottom:-1em; overflow:hidden; background:rgb(255,255,128);">
					<strong><span style="font-size:2em; line-height:1em; font-family:oswald; font-weight:300; display:inline-block; margin:5px 0 0;">Time Left to Apply Premium Coupon: <span id="timeLeft"></span></span> <span class="button" style="float:right; margin-right:1em;">Click Here for 50% Off Subscription</span></strong>
					<img style="position:absolute; top:-7px; right:-15px;" src="<?php echo get_template_directory_uri() ?>/images/super-savings.png" alt="Super Savings">
				</div>
			</a>
		</div>
	<?php } ?>
	<script>
		var time = <?php echo time() ?>;
		var startTime = <?php echo $startTime ?>;
		setInterval(function() {
			elapsedTime = time - startTime;
			timeLeft = 86400 - elapsedTime;
			time += 1;
			timeLeft = 86400 - elapsedTime;
			if (timeLeft > 0) jQuery("#timeLeft").html(readableTime(timeLeft));
			else jQuery("#timeLeft").html("00:00:00");
		}, 1000);

		// make time readable

		function readableTime(time) {
			var formattedTime = parseInt(time / 3600); // hours
			minutes = parseInt((time % 3600) / 60); // minutes
			if (minutes < 10) minutes = "0" + minutes;
			formattedTime += ":" + minutes;
			seconds = (time % 60); // seconds
			if (seconds < 10) seconds = "0" + seconds;
			formattedTime += ":" + seconds;
			return formattedTime;
		}
	</script>
<?php } ?>
		<div id="content-container" class="clearfix">
			<div id="main-content" class="clearfix" style="position:relative;">
				<?php if (is_front_page() && current_user_cannot("access_s2member_level0")) { ?>
					<div style="position:absolute; top:0; right:-210px; z-index:1000; background:white;" class="fb-like-box" data-href="https://www.facebook.com/pages/Marriedgamesorg/210751609048781" data-width="193" data-height="340" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
				<?php } ?>
			<?php
				if ( is_tree("2518") /* games */ || is_tree("2571") /* ideas */ ) {
				   include "restriction.php";
				}
			?>
