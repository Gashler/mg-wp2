<?php if (current_user_cannot("access_s2member_level1")){ ?>
	<div class="restriction">
		<div class="play-for-free-container" style="text-align:center;">
			<a href="/register" class="button" style="display:inline-block !important;">Access as Premium Member</a>
		</div><!-- play-for-free-container -->
		<div class="restriction-bg"></div><!-- restrictionBG -->
	</div><!-- restriction -->
<?php } ?>