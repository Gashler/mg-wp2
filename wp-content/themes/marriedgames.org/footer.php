</div><!-- main-content -->
<div style="clear:both;"></div>
</div><!-- content-container -->
</div><!-- wrapper -->
<div class="menu-strip-container" id="footer-menu-strip-container">
    <div class="menu" id="footer-main-menu">
        <ul>
            <div class="container" style="width:100%;">
                <li>
                    <a href="<?php echo APP_URL ?>">About</a>
                </li>
                <li>
                    <a href="/company/contact-us/">Contact</a>
                </li>
                <li>
                    <a href="/company/privacy/">Privacy</a>
                </li>
                <li>
                    <a href="/company/terms/">Terms</a>
                </li>
                <?php if (current_user_cannot("access_s2member_level1")) { ?>
                    <!--<li><a href="/category/special-offers/">Special Offers</a></li>-->
                <?php } ?>
                <?php if (is_front_page()) { ?>
                    <li>
                        <a href="/category/romantic-ideas/">Blog</a>
                    </li>
                <?php } ?>
            </div>
        </ul>
    </div><!-- main-menu -->
</div><!-- main-menu-container -->
<div id="footerDecoration"></div>
<div id="footer">
    <!--<?php if (current_user_cannot("access_s2member_level1") && !(is_page(505))) { ?>
        <a href="/trial" target="_blank">
            <button style="margin:0 auto;">
                Play Now
            </button>
        </a>
    <?php } ?>-->
<?php if (is_front_page()) {
    include "popular-searches.php";
} ?>

    <?php if (current_user_cannot("access_s2member_level2")) { ?>
        <div style="margin:1em 0;">
            <?php echo do_shortcode("[s2Member-Security-Badge v='1' /]"); ?>
        </div>
    <?php } ?>
    <div id="copyright">
        <?php echo sprintf(__('%1$s %2$s %3$s. All Rights Reserved.', 'blankslate'), '&copy;', date('Y'), esc_html(get_bloginfo('name'))); ?>
    </div>
</div><!-- footer -->

<!-- free trial popup -->

<?php if (current_user_cannot("access_s2member_level0") && !(is_page(14)) && !(is_page(336)) && !(is_page(846)) && !(is_page(2232)) && !(is_page(1976)) && !(is_page(2820)) && !(is_page(2878)) && !(is_page(2917)) && !(is_page(2936)) && !(is_page(2933)) && !(is_page(2930)) && !(is_page(2983)) && !(is_page(2972)) && !(is_page(3074)) && !(is_page(3503))) { ?>
    <div id="free-trial" class="homePop" style="display:none; text-align:center;">
        <div style="text-align:center;">
            <h2 style="font-size:3em; margin-bottom:-.5em; margin-top:0;"><img alt="100% Money Back Guarantee" style="vertical-align:middle; position:relative; left:-66px; margin-right:-66px;" src="/wp-content/themes/marriedgames.org/images/seals/satisfaction-guarantee.png"> Free 24 Hour Trial</h2>
            <p style="margin-top:0; font-weight:bold;">(One Time Offer)</p>
            <p style="text-align:left;"><strong>When you sign up in the next 90 seconds</strong>, you'll get a free 24 hour trial of the complete MarriedGames.org lovemaking suite. If you choose to continue your membership past the trial period, your credit card information or PayPal account will ensure that you enjoy an uninterrupted subscription at only <span style="color:red; text-decoration: line-through;">$99.99</span> $59/year (less than 50&cent; when you have sex every 3 - 4 days). If you're not 100% satisfied, simply cancel during the trial period, and you'll never be charged. Don't miss out on this no-risk opportunity for a better love life!</p>
            <p style="font-size:2em;" class="counter">90</p>
            <button onclick="closePopup();">No Thanks</button>
            <a href="/special-offer" class="button">I Want a Free Trial &rsaquo;</a><br>
            <img width="150" height="14" src="<?php echo get_template_directory_uri() ?>/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
        </div>
    </div>
    <script>

        if (getCookie("offeredFreeTrial") == "") {
            setTimeout(function() {
                pop(jQuery("#free-trial").html());
                var counter = 91;
                setInterval(function() {
                    if (counter > 0) {
                        counter --;
                        jQuery(".counter").html(counter);
                    }
                    else {
                        closePopup();
                        setCookie("offeredFreeTrial", 1, 7);
                    }
                }, 1000);
            }, 90000);
        }

    </script>
<?php } ?>


<?php wp_footer(); ?>
<?php if (!(is_page(336))) { ?>
    <script>
        var husband = '<?php echo S2MEMBER_CURRENT_USER_FIRST_NAME ?>';
        var wife = '<?php echo S2MEMBER_CURRENT_USER_LAST_NAME ?>';
    </script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
<?php } ?><meta name="google-site-verification" content="FcRw3Fn71TmpbmPOXDnbc1h-zdUnJD-169nPuKTkQas" />
<?php if (is_page(48)): ?>
    <script>
        $('#go-to-app').submit();
    </script>
<?php endif ?>
</body>
</html>
