<?php
/*
Template Name: Foreplay Generator
*/
?>
<?php get_header(); ?>
<article id="content">
<?php the_post(); ?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<h1 class="entry-title"><?php the_title(); ?></h1>
<div class="entry-content">
<?php 
if ( has_post_thumbnail() ) {
the_post_thumbnail();
} 
?>
<?php the_content(); ?>
<div class="generatedContent">
	<?php include("wp-content/themes/marriedgames.org/getForeplayContent.php"); ?>
</div>
<input id="newAction" type="submit" value="New Action" class="button" active />
<script>
	
	jQuery("#newAction").click(function(){
		jQuery(".generatedContent").load("wp-content/themes/marriedgames.org/getForeplayContent.php");
	});
	
</script>
<?php wp_link_pages('before=<div class="page-link">' . __( 'Pages:', 'blankslate' ) . '&after=</div>') ?>
<?php edit_post_link( __( 'Edit', 'blankslate' ), '<div class="edit-link">', '</div>' ) ?>
</div>
</div>
</article>
<?php get_sidebar(); ?>
<?php get_footer(); ?>