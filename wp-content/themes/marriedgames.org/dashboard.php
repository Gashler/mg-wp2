<style>h1 { display: none !important; }</style>
<img src="<?php echo APP_URL ?>/img/loading.gif">
<?php
    // token validation between Wordpress and Laravel
    if (!$_SESSION['token']) {
        // connect to mg_app database
        $conn = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // generate and store token
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $token = '';
        for ($i = 0; $i < 40; $i++) {
            $token .= $characters[rand(0, $charactersLength - 1)];
        }
        $sql = "REPLACE INTO tokens SET token_key = 'wp', token_value = '$token';";
        if ($conn->query($sql) !== true) {
            die("Error: " . $sql . "<br>" . $conn->error);
        }
        $conn->close();
        $_SESSION['token'] = $token;
    }

    // prepare data
    $fields = json_decode(S2MEMBER_CURRENT_USER_FIELDS, true);
    $user = wp_get_current_user();
    if (S2MEMBER_CURRENT_USER_ACCESS_LEVEL > 0) {
        $subscribed = true;
    } else {
        $subscribed = false;
    }
?>
<?php // submit user data to app ?>
<form id="go-to-app" action="<?php echo APP_URL ?>/wp-login" method="post">
    <input type="hidden" name="wp_user_id" value="<?php echo S2MEMBER_CURRENT_USER_ID ?>">
    <input type="hidden" name="husband" value="<?php echo $fields['first_name'] ?>">
    <input type="hidden" name="wife" value="<?php echo $fields['last_name'] ?>">
    <input type="hidden" name="login" value="<?php echo $user->user_login ?>">
    <input type="hidden" name="email" value="<?php echo $fields['email'] ?>">
    <input type="hidden" name="subscribed" value="<?php echo $subscribed ?>">
    <input type="hidden" name="token" value="<?php echo $token ?>">
</form>
<script type="text/javascript">
    document.getElementById('go-to-app').submit();
</script>
