jQuery(document).ready( function($) {		  
	var insertCzCode = function(cmaz_code) {
		var cmaz_content = $('#content').val();
			//if( ! tinyMCE.activeEditor || tinyMCE.activeEditor.isHidden()) {
				if ($('#content')[0].selectionStart || $('#content')[0].selectionStart == '0') {
						var startPos = $('#content')[0].selectionStart;
						var endPos = $('#content')[0].selectionEnd;
						cmaz_content = cmaz_content.substring(0, startPos) + cmaz_code + cmaz_content.substring(endPos, cmaz_content.length);
						$("#content").val(cmaz_content);
				} else {
					cmaz_content = cmaz_content + cmaz_code;
					$("#content").val(cmaz_content);
				}
			//}else{
			//tinyMCE.activeEditor.execCommand('mceInsertContent', 0, cmaz_code);
			//}
	} 
	var openCmaz = function() {
		jQuery.post(ajaxurl, {
			action: "cmaz_tinymce_html"			
		}, function(data) {
			$("#cmaz-form").html(data);
		
			$('#cmaz-submit1').click(function(){
				var shortcode = '';
				var scbase = '[cmaz';

				var caid = jQuery("#cmaz-caid").val();
				
				//var autoIgnore = jQuery("#cmaz-autoignore").val();
				if (caid != '')
				{
					shortcode = scbase;
					if (caid > 0)
						shortcode += ' id="' + caid + '"'; 
					shortcode += ']';
				}
				
				jQuery('#cmaz-caid').val('');
				insertCzCode(shortcode);
				$( "#cmaz-form" ).dialog( "close" );
				return false;
			});
					
			$( "#cmaz-form" ).dialog({
					autoOpen: true,
					width: 600,
					height: 600,
					modal: false,
					buttons: {
						Cancel: function() {
							$( this ).dialog( "close" );
						}
					},
					close: function() {
						//allFields.val( "" ).removeClass( "ui-state-error" );
					}
			});
			
		});
		return false;
	}				
		
	var title;
	$("#titlediv .inside").append('<a href="#" id="cmaz-editor-insert" style="width:35px;float:center;vertical-align:bottom !important;margin-top:20px !important;margin-left:50%;"><img src="' + CmAz.imgurl + '" border="0"></a><div id="cmaz-form" title="ComboZon - Insert Ad" style="display:none;"></div>');
	$("#cmaz-editor-insert").click(openCmaz);	

});
