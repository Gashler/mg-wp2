<?php
defined('WYSIJA') or die('Restricted access');

class WYSIJA_model_stats_newsletters_in_time extends WYSIJA_model {

	/**
	 * the shortcode of the link unsubscribe
	 * @var string
	 */
	protected $unsubscribe_link_shortcode = '[unsubscribe_link]';

	/**
	 * Counts emails which were REALLY SENT.
	 * @param datetime $from_date Filter by from date (optional)
	 * @param datetime $to_date Filter by to date (optional)
	 * @param int $email_id id of current email (optional)
	 * @param int $group_by Group by a specific field (optional)
	 * @return stats (count) based on time frame
	 * Array(<br/ >
	 *   [2005,3,1] => 15,<br/ >
	 *   [2005,4,1] => 33,<br/ >
	 *   [2005,5,1] => 36,<br/ >
	 *   [2005,6,1] => 25,<br/ >
	 *   [2005,7,1] => 12,<br/ >
	 *   [2013,8,1] => 9<br/ >
	 * )
	 */
	public function get_sent_newsletters_total($from_date = NULL, $to_date = NULL, $email_id = 0, $user_id = 0, $group_by = WYSIJA_module_statistics::GROUP_BY_DATE) {
		$helper_toolbox = WYSIJA::get('toolbox', 'helper');
		$sql_where	  = array( 'eus.`sent_at` > 0' );
		if (!empty($from_date))
			$sql_where[] = 'eus.`sent_at` >= '.$helper_toolbox->localtime_to_servertime(strtotime($from_date));
		if (!empty($to_date))
			$sql_where[] = 'eus.`sent_at` <= '.$helper_toolbox->localtime_to_servertime(strtotime($to_date));
		if (!empty($email_id))
			$sql_where[] = 'e.`email_id` = '.(int)$email_id;
		if (!empty($user_id))
			$sql_where[] = 'eus.`user_id` = '.(int)$user_id;

		$query = '
            SELECT
                `time`,
                COUNT(*) AS `count`,
		GROUP_CONCAT(DISTINCT `email_id`) as `lists`
            FROM
                (SELECT
                    eus.`sent_at`,
                    DATE_FORMAT(FROM_UNIXTIME(eus.`sent_at`), "'.$this->get_date_time_format($group_by).'") AS `time`,
		    `email_id`
                FROM
                    [wysija]email_user_stat eus
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    eus.`sent_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `sent_at`
            ';

		$result = $this->get_results($query);

		// format data, consider `time` as key
		$tmp = array( );
		if (!empty($result)) {
			// $helper_mailer = WYSIJA::get('mailer', 'helper');
			// $helper_shortcodes = WYSIJA::get('shortcodes', 'helper');
			foreach ($result as $value) {
//                 $emails = explode(':::', $value['lists']);
//                $_tmp2 = array();
//                foreach ($emails as $email) {
//                    break;
//                    $_tmp3 = explode('^^^', $email);
//                    if (!empty($_tmp3[0]) && !empty($_tmp3[1])) {
//                        // render shortcodes
//                        $_email_object = new stdClass();
//                        $_email_object->subject = $_tmp3[1];
//                        $helper_mailer->parseSubjectUserTags($_email_object);
//                        $_tmp3[1] = $helper_shortcodes->replace_subject($_email_object);
//                        // assign to the tmp table
//                        $_tmp2[$_tmp3[0]] = $_tmp3[1];
//                    }
//                }
//                $value['lists'] = $_tmp2;
				$value['lists']	  = explode(',', $value['lists']);
				$tmp[$value['time']] = $value;
			}
		}
		return $tmp;
	}

	/**
	 * @param datetime $from_date Filter by from date (optional)
	 * @param datetime $to_date Filter by to date (optional)
	 * @param int $email_id id of current email (optional)
	 * @param int $group_by Group by a specific field (optional)
	 * @return stats (count) based on time frame
	 * Array(<br/ >
	 *   [2005,3,1] => 15,<br/ >
	 *   [2005,4,1] => 33,<br/ >
	 *   [2005,5,1] => 36,<br/ >
	 *   [2005,6,1] => 25,<br/ >
	 *   [2005,7,1] => 12,<br/ >
	 *   [2013,8,1] => 9<br/ >
	 * )
	 */
	public function get_stat_sent($from_date = NULL, $to_date = NULL, $email_id = 0, $user_id = 0, $group_by = WYSIJA_module_statistics::GROUP_BY_DATE) {
		$helper_toolbox = WYSIJA::get('toolbox', 'helper');
		$sql_where	  = array( 'eus.`sent_at` > 0' );
		if (!empty($from_date))
			$sql_where[] = 'eus.`sent_at` >= '.$helper_toolbox->localtime_to_servertime(strtotime($from_date));
		if (!empty($to_date))
			$sql_where[] = 'eus.`sent_at` <= '.$helper_toolbox->localtime_to_servertime(strtotime($to_date));
		if (!empty($email_id))
			$sql_where[] = 'e.`email_id` = '.(int)$email_id;
		if (!empty($user_id))
			$sql_where[] = 'eus.`user_id` = '.(int)$user_id;

		// full info query (subject + mail+id), temporarily not in use
		$query = '
            SELECT
                `time`,
                COUNT(*) AS `count` ,
                GROUP_CONCAT(DISTINCT `email_id`,"^^^",`subject` SEPARATOR ":::") as `lists`
            FROM
                (SELECT
                    eus.`sent_at`,
                    DATE_FORMAT(FROM_UNIXTIME(eus.`sent_at`), "'.$this->get_date_time_format($group_by).'") AS `time`,
                    e.`email_id`,
                    e.`subject`
                FROM
                    [wysija]email_user_stat eus
                JOIN
                     [wysija]email e ON eus.email_id = e.email_id
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    eus.`sent_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `sent_at`
            ';
		// this query counts emails which were REALLY SENT. Temporarily, not in use.
		$query = '
            SELECT
                `time`,
                COUNT(*) AS `count`
            FROM
                (SELECT
                    eus.`sent_at`,
                    DATE_FORMAT(FROM_UNIXTIME(eus.`sent_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    [wysija]email_user_stat eus
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    eus.`sent_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `sent_at`
            ';

		// this query counts emails which were REALLY SENT and NOT OPENED OR NOT CLICKED yet. In short, it counts emails whose status is "SENT"
		$sql_where[] = '`status` = 0';
		$query	   = '
            SELECT
                `time`,
                COUNT(*) AS `count`
            FROM
                (SELECT
                    eus.`sent_at`,
                    DATE_FORMAT(FROM_UNIXTIME(eus.`sent_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    [wysija]email_user_stat eus
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    eus.`sent_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `sent_at`
            ';
		$result	  = $this->get_results($query);

		// format data, consider `time` as key
		$tmp = array( );
		if (!empty($result)) {
			// $helper_mailer = WYSIJA::get('mailer', 'helper');
			// $helper_shortcodes = WYSIJA::get('shortcodes', 'helper');
			foreach ($result as $value) {
//                 $emails = explode(':::', $value['lists']);
//                $_tmp2 = array();
//                foreach ($emails as $email) {
//                    break;
//                    $_tmp3 = explode('^^^', $email);
//                    if (!empty($_tmp3[0]) && !empty($_tmp3[1])) {
//                        // render shortcodes
//                        $_email_object = new stdClass();
//                        $_email_object->subject = $_tmp3[1];
//                        $helper_mailer->parseSubjectUserTags($_email_object);
//                        $_tmp3[1] = $helper_shortcodes->replace_subject($_email_object);
//                        // assign to the tmp table
//                        $_tmp2[$_tmp3[0]] = $_tmp3[1];
//                    }
//                }
//                $value['lists'] = $_tmp2;
				$tmp[$value['time']] = $value;
			}
		}
		return $tmp;
	}

	/**
	 * @param datetime $from_date Filter by from date (optional)
	 * @param datetime $to_date Filter by to date (optional)
	 * @param int $email_id id of current email (optional)
	 * @param int $user_id id of current subscriber (optional)
	 * @param boolean $is_opened true = get opened newsletters only; false = get unopend newsletters only
	 * @param int $group_by Group by date / month / year
	 * @return stats (count) based on time frame
	  [2005,3,1] => 15
	  [2005,4,1] => 33
	  [2005,5,1] => 36
	  [2005,6,1] => 25
	  [2005,7,1] => 12
	  [2013,8,1] => 9
	 */
	public function get_stat_open($from_date = NULL, $to_date = NULL, $email_id = 0, $user_id = 0, $is_opened = true, $group_by = WYSIJA_module_statistics::GROUP_BY_DATE) {
		$helper_toolbox = WYSIJA::get('toolbox', 'helper');
		$sql_where	  = array( 1 );

		if ($is_opened)
			$sql_where[] = 'opened_at > 0';
		else
			$sql_where[] = 'opened_at <= 0';

		if (!empty($from_date))
			$sql_where[] = 'opened_at >= '.$helper_toolbox->localtime_to_servertime(strtotime($from_date));
		if (!empty($to_date))
			$sql_where[] = 'opened_at <= '.$helper_toolbox->localtime_to_servertime(strtotime($to_date));
		if (!empty($email_id))
			$sql_where[] = 'email_id = '.(int)$email_id;
		if (!empty($user_id))
			$sql_where[] = 'user_id = '.(int)$user_id;

		// full info query (subject + mail+id), temporarily not in use
		$query = '
            SELECT
                `time`,
                COUNT(*) AS `count` ,
                GROUP_CONCAT(DISTINCT `email_id`,"^^^",`subject` SEPARATOR ":::") as `lists`
            FROM
                (SELECT
                    `opened_at`,
                    DATE_FORMAT(FROM_UNIXTIME(`opened_at`), "'.$this->get_date_time_format($group_by).'") AS `time`,
                    e.`email_id`,
                    e.`subject`
                FROM
                    [wysija]email_user_stat eus
                JOIN
                     [wysija]email e ON eus.email_id = e.email_id
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `opened_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `opened_at`
            ';

		// this query counts emails which were REALLY OPENED. Temporarily, not in use.
		$query	   = '
            SELECT
                `time`,
                COUNT(*) AS `count`
            FROM
                (SELECT
                    `opened_at`,
                    DATE_FORMAT(FROM_UNIXTIME(`opened_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    [wysija]email_user_stat eus
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `opened_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `opened_at`
            ';
		// this query counts emails which were REALLY SENT and REALLY OPENED but NOT CLICKED yet. In short, it counts emails whose status is "OPENED"
		$sql_where[] = '`status` = 1';

		$query  = '
            SELECT
                `time`,
                COUNT(*) AS `count`
            FROM
                (SELECT
                    `opened_at`,
                    DATE_FORMAT(FROM_UNIXTIME(`opened_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    [wysija]email_user_stat eus
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `opened_at` ASC
                ) AS `temp_table`
            GROUP BY `time`
            ORDER BY `opened_at`
            ';
		$result = $this->get_results($query);

		// format data, consider `time` as key
		$tmp = array( );
//        $helper_mailer = WYSIJA::get('mailer', 'helper');
//        $helper_shortcodes = WYSIJA::get('shortcodes', 'helper');
		if (!empty($result)) {
			foreach ($result as $value) {
//                $emails = explode(':::', $value['lists']);
//                $_tmp2 = array();
//                foreach ($emails as $email) {
//
//                    $_tmp3 = explode('^^^', $email);
//                    if (!empty($_tmp3[0]) && !empty($_tmp3[1])) {
//                        // render shortcodes
//                        $_email_object = new stdClass();
//                        $_email_object->subject = $_tmp3[1];
//                        $helper_mailer->parseSubjectUserTags($_email_object);
//                        $_tmp3[1] = $helper_shortcodes->replace_subject($_email_object);
//                        // assign to the tmp table
//                        $_tmp2[$_tmp3[0]] = $_tmp3[1];
//                    }
//                }
//                $value['lists'] = $_tmp2;

				$tmp[$value['time']] = $value;
			}
		}
		return $tmp;
	}

	/**
	 *
	 * @param datetime $from_date (optional)
	 * @param datetime $to_date (optional)
	 * @param int $email_id id of current email (optional)
	 * @param int $user_id id of current subscriber (optional)
	 * @param int $group_by (optional)
	 * @return stats (count) based on time frame
	  [2005,3,1] => 15
	  [2005,4,1] => 33
	  [2005,5,1] => 36
	  [2005,6,1] => 25
	  [2005,7,1] => 12
	  [2013,8,1] => 9
	 */
	public function get_stat_click($from_date = NULL, $to_date = NULL, $email_id = 0, $user_id = 0, $group_by = WYSIJA_module_statistics::GROUP_BY_DATE) {
		$helper_toolbox = WYSIJA::get('toolbox', 'helper');
		$sql_where	  = array( 'clicked_at > 0' );
		if (!empty($from_date))
			$sql_where[] = 'clicked_at >= '.$helper_toolbox->localtime_to_servertime(strtotime($from_date));
		if (!empty($to_date))
			$sql_where[] = 'clicked_at <= '.$helper_toolbox->localtime_to_servertime(strtotime($to_date));
		if (!empty($email_id))
			$sql_where[] = 'email_id = '.(int)$email_id;
		if (!empty($user_id))
			$sql_where[] = 'user_id = '.(int)$user_id;

		// full info query (subject + mail+id), temporarily not in use
		$query = '
            SELECT
                `time`,
                SUM(`number_clicked`) AS `count` ,
                GROUP_CONCAT(DISTINCT `email_id`,"^^^",`subject` SEPARATOR ":::") as `lists`
            FROM
                (SELECT
                    euu.`number_clicked`,
                    DATE_FORMAT(FROM_UNIXTIME(euu.`clicked_at`), "'.$this->get_date_time_format($group_by).'") AS `time`,
                    e.`email_id`,
                    e.`subject`
                FROM
                    [wysija]email_user_url euu
                JOIN
                     [wysija]email e ON euu.email_id = e.email_id
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `clicked_at` ASC
                ) AS `temp_table`
            GROUP BY `time`;
            ';
		// this query counts number_clicks which were made by users, based on user+email+link. Temporarily, not in use
		$query = '
            SELECT
                `time`,
                SUM(`number_clicked`) AS `count`
            FROM
                (SELECT
                    euu.`number_clicked`,
                    DATE_FORMAT(FROM_UNIXTIME(euu.`clicked_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    [wysija]email_user_url euu
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `clicked_at` ASC
                ) AS `temp_table`
            GROUP BY `time`;
            ';

		// this query counts emails which were REALLY SENT and REALLY OPENED and REALLY CLICKED. In short, it counts emails whose status is "CLICKED"
		// and here, we distinct by email + user. We don't care how many links there are in a same newsletter
		$query = '
            SELECT
                `time`,
                COUNT(`url_id`) AS `count`
            FROM
                (SELECT
                    euu.`url_id`,
                    DATE_FORMAT(FROM_UNIXTIME(euu.`clicked_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    [wysija]email_user_url euu
                WHERE '.implode(' AND ', $sql_where).'
		GROUP BY
		    `email_id`,
		    `user_id`
                ORDER BY
                    `clicked_at` ASC
                ) AS `temp_table`
            GROUP BY `time`;
            ';

		$result = $this->get_results($query);

		// format data, consider `time` as key
		$tmp = array( );
		if (!empty($result)) {
//            $helper_mailer = WYSIJA::get('mailer', 'helper');
//            $helper_shortcodes = WYSIJA::get('shortcodes', 'helper');
			foreach ($result as $value) {
//                $emails = explode(':::', $value['lists']);
//                $_tmp2 = array();
//                foreach ($emails as $email) {
//                    $_tmp3 = explode('^^^', $email);
//                    if (!empty($_tmp3[0]) && !empty($_tmp3[1])) {
//                        // render shortcodes
//                        $_email_object = new stdClass();
//                        $_email_object->subject = $_tmp3[1];
//                        $helper_mailer->parseSubjectUserTags($_email_object);
//                        $_tmp3[1] = $helper_shortcodes->replace_subject($_email_object);
//                        // assign to the tmp table
//                        $_tmp2[$_tmp3[0]] = $_tmp3[1];
//                    }
//                }
//                $value['lists'] = $_tmp2;
				$tmp[$value['time']] = $value;
			}
		}
		return $tmp;
	}

	/**
	 *
	 * @param datetime $from_date (optional)
	 * @param datetime $to_date (optional)
	 * @param int $group_by (optional)
	 * @return stats (count) based on time frame
	  [2005,3,1] => 15
	  [2005,4,1] => 33
	  [2005,5,1] => 36
	  [2005,6,1] => 25
	  [2005,7,1] => 12
	  [2013,8,1] => 9
	 *
	 */
	public function get_stat_unsubscribe($from_date = NULL, $to_date = NULL, $group_by = WYSIJA_module_statistics::GROUP_BY_DATE) {
		$helper_toolbox = WYSIJA::get('toolbox', 'helper');
		$sql_where	  = array( 'unsub_date > 0' );
		if (!empty($from_date))
			$sql_where[] = 'unsub_date >= '.$helper_toolbox->localtime_to_servertime(strtotime($from_date));
		if (!empty($to_date))
			$sql_where[] = 'unsub_date <= '.$helper_toolbox->localtime_to_servertime(strtotime($to_date));

		// full info query (subject + mail+id), temporarily not in use
		$query = '
            SELECT
                `time`,
                COUNT(*) AS `count` ,
                GROUP_CONCAT(DISTINCT `list_id`,"^^^",`name` SEPARATOR ":::") as `lists`
            FROM
                (SELECT
                    ul.`unsub_date`,
                    DATE_FORMAT(FROM_UNIXTIME(ul.`unsub_date`), "'.$this->get_date_time_format($group_by).'") AS `time`,
                    l.`list_id`,
                    l.`name`
                FROM
                    [wysija]user_list ul
                JOIN
                    [wysija]list l ON ul.`list_id` = l.`list_id`
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `unsub_date` ASC
                ) AS `temp_table`
            GROUP BY `time`;
            ';

		// count unsubscribes based list, user_list, user
		$query = '
            SELECT
                `time`,
                COUNT(*) AS `count`
            FROM
                (SELECT
                    ul.`unsub_date`,
                    DATE_FORMAT(FROM_UNIXTIME(ul.`unsub_date`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    `[wysija]user_list` ul
                WHERE '.implode(' AND ', $sql_where).'
                ORDER BY
                    `unsub_date` ASC
                ) AS `temp_table`
            GROUP BY `time`;
            ';

		// count unsubsribes based on the click on [unsubscribe] link
		$sql_where_3 = array( );
		$sql_where_3[] = 'euu.`clicked_at` > 0';
		$sql_where_3[] = 'euu.url_id = '.$this->get_unsubscribe_url_id();

		if (!empty($from_date))
			$sql_where_3[] = '`clicked_at` >= '.$helper_toolbox->localtime_to_servertime(strtotime($from_date));
		if (!empty($to_date))
			$sql_where_3[] = '`clicked_at` <= '.$helper_toolbox->localtime_to_servertime(strtotime($to_date));
		$query		 = '
            SELECT
                `time`,
                COUNT(*) AS `count`
            FROM
                (SELECT
                    euu.`clicked_at`,
                    DATE_FORMAT(FROM_UNIXTIME(euu.`clicked_at`), "'.$this->get_date_time_format($group_by).'") AS `time`
                FROM
                    `[wysija]email_user_url` euu
                WHERE '.implode(' AND ', $sql_where_3).'
                ORDER BY
                    `clicked_at` ASC
                ) AS `temp_table`
            GROUP BY `time`;
            ';
		$result		= $this->get_results($query);

		// format data, consider `time` as key
		$tmp = array( );
		if (!empty($result)) {
			foreach ($result as $value) {
//                $lists = explode(':::', $value['lists']);
//                $_tmp2 = array();
//                foreach ($lists as $list) {
//                    $_tmp3 = explode('^^^', $list);
//                    if (!empty($_tmp3[0]) && !empty($_tmp3[1])) {
//                        $_tmp2[$_tmp3[0]] = $_tmp3[1];
//                    }
//                }
//                $value['lists'] = $_tmp2;
				$tmp[$value['time']] = $value;
			}
		}
		return $tmp;
	}

	/**
	 * Get id of unsubscribe link
	 * @return type
	 */
	protected function get_unsubscribe_url_id() {
		$query  = '
	    SELECT `url_id` FROM `[wysija]url` WHERE `url` = "'.$this->unsubscribe_link_shortcode.'"
	    ';
		$result = $this->get_results($query);
		return ($result && !empty($result[0]['url_id']) ? (int)$result[0]['url_id'] : 0);
	}

	/**
	 * Get the subscribe date of a user
	 * @param int $user_id
	 * @return string (formated date/time) | NULL
	 */
	public function get_user_subscribe_date($user_id, $group_by = WYSIJA_module_statistics::GROUP_BY_DATE) {
		$query = '
            SELECT
                DATE_FORMAT(FROM_UNIXTIME(`created_at`), "'.$this->get_date_time_format($group_by).'") AS `created_at`
            FROM [wysija]user
            WHERE `user_id` = '.(int)$user_id;

		$result = $this->get_results($query);
		return !empty($result[0]['created_at']) ? $result[0]['created_at'] : NULL;
	}

	/**
	 * Get date time format, base on a type of group by (date, month, year, etc)
	 * @param type $group_by a const of class class Stats_group_by
	 * @return string date_time format
	 * @link http://www.w3schools.com/sql/func_date_format.asp
	 */
	protected function get_date_time_format($group_by) {
		// we are using %m (instead of %c), %d (instead of %e) to make sure:sorting by date works correctly
		switch ($group_by) {
			case WYSIJA_module_statistics::GROUP_BY_YEAR:
				$date_time_format = '%Y,01,01'; // year, month, day, hours, minutes, seconds, milliseconds
				break;
			case WYSIJA_module_statistics::GROUP_BY_MONTH:
				$date_time_format = '%Y,%m,01'; // year, month, day, hours, minutes, seconds, milliseconds
				break;
			case WYSIJA_module_statistics::GROUP_BY_DATE:
			default:
				$date_time_format = '%Y,%m,%d'; // year, month, day, hours, minutes, seconds, milliseconds
				break;
		}
		return $date_time_format;
	}

}