<?php
defined('WYSIJA') or die('Restricted access');

require_once(dirname(__FILE__).DS.'stats_newsletters_in_time_model.php');

class WYSIJANLP_module_stats_newsletters_in_time extends WYSIJA_module_statisticschart {

	public $name  = 'stats_newsletters_in_time';

	public $model = 'WYSIJA_model_stats_newsletters_in_time';

	public $view  = 'stats_newsletters_in_time_view';

	static protected $stats_data;

	/**
	 * Count the latest of an email or all current + previous of an email
	 * @var type
	 */
	protected $count_by_latest_status = true; // True = at a same time, combination of (1 email + 1 user) is connected to only 1 LATEST status

	public function __construct() {
		$this->extended_plugin = WYSIJANLP;
		parent::__construct();
	}

	/**
	 * hookStats - main view of statistics page
	 * @param array $params Input params
	  [top] => 5
	  [from] =>
	  [to] =>
	  [order_by] =>
	  [order_direction] => 2
	 *
	 * )
	 * @return longtext rendered view
	 */
	public function hook_stats($params, $sent_stat = true, $open_stat = true, $click_stat = true, $unsubscribe_stat = true, $unread_stat = false) {
		if (!$this->is_premium)
			return;


		$this->data['title'] = empty($this->data['title']) ? __('Open, clicks & unsubscribes', WYSIJA) : $this->data['title'];

		$this->data['stats_open_rate'] = $this->get_stats($params, $sent_stat, $open_stat, $click_stat, $unsubscribe_stat, $unread_stat);

		$this->view_show = 'hook_stats';

		wp_enqueue_script('amcharts', WYSIJANLP_URL."js/amcharts/amcharts.js", array( ), WYSIJA::get_version());

		return $this->render();
	}

	/**
	 * page Wysija >> Subscribers >> view detail
	 * @param array $params
	 * @return string
	 */
	public function hook_subscriber_bottom($params) {
		// @todo: possible confict if jquery.datepicker is already loaded
		wp_enqueue_script('jquery.datepicker', WYSIJA_URL."js/jquery/ui/jquery.ui.datepicker.js", array( 'jquery' ), WYSIJA::get_version());
		$this->data['title'] = __('Newsletters in time', WYSIJA);
		return $this->hook_stats($params, false, true, true, false, false);
	}

	/**
	 * hook_newsletter - page Wysija >> Newsletters >> view detail
	 * @param array $params
	 * @return string
	 */
	public function hook_newsletter_bottom($params) {
		$this->data['title'] = __('Newsletters in time', WYSIJA);
		return $this->hook_stats($params, true, true, false, false);
	}

	/**
	 * Get stats data
	 * @param array $params Input params
	  [top] => 5
	  [from] =>
	  [to] =>
	  [order_by] =>
	  [order_direction] => 2
	  [email_id] => 3
	  [user_id] => 4

	 * @return array
	  [0] => Array
	  (
	  [time] => 2005,3,1
	  [unsubscribe] => 0
	  [click] => 0
	  [open] => 15
	  )
	  ...
	  [n] => Array
	  (
	  [time] => 2005,4,1
	  [unsubscribe] => 0
	  [click] => 0
	  [open] => 33
	  )
	 */
	protected function get_stats($params, $sent_stat = false, $open_stat = false, $click_stat = false, $unsubscribe_stat = false, $unread_stat = false) {
		if (!empty(self::$stats_data))
			return self::$stats_data;


		$from_date = !empty($params['from']) ? $params['from'] : null;
		$to_date   = !empty($params['to']) ? $params['to'] : null;
		$email_id  = !empty($params['email_id']) ? $params['email_id'] : 0;
		$user_id   = !empty($params['user_id']) ? $params['user_id'] : 0;
		$group_by  = !empty($params['group_by']) ? $params['group_by'] : 0;

		$tmp = array( );

		// if we consider the latest status as email's status, add this additional query, so that we know how many newsletters were sent
		if ($this->count_by_latest_status) {
			// total of really sent newsletters. This is not "sent" status
			$tmp['sent_newsletters_total'] = $this->model_obj->get_sent_newsletters_total($from_date, $to_date, $email_id, $user_id, $group_by);
		}

		if ($sent_stat)
			$tmp['sent']		= $this->model_obj->get_stat_sent($from_date, $to_date, $email_id, $user_id, $group_by);
		if ($open_stat)
			$tmp['open']		= $this->model_obj->get_stat_open($from_date, $to_date, $email_id, $user_id, true, $group_by);
		$unread_stat		= $unread_stat;
//        if ($unread_stat)
//            $tmp['unread'] = $this->model_obj->get_stat_open($from_date, $to_date, $email_id, $user_id, false);
		if ($click_stat)
			$tmp['click']	   = $this->model_obj->get_stat_click($from_date, $to_date, $email_id, $user_id, $group_by);
		if ($unsubscribe_stat)
			$tmp['unsubscribe'] = $this->model_obj->get_stat_unsubscribe($from_date, $to_date, $group_by);

		$_periods = array( );
		foreach ($tmp as $stat_name => $stats) {
			$_periods = array_merge($_periods, array_keys($stats));
		}
		$periods  = array_values(array_unique($_periods)); // remove duplicates AND reset key to 0,1,2,3... after removing duplicates
		// in case we are getting data for a single subscriber, and $from_date is not specified, we will include the date of subscribing as default
		$including_subscribe_date = false; // check if we are including or not this event
		if (!empty($user_id) && empty($from_date)) {
			$subscribe_date		   = $this->model_obj->get_user_subscribe_date($user_id);
			$including_subscribe_date = true;
			if (!in_array($subscribe_date, $periods)) {
				$periods[] = $subscribe_date;
			}
		}

		sort($periods); // sort, in order to make a continous timeline

		self::$stats_data = array( );
		// add some additional fields (more_info, count, etc...)
		foreach ($periods as $index => $period) {
			$data = array( );
			$data['time'] = $period;
			$count		= 0;
			foreach ($tmp as $stat_name => $stats) {
				if (!empty($stats[$period])) {
					$data[$stat_name] = $stats[$period];
				}
				else {
					$data[$stat_name] = array(
						'count'					  => 0
					);
				}
				// prepare flat data for AmChart (balloon - showing list of newsletters or lists of "Wysija lists"
//                $data[$stat_name . '_more_info'] = !empty($data[$stat_name]['lists'])
//                                                    ?
//                                                    implode(' ', $data[$stat_name]['lists']) :
//                                                    // if $including_subscribe_date + no data + first period => This must be subscribed date
//                                                    (($index == 0 && $including_subscribe_date) ? __('Subscribed date',WYSIJA) : __('Data is not available.',WYSIJA))
//                                                    ;
				// prepare flat data for AmChart (balloon - count)
				$data[$stat_name.'_count'] = $data[$stat_name]['count'];
//		if ($stat_name != 'unsubscribe')// @todo: enable this condition, only when we consider "unsubscribe" as number of unsubsribed users. We don't have a record in which we know when a user unsubscribes and from which newsletter it takes effect
				$count += $data[$stat_name]['count'];
				$data['newsletters_count']   = $count;
			}
			self::$stats_data[] = $data;
		}
		$this->calculate_percentage(self::$stats_data);

		return self::$stats_data;
	}

	/**
	 * Prepare "percentage" for chart. If data is invalid (for example: division by zero; or the base data is not available), just return the real count instead of percentage
	 *
	 * @param array $stats_data
	 */
	protected function calculate_percentage(Array &$dataset = array( )) {
		foreach ($dataset as $index => &$data) {
			if (empty($data['newsletters_count']) || $data['newsletters_count'] <= 0) {
				unset($dataset[$index]);
				continue;
			}
			if ($this->count_by_latest_status) {
				// Show xx newsletters sent at balloon
				//if ($data['newsletters_count'] > 1)
				if (empty($data['sent_newsletters_total_count']))
					$text_newsletters_sent = ' <br>0 '.__('newsletter sent');
				elseif (!empty($data['sent_newsletters_total']['lists'])) {
					if (count($data['sent_newsletters_total']['lists']) > 1)
						$text_newsletters_sent = ' <br>'.count($data['sent_newsletters_total']['lists']).' '.__('newsletters sent');
					else
						$text_newsletters_sent = ' <br>'.count($data['sent_newsletters_total']['lists']).' '.__('newsletter sent');
				}


				if (isset($data['open_count']) && $data['newsletters_count']) {
					$data['open_balloon']	= $data['open_count'].' ('.round(($data['open_count'] / $data['newsletters_count']) * 100).'%)'.$text_newsletters_sent;
					$data['open_percentage'] = round(($data['open_count'] / $data['newsletters_count']) * 100);
				}

				if (isset($data['click_count'])) {
					$data['click_balloon']	= $data['click_count'].' ('.round(($data['click_count'] / $data['newsletters_count']) * 100).'%)'.$text_newsletters_sent;
					$data['click_percentage'] = round(($data['click_count'] / $data['newsletters_count']) * 100);
				}

				if (isset($data['unsubscribe_count'])) {
					$data['unsubscribe_balloon']	= $data['unsubscribe_count'].' ('.round(($data['unsubscribe_count'] / $data['newsletters_count']) * 100).'%)';
					$data['unsubscribe_percentage'] = round(($data['unsubscribe_count'] / $data['newsletters_count']) * 100);
				}
			}
			else {
				// Show xx newsletters sent at balloon
				if (!empty($data['sent_count']))
					if ($data['sent_count'] > 1)
						$text_newsletters_sent = ' <br>'.$data['sent_count'].' '.__('newsletters sent');
					else
						$text_newsletters_sent = ' <br>'.$data['sent_count'].' '.__('newsletter sent');
				else
					$text_newsletters_sent = '';

				if (isset($data['open_count']) && !empty($data['sent_count']))
					$data['open_percentage'] = $data['open_count'].' ('.round(($data['open_count'] / $data['sent_count']) * 100).'%)'.$text_newsletters_sent;
				elseif (isset($data['open_count']))
					$data['open_percentage'] = $data['open_count'];

				//            if (!empty($data['unread_count']) && !empty($data['sent_count']))
				//                $data['unread_percentage'] = round(($data['unread_count'] / $data['sent_count']) * 100) . '% (' . $data['unread_count'] . ')';
				//            elseif (!empty($data['unread_count']))
				//                $data['unread_percentage'] = $data['unread_count'];

				if (isset($data['click_count']) && !empty($data['open_count']))
					$data['click_percentage'] = $data['click_count'].' ('.round(($data['click_count'] / $data['open_count']) * 100).'%)'.$text_newsletters_sent;
				elseif (isset($data['click_count']))
					$data['click_percentage'] = $data['click_count'];

				if (isset($data['unsubscribe_count']) && !empty($data['click_count']))
					$data['unsubscribe_percentage'] = $data['unsubscribe_count'].' ('.round(($data['unsubscribe_count'] / $data['click_count']) * 100).'%)';
				elseif (isset($data['unsubscribe_count']))
					$data['unsubscribe_percentage'] = $data['unsubscribe_count'];
			}
		}
	}

}