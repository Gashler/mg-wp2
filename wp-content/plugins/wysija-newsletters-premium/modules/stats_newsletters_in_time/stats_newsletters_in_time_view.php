<?php
defined('WYSIJA') or die('Restricted access');

class WYSIJA_module_view_stats_newsletters_in_time_view extends WYSIJA_view_back {

	public function hook_stats($data) {
		$div_id = 'chartdiv'.rand(1, 1000000); // make sure id is unique, even this hook is called more than once
		?>
		<div class="container-<?php echo $div_id; ?> container" rel="<?php echo $data['module_name']; ?>">
			<h3 class="title"><?php echo $data['title']; ?></h3>
			<?php if (!empty($data['stats_open_rate'])) { ?>
				<div id="<?php echo $div_id; ?>" class="stats-open-rate" style="width: 100%; height: 400px; display:block"></div>
			<?php }
			else { ?>
				<div class="notice-msg updated inline"><ul><li><?php echo $data['messages']['data_not_available']; ?></li></ul></div>
		<?php } ?>
		</div>
		<?php if (empty($data['stats_open_rate'])) return; ?>
		<?php $first_record = $data['stats_open_rate'][0]; ?>
		<script type="text/javascript">
			dateFormat = '<?php echo $data['js_date_format'] ?>';
			chartData = <?php echo json_encode($data['stats_open_rate']); ?>;
			divId = '<?php echo $div_id; ?>';
			chartTranslate = {
				sent: '<?php echo __('Sent', WYSIJA); ?>',
				open: '<?php echo __('Open', WYSIJA); ?>',
				click: '<?php echo __('Click', WYSIJA); ?>',
				unsubscribe: '<?php echo __('Unsubscribe', WYSIJA); ?>'
			};

			jQuery(chartData).each(function(index, element) {
				// format Datetime
				if (typeof jQuery.browser.msie !== 'undefined' && jQuery.browser.msie) {
					_tmp = chartData[index].time.split(',');
					_year = typeof _tmp[0] !== 'undefined' ? _tmp[0] : 1970;
					_month = typeof _tmp[1] !== 'undefined' ? _tmp[1] : 1;
					_day = typeof _tmp[2] !== 'undefined' ? _tmp[1] : 1;
					chartData[index].time = new Date(_year, _month, _day);
				}
				else {
					chartData[index].time = new Date(chartData[index].time);
				}
				chartData[index].balloonText = jQuery.datepicker.formatDate(dateFormat, chartData[index].time);

			});
			// AmCharts.ready(function (){
			// SERIAL CHART
			chart = new AmCharts.AmSerialChart();
			chart.dataProvider = chartData;
			chart.categoryField = "time";
		<?php if (!empty($this->font_family)) { ?>
					chart.fontFamily = '<?php echo implode(',', $this->font_family); ?>';
		<?php } ?>
				chart.fontSize = <?php echo $this->font_size; ?>;

				// Set pointer of balloon
				var balloon = chart.balloon;
				balloon.cornerRadius = 0;
				balloon.pointerWidth = 5;

				// AXES
				// category
				chart.categoryAxis.parseDates = true;
				chart.categoryAxis.minPeriod = 'DD';
				chart.categoryAxis.dateFormats = [
					{period: 'fff', format: 'JJ:NN:SS'},
					{period: 'ss', format: 'JJ:NN:SS'},
					{period: 'mm', format: 'JJ:NN'},
					{period: 'hh', format: 'JJ:NN'},
					{period: 'DD', format: 'YYYY/MM/DD'},
					{period: 'MM', format: 'YYYY/MM'},
					{period: 'YYYY', format: 'YYYY'}
				];

				// Graph: Open rate
		<?php if (isset($first_record['sent'])) { ?>
				graph = new AmCharts.AmGraph();
				graph.type = "smoothedLine"; // this line makes the graph smoothed line.
				// graph.balloonText = "[[sent_more_info]] \n\n [[balloonText]] : [[value]]";
				graph.balloonText = "[[balloonText]] : [[value]]";
				graph.bullet = "round";
				graph.bulletSize = 5;
				graph.lineThickness = 2;
				graph.valueField = "sent_percentage";
				graph.title = chartTranslate.sent;
				// graph.lineColor = "<?php // echo $this->get_random_color();  ?>";
				// chart.addGraph(graph); // we don't show this graph yet.
		<?php } ?>

			// Graph: Click rate
		<?php if (isset($first_record['click'])) { ?>
				graph = new AmCharts.AmGraph();
				graph.type = "smoothedLine"; // this line makes the graph smoothed line.
				// graph.balloonText = "[[click_more_info]] \n\n [[balloonText]] : [[click_balloon]]";
				graph.balloonText = "[[balloonText]] : [[click_balloon]]";
				graph.bullet = "round";
				graph.bulletSize = 5;
				graph.lineThickness = 2;
				graph.valueField = "click_percentage";
				graph.lineColor = "<?php echo $this->get_random_color(); ?>";
				graph.title = chartTranslate.click;
				chart.addGraph(graph);
		<?php } ?>

			// Graph: Open rate
		<?php if (isset($first_record['open'])) { ?>
				graph = new AmCharts.AmGraph();
				graph.type = "smoothedLine"; // this line makes the graph smoothed line.
				//graph.balloonText = "[[open_more_info]] \n\n [[balloonText]] : [[open_balloon]]";
				graph.balloonText = "[[balloonText]] : [[open_balloon]]";
				graph.bullet = "round";
				graph.bulletSize = 5;
				graph.lineThickness = 2;
				graph.valueField = "open_percentage";
				graph.lineColor = "<?php echo $this->get_random_color(); ?>";
				graph.title = chartTranslate.open;


				chart.addGraph(graph);

		<?php } ?>

		<?php
// Ignore the next color. The next color is for "Un-opened"
		$this->get_random_color();
		$this->get_random_color();
		?>
			// Graph: Unsubscribe rate
		<?php if (isset($first_record['unsubscribe'])) { ?>
				graph = new AmCharts.AmGraph();
				graph.type = "smoothedLine"; // this line makes the graph smoothed line.
				// graph.balloonText = "[[unsubscribe_more_info]] \n\n [[balloonText]] : [[unsubscribe_balloon]]";
				graph.balloonText = "[[balloonText]] : [[unsubscribe_balloon]]";
				graph.bullet = "round";
				graph.bulletSize = 5;
				graph.lineThickness = 2;
				graph.valueField = "unsubscribe_percentage";
				graph.title = chartTranslate.unsubscribe;
				graph.lineColor = "<?php echo $this->get_random_color(); ?>";
				chart.addGraph(graph);
		<?php } ?>

			// LEGEND
			var legend = new AmCharts.AmLegend();
			legend.markerType = "circle";
			chart.addLegend(legend);

			// WRITE
			chart.write(divId);

			//});
		</script>
		<?php
	}

}