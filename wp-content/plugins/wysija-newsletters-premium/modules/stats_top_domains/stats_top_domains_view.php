<?php
defined('WYSIJA') or die('Restricted access');

class WYSIJA_module_view_stats_top_domains_view extends WYSIJA_view_back {

	/**
	 *
	 * @param type $data
	  [module_name] => stats_top_domains
	  [top_domains] => Array
	  (
	  [domains] => Array
	  (
	  [0] => Array
	  (
	  [domain] => orange.fr
	  [sent] => 33
	  [opens] => 33
	  [clicks] => 9
	  )

	  [1] => Array
	  (
	  [domain] => gmail.com
	  [sent] => 20
	  [opens] => 19
	  [clicks] => 2
	  )

	  [n] => Array
	  (
	  [domain] => wanadoo.fr
	  [sent] => 17
	  [opens] => 17
	  [clicks] => 10
	  )
	  )

	  [count] => 418
	  )

	  [order_direction] => Array
	  (
	  [sent] =>
	  [opens] =>
	  [clicks] =>
	  )
	 */
	public function hook_stats($data) {
		?>
		<div class="container-top-domains container" rel="<?php echo $data['module_name']; ?>">
			<h3 class="title"><?php echo __('Top domains', WYSIJA); ?></h3>
			<?php if ($data['top_domains']['count'] == 0) { ?>
				<div class="notice-msg updated inline"><ul><li><?php echo $data['messages']['data_not_available']; ?></li></ul></div>
			<?php }
			else { ?>
				<table class="widefat fixed">
					<thead>
					<th class="check-column">&nbsp;</th>
					<th><?php echo __('Domain', WYSIJA); ?></th>
					<th class="sortable sort-filter <?php echo $data['order_direction']['sent']; ?>" rel="sent">
						<a href="javascript:void(0);" class="orderlink">
							<span><?php echo __('Sent'); ?></span>
							<span class="sorting-indicator"></span>
						</a>
					</th>
					<th class="sortable sort-filter <?php echo $data['order_direction']['opens']; ?>" rel="open"><a href="javascript:void(0);" class="orderlink"><span><?php echo __('Opens'); ?></span><span class="sorting-indicator"></span></a></th>
					<th class="sortable sort-filter <?php echo $data['order_direction']['clicks']; ?>" rel="click"><a href="javascript:void(0);" class="orderlink"><span><?php echo __('Clicks'); ?></span><span class="sorting-indicator"></span></a></th>
					<?php if ($data['show_rates']) { ?>
						<th class="rates" rel="rates"><?php echo __('Rates'); ?></th>
			<?php } ?>
					</thead>
					<tbody class="list:user user-list">
						<?php
						$i   = 1;
						$alt = false;
						foreach ($data['top_domains']['domains'] as $domain) {
							?>
							<tr class="<?php echo $alt ? 'alternate' : '';
							$alt = !$alt;
							?>">
								<td><?php echo $i;
							$i++;
							?></td>
								<td><?php echo $domain['domain']; ?></td>
								<td>
								<?php echo $data['show_sent_as_total_of_sent_newsletters'] ? $domain['sent'] + $domain['opens'] + $domain['clicks'] : $domain['sent']; ?>
								</td>
								<td><?php echo $domain['opens']; ?></td>
								<td><?php echo $domain['clicks']; ?></td>
							<?php if ($data['show_rates']) { ?>
									<td><?php $this->render_chart_of_rates($domain); ?></td>
							<?php } ?>
							</tr>
				<?php
			}
			?>
					</tbody>
				</table>
			<?php } ?>
			<?php
			$this->model->countRows = $data['top_domains']['count'];
			if (empty($this->viewObj))
				$this->viewObj = new stdClass();
			$this->viewObj->msgPerPage = __('Show', WYSIJA).':';
			$this->viewObj->title = '';
			$this->limitPerPage();
			?>
			<div class="cl"></div>
		</div>
		<?php
	}

	/**
	 * Render the chart of rates (unopens, opens, clicks)
	 * @param array $record Array( <br/>
	 * 			['sent'] => int
	 * 			['open'] => int,
	 * 			['clicks'] => int,
	 */
	protected function render_chart_of_rates(Array $record) {
		// prepare data of PieChart
		$data = array(
			array(
				'label' => __('Clicked', WYSIJA),
				'stats' => !empty($record['clicks']) ? (int)$record['clicks'] : 0
			),
			array(
				'label' => __('Opened', WYSIJA),
				'stats' => !empty($record['opens']) ? (int)$record['opens'] : 0
			),
			array(
				'label' => __('Unopened', WYSIJA),
				'stats' => !empty($record['sent']) ? (int)$record['sent'] : 0
			)
		);

		$container = 'top-newsletter-'.md5(rand().time()); // the Id of container element
		echo '<div id="'.$container.'" style="min-height:150px; width:100%"></div>';
		?>
		<script type="text/javascript">
			chartData = <?php echo json_encode($data); ?>;
			//AmCharts.ready(function () {
			// PIE CHART
			chart = new AmCharts.AmPieChart();
			chart.dataProvider = chartData;
			chart.titleField = "label";
			chart.valueField = "stats";
			chart.outlineColor = "#FFFFFF";
			chart.outlineAlpha = 0.8;
			chart.outlineThickness = 2;
			chart.labelRadius = 10;
			chart.hideLabelsPercent = 1;
			chart.labelsEnabled = false;
			//	    chart.balloonText = "[[label]][[value]] ([[percents]]%)";
			chart.marginBottom = 0;
			chart.marginLeft = 0;
			chart.marginRight = 0;
			chart.marginTop = 0;
			chart.pieX = '50%';
			chart.pieY = '50%';
		<?php if (!empty($this->font_family)) { ?>
				chart.fontFamily = '<?php echo implode(',', $this->font_family); ?>';
		<?php } ?>
			chart.fontSize = <?php echo $this->font_size; ?>;
			chart.colors = ["<?php echo implode('", "', $this->get_all_colors()); ?>"];

			// Set pointer of balloon
			var balloon = chart.balloon;
			balloon.cornerRadius = 0;
			balloon.pointerWidth = 5;

			// WRITE
			chart.write('<?php echo $container; ?>');
		</script>
		<?php
	}

}